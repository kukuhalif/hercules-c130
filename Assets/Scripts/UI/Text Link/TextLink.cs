﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI
{
    public class TextLink : TEXLink //, IPointerEnterHandler, IPointerExitHandler
    {
        //bool isHoveringOnLink;

        private void Start()
        {
            onClick.AddListener(RunLink);
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyThemeListener);
            Reload();
        }

        private void OnDestroy()
        {
            onClick.RemoveListener(RunLink);
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyThemeListener);
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            TEXDraw td = GetComponent<TEXDraw>();
            td.raycastTarget = true;
        }

        public void SetColor(Color color)
        {
            if (target == null)
                target = GetComponent<TEXDraw>();

            Target.color = color;
        }

        //public void OnPointerEnter(PointerEventData eventData)
        //{
        //    VirtualTrainingInputSystem.OnEndLeftClick += OnClick;
        //}

        //public void OnPointerExit(PointerEventData eventData)
        //{
        //    VirtualTrainingInputSystem.OnEndLeftClick -= OnClick;
        //}

        public List<string> GetLinks()
        {
            if (m_Orchestrator == null)
                return new List<string>();

            List<string> links = new List<string>();
            for (int i = 0; i < m_Orchestrator.rendererState.vertexLinks.Count; i++)
            {
                links.Add(m_Orchestrator.rendererState.vertexLinks[i].key);
            }
            return links;
        }

        public static void ParseLink(string rawLink, ref string code, ref string content)
        {
            // link template
            // \link[pdf_nama pdf file]{link untuk pdf}
            // \link[schematic_schematic id]{link untuk skematik}

            string[] splits = rawLink.Split('_');
            if (splits.Length > 0)
            {
                code = splits[0].ToLower();
                content = rawLink.Replace(code + "_", "");
            }
        }

        private void ApplyThemeListener(ApplyUIThemesEvent e)
        {
            Reload();
        }

        private void Reload()
        {
            UITheme theme = DatabaseManager.GetUiTheme();
            normal = theme.TextLinkNormalColor;
            hover = theme.TextLinkHoverColor;
            pressed = theme.TextLinkPressedColor;
            after = theme.TextLinkAfterColor;
        }

        protected override void DefaultCursor()
        {
            // isHoveringOnLink = false;
            VirtualTrainingCursor.DefaultCursor();
        }

        protected override void LinkCursor()
        {
            // isHoveringOnLink = true;
            VirtualTrainingCursor.Link();
        }

        //private void OnClick(Vector2 pointerPosition, GameObject raycastedUI)
        //{
        //    if (isHoveringOnLink)
        //        return;

        //    Debug.Log("clicked");
        //}

        private void RunLink(string txt)
        {
            string code = "", content = "";
            ParseLink(txt, ref code, ref content);

            switch (code)
            {
                case "pdf":

                    EventManager.TriggerEvent(new ShowPdfLinkEvent(content));

                    break;

                case "schematic":

                    EventManager.TriggerEvent(new ShowSchematicLinkEvent(content));

                    break;
            }
        }
    }
}
