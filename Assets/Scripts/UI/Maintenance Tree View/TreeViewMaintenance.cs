using System;
using System.Collections;
using System.Collections.Generic;
using UIWidgets;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TreeViewMaintenance : TreeView<TreeViewMaintenanceComponent, ITreeViewMaintenanceItem>
    {
        // maintenance data
        List<MaintenanceTreeElement> maintenanceRoot = null;

        public void GenerateTree(List<MaintenanceTreeElement> roots)
        {
            maintenanceRoot = roots;
            Nodes = GetData();
        }

        // initialize filtered tree

        List<MaintenanceTreeNode> allMaintenance = new List<MaintenanceTreeNode>();

        protected override void InitializeTree()
        {
            if (maintenanceRoot == null)
                return;

            Nodes = GetData();
        }

        protected override void ShowFilteredElement()
        {
            // filter by name for folder type
            // else by description

            var rootNodes = new ObservableList<TreeNode<ITreeViewMaintenanceItem>>();
            for (int i = 0; i < allMaintenance.Count; i++)
            {
                if (allMaintenance[i].maintenanceElement.isFolder && allMaintenance[i].maintenanceElement.name.ToLower().Contains(GetSearchString().ToLower()))
                {
                    var child = allMaintenance[i];
                    rootNodes.Add(child);
                }
                else if (!allMaintenance[i].maintenanceElement.isFolder && allMaintenance[i].maintenanceElement.data.description.ToLower().Contains(GetSearchString().ToLower()))
                {
                    var child = allMaintenance[i];
                    rootNodes.Add(child);
                }
            }
            Nodes = rootNodes;
        }

        //---------------------------------------------

        // initialize complete maintenance tree

        private MaintenanceTreeNode CreateNode(MaintenanceTreeElement maintenanceTreeElement, bool isFolder, ITreeViewMaintenanceItem item, ObservableList<TreeNode<ITreeViewMaintenanceItem>> nodes)
        {
            var node = new MaintenanceTreeNode(maintenanceTreeElement, isFolder, item, nodes, false, true);
            allMaintenance.Add(node);
            return node;
        }

        private ObservableList<TreeNode<ITreeViewMaintenanceItem>> GetData()
        {
            allMaintenance.Clear();

            var rootNodes = new ObservableList<TreeNode<ITreeViewMaintenanceItem>>();
            for (int i = 0; i < maintenanceRoot.Count; i++)
            {
                AddChildNode((MaintenanceTreeElement)maintenanceRoot[i], rootNodes, new ObservableList<TreeNode<ITreeViewMaintenanceItem>>());
            }

            return rootNodes;
        }

        private void AddChildNode(MaintenanceTreeElement currentElement, ObservableList<TreeNode<ITreeViewMaintenanceItem>> currentNodes, ObservableList<TreeNode<ITreeViewMaintenanceItem>> childNodes)
        {
            // only set materi tree element data if tree element depth >= 0
            // -1 == root

            bool isFolder = currentElement.depth >= 0 ? false : true;
            if (currentElement.isFolder)
                isFolder = true;

            currentNodes.Add(CreateNode(currentElement, isFolder, new TreeViewMaintenanceItemData(currentElement), childNodes));

            if (currentElement.hasChildren)
            {
                for (int i = 0; i < currentElement.children.Count; i++)
                {
                    AddChildNode((MaintenanceTreeElement)currentElement.children[i], childNodes, new ObservableList<TreeNode<ITreeViewMaintenanceItem>>());
                }
            }
        }

        //---------------------------------------------
    }
}
