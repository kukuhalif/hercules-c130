using System;
using System.ComponentModel;
using UIWidgets;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [Serializable]
    public class TreeViewMaintenanceItemData : ITreeViewMaintenanceItem
    {
        [SerializeField] string name;
        MaintenanceTreeElement maintenanceTreeElement;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewMaintenanceItemData"/> class.
        /// </summary>
        /// <param name="materiName">Name.</param>
        public TreeViewMaintenanceItemData(MaintenanceTreeElement maintenanceTreeElement)
        {
            this.maintenanceTreeElement = maintenanceTreeElement;
        }

        /// <summary>
        /// OnChange event.
        /// </summary>
        [Obsolete("Use PropertyChanged.")]
        public event OnChange OnChange = () => { };

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

        /// <summary>
        /// Notify property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
#pragma warning disable 0618
            OnChange();
#pragma warning restore 0618
        }

        /// <summary>
        /// Display item data using specified component.
        /// </summary>
        /// <param name="component">Component.</param>
        public void Display(TreeViewMaintenanceComponent component)
        {
            if (component.Icon != null)
            {
                component.Icon.sprite = null;
                component.Icon.color = Color.clear;
            }

            if (maintenanceTreeElement.isFolder)
                component.TexDraw.text = maintenanceTreeElement.name;
            else
                component.TexDraw.text = string.IsNullOrEmpty(maintenanceTreeElement.data.description) ? maintenanceTreeElement.data.title : maintenanceTreeElement.data.description;
        }
    }
}
