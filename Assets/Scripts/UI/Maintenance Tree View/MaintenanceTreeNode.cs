using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UIWidgets;

namespace VirtualTraining.UI
{
    public class MaintenanceTreeNode : TreeNode<ITreeViewMaintenanceItem>
    {
        private bool isFolder;
        private MaintenanceTreeElement maintenanceTreeElement;

        public MaintenanceTreeNode(MaintenanceTreeElement maintenanceTreeElement, bool isFolder, ITreeViewMaintenanceItem nodeItem, ObservableList<TreeNode<ITreeViewMaintenanceItem>> nodeNodes = null, bool nodeIsExpanded = false, bool nodeIsVisible = true) : base(nodeItem, nodeNodes, nodeIsExpanded, nodeIsVisible)
        {
            this.maintenanceTreeElement = maintenanceTreeElement;
            this.isFolder = isFolder;
        }

        public MaintenanceTreeElement maintenanceElement { get => maintenanceTreeElement; }
        public bool IsFolder { get => isFolder; }
    }
}
