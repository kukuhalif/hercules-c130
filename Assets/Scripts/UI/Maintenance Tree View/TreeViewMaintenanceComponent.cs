using System;
using UnityEngine;
using UnityEngine.UI;
using System.ComponentModel;
using UIWidgets;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TreeViewMaintenanceComponent : TreeViewComponentBase<ITreeViewMaintenanceItem>
    {
        [SerializeField] public TEXDraw TexDraw;
        [SerializeField] public TextLink TextLink;
        InteractionButton button;

        static ITreeViewMaintenanceItem selected;

        ITreeViewMaintenanceItem data;

        /// <summary>
        /// Item.
        /// </summary>
        public ITreeViewMaintenanceItem item
        {
            get
            {
                return data;
            }

            set
            {
                if (data != null)
                {
                    data.PropertyChanged -= UpdateView;
                }

                data = value;
                if (data != null)
                {
                    data.PropertyChanged += UpdateView;
                }

                UpdateView();
            }
        }

        protected override void ToggleNode()
        {
            base.ToggleNode();

            VirtualTraining.Core.AudioPlayer.PlaySFX(Node.IsExpanded ? SFX.ExpandTreeElement : SFX.CollapseTreeElement);
        }

        /// <summary>
        /// Set data.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="depth">Depth.</param>
        public override void SetData(TreeNode<ITreeViewMaintenanceItem> node, int depth)
        {
            base.SetData(node, depth);

            item = (node == null) ? null : node.Item;
        }

        /// <summary>
        /// Update view.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ev">Event.</param>
        protected virtual void UpdateView(object sender = null, PropertyChangedEventArgs ev = null)
        {
            if (item == null)
            {
                Icon.sprite = null;
                TexDraw.text = string.Empty;
            }
            else
            {
                item.Display(this);
            }

            if (button == null)
                button = GetComponent<InteractionButton>();

            MaintenanceTreeNode maintenanceTreeNode = Node as MaintenanceTreeNode;
            button.SetActive(!maintenanceTreeNode.IsFolder);

            if (selected == null)
            {
                if (!maintenanceTreeNode.IsFolder)
                {
                    if (Node.Clicked)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
            else
            {
                if (!maintenanceTreeNode.IsFolder)
                {
                    if (selected == data)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
        }

        /// <summary>
        /// Remove listeners.
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (data != null)
            {
                data.PropertyChanged -= UpdateView;
            }

            onClickItem.RemoveListener(OnClick);
            ClickedOff();
        }

        protected override void Start()
        {
            base.Start();

            button.DisableSFX();
            onClickItem.AddListener(OnClick);
        }

        private void ClickedOn()
        {
            if (button != null)
                button.ForceOn(true);
            EventManager.AddListener<MaintenancePlayEvent>(PlayMaintenanceListener);
            if (Node != null)
                Node.Clicked = true;

            selected = data;
        }

        private void ClickedOff()
        {
            if (button != null)
                button.ForceOn(false);
            EventManager.RemoveListener<MaintenancePlayEvent>(PlayMaintenanceListener);
            if (Node != null)
                Node.Clicked = false;
        }

        private void OnClick(ListViewItem e)
        {
            ToggleNode();

            if (selected == data)
                return;

            MaintenanceTreeNode maintenanceTreeNode = Node as MaintenanceTreeNode;
            if (!maintenanceTreeNode.IsFolder)
            {
                EventManager.TriggerEvent(new MaintenancePlayEvent(maintenanceTreeNode.maintenanceElement.data));

                ClickedOn();
            }
        }

        private void PlayMaintenanceListener(MaintenancePlayEvent e)
        {
            ClickedOff();
            selected = null;
        }
    }
}
