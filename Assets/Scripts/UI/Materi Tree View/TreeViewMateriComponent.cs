﻿using System;
using UnityEngine;
using System.ComponentModel;
using UIWidgets;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    /// <summary>
    /// TreeViewMateri element component.
    /// </summary>
    public class TreeViewMateriComponent : TreeViewComponentBase<ITreeViewMateriItem>
    {
        [SerializeField] public TextMeshProUGUI TextTMPro;

        static ITreeViewMateriItem selected;

        ITreeViewMateriItem data;
        InteractionButton button;

        /// <summary>
        /// Item.
        /// </summary>
        public ITreeViewMateriItem item
        {
            get
            {
                return data;
            }

            set
            {
                if (data != null)
                {
                    data.PropertyChanged -= UpdateView;
                }

                data = value;
                if (data != null)
                {
                    data.PropertyChanged += UpdateView;
                }

                UpdateView();
            }
        }

        protected override void ToggleNode()
        {
            base.ToggleNode();

            VirtualTraining.Core.AudioPlayer.PlaySFX(Node.IsExpanded ? SFX.ExpandTreeElement : SFX.CollapseTreeElement);
        }

        /// <summary>
        /// Set data.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="depth">Depth.</param>
        public override void SetData(TreeNode<ITreeViewMateriItem> node, int depth)
        {
            base.SetData(node, depth);

            item = (node == null) ? null : node.Item;
        }

        /// <summary>
        /// Update view.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ev">Event.</param>
        protected virtual void UpdateView(object sender = null, PropertyChangedEventArgs ev = null)
        {
            if (item == null)
            {
                Icon.sprite = null;
                if (TextTMPro != null)
                    TextTMPro.text = string.Empty;
            }
            else
            {
                item.Display(this);
            }

            if (button == null)
                button = GetComponent<InteractionButton>();

            MateriTreeNode materiTreeNode = Node as MateriTreeNode;
            button.SetActive(!materiTreeNode.IsFolder);

            if (selected == null)
            {
                if (!materiTreeNode.IsFolder)
                {
                    if (Node.Clicked)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
            else
            {
                if (!materiTreeNode.IsFolder)
                {
                    if (selected == data)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
        }

        /// <summary>
        /// Remove listeners.
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (data != null)
            {
                data.PropertyChanged -= UpdateView;
            }

            onClickItem.RemoveListener(OnClick);
            ClickedOff();

            EventManager.RemoveListener<MateriEvent>(MateriListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
        }

        protected override void Start()
        {
            base.Start();

            button.DisableSFX();
            onClickItem.AddListener(OnClick);

            EventManager.AddListener<MateriEvent>(MateriListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
        }

        public void ClickedOn()
        {
            if (button != null)
                button.ForceOn(true);
            if (Node != null)
                Node.Clicked = true;

            selected = data;
        }

        public void ClickedOff()
        {
            if (button != null)
                button.ForceOn(false);
            if (Node != null)
                Node.Clicked = false;

            selected = null;
        }

        private void OnClick(ListViewItem e)
        {
            ToggleNode();

            if (selected == data)
                return;

            MateriTreeNode materiTreeNode = Node as MateriTreeNode;
            if (!materiTreeNode.IsFolder)
            {
                EventManager.TriggerEvent(new MateriEvent(materiTreeNode.materiElement));

                ClickedOn();
            }
        }

        private void MateriListener(MateriEvent e)
        {
            MateriTreeNode materiTreeNode = Node as MateriTreeNode;
            if (!materiTreeNode.IsFolder && e.materiTreeElement.id != materiTreeNode.materiElement.id)
                ClickedOff();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            MateriTreeNode materiTreeNode = Node as MateriTreeNode;
            if (!materiTreeNode.IsFolder)
                ClickedOff();
        }
    }
}