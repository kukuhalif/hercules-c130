﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UIWidgets;

namespace VirtualTraining.UI
{
    public class MateriTreeNode : TreeNode<ITreeViewMateriItem>
    {
        private bool isFolder;
        private MateriTreeElement materiTreeElement;

        public MateriTreeNode(MateriTreeElement materiTreeElement, bool isFolder, ITreeViewMateriItem nodeItem, ObservableList<TreeNode<ITreeViewMateriItem>> nodeNodes = null, bool nodeIsExpanded = false, bool nodeIsVisible = true) : base(nodeItem, nodeNodes, nodeIsExpanded, nodeIsVisible)
        {
            this.materiTreeElement = materiTreeElement;
            this.isFolder = isFolder;
        }

        public MateriTreeElement materiElement { get => materiTreeElement; }
        public bool IsFolder { get => isFolder; }
    }
}