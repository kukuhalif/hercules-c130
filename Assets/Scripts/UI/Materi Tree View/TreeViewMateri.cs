﻿using System;
using System.Collections;
using System.Collections.Generic;
using UIWidgets;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TreeViewMateri : TreeView<TreeViewMateriComponent, ITreeViewMateriItem>
    {
        // initialize filtered tree

        List<MateriTreeNode> allMateries = new List<MateriTreeNode>();

        protected override void InitializeTree()
        {
            Nodes = GetData();
        }

        protected override void ShowFilteredElement()
        {
            var rootNodes = new ObservableList<TreeNode<ITreeViewMateriItem>>();
            for (int i = 0; i < allMateries.Count; i++)
            {
                if (allMateries[i].materiElement.name.ToLower().Contains(GetSearchString().ToLower()))
                {
                    var child = allMateries[i];
                    rootNodes.Add(child);
                }
            }
            Nodes = rootNodes;
        }

        //---------------------------------------------

        public override void Start()
        {
            base.Start();

            EventManager.AddListener<PlayMateriFromDesciptionPanel>(PlayMateriFromDescriptionPanelListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<PlayMateriFromDesciptionPanel>(PlayMateriFromDescriptionPanelListener);
        }


        private void PlayMateriFromDescriptionPanelListener(PlayMateriFromDesciptionPanel e)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                TreeViewMateriComponent tmc = Items[i] as TreeViewMateriComponent;
                MateriTreeNode mtn = (MateriTreeNode)tmc.Node;

                if (gameObject.activeInHierarchy)
                    mtn.IsExpanded = true;

                if (mtn.materiElement.id == e.materiTreeElement.id)
                {
                    tmc.ClickedOn();
                }
                else if (!mtn.IsFolder)
                {
                    tmc.ClickedOff();
                }
            }

            EventManager.TriggerEvent(new MateriEvent(e.materiTreeElement));
        }

        // initialize complete materi tree

        private MateriTreeNode CreateNode(MateriTreeElement materiTreeElement, bool isFolder, ITreeViewMateriItem item, ObservableList<TreeNode<ITreeViewMateriItem>> nodes)
        {
            var node = new MateriTreeNode(materiTreeElement, isFolder, item, nodes, false, true);
            allMateries.Add(node);
            return node;
        }

        private ObservableList<TreeNode<ITreeViewMateriItem>> GetData()
        {
            allMateries.Clear();

            var rootNodes = new ObservableList<TreeNode<ITreeViewMateriItem>>();
            MateriTreeElement materiRoot = DatabaseManager.GetMateriTree();

            if (materiRoot.hasChildren)
                for (int i = 0; i < materiRoot.children.Count; i++)
                {
                    AddChildNode((MateriTreeElement)materiRoot.children[i], rootNodes, new ObservableList<TreeNode<ITreeViewMateriItem>>());
                }

            return rootNodes;
        }

        private void AddChildNode(MateriTreeElement currentElement, ObservableList<TreeNode<ITreeViewMateriItem>> currentNodes, ObservableList<TreeNode<ITreeViewMateriItem>> childNodes)
        {
            // only set materi tree element data if tree element depth >= 0
            // -1 == root

            bool isFolder = currentElement.depth >= 0 ? false : true;
            if (currentElement.isFolder)
                isFolder = true;

            currentNodes.Add(CreateNode(currentElement, isFolder, new TreeViewMateriItemData(currentElement.name), childNodes));

            if (currentElement.hasChildren)
            {
                for (int i = 0; i < currentElement.children.Count; i++)
                {
                    AddChildNode((MateriTreeElement)currentElement.children[i], childNodes, new ObservableList<TreeNode<ITreeViewMateriItem>>());
                }
            }
        }

        //---------------------------------------------
    }
}