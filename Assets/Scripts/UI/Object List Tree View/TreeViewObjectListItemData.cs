using System;
using System.ComponentModel;
using UIWidgets;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [Serializable]
    public class TreeViewObjectListItemData : ITreeViewObjectListItem
    {
        [SerializeField] string name;
        ObjectListTreeElement objectListTreeElement;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public void ChangeAction(ObjectListAction action)
        {
            objectListTreeElement.action = action;
        }

        public ObjectListAction CurrentAction => objectListTreeElement.action;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewObjectListItemData"/> class.
        /// </summary>
        /// <param name="materiName">Name.</param>
        public TreeViewObjectListItemData(ObjectListTreeElement objectListTreeElement)
        {
            this.objectListTreeElement = objectListTreeElement;
        }

        /// <summary>
        /// OnChange event.
        /// </summary>
        [Obsolete("Use PropertyChanged.")]
        public event OnChange OnChange = () => { };

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

        /// <summary>
        /// Notify property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
#pragma warning disable 0618
            OnChange();
#pragma warning restore 0618
        }

        /// <summary>
        /// Display item data using specified component.
        /// </summary>
        /// <param name="component">Component.</param>
        public void Display(TreeViewObjectListComponent component)
        {
            if (component.Icon != null)
            {
                component.Icon.sprite = null;
                component.Icon.color = Color.clear;
            }

            component.text.text = objectListTreeElement.name;

            if (objectListTreeElement.action == ObjectListAction.Solo)
            {
                component.solo.IsOn = true;
            }
            else if (objectListTreeElement.action == ObjectListAction.Highlight)
            {
                component.highlight.IsOn = true;
            }
            else if (objectListTreeElement.action == ObjectListAction.Hide)
            {
                component.hide.IsOn = true;
            }
            else if (objectListTreeElement.action == ObjectListAction.Xray)
            {
                component.xRay.IsOn = true;
            }
            else
            {
                component.solo.IsOn = false;
                component.highlight.IsOn = false;
                component.hide.IsOn = false;
                component.xRay.IsOn = false;
            }
        }
    }
}
