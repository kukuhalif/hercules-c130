using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UIWidgets;

namespace VirtualTraining.UI
{
    public class ObjectListTreeNode : TreeNode<ITreeViewObjectListItem>
    {
        private bool isFolder;
        private ObjectListTreeElement objectListTreeElement;

        public ObjectListTreeNode(ObjectListTreeElement objectListTreeElement, bool isFolder, ITreeViewObjectListItem nodeItem, ObservableList<TreeNode<ITreeViewObjectListItem>> nodeNodes = null, bool nodeIsExpanded = false, bool nodeIsVisible = true) : base(nodeItem, nodeNodes, nodeIsExpanded, nodeIsVisible)
        {
            this.objectListTreeElement = objectListTreeElement;
            this.isFolder = isFolder;
        }

        public ObjectListTreeElement ObjectListElement { get => objectListTreeElement; }
        public bool IsFolder { get => isFolder; }
    }
}
