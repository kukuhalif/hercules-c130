using System.ComponentModel;

namespace VirtualTraining.UI
{
	/// <summary>
	/// TreeViewMateriItem interface.
	/// </summary>
	public interface ITreeViewObjectListItem : INotifyPropertyChanged
	{
		/// <summary>
		/// Display item data using specified component.
		/// </summary>
		/// <param name="component">Component.</param>
		void Display(TreeViewObjectListComponent component);
	}
}