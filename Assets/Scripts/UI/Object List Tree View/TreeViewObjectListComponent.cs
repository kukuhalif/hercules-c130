using System;
using UnityEngine;
using UnityEngine.UI;
using System.ComponentModel;
using UIWidgets;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TreeViewObjectListComponent : TreeViewComponentBase<ITreeViewObjectListItem>
    {
        [SerializeField] public TextMeshProUGUI text;
        [SerializeField] public InteractionToggle solo, highlight, hide, xRay;

        ITreeViewObjectListItem data;

        /// <summary>
        /// Item.
        /// </summary>
        public ITreeViewObjectListItem item
        {
            get
            {
                return data;
            }

            set
            {
                if (data != null)
                {
                    data.PropertyChanged -= UpdateView;
                }

                data = value;
                if (data != null)
                {
                    data.PropertyChanged += UpdateView;
                }

                UpdateView();
            }
        }

        protected override void ToggleNode()
        {
            base.ToggleNode();

            VirtualTraining.Core.AudioPlayer.PlaySFX(Node.IsExpanded ? SFX.ExpandTreeElement : SFX.CollapseTreeElement);
        }

        /// <summary>
        /// Set data.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="depth">Depth.</param>
        public override void SetData(TreeNode<ITreeViewObjectListItem> node, int depth)
        {
            base.SetData(node, depth);

            item = (node == null) ? null : node.Item;
        }

        /// <summary>
        /// Update view.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ev">Event.</param>
        protected virtual void UpdateView(object sender = null, PropertyChangedEventArgs ev = null)
        {
            if (item == null)
            {
                Icon.sprite = null;
                text.text = string.Empty;
            }
            else
            {
                item.Display(this);
            }
        }

        /// <summary>
        /// Remove listeners.
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (data != null)
            {
                data.PropertyChanged -= UpdateView;
            }

            solo.OnChangeStateEvent -= Solo_OnChangeStateEvent;
            highlight.OnChangeStateEvent -= Highlight_OnChangeStateEvent;
            hide.OnChangeStateEvent -= Hide_OnChangeStateEvent;
            xRay.OnChangeStateEvent -= XRay_OnChangeStateEvent;
        }

        protected override void Start()
        {
            base.Start();

            solo.OnChangeStateEvent += Solo_OnChangeStateEvent;
            highlight.OnChangeStateEvent += Highlight_OnChangeStateEvent;
            hide.OnChangeStateEvent += Hide_OnChangeStateEvent;
            xRay.OnChangeStateEvent += XRay_OnChangeStateEvent;
        }

        private void SwitchOff(ObjectListAction from, ObjectListAction action)
        {
            if (from == action)
            {
                TreeViewObjectListItemData id = data as TreeViewObjectListItemData;
                id.ChangeAction(ObjectListAction.None);
            }
        }

        private void XRay_OnChangeStateEvent(bool on)
        {
            TreeViewObjectListItemData id = data as TreeViewObjectListItemData;
            if (on)
            {
                id.ChangeAction(ObjectListAction.Xray);

                solo.IsOn = false;
                highlight.IsOn = false;
                hide.IsOn = false;
            }
            else
                SwitchOff(ObjectListAction.Xray, id.CurrentAction);

            EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent());
        }

        private void Hide_OnChangeStateEvent(bool on)
        {
            TreeViewObjectListItemData id = data as TreeViewObjectListItemData;
            if (on)
            {
                id.ChangeAction(ObjectListAction.Hide);

                solo.IsOn = false;
                highlight.IsOn = false;
                xRay.IsOn = false;
            }
            else
                SwitchOff(ObjectListAction.Hide, id.CurrentAction);

            EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent());
        }

        private void Highlight_OnChangeStateEvent(bool on)
        {
            TreeViewObjectListItemData id = data as TreeViewObjectListItemData;
            if (on)
            {
                id.ChangeAction(ObjectListAction.Highlight);

                solo.IsOn = false;
                hide.IsOn = false;
                xRay.IsOn = false;
            }
            else
                SwitchOff(ObjectListAction.Highlight, id.CurrentAction);

            EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent());
        }

        private void Solo_OnChangeStateEvent(bool on)
        {
            TreeViewObjectListItemData id = data as TreeViewObjectListItemData;
            if (on)
            {
                id.ChangeAction(ObjectListAction.Solo);

                highlight.IsOn = false;
                hide.IsOn = false;
                xRay.IsOn = false;
            }
            else
                SwitchOff(ObjectListAction.Solo, id.CurrentAction);

            EventManager.TriggerEvent(new ObjectListToggleStateChangedEvent());
        }
    }
}
