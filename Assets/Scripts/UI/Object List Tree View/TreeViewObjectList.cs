using System;
using System.Collections;
using System.Collections.Generic;
using UIWidgets;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TreeViewObjectList : TreeView<TreeViewObjectListComponent, ITreeViewObjectListItem>
    {
        List<ObjectListTreeNode> allObjects = new List<ObjectListTreeNode>();

        protected override void OnDisable()
        {
            base.OnDisable();

            Nodes = new ObservableList<TreeNode<ITreeViewObjectListItem>>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            InitializeTree();
        }

        public List<ObjectListTreeNode> GetNodes()
        {
            return allObjects;
        }

        // initialize filtered tree

        protected override void InitializeTree()
        {
            Nodes = GetData();
        }

        protected override void ShowFilteredElement()
        {
            var rootNodes = new ObservableList<TreeNode<ITreeViewObjectListItem>>();
            for (int i = 0; i < allObjects.Count; i++)
            {
                if (allObjects[i].ObjectListElement.name.ToLower().Contains(GetSearchString().ToLower()))
                {
                    var child = allObjects[i];
                    rootNodes.Add(child);
                }
            }
            Nodes = rootNodes;
        }

        //---------------------------------------------

        // initialize complete ObjectList tree

        private ObjectListTreeNode CreateNode(ObjectListTreeElement ObjectListTreeElement, bool isFolder, ITreeViewObjectListItem item, ObservableList<TreeNode<ITreeViewObjectListItem>> nodes)
        {
            var node = new ObjectListTreeNode(ObjectListTreeElement, isFolder, item, nodes, false, true);
            allObjects.Add(node);
            return node;
        }

        private ObservableList<TreeNode<ITreeViewObjectListItem>> GetData()
        {
            allObjects.Clear();

            var rootNodes = new ObservableList<TreeNode<ITreeViewObjectListItem>>();
            ObjectListTreeElement ObjectListRoot = DatabaseManager.GetObjectListTree();

            if (ObjectListRoot.hasChildren)
                for (int i = 0; i < ObjectListRoot.children.Count; i++)
                {
                    AddChildNode((ObjectListTreeElement)ObjectListRoot.children[i], rootNodes, new ObservableList<TreeNode<ITreeViewObjectListItem>>());
                }

            return rootNodes;
        }

        private void AddChildNode(ObjectListTreeElement currentElement, ObservableList<TreeNode<ITreeViewObjectListItem>> currentNodes, ObservableList<TreeNode<ITreeViewObjectListItem>> childNodes)
        {
            // only set ObjectList tree element data if tree element depth >= 0
            // -1 == root

            currentElement.action = ObjectListAction.None;

            bool isFolder = currentElement.depth >= 0 ? false : true;
            if (currentElement.isFolder)
                isFolder = true;

            currentNodes.Add(CreateNode(currentElement, isFolder, new TreeViewObjectListItemData(currentElement), childNodes));

            if (currentElement.hasChildren)
            {
                for (int i = 0; i < currentElement.children.Count; i++)
                {
                    AddChildNode((ObjectListTreeElement)currentElement.children[i], childNodes, new ObservableList<TreeNode<ITreeViewObjectListItem>>());
                }
            }
        }

        //---------------------------------------------
    }
}