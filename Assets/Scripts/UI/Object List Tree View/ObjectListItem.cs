using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI
{
    public class ObjectListItem : UIElement
    {
        protected override void ApplyTheme()
        {
            Image bg = gameObject.GetComponent<Image>();
            bg.color = theme.buttonBgDefaultColor;

            TextMeshProUGUI text = transform.GetComponentInChildren<TextMeshProUGUI>();
            text.color = theme.buttonIconDefaultColor;
        }
    }
}
