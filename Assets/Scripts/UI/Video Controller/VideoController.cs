using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class VideoController : MonoBehaviour
    {
        [SerializeField] VideoPlayer videoPlayer;
        [SerializeField] InteractionButton playPauseButton;
        [SerializeField] InteractionButton beginButton;
        [SerializeField] InteractionButton lastButton;
        [SerializeField] Slider slider;

        UIInteraction sliderHandleInteraction;
        bool isPlaying, clickDown;

        private void Start()
        {
            playPauseButton.OnClickEvent += PlayPauseListener;
            beginButton.OnClickEvent += Begin;
            lastButton.OnClickEvent += Last;

            sliderHandleInteraction = slider.gameObject.AddComponent<UIInteraction>();
            sliderHandleInteraction.OnClickDown += OnClickDownHandle;
            sliderHandleInteraction.OnClickUp += OnClickUpHandle;

            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void OnDestroy()
        {
            playPauseButton.OnClickEvent -= PlayPauseListener;
            beginButton.OnClickEvent -= Begin;
            lastButton.OnClickEvent -= Last;

            if (sliderHandleInteraction != null)
            {
                sliderHandleInteraction.OnClickDown -= OnClickDownHandle;
                sliderHandleInteraction.OnClickUp -= OnClickUpHandle;
            }

            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            videoPlayer.SetDirectAudioVolume(0, e.data.voiceVolume);
        }

        private void OnClickDownHandle()
        {
            clickDown = true;
            isPlaying = videoPlayer.isPlaying;
            videoPlayer.Pause();
        }

        private void OnClickUpHandle()
        {
            clickDown = false;
            if (isPlaying)
            {
                videoPlayer.Play();
            }
        }

        private void Update()
        {
            if (clickDown)
                videoPlayer.frame = (long)(slider.value * videoPlayer.frameCount);
            else
                slider.value = (float)videoPlayer.frame / (float)videoPlayer.frameCount;
        }

        private void PlayPauseListener()
        {
            if (videoPlayer.isPlaying)
            {
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
                videoPlayer.Pause();
            }
            else
            {
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().pause);
                videoPlayer.Play();
            }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void Play(VideoClip clip, bool looping)
        {
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().pause);
            videoPlayer.clip = clip;
            videoPlayer.isLooping = looping;
            videoPlayer.frame = 0;
            videoPlayer.Prepare();

            StartCoroutine(WaitVideoReady());
        }

        public void ReleaseTexture()
        {
            videoPlayer.targetTexture.Release();
        }

        public void Begin()
        {
            videoPlayer.frame = 0;
            videoPlayer.Pause();
            slider.value = 0;
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
        }

        public void Last()
        {
            videoPlayer.frame = (long)videoPlayer.frameCount;
            videoPlayer.Pause();
            slider.value = 1;
            playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
        }

        private IEnumerator WaitVideoReady()
        {
            while (!videoPlayer.isPrepared)
                yield return null;

            videoPlayer.Play();

        }
    }
}
