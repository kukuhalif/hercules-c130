using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class ObjectListPanel : UIPanel
    {
        [SerializeField] TreeViewObjectList treeViewObjectList;
        [SerializeField] TextMeshProUGUI[] titles;

        List<PartObject> partObjects = new List<PartObject>();

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<ObjectInteractionShowPartObjectListEvent>(ObjectInteractionListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<ObjectInteractionShowPartObjectListEvent>(ObjectInteractionListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isPanelMinimized)
                base.ShowAllPanelListener(e);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            for (int i = 0; i < titles.Length; i++)
            {
                titles[i].color = theme.genericTextColor;
            }
        }

        private void PlayMateriListener(MateriEvent e)
        {
            if (isPanelActive)
                ClosePanel();
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            if (isPanelActive)
                ClosePanel();
        }

        private void ObjectInteractionListener(ObjectInteractionShowPartObjectListEvent e)
        {
            if (e.on)
                ShowPanel();
            else
                ClosePanel();
        }

        protected override void OnEnablePanel()
        {
            base.OnEnablePanel();

            EventManager.TriggerEvent(new ObjectListPanelEvent(true));
            EventManager.TriggerEvent(new FreePlayEvent());
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            EventManager.AddListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            EventManager.TriggerEvent(new ObjectListPanelEvent(false));
            EventManager.TriggerEvent(new PartObjectEvent());
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            EventManager.RemoveListener<ObjectListToggleStateChangedEvent>(ToggleChangedListener);
        }

        private void ToggleChangedListener(ObjectListToggleStateChangedEvent e)
        {
            StartCoroutine(EndLeftClickAction());
        }

        IEnumerator EndLeftClickAction()
        {
            EventManager.TriggerEvent(new PartObjectEvent());
            partObjects.Clear();

            yield return null;

            var nodes = treeViewObjectList.GetNodes();
            for (int i = 0; i < nodes.Count; i++)
            {
                var objs = nodes[i].ObjectListElement.ObjectListData.objects;
                for (int j = 0; j < objs.Count; j++)
                {
                    if (nodes[i].ObjectListElement.action != ObjectListAction.None)
                    {
                        PartObject partObject = new PartObject();
                        partObject.target.Id = objs[j].Id;

                        switch (nodes[i].ObjectListElement.action)
                        {
                            case ObjectListAction.Solo:
                                partObject.action = PartObjectAction.Solo;
                                break;
                            case ObjectListAction.Highlight:
                                partObject.action = PartObjectAction.Highlight;
                                break;
                            case ObjectListAction.Hide:
                                partObject.action = PartObjectAction.Disable;
                                break;
                            case ObjectListAction.Xray:
                                partObject.action = PartObjectAction.Xray;
                                break;
                        }

                        partObjects.Add(partObject);
                    }
                }
            }

            yield return null;

            EventManager.TriggerEvent(new PartObjectEvent(partObjects));
        }
    }
}
