﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    public class MainMenuPanel : UIPanel
    {
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] ManualPanel manualPanel;
        [SerializeField] Taskbar taskbar;
        [SerializeField] Image projectIcon;
        [SerializeField] TextMeshProUGUI projectName;
        [SerializeField] TextMeshProUGUI projectSubName;
        [SerializeField] InteractionButton systemButton;
        [SerializeField] InteractionButton quizButton;
        [SerializeField] InteractionButton manualButton;
        [SerializeField] InteractionButton helpButton;
        [SerializeField] InteractionButton aboutButton;
        [SerializeField] InteractionButton quitButton;

        protected override void Start()
        {
            base.Start();

            projectIcon.sprite = DatabaseManager.GetProjectDetails().icon;
            projectName.text = DatabaseManager.GetProjectDetails().name;
            projectSubName.text = DatabaseManager.GetProjectDetails().subName;

            systemButton.OnClickEvent += ShowSystem;
            quizButton.OnClickEvent += LoadQuiz;
            manualButton.OnClickEvent += ShowManual;
            helpButton.OnClickEvent += ShowHelp;
            aboutButton.OnClickEvent += ShowAbout;
            quitButton.OnClickEvent += Quit;

            LayoutElement[] layouts = transform.GetAllComponentsInChildsExcludeThis<LayoutElement>();
            for (int i = 0; i < layouts.Length; i++)
            {
                layouts[i].CalculateLayoutInputHorizontal();
                layouts[i].CalculateLayoutInputVertical();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            systemButton.OnClickEvent -= ShowSystem;
            quizButton.OnClickEvent -= LoadQuiz;
            manualButton.OnClickEvent -= ShowManual;
            helpButton.OnClickEvent -= ShowHelp;
            aboutButton.OnClickEvent -= ShowAbout;
            quitButton.OnClickEvent -= Quit;
        }

        protected override void OnEnablePanel()
        {
            base.OnEnablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!BodyRect.RectContains(pointerPosition) && !taskbar.RectContain(pointerPosition))
                ClosePanel();
        }

        private void ShowSystem()
        {
            systemPanel.ShowPanel();
            ClosePanel();
        }

        private void LoadQuiz()
        {
            ClosePanel(() =>
            {
                VirtualTrainingSceneManager.OpenQuiz();
            }, true, true, false);
        }

        private void ShowManual()
        {
            manualPanel.ShowPanel();
            ClosePanel();
        }

        private void ShowHelp()
        {
            EventManager.TriggerEvent(new ShowPdfEvent(DatabaseManager.GetProjectDetails().helpPDF));
            ClosePanel();
        }

        private void ShowAbout()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent(DatabaseManager.GetProjectDetails().about, null, null, "Ok"));
            ClosePanel();
        }

        private void Quit()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Are you sure want to exit?", QuitCallback, null));
        }

        private void QuitCallback()
        {
            VirtualTrainingSceneManager.Quit();
        }
    }
}
