﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class Task : UIElement
    {
        [SerializeField] TMP_Text taskName;
        [SerializeField] InteractionButton button;

        Action showPanelCallback;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<ShowUIPanelEvent>(ShowUIPanelListener);
            EventManager.AddListener<CloseUIPanelEvent>(CloseUIPanelListener);
            button.OnClickEvent += ShowPanel;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<ShowUIPanelEvent>(ShowUIPanelListener);
            EventManager.RemoveListener<CloseUIPanelEvent>(CloseUIPanelListener);
            button.OnClickEvent -= ShowPanel;
        }

        private void ShowUIPanelListener(ShowUIPanelEvent e)
        {
            if (taskName.text == e.name)
                // todo : cache / use object pooling
                Destroy(gameObject);
        }

        private void CloseUIPanelListener(CloseUIPanelEvent e)
        {
            if (taskName.text == e.name)
                // todo : cache / use object pooling
                Destroy(gameObject);
        }

        public void Setup(Transform parent, string name, Action showPanelCallback)
        {
            transform.SetParent(parent);
            transform.localScale = Vector3.one;
            this.taskName.text = name;
            gameObject.SetActive(true);
            this.showPanelCallback = showPanelCallback;
        }

        private void ShowPanel()
        {
            showPanelCallback.Invoke();

            // todo : cache / use object pooling
            Destroy(gameObject);
        }
    }
}
