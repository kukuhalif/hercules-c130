using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{

    public class TaskbarExpandPanel : UIPanel
    {
        [SerializeField] Taskbar taskbar;
        [SerializeField] DescriptionPanel descriptionPanel;
        [SerializeField] InteractionButton showAll;
        [SerializeField] InteractionButton showDescription;
        [SerializeField] InteractionButton minimizeAll;
        [SerializeField] InteractionButton closeAll;
        [SerializeField] InteractionButton resetAll;

        protected override void Start()
        {
            base.Start();
            showAll.OnClickEvent += ShowAllListener;
            showDescription.OnClickEvent += ShowDescriptionListener;
            minimizeAll.OnClickEvent += MinimizeAllListener;
            closeAll.OnClickEvent += CloseAllListener;
            resetAll.OnClickEvent += ResetAllListener;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            showAll.OnClickEvent -= ShowAllListener;
            showDescription.OnClickEvent -= ShowDescriptionListener;
            minimizeAll.OnClickEvent -= MinimizeAllListener;
            closeAll.OnClickEvent -= CloseAllListener;
            resetAll.OnClickEvent -= ResetAllListener;
        }

        protected override void OnEnablePanel()
        {
            base.OnEnablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();
            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!BodyRect.RectContains(pointerPosition) && !taskbar.RectContain(pointerPosition))
                ClosePanel();
        }

        private void ShowAllListener()
        {
            EventManager.TriggerEvent(new ShowAllUIPanelEvent());
        }

        private void ShowDescriptionListener()
        {
            if (descriptionPanel.IsMateriDataExist())
                descriptionPanel.ShowPanel();
        }

        private void MinimizeAllListener()
        {
            EventManager.TriggerEvent(new MinimizeAllUIPanelEvent());
        }

        private void CloseAllListener()
        {
            EventManager.TriggerEvent(new CloseAllUIPanelEvent());
        }

        private void ResetAllListener()
        {
            EventManager.TriggerEvent(new ResetAllUIPanelEvent());
        }
    }
}
