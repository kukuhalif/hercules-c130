﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public class Taskbar : UIElement
    {
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] Task taskTemplate;
        [SerializeField] MainMenuPanel mainMenuPanel;
        [SerializeField] SystemPanel systemPanel;
        [SerializeField] ManualPanel manualPanel;
        [SerializeField] SettingPanel settingPanel;
        [SerializeField] InteractionButton mainMenuButton;
        [SerializeField] InteractionButton freePlayButton;
        [SerializeField] InteractionButton settingButton;
        [SerializeField] InteractionButton expandButton;
        [SerializeField] Image background;
        [SerializeField] TaskbarExpandPanel expandPanel;

        [SerializeField] InteractionButton loadingScreenTest;
        [SerializeField] float progress = 0f;
        int labelIdx;
        string[] label = { "klik kanan cuk", "load kedua", "load ketiga", "load keempat", "load kelima", "load keenam", "load ketujuh" };

        RectTransform rectTransform;

        protected override void Start()
        {
            base.Start();
            rectTransform = GetComponent<RectTransform>();
            VirtualTrainingUiUtility.SetTaskbar(rectTransform);
            EventManager.AddListener<MinimizeUIPanelEvent>(MinimizeUIPanelListener);
            EventManager.AddListener<ShowStartingUIPanelEvent>(ShowListener);
            mainMenuButton.OnClickEvent += MainMenuButtonListener;
            freePlayButton.OnClickEvent += FreePlayButtonListener;
            settingButton.OnClickEvent += SettingButtonListener;
            expandButton.OnClickEvent += ExpandTaskbarListener;

            loadingScreenTest.OnClickEvent += LoadingScreenTest_OnClickEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MinimizeUIPanelEvent>(MinimizeUIPanelListener);
            mainMenuButton.OnClickEvent -= MainMenuButtonListener;
            freePlayButton.OnClickEvent -= FreePlayButtonListener;
            settingButton.OnClickEvent -= SettingButtonListener;
            expandButton.OnClickEvent -= ExpandTaskbarListener;

            loadingScreenTest.OnClickEvent -= LoadingScreenTest_OnClickEvent;
        }

        private void LoadingScreenTest_OnClickEvent()
        {
            progress = 0f;
            labelIdx = 0;
            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), false, true, Progress, Label, EndLoading, false));
            VirtualTrainingInputSystem.OnEndRightClick += VirtualTrainingInputSystem_OnEndRightClick;
        }

        private void VirtualTrainingInputSystem_OnEndRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            progress += 0.3f;
            labelIdx++;
        }

        private float Progress()
        {
            return progress;
        }

        private string Label()
        {
            return label[labelIdx];
        }

        private void EndLoading()
        {
            VirtualTrainingInputSystem.OnEndRightClick -= VirtualTrainingInputSystem_OnEndRightClick;
            EventManager.TriggerEvent(new ConfirmationPanelEvent("sudah selesai", null, null));
        }

        private void ShowListener(ShowStartingUIPanelEvent e)
        {
            canvasGroup.DOFade(1, 1);
            EventManager.RemoveListener<ShowStartingUIPanelEvent>(ShowListener);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();
            background.color = theme.taskbarBgColor;
        }

        private void MinimizeUIPanelListener(MinimizeUIPanelEvent e)
        {
            Task newTask = Instantiate(taskTemplate);
            newTask.Setup(taskTemplate.transform.parent, e.panelName, e.maximizeCallback);
        }

        private void MainMenuButtonListener()
        {
            if (!systemPanel.isPanelActive && !manualPanel.isPanelActive)
            {
                if (mainMenuPanel.isPanelActive)
                    mainMenuPanel.ClosePanel();
                else
                    mainMenuPanel.ShowPanel();
            }
        }

        private void ExpandTaskbarListener()
        {
            if (expandPanel.isPanelActive)
                expandPanel.ClosePanel();
            else
                expandPanel.ShowPanel();
        }

        private void FreePlayButtonListener()
        {
            EventManager.TriggerEvent(new FreePlayEvent());
        }

        public void SettingButtonListener()
        {
            if (settingPanel.isPanelActive)
                settingPanel.ClosePanel();
            else
                settingPanel.ShowPanel(null);
        }

        public bool RectContain(Vector2 position)
        {
            return rectTransform.RectContains(position);
        }
    }
}