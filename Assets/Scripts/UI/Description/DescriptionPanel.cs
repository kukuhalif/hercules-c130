﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
namespace VirtualTraining.UI
{
    public class DescriptionPanel : UIPanel
    {
        // ui reference
        [SerializeField] TEXDraw title;
        [SerializeField] TEXDraw description;
        [SerializeField] InteractionButton pdfButton;
        [SerializeField] InteractionButton schematicButton;
        [SerializeField] InteractionButton maintenanceButton;
        [SerializeField] InteractionButton troubleshootButton;
        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionButton previousMateriButton;
        [SerializeField] InteractionButton nextMateriButton;
        [SerializeField] ScrollRect scrollRect;

        // cache
        private MateriTreeElement currentMateriTreeElement;
        private List<MateriTreeElement> materies = new List<MateriTreeElement>();

        protected override void Awake()
        {
            base.Awake();
            EventManager.AddListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);

            var materiTree = DatabaseManager.GetMateriTree();
            TreeElementUtility.TreeToList(materiTree, materies);

            // remove empty materi data
            List<MateriTreeElement> toRemove = new List<MateriTreeElement>();
            for (int i = 0; i < materies.Count; i++)
            {
                if (materies[i].depth < 0)
                    toRemove.Add(materies[i]);
            }
            for (int i = 0; i < toRemove.Count; i++)
            {
                materies.Remove(toRemove[i]);
            }
        }

        protected override void Start()
        {
            base.Start();

            pdfButton.OnClickEvent += OpenPdf;
            schematicButton.OnClickEvent += OpenSchematic;
            maintenanceButton.OnClickEvent += OpenMaintenance;
            troubleshootButton.OnClickEvent += OpenTroubleshoot;
            resetButton.OnClickEvent += ResetMateri;
            previousMateriButton.OnClickEvent += PrevMateri;
            nextMateriButton.OnClickEvent += NextMateri;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<DescriptionPanelEvent>(DescriptionEventListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);

            pdfButton.OnClickEvent -= OpenPdf;
            schematicButton.OnClickEvent -= OpenSchematic;
            maintenanceButton.OnClickEvent -= OpenMaintenance;
            troubleshootButton.OnClickEvent -= OpenTroubleshoot;
            resetButton.OnClickEvent -= ResetMateri;
            previousMateriButton.OnClickEvent -= PrevMateri;
            nextMateriButton.OnClickEvent -= NextMateri;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            title.color = theme.genericTextColor;
            description.color = theme.genericTextColor;
        }

        private void DescriptionEventListener(DescriptionPanelEvent e)
        {
            title.text = e.materiTreeElement.MateriData.name;
            description.text = e.materiTreeElement.MateriData.description;

            currentMateriTreeElement = e.materiTreeElement;

            pdfButton.gameObject.SetActive(currentMateriTreeElement.MateriData.pdfs != null && currentMateriTreeElement.MateriData.pdfs.Count > 0);
            schematicButton.gameObject.SetActive(currentMateriTreeElement.MateriData.schematics != null && currentMateriTreeElement.MateriData.schematics.Count > 0);
            maintenanceButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Maintenance);
            troubleshootButton.gameObject.SetActive(currentMateriTreeElement.MateriData.elementType == MateriElementType.Troubleshoot);

            scrollRect.verticalNormalizedPosition = 0f;

            ShowPanel(ScrollUp);
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            DisableDescriptionPanel();
        }

        private void ScrollUp()
        {
            StartCoroutine(ScrollUpAction());
        }

        private IEnumerator ScrollUpAction()
        {
            while (scrollRect.verticalNormalizedPosition < 1f)
            {
                yield return null;
                scrollRect.verticalNormalizedPosition = 1f;
            }
        }

        private void DisableDescriptionPanel()
        {
            ClosePanel(ClearUI);
        }

        private void ClearUI()
        {
            currentMateriTreeElement = null;
            title.text = "";
            description.text = "";
        }

        private int GetCurrentMateriIndex()
        {
            for (int i = 0; i < materies.Count; i++)
            {
                if (currentMateriTreeElement.id == materies[i].id)
                    return i;
            }

            return -1;
        }

        private void OpenPdf()
        {
            if (currentMateriTreeElement.MateriData.pdfs.Count == 1)
                EventManager.TriggerEvent(new ShowPdfEvent(currentMateriTreeElement.MateriData.pdfs[0]));
            else if (currentMateriTreeElement.MateriData.pdfs.Count > 1)
            {
                ContextMenu.Load();

                for (int i = 0; i < currentMateriTreeElement.MateriData.pdfs.Count; i++)
                {
                    string menuItem = currentMateriTreeElement.MateriData.pdfs[i].pdfAsset.name;
                    PdfData pdfData = currentMateriTreeElement.MateriData.pdfs[i];
                    ContextMenu.AddItem(menuItem, delegate { EventManager.TriggerEvent(new ShowPdfEvent(pdfData)); });
                }

                ContextMenu.Show(Canvas, this);
            }
        }

        private void OpenSchematic()
        {
            List<Schematic> contextMenuSchematic = new List<Schematic>();

            for (int i = 0; i < currentMateriTreeElement.MateriData.schematics.Count; i++)
            {
                if (string.IsNullOrEmpty(currentMateriTreeElement.MateriData.schematics[i].descriptionId))
                {
                    contextMenuSchematic.Add(currentMateriTreeElement.MateriData.schematics[i]);
                }
            }


            if (contextMenuSchematic.Count == 1)
                EventManager.TriggerEvent(new ShowSchematicEvent(contextMenuSchematic[0]));
            else if (contextMenuSchematic.Count > 1)
            {
                ContextMenu.Load();

                for (int i = 0; i < contextMenuSchematic.Count; i++)
                {
                    string menuItem = contextMenuSchematic[i].image == null ? contextMenuSchematic[i].video.name : contextMenuSchematic[i].image.name;
                    Schematic schematic = contextMenuSchematic[i];
                    ContextMenu.AddItem(menuItem, delegate { EventManager.TriggerEvent(new ShowSchematicEvent(schematic)); });
                }

                ContextMenu.Show(Canvas, this);
            }
        }

        private void OpenMaintenance()
        {
            EventManager.TriggerEvent(new MaintenanceUIEvent(currentMateriTreeElement.MateriData));
        }

        private void OpenTroubleshoot()
        {
            EventManager.TriggerEvent(new TroubleshootUIEvent(currentMateriTreeElement.MateriData));
        }

        private void PrevMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index--;

            if (index > -1)
            {
                EventManager.TriggerEvent(new PlayMateriFromDesciptionPanel(materies[index]));
            }
        }

        private void NextMateri()
        {
            int index = GetCurrentMateriIndex();

            if (index == -1)
                return;

            index++;

            if (index < materies.Count)
            {
                EventManager.TriggerEvent(new PlayMateriFromDesciptionPanel(materies[index]));
            }
        }

        private void ResetMateri()
        {
            EventManager.TriggerEvent(new MateriEvent(currentMateriTreeElement));
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (currentMateriTreeElement != null)
                base.ShowAllPanelListener(e);
        }

        public bool IsMateriDataExist()
        {
            return currentMateriTreeElement != null;
        }
    }
}
