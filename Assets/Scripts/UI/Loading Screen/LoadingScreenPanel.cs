using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using VirtualTraining.Core;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.UI
{
    public class LoadingScreenPanel : UIPanel
    {
        [SerializeField] VideoPlayer videoPlayer;
        [SerializeField] Image loadingBar;
        [SerializeField] TextMeshProUGUI label;

        VideoClip clip;
        bool endVideo;
        bool hideUI;

        Func<float> GetProgress;
        Func<string> GetLabel;
        Action endLoadingCallback;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<ShowLoadingScreenEvent>(EventListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<ShowLoadingScreenEvent>(EventListener);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            label.color = theme.buttonIconPressedColor;
            loadingBar.color = theme.buttonIconPressedColor;
        }

        private void Update()
        {
            if (GetLabel != null)
                label.text = GetLabel();

            if (GetProgress != null)
            {
                float progress = GetProgress();
                loadingBar.fillAmount = progress;
                if (progress >= 1f && endVideo)
                {
                    endLoadingCallback.Invoke();
                    videoPlayer.Stop();
                    ClosePanel();
                    GetProgress = null;
                }
            }
        }

        private void EventListener(ShowLoadingScreenEvent e)
        {
            var data = SettingUtility.GetCurrentSetting();
            videoPlayer.SetDirectAudioVolume(0, data.voiceVolume);

            endLoadingCallback = e.endLoadingCallback;

            hideUI = e.hideUI;

            videoPlayer.Stop();
            videoPlayer.targetTexture.Release();

            loadingBar.gameObject.SetActive(false);
            label.gameObject.SetActive(false);

            loadingBar.fillAmount = 0f;
            label.text = "";
            GetProgress = e.progress;
            GetLabel = e.label;

            if (e.waitVideo)
                endVideo = false;
            else
                endVideo = true;

            var videos = e.clips;
            int selected = UnityEngine.Random.Range(0, videos.Count);
            videoPlayer.clip = clip = videos[selected];

            ShowPanel(null, false, false, e.animatePanel);
            StartCoroutine(PlayVideo());
        }

        IEnumerator PlayVideo()
        {
            while (!gameObject.activeInHierarchy)
                yield return null;

            videoPlayer.Play();

            if (!hideUI)
            {
                StartCoroutine(ShowUI());
            }

            yield return new WaitForSeconds((float)clip.length);
            endVideo = true;
        }

        private IEnumerator ShowUI()
        {
            yield return new WaitForSeconds(AnimationDuration);
            loadingBar.gameObject.SetActive(true);
            label.gameObject.SetActive(true);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            videoPlayer.Stop();
            videoPlayer.targetTexture.Release();
        }
    }
}
