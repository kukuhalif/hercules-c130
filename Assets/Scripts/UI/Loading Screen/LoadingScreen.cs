﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] Camera cam;
        [SerializeField] CanvasGroup canvas;

        float loadingBar;
        string progressText;

        float maxLoadSceneProgress = 0.8f;
        List<AsyncOperation> asyncs = new List<AsyncOperation>();
        bool pressed = false;
        VideoPlayer player;
        bool isLoadSceneComplete = false;
        float loadSceneProgress;

        bool ended = false;
        bool lastCount = false;

        bool featureInitDone = false, uiInitDone = false;

        private void Awake()
        {
            player = GetComponent<VideoPlayer>();
            EventManager.AddListener<InializationDoneEvent>(InitializationDoneListener);
            EventManager.AddListener<InitializationUIDoneEvent>(InitializationUIDoneListener);
        }

        private void Start()
        {
            cam.enabled = true;
            canvas.alpha = 1f;

            if (SceneManager.sceneCount == 1)
            {
                var data = SettingUtility.GetCurrentSetting();
                player.SetDirectAudioVolume(0, data.voiceVolume);

                StartCoroutine(StartTask());
            }
            else
            {
                StartCoroutine(UIFound());
            }
        }

        IEnumerator UIFound()
        {
            while (!uiInitDone)
                yield return null;

            EventManager.TriggerEvent(new ShowStartingUIPanelEvent());
            Destroy(gameObject);
        }

        private void InitializationDoneListener(InializationDoneEvent e)
        {
            featureInitDone = true;
        }

        private void InitializationUIDoneListener(InitializationUIDoneEvent e)
        {
            uiInitDone = true;
        }

        private IEnumerator DestroyUI()
        {
            yield return null;
            Destroy(gameObject);

            Debug.Log("loading screen destroyed");
        }

        private float GetProgress()
        {
            return loadingBar;
        }

        private string GetLabel()
        {
            return progressText;
        }

        IEnumerator StartTask()
        {
            yield return null;
            player.clip = DatabaseManager.GetIntroVideo();

            yield return new WaitForSeconds(0.5f);
            if (player.clip != null)
                player.Play();
            var scenes = DatabaseManager.IsVRMode() ? DatabaseManager.GetVRSceneNames() : DatabaseManager.GetPCSceneNames();
            for (int i = 0; i < scenes.Count; i++)
            {
                if (scenes[i] != SceneManager.GetActiveScene().name)
                {
                    AsyncOperation async = SceneManager.LoadSceneAsync(scenes[i], LoadSceneMode.Additive);
                    async.allowSceneActivation = false;
                    asyncs.Add(async);
                    yield return null;
                }
            }
            if (player.clip != null)
                Invoke("ActivateScene", (int)player.clip.length);
            else
                ActivateScene();
        }

        private void OnDestroy()
        {
            CancelInvoke("ActivateScene");
            EventManager.RemoveListener<InializationDoneEvent>(InitializationDoneListener);
            EventManager.RemoveListener<InitializationUIDoneEvent>(InitializationUIDoneListener);
        }

        private void Update()
        {
            if (ended && !lastCount)
            {
                loadingBar += Time.deltaTime * 0.1f;
                if (loadingBar >= 0.95)
                {
                    loadingBar = 0.95f;
                    lastCount = true;
                }
            }

            if (!isLoadSceneComplete)
            {
                if (!pressed && (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Mouse0) || Input.touchCount > 0))
                {
                    pressed = true;
                    if (!ended)
                        StartCoroutine(EndVideo());
                }
                if (!ended)
                    SetLoadingBar();
            }

            if (isLoadSceneComplete && featureInitDone && uiInitDone)
            {
                loadingBar = 1f;
            }
        }

        void ActivateScene()
        {
            if (!ended)
                StartCoroutine(EndVideo());
        }

        void EndLoadingCallback()
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(DatabaseManager.GetModelSceneName()));
            EventManager.TriggerEvent(new ShowStartingUIPanelEvent());
            StartCoroutine(DestroyUI());
        }

        IEnumerator EndVideo()
        {
            ended = true;

            canvas.DOFade(0, 0.5f);

            EventManager.TriggerEvent(new ShowLoadingScreenEvent(DatabaseManager.GetAppsLoadingVideos(), false, false, GetProgress, GetLabel, EndLoadingCallback, true));

            player.Stop();

            progressText = "loading scenes";

            yield return null;

            for (int i = 0; i < asyncs.Count; i++)
            {
                while (asyncs[i].progress < 0.9f)
                {
                    yield return null;
                }
            }

            for (int i = 0; i < asyncs.Count; i++)
            {
                asyncs[i].allowSceneActivation = true;
                yield return null;
            }

            for (int i = 0; i < asyncs.Count; i++)
            {
                while (!asyncs[i].isDone)
                    yield return null;
            }

            if (!lastCount)
                yield return null;

            loadingBar = 1f;
            isLoadSceneComplete = true;
        }

        void SetLoadingBar()
        {
            float maxProgress = 0;

            for (int i = 0; i < asyncs.Count; i++)
            {
                if (asyncs[i].progress > maxProgress)
                    maxProgress = asyncs[i].progress;
            }

            loadSceneProgress = maxProgress;

            loadingBar = Mathf.Lerp(0f, maxLoadSceneProgress, loadSceneProgress);
        }

    }
}
