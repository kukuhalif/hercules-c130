﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VirtualTraining.UI
{
    public static class ContextMenu
    {
        static ContextMenuContainer CURRENT_CONTEXT_MENU;

        public static void Load()
        {
            if (CURRENT_CONTEXT_MENU == null)
            {
                CURRENT_CONTEXT_MENU = LoadFromResources();
            }

            CURRENT_CONTEXT_MENU.Clear();
        }

        public static void AddItem(string name, Action action)
        {
            CURRENT_CONTEXT_MENU.AddMenuItem(name, action);
        }

        public static void Show(Canvas owner, UIPanel panel)
        {
            CURRENT_CONTEXT_MENU.Show(owner, panel);
        }

        private static ContextMenuContainer LoadFromResources()
        {
            return ContextMenuContainer.Instantiate(Resources.Load<ContextMenuContainer>("Context Menu/Context Menu"));
        }
    }
}
