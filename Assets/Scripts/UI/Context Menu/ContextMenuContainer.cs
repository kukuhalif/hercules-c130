using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class ContextMenuContainer : UIElement
    {
        [SerializeField] ContextMenuItem template;

        RectTransform rectTransform;
        List<ContextMenuItem> items = new List<ContextMenuItem>();

        protected override void ApplyTheme()
        {
            Image bg = GetComponent<Image>();
            bg.color = theme.panelContentColor;
        }

        public void AddMenuItem(string name, Action pressedCallback)
        {
            ContextMenuItem newItem = Instantiate(template);

            newItem.gameObject.SetActive(true);

            newItem.Setup(name, () =>
            {
                pressedCallback.Invoke();
                Destroy(gameObject);
            });

            newItem.transform.SetParent(transform);
            newItem.transform.Reset();

            items.Add(newItem);
        }

        public void Show(Canvas owner, UIPanel panel)
        {
            transform.SetParent(owner.transform);
            transform.SetAsLastSibling();
            rectTransform = GetComponent<RectTransform>();
            rectTransform.position = VirtualTrainingInputSystem.PointerPosition;

            VirtualTrainingInputSystem.OnStartLeftClick += StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick += StartClick;
            VirtualTrainingInputSystem.OnStartRightClick += StartClick;

            panel.SetOnTop();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            VirtualTrainingInputSystem.OnStartLeftClick -= StartClick;
            VirtualTrainingInputSystem.OnStartMiddleClick -= StartClick;
            VirtualTrainingInputSystem.OnStartRightClick -= StartClick;
        }

        private void Update()
        {
            if (rectTransform != null)
                rectTransform.ConfinePanel(true);
        }

        private void StartClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (!rectTransform.RectContains(pointerPosition))
                Destroy(gameObject);
        }

        public void Clear()
        {
            for (int i = 0; i < items.Count; i++)
            {
                Destroy(items[i].gameObject);
            }

            items.Clear();
        }
    }
}
