using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UIThemeManager : MonoBehaviour
    {
        string prefKey = "";

        private void Awake()
        {
            prefKey = VirtualTrainingSceneManager.ProjectName + "selected ui theme";
        }

        private void Start()
        {
            if (PlayerPrefs.HasKey(prefKey))
            {
                int themeIndex = PlayerPrefs.GetInt(prefKey);
                ApplyUITheme(themeIndex);
            }

            EventManager.AddListener<SwitchUIThemeEvent>(SwitchThemeListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<SwitchUIThemeEvent>(SwitchThemeListener);
        }

        private void SwitchThemeListener(SwitchUIThemeEvent e)
        {
            PlayerPrefs.SetInt(prefKey, e.index);

            ApplyUITheme(e.index);
        }

        private void ApplyUITheme(int index)
        {
            DatabaseManager.SetUiTheme(index);

            EventManager.TriggerEvent(new ApplyUIThemesEvent());
        }
    }
}
