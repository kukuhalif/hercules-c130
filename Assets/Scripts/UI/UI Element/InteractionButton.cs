using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class InteractionButton : UIElement, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public delegate void OnClick();
        public event OnClick OnClickEvent;

        [SerializeField] bool setColor = true;
        [Tooltip("is active on start")]
        [SerializeField] bool isActive = true;
        [Tooltip("true kalo icon dipake sama controller lain")]
        [SerializeField] bool ignoreIcon = false;

        Image background;
        Image icon;
        TextMeshProUGUI tmpro;
        TEXDraw texDraw;
        TextLink textLink;

        bool disableSFX = false;
        bool forceOn = false;

        public bool ForcedOn => forceOn;

        private void Awake()
        {
            if (setColor)
            {
                background = GetComponent<Image>();
                if (!ignoreIcon)
                    icon = transform.GetComponentInChildsExcludeThis<Image>();
                tmpro = transform.GetComponentInChildsExcludeThis<TextMeshProUGUI>();
                texDraw = transform.GetComponentInChildsExcludeThis<TEXDraw>();
                textLink = transform.GetComponentInChildsExcludeThis<TextLink>();
            }
        }

        protected override void Start()
        {
            base.Start();

            if (setColor)
            {
                if (icon != null)
                    icon.raycastTarget = false;

                if (tmpro != null)
                    tmpro.raycastTarget = false;
            }
        }

        private void OnDisable()
        {
            Highlight(false);
        }

        protected override void ApplyTheme()
        {
            if (isActive)
            {
                SetColor(false);
                Highlight(false);
            }
            else
            {
                if (icon != null)
                    icon.color = theme.buttonIconDisabledColor;

                if (tmpro != null)
                    tmpro.color = theme.buttonIconDisabledColor;

                if (textLink != null)
                    textLink.SetColor(theme.buttonIconDisabledColor);

                if (background != null)
                    background.color = theme.buttonBgDisabledColor;
            }
        }

        private void SetColor(bool down)
        {
            if (!setColor)
                return;

            if (forceOn)
            {
                if (icon != null)
                    icon.color = theme.buttonIconPressedColor;

                if (tmpro != null)
                    tmpro.color = theme.buttonIconPressedColor;

                if (textLink != null)
                    textLink.SetColor(theme.buttonIconPressedColor);

                return;
            }

            if (icon != null)
                icon.color = down ? theme.buttonIconPressedColor : theme.buttonIconDefaultColor;

            if (tmpro != null)
                tmpro.color = down ? theme.buttonIconPressedColor : theme.buttonIconDefaultColor;

            if (textLink != null)
                textLink.SetColor(down ? theme.buttonIconPressedColor : theme.buttonIconDefaultColor);
        }

        private void Highlight(bool on)
        {
            if (!setColor)
                return;

            if (background != null)
                background.color = on ? theme.buttonBgHighlightColor : theme.buttonBgDefaultColor;
        }

        private void Click()
        {
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.ButtonClick);
            OnClickEvent?.Invoke();
        }

        public void DisableSFX()
        {
            disableSFX = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (isActive)
                SetColor(true);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (isActive)
                Highlight(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (isActive)
                Highlight(false);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (isActive)
                SetColor(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (isActive)
                Click();
        }

        public void SetActive(bool active)
        {
            isActive = active;
            if (isActive)
            {
                SetColor(false);
                Highlight(false);
            }
            else
            {
                if (icon != null)
                    icon.color = theme.buttonIconDisabledColor;

                if (tmpro != null)
                    tmpro.color = theme.buttonIconDisabledColor;

                if (textLink != null)
                    textLink.SetColor(theme.buttonIconDisabledColor);

                if (background != null)
                    background.color = theme.buttonBgDisabledColor;
            }
        }

        public void ForceOn(bool on)
        {
            forceOn = on;
            SetColor(on);
        }

        public void SetIcon(Sprite icon)
        {
            if (this.icon == null)
                this.icon = transform.GetComponentInFirstChild<Image>();

            if (this.icon != null)
                this.icon.sprite = icon;
        }

        public void SetText(string text)
        {
            if (tmpro == null)
                tmpro = transform.GetComponentInFirstChild<TextMeshProUGUI>();

            if (tmpro != null)
                tmpro.text = text;

            if (texDraw == null)
                texDraw = transform.GetComponentInFirstChild<TEXDraw>();

            if (texDraw != null)
                texDraw.text = text;
        }
    }
}
