using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class UISpawner : MonoBehaviour
    {
        private void Start()
        {
            GameObject canvasFullscreen = (GameObject)Instantiate(Resources.Load("UI/Canvas Fullscreen"));
            canvasFullscreen.transform.SetParent(transform);

            GameObject confirmationCanvas = (GameObject)Instantiate(Resources.Load("UI/Canvas Confirmation Panel"));
            confirmationCanvas.transform.SetParent(transform);

            GameObject loadingCanvas = (GameObject)Instantiate(Resources.Load("UI/Canvas Loading Video Panel"));
            loadingCanvas.transform.SetParent(transform);
        }
    }
}
