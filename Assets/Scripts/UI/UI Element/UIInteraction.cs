using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI
{
    /*
     * kalau ada component yg numpuk (parent ada ini, child ada ini)
     * maka yang parent gaakan bisa ke trigger eventnya
     * karena child yg diutamakan oleh unity
     * dan karent ini pake raycast
     */


    public class UIInteraction : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public delegate void OnInteract();
        public event OnInteract OnClickDown;
        public event OnInteract OnClickUp;
        public event OnInteract OnCursorEnter;
        public event OnInteract OnCursorExit;

        public void AddImage()
        {
            Image img = gameObject.AddComponent<Image>();
            img.color = Color.clear;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnClickDown?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnClickUp?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnCursorEnter?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnCursorExit?.Invoke();
        }
    }
}
