﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;
using DG.Tweening;

namespace VirtualTraining.UI
{
    public class UIPanel : UIElement
    {
        private static int selectedOrder = int.MinValue;

        private delegate void PanelAction(UIPanel sender, bool theHighest);
        private static event PanelAction PanelSelection;

        private enum ResizeMode
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
            Left,
            Right,
            Top,
            Bottom
        }


        private float animationDuration = 0.3f;
        private float aspectRatio;
        private int resizePadding = 5;
        private bool isDragging;
        private bool isResizing;
        private bool isActive = true;
        private bool isMinimized = false;
        private bool isLocked;
        private bool isMaximized = false;
        private bool isLeftClickDown;
        private bool firstReset;
        private ResizeMode resizeMode;
        private Canvas canvas;
        private CanvasGroup canvasGroup;
        private Vector2 originalPivot;
        private Vector2 firstDragPosition;
        private Vector2 originalDragPosition;
        private Vector2 originalSizeDelta;
        private Vector2 originalLocalPointerPosition;
        private Vector2 minSize;
        private Image titleImage;
        private UIInteraction titleInteraction;
        private UIInteraction bodyInteraction;
        private UIInteraction bottomLeftResizeInteraction;
        private UIInteraction bottomRightResizeInteraction;
        private UIInteraction topRightResizeInteraction;
        private UIInteraction topLeftResizeInteraction;
        private UIInteraction topResizeInteraction;
        private UIInteraction leftResizeInteraction;
        private UIInteraction rightResizeInteraction;
        private UIInteraction bottomResizeInteraction;
        private TextMeshProUGUI titleText;

        [SerializeField] bool alwaysOnTop = false;
        [SerializeField] bool theHighest = false;
        [SerializeField] bool hideAtStart = true;
        [SerializeField] bool setTitleColor = true;
        [SerializeField] bool setBodyColor = true;
        [SerializeField] bool resizable = false;
        [SerializeField] bool keepAspectRatio;
        [SerializeField] bool addUIInteractionComponent = true;
        [SerializeField] bool ignoreShowAllPanelEvent;
        [SerializeField] bool ignoreMinimizeAllPanelEvent;
        [SerializeField] bool ignoreCloseAllPanelEvent;
        [SerializeField] bool disableSFX;
        [SerializeField] InteractionButton minimizeButton;
        [SerializeField] InteractionButton maximizeWindowedButton;
        [SerializeField] InteractionButton closeButton;
        [SerializeField] InteractionToggle lockToggle;
        [SerializeField] RectTransform titlePanel;
        [SerializeField] RectTransform bodyPanel;
        [SerializeField] RectTransform backgroundPanel;

        public Canvas Canvas { get => canvas; }
        public RectTransform BodyRect { get => bodyPanel; }
        public bool isPanelActive { get => isActive; }
        public bool isPanelMinimized { get => isMinimized; }

        protected float AnimationDuration { get => animationDuration; }

        protected virtual void Awake()
        {
            // body rect
            if (bodyPanel == null)
                bodyPanel = gameObject.GetComponent<RectTransform>();

            aspectRatio = bodyPanel.rect.width / bodyPanel.rect.height;
            minSize = bodyPanel.sizeDelta;
            //------------------------------------

            // title text
            if (titlePanel != null)
                titleText = titlePanel.transform.GetComponentInFirstChild<TextMeshProUGUI>();
            //------------------------------------

            // add ui interaction component
            if (addUIInteractionComponent)
            {
                if (titlePanel != null)
                {
                    titleInteraction = titlePanel.gameObject.AddComponent<UIInteraction>();
                    titleInteraction.OnClickDown += OnTitleClickDown;
                }

                if (bodyPanel != null)
                {
                    bodyInteraction = bodyPanel.gameObject.AddComponent<UIInteraction>();
                    bodyInteraction.OnClickDown += OnBodyClickDown;
                }
            }
            //------------------------------------

            // get container canvas
            Canvas canvas = transform.GetComponentsInParentRecursive<Canvas>();
            this.canvas = canvas;
            this.canvasGroup = canvas.GetComponent<CanvasGroup>();
            //------------------------------------

            // set selected order for all panel
            if (selectedOrder <= canvas.sortingOrder)
                selectedOrder = canvas.sortingOrder + 1;
            //------------------------------------

            // add select panel listener for non always on top panel
            PanelSelection += SelectPanelListener;
            //------------------------------------

            // add input listener
            VirtualTrainingInputSystem.OnLeftClickDown += LeftClickDown; // for update
            VirtualTrainingInputSystem.OnEndLeftClick += EndLeftClick;
            //------------------------------------

            // add panel button control listener
            if (minimizeButton != null)
            {
                minimizeButton.OnClickEvent += Minimize;
                minimizeButton.SetIcon(DatabaseManager.GetUITexture().minimizePanel);
                minimizeButton.DisableSFX();
            }
            if (maximizeWindowedButton != null)
            {
                maximizeWindowedButton.OnClickEvent += WindowedOrMaximize;
                maximizeWindowedButton.SetIcon(DatabaseManager.GetUITexture().maximizePanel);
                maximizeWindowedButton.DisableSFX();
            }
            if (closeButton != null)
            {
                closeButton.OnClickEvent += ClosePanel;
                closeButton.SetIcon(DatabaseManager.GetUITexture().closePanel);
                closeButton.DisableSFX();
            }
            if (lockToggle != null)
            {
                lockToggle.OnChangeStateEvent += LockPanelToggleListener;
            }
            //------------------------------------
        }

        protected override void Start()
        {
            base.Start();

            // set original state
            if (bodyPanel != null)
                originalPivot = bodyPanel.pivot;
            if (backgroundPanel != null)
                backgroundPanel.ConfinePanel(false);
            //------------------------------------

            // set sorting order for always on top panel
            if (alwaysOnTop)
                canvas.sortingOrder = selectedOrder + 1;
            //------------------------------------

            // hide at start
            if (hideAtStart)
                ClosePanel(null, true, true, false);
            else
                StartCoroutine(ShowPanelAction(null, true, false));
            //------------------------------------

            // setup resize panel
            if (resizable)
            {
                // right resize panel
                GameObject rightResizeObj = new GameObject("right resize panel", typeof(RectTransform));
                rightResizeObj.transform.SetParent(transform);
                rightResizeObj.transform.Reset();

                RectTransform rightRect = rightResizeObj.GetComponent<RectTransform>();
                rightRect.pivot = new Vector2(1, 0.5f);
                rightRect.anchorMin = new Vector2(1, 0);
                rightRect.anchorMax = new Vector2(1, 1);
                rightRect.anchoredPosition = Vector2.zero;
                rightRect.sizeDelta = new Vector2(resizePadding, -(resizePadding * 2));

                rightResizeInteraction = rightResizeObj.AddComponent<UIInteraction>();
                rightResizeInteraction.AddImage();
                rightResizeInteraction.OnClickDown += OnRightResizeClickDown;
                rightResizeInteraction.OnCursorEnter += OnPointerRightResize;
                rightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // left resize panel
                GameObject leftResizeObj = new GameObject("left resize panel", typeof(RectTransform));
                leftResizeObj.transform.SetParent(transform);
                leftResizeObj.transform.Reset();

                RectTransform leftRect = leftResizeObj.GetComponent<RectTransform>();
                leftRect.pivot = new Vector2(0, 0.5f);
                leftRect.anchorMin = new Vector2(0, 0);
                leftRect.anchorMax = new Vector2(0, 1);
                leftRect.anchoredPosition = Vector2.zero;
                leftRect.sizeDelta = new Vector2(resizePadding, -(resizePadding * 2));

                leftResizeInteraction = leftResizeObj.AddComponent<UIInteraction>();
                leftResizeInteraction.AddImage();
                leftResizeInteraction.OnClickDown += OnLeftResizeClickDown;
                leftResizeInteraction.OnCursorEnter += OnPointerLeftResize;
                leftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // top resize panel
                GameObject topResizeObj = new GameObject("top resize panel", typeof(RectTransform));
                topResizeObj.transform.SetParent(transform);
                topResizeObj.transform.Reset();

                RectTransform topRect = topResizeObj.GetComponent<RectTransform>();
                topRect.pivot = new Vector2(0.5f, 1);
                topRect.anchorMin = new Vector2(0, 1);
                topRect.anchorMax = new Vector2(1, 1);
                topRect.anchoredPosition = Vector2.zero;
                topRect.sizeDelta = new Vector2(-(resizePadding * 2), resizePadding);

                topResizeInteraction = topResizeObj.AddComponent<UIInteraction>();
                topResizeInteraction.AddImage();
                topResizeInteraction.OnClickDown += OnTopResizeClickDown;
                topResizeInteraction.OnCursorEnter += OnPointerTopResize;
                topResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom resize panel
                GameObject bottomResizeObj = new GameObject("bottom resize panel", typeof(RectTransform));
                bottomResizeObj.transform.SetParent(transform);
                bottomResizeObj.transform.Reset();

                RectTransform bottomRect = bottomResizeObj.GetComponent<RectTransform>();
                bottomRect.pivot = new Vector2(0.5f, 0);
                bottomRect.anchorMin = new Vector2(0, 0);
                bottomRect.anchorMax = new Vector2(1, 0);
                bottomRect.anchoredPosition = Vector2.zero;
                bottomRect.sizeDelta = new Vector2(-(resizePadding * 2), resizePadding);

                bottomResizeInteraction = bottomResizeObj.AddComponent<UIInteraction>();
                bottomResizeInteraction.AddImage();
                bottomResizeInteraction.OnClickDown += OnBottomResizeClickDown;
                bottomResizeInteraction.OnCursorEnter += OnPointerBottomResize;
                bottomResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // top right resize panel
                GameObject topRightResizeObj = new GameObject("top right resize panel", typeof(RectTransform));
                topRightResizeObj.transform.SetParent(transform);
                topRightResizeObj.transform.Reset();

                RectTransform topRightRect = topRightResizeObj.GetComponent<RectTransform>();
                topRightRect.pivot = new Vector2(1, 1);
                topRightRect.anchorMin = new Vector2(1, 1);
                topRightRect.anchorMax = new Vector2(1, 1);
                topRightRect.anchoredPosition = Vector2.zero;
                topRightRect.sizeDelta = new Vector2(resizePadding, resizePadding);

                topRightResizeInteraction = topRightResizeObj.AddComponent<UIInteraction>();
                topRightResizeInteraction.AddImage();
                topRightResizeInteraction.OnClickDown += OnTopRightResizeClickDown;
                topRightResizeInteraction.OnCursorEnter += OnPointerTopRightResizeEnter;
                topRightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // top left resize panel
                GameObject topLeftResizeObj = new GameObject("top left resize panel", typeof(RectTransform));
                topLeftResizeObj.transform.SetParent(transform);
                topLeftResizeObj.transform.Reset();

                RectTransform topLeftRect = topLeftResizeObj.GetComponent<RectTransform>();
                topLeftRect.pivot = new Vector2(0, 1);
                topLeftRect.anchorMin = new Vector2(0, 1);
                topLeftRect.anchorMax = new Vector2(0, 1);
                topLeftRect.anchoredPosition = Vector2.zero;
                topLeftRect.sizeDelta = new Vector2(resizePadding, resizePadding);

                topLeftResizeInteraction = topLeftResizeObj.AddComponent<UIInteraction>();
                topLeftResizeInteraction.AddImage();
                topLeftResizeInteraction.OnClickDown += OnTopLeftResizeClickDown;
                topLeftResizeInteraction.OnCursorEnter += OnPointerTopLeftResizeEnter;
                topLeftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom left resize panel
                GameObject bottomLeftResizeObj = new GameObject("bottom left resize panel", typeof(RectTransform));
                bottomLeftResizeObj.transform.SetParent(transform);
                bottomLeftResizeObj.transform.Reset();

                RectTransform bottomLeftRect = bottomLeftResizeObj.GetComponent<RectTransform>();
                bottomLeftRect.pivot = new Vector2(0, 0);
                bottomLeftRect.anchorMin = new Vector2(0, 0);
                bottomLeftRect.anchorMax = new Vector2(0, 0);
                bottomLeftRect.anchoredPosition = Vector2.zero;
                bottomLeftRect.sizeDelta = new Vector2(resizePadding, resizePadding);

                bottomLeftResizeInteraction = bottomLeftResizeObj.AddComponent<UIInteraction>();
                bottomLeftResizeInteraction.AddImage();
                bottomLeftResizeInteraction.OnClickDown += OnBottomLeftResizeClickDown;
                bottomLeftResizeInteraction.OnCursorEnter += OnPointerBottomLeftResizeEnter;
                bottomLeftResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------

                // bottom right resize panel
                GameObject bottomRightResizeObj = new GameObject("bottom right resize panel", typeof(RectTransform));
                bottomRightResizeObj.transform.SetParent(transform);
                bottomRightResizeObj.transform.Reset();

                RectTransform bottomRightRect = bottomRightResizeObj.GetComponent<RectTransform>();
                bottomRightRect.pivot = new Vector2(1, 0);
                bottomRightRect.anchorMin = new Vector2(1, 0);
                bottomRightRect.anchorMax = new Vector2(1, 0);
                bottomRightRect.anchoredPosition = Vector2.zero;
                bottomRightRect.sizeDelta = new Vector2(resizePadding, resizePadding);

                bottomRightResizeInteraction = bottomRightResizeObj.AddComponent<UIInteraction>();
                bottomRightResizeInteraction.AddImage();
                bottomRightResizeInteraction.OnClickDown += OnBottomRightResizeClickDown;
                bottomRightResizeInteraction.OnCursorEnter += OnPointerBottomRightResizeEnter;
                bottomRightResizeInteraction.OnCursorExit += OnPointerResizeExit;
                //------------------------------------
            }
            //------------------------------------

            // event listener
            if (!ignoreShowAllPanelEvent)
                EventManager.AddListener<ShowAllUIPanelEvent>(ShowAllPanelListener);

            if (!ignoreMinimizeAllPanelEvent)
                EventManager.AddListener<MinimizeAllUIPanelEvent>(MinimizeAllPanelListener);

            if (!ignoreCloseAllPanelEvent)
                EventManager.AddListener<CloseAllUIPanelEvent>(CloseAllPanelListener);

            if (titlePanel != null)
                EventManager.AddListener<ResetAllUIPanelEvent>(ResetAllPanelListener);
            //------------------------------------

            // set panel original state
            VirtualTrainingUiUtility.SetOriginalState(bodyPanel);
            //------------------------------------
        }

        protected override void ApplyTheme()
        {
            if (setTitleColor && titlePanel != null)
            {
                if (titleImage == null)
                    titleImage = titlePanel.GetComponent<Image>();

                if (titleImage != null)
                    titleImage.color = theme.panelTitleColor;
            }

            if (setBodyColor && bodyPanel != null)
            {
                Image bodyImage = bodyPanel.GetComponent<Image>();

                if (bodyImage != null)
                    bodyImage.color = theme.panelContentColor;
            }

            if (backgroundPanel != null)
            {
                Image backgroundImage = backgroundPanel.GetComponent<Image>();

                if (backgroundImage != null)
                    backgroundImage.color = theme.backgroundColor;
            }

            if (titleText != null)
                titleText.color = theme.panelTitleTextColor;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            PanelSelection -= SelectPanelListener;

            VirtualTrainingInputSystem.OnLeftClickDown -= LeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= EndLeftClick;

            if (addUIInteractionComponent)
            {
                if (minimizeButton != null)
                    minimizeButton.OnClickEvent -= Minimize;
                if (maximizeWindowedButton != null)
                    maximizeWindowedButton.OnClickEvent -= WindowedOrMaximize;
                if (closeButton != null)
                    closeButton.OnClickEvent -= ClosePanel;
            }

            if (titleInteraction != null)
                titleInteraction.OnClickDown -= OnTitleClickDown;
            if (bodyInteraction != null)
                bodyInteraction.OnClickDown -= OnBodyClickDown;

            if (topResizeInteraction != null)
            {
                topResizeInteraction.OnClickDown -= OnTopResizeClickDown;
                topResizeInteraction.OnCursorEnter -= OnPointerTopResize;
                topResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomResizeInteraction != null)
            {
                bottomResizeInteraction.OnClickDown -= OnBottomResizeClickDown;
                bottomResizeInteraction.OnCursorEnter -= OnPointerBottomResize;
                bottomResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (leftResizeInteraction != null)
            {
                leftResizeInteraction.OnClickDown -= OnLeftResizeClickDown;
                leftResizeInteraction.OnCursorEnter -= OnPointerLeftResize;
                leftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (rightResizeInteraction != null)
            {
                rightResizeInteraction.OnClickDown -= OnRightResizeClickDown;
                rightResizeInteraction.OnCursorEnter -= OnPointerRightResize;
                rightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (topLeftResizeInteraction != null)
            {
                topLeftResizeInteraction.OnClickDown -= OnTopLeftResizeClickDown;
                topLeftResizeInteraction.OnCursorEnter -= OnPointerTopLeftResizeEnter;
                topLeftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (topRightResizeInteraction != null)
            {
                topRightResizeInteraction.OnClickDown -= OnTopRightResizeClickDown;
                topRightResizeInteraction.OnCursorEnter -= OnPointerTopRightResizeEnter;
                topRightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomLeftResizeInteraction != null)
            {
                bottomLeftResizeInteraction.OnClickDown -= OnBottomLeftResizeClickDown;
                bottomLeftResizeInteraction.OnCursorEnter -= OnPointerBottomLeftResizeEnter;
                bottomLeftResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }
            if (bottomRightResizeInteraction != null)
            {
                bottomRightResizeInteraction.OnClickDown -= OnBottomRightResizeClickDown;
                bottomRightResizeInteraction.OnCursorEnter -= OnPointerBottomRightResizeEnter;
                bottomRightResizeInteraction.OnCursorExit -= OnPointerResizeExit;
            }

            if (lockToggle != null)
            {
                lockToggle.OnChangeStateEvent -= LockPanelToggleListener;
            }

            // event listener
            if (!ignoreShowAllPanelEvent)
                EventManager.RemoveListener<ShowAllUIPanelEvent>(ShowAllPanelListener);

            if (!ignoreMinimizeAllPanelEvent)
                EventManager.RemoveListener<MinimizeAllUIPanelEvent>(MinimizeAllPanelListener);

            if (!ignoreCloseAllPanelEvent)
                EventManager.RemoveListener<CloseAllUIPanelEvent>(CloseAllPanelListener);

            if (titlePanel != null)
                EventManager.RemoveListener<ResetAllUIPanelEvent>(ResetAllPanelListener);
            //------------------------------------

            VirtualTrainingCursor.ConfineCursor(false);
            VirtualTrainingCursor.DefaultCursor();
        }

        protected virtual void OnEnablePanel()
        {
            if (bodyPanel != null)
                bodyPanel.RefreshLayout();
            ConfinePanel();
        }

        protected virtual void OnDisablePanel()
        {

        }

        protected void SetTitleText(string text)
        {
            if (titleText != null)
                titleText.text = text;
        }

        private void SelectPanelListener(UIPanel sender, bool theHighest)
        {
            if (alwaysOnTop)
            {
                if (sender == this && theHighest)
                    canvas.sortingOrder = selectedOrder + 3;
                else
                    canvas.sortingOrder = selectedOrder + 2;
            }
            else
            {
                if (sender == this)
                {
                    if (theHighest)
                        canvas.sortingOrder = selectedOrder + 1;
                    else
                        canvas.sortingOrder = selectedOrder;

                }
                else
                {
                    canvas.sortingOrder -= 1;
                }
            }
        }

        private void OnTitleClickDown()
        {
            SetOnTop();

            if (isMaximized)
                return;

            VirtualTrainingCursor.ConfineCursor(true);

            isDragging = true;

            if (bodyPanel != null)
                firstDragPosition = bodyPanel.anchoredPosition;

            if (setTitleColor && titleImage != null)
                titleImage.color = theme.panelTitleDraggedColor;

            if (titleText != null)
                titleText.color = theme.panelTitleTextDraggedColor;
        }

        private void OnBodyClickDown()
        {
            SetOnTop();
        }

        private void OnPointerLeftResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.HorizontalResize();
        }

        private void OnPointerRightResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.HorizontalResize();
        }

        private void OnPointerTopResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.VerticalResize();
        }

        private void OnPointerBottomResize()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.VerticalResize();
        }

        private void OnPointerTopLeftResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.TopLeftResize();
        }

        private void OnPointerTopRightResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.TopRightResize();
        }

        private void OnPointerBottomLeftResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.BottomLeftResize();
        }

        private void OnPointerBottomRightResizeEnter()
        {
            if (!isResizing && !isLeftClickDown && !isMaximized)
                VirtualTrainingCursor.BottomRightResize();
        }

        private void OnPointerResizeExit()
        {
            if (!isResizing && !isLeftClickDown)
                VirtualTrainingCursor.DefaultCursor();
        }

        private void OnRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Right;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(0, 0.5f));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Left;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(1, 0.5f));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Top;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(0.5f, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.Bottom;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(0.5f, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.TopLeft;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(1, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnTopRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.TopRight;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(0, 0));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomLeftResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.BottomLeft;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(1, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void OnBottomRightResizeClickDown()
        {
            if (isMaximized)
                return;

            isResizing = true;

            if (bodyPanel != null)
            {
                VirtualTrainingCursor.ConfineCursor(true);
                resizeMode = ResizeMode.BottomRight;
                originalPivot = bodyPanel.pivot;
                bodyPanel.SetPivot(new Vector2(0, 1));
                originalSizeDelta = bodyPanel.sizeDelta;
                originalDragPosition = transform.localPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out originalLocalPointerPosition);
            }

            SetOnTop();
        }

        private void LeftClickDown(Vector2 axisValue)
        {
            isLeftClickDown = true;

            if (isDragging && titlePanel != null)
            {
                bodyPanel.anchoredPosition = firstDragPosition + axisValue / canvas.scaleFactor;
                ConfinePanel();
            }

            else if (isResizing && bodyPanel != null)
            {
                Vector2 localPointerPosition;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(bodyPanel, VirtualTrainingInputSystem.PointerPosition, null, out localPointerPosition);
                Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;

                Vector2 maxSize = new Vector2(Screen.width, Screen.height);
                Vector2 sizeDelta = Vector2.zero;

                switch (resizeMode)
                {
                    case ResizeMode.TopLeft:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, offsetToOriginal.y);

                        break;
                    case ResizeMode.TopRight:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, offsetToOriginal.y);

                        break;
                    case ResizeMode.BottomLeft:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, -offsetToOriginal.y);

                        break;
                    case ResizeMode.BottomRight:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, -offsetToOriginal.y);

                        break;
                    case ResizeMode.Left:

                        sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, 0);

                        break;
                    case ResizeMode.Right:

                        sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, 0);

                        break;
                    case ResizeMode.Top:

                        sizeDelta = originalSizeDelta + new Vector2(0, offsetToOriginal.y);

                        break;
                    case ResizeMode.Bottom:

                        sizeDelta = originalSizeDelta + new Vector2(0, -offsetToOriginal.y);

                        break;
                }

                sizeDelta = new Vector2(
                    Mathf.Clamp(sizeDelta.x, minSize.x, maxSize.x),
                    Mathf.Clamp(sizeDelta.y, minSize.y, maxSize.y)
                );

                if (keepAspectRatio)
                {
                    sizeDelta.x = sizeDelta.y * aspectRatio;
                }

                bodyPanel.sizeDelta = sizeDelta;

                transform.localPosition = originalDragPosition;
            }
        }

        private void EndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isLeftClickDown = false;

            if (isResizing)
            {
                VirtualTrainingCursor.DefaultCursor();
                if (bodyPanel != null)
                    bodyPanel.SetPivot(originalPivot);

                ConfinePanel();
                VirtualTrainingUiUtility.SavePanelState(bodyPanel, !isMaximized);
            }

            isDragging = false;
            isResizing = false;
            VirtualTrainingCursor.ConfineCursor(false);

            if (setTitleColor && titleImage != null)
                titleImage.color = theme.panelTitleColor;

            if (titleText != null)
                titleText.color = theme.panelTitleTextColor;
        }

        private void ShowPanelCallback()
        {
            ShowPanel();
        }

        private void ConfinePanel()
        {
            // if theHighest is true = confine panel to window size, else confine panel to window size - taskbar

            if (bodyPanel != null)
                bodyPanel.ConfinePanel(!theHighest);

            if (backgroundPanel != null)
                backgroundPanel.ConfinePanel(false);
        }

        private void LockPanelToggleListener(bool on)
        {
            isLocked = on;
        }

        protected virtual void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            ShowPanel();
        }

        protected virtual void MinimizeAllPanelListener(MinimizeAllUIPanelEvent e)
        {
            if (isPanelActive)
                Minimize();
        }

        protected virtual void CloseAllPanelListener(CloseAllUIPanelEvent e)
        {
            if (isPanelActive)
                ClosePanel();
        }

        protected virtual void ResetAllPanelListener(ResetAllUIPanelEvent e)
        {
            if (maximizeWindowedButton != null)
                maximizeWindowedButton.SetIcon(DatabaseManager.GetUITexture().maximizePanel);
            isMaximized = false;
            VirtualTrainingUiUtility.ResetPanel(bodyPanel, animationDuration);
        }

        protected void SetAspectRatio(int width, int height)
        {
            aspectRatio = (float)width / (float)height;
            float lastWidth = bodyPanel.sizeDelta.x;
            float newHeight = lastWidth / aspectRatio;
            bodyPanel.sizeDelta = new Vector2(lastWidth, newHeight);
            ConfinePanel();
        }

        public void SetOnTop()
        {
            PanelSelection.Invoke(this, theHighest);
        }

        public void SetBodyPosition(Vector2 position)
        {
            if (bodyPanel != null)
            {
                bodyPanel.anchoredPosition = position;
            }
        }

        private void ClosePanel()
        {
            ClosePanel(null, false);
        }

        public void ClosePanel(Action onClose = null, bool forceClose = false, bool ignoreAnimation = false, bool playSFX = true)
        {
            VirtualTrainingUiUtility.SavePanelState(bodyPanel, !isMaximized);

            if (forceClose)
            {
                if (isLocked && lockToggle != null)
                    lockToggle.IsOn = false;

                isLocked = false;
            }

            if (!isActive || isLocked)
            {
                onClose?.Invoke();
                return;
            }

            float duration = animationDuration;

            if (ignoreAnimation)
                duration = 0;

            bodyPanel.SetPivot(new Vector2(0.5f, 0.5f));
            canvasGroup.DOFade(0, duration / 2f);
            bodyPanel.DOScaleY(0, duration).OnComplete(() =>
            {
                isActive = false;
                isMinimized = false;
                EventManager.TriggerEvent(new CloseUIPanelEvent(gameObject.name));

                canvas.gameObject.SetActive(false);

                OnDisablePanel();
                onClose?.Invoke();
            });

            if (playSFX && !disableSFX)
                AudioPlayer.PlaySFX(SFX.ClosePanel);
        }

        public void ShowPanel(Action onShow = null, bool restorePosition = true, bool firstReset = true, bool animate = true)
        {
            if (isActive)
            {
                onShow?.Invoke();
                return;
            }

            EventManager.TriggerEvent(new ShowUIPanelEvent(gameObject.name));
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.ShowPanel);

            canvas.gameObject.SetActive(true);

            if (restorePosition)
                VirtualTrainingUiUtility.RestorePanel(bodyPanel, !isMaximized, animationDuration);
            else
                VirtualTrainingUiUtility.SavePanelState(bodyPanel, !isMaximized);

            StartCoroutine(ShowPanelAction(onShow, firstReset, animate));
        }

        IEnumerator ShowPanelAction(Action onShow, bool firstReset, bool animate)
        {
            yield return null;
            isActive = true;
            isMinimized = false;
            onShow?.Invoke();

            if (firstReset && !this.firstReset)
            {
                VirtualTrainingUiUtility.ResetPanel(bodyPanel, animationDuration);
                this.firstReset = true;
            }

            bodyPanel.SetPivot(new Vector2(0.5f, 0.5f));

            if (animate)
            {
                bodyPanel.transform.localScale = new Vector3(1, 0, 1);
                canvasGroup.DOFade(1, animationDuration);
                bodyPanel.DOScaleY(1, animationDuration).OnComplete(() =>
                {
                    OnEnablePanel();
                    SetOnTop();
                });
            }
            else
            {
                canvasGroup.alpha = 1;
                bodyPanel.transform.localScale = Vector3.one;

                OnEnablePanel();
                SetOnTop();
            }
        }

        public void Minimize()
        {
            if (!isActive || isLocked)
                return;

            isActive = false;
            isMinimized = true;
            EventManager.TriggerEvent(new MinimizeUIPanelEvent(gameObject.name, ShowPanelCallback));
            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.MinimizePanel);

            canvasGroup.DOFade(0, animationDuration / 2f);
            VirtualTrainingUiUtility.MinimizePanel(bodyPanel, !isMaximized, animationDuration, () =>
            {
                canvas.gameObject.SetActive(false);

                OnDisablePanel();
            });
        }

        public void WindowedOrMaximize()
        {
            isMinimized = false;
            if (isMaximized) // set to windowed
            {
                maximizeWindowedButton.SetIcon(DatabaseManager.GetUITexture().maximizePanel);

                VirtualTrainingUiUtility.RestorePanel(bodyPanel, isMaximized, animationDuration);
                if (!disableSFX)
                    AudioPlayer.PlaySFX(SFX.WindowedPanel);
            }
            else // set to maximized
            {
                maximizeWindowedButton.SetIcon(DatabaseManager.GetUITexture().windowedPanel);

                VirtualTrainingUiUtility.FullscreenPanel(bodyPanel, true, animationDuration, ConfinePanel);
                if (!disableSFX)
                    AudioPlayer.PlaySFX(SFX.MaximizePanel);
            }

            isMaximized = !isMaximized;

            SetOnTop();
        }
    }
}

