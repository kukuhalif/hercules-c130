using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UIElement : MonoBehaviour
    {
        protected UITheme theme
        {
            get => DatabaseManager.GetUiTheme();
        }

        protected virtual void Start()
        {
            EventManager.AddListener<ApplyUIThemesEvent>(ApplyThemeListener);

            ApplyTheme();
        }

        protected virtual void OnDestroy()
        {
            EventManager.RemoveListener<ApplyUIThemesEvent>(ApplyThemeListener);
        }

        private void ApplyThemeListener(ApplyUIThemesEvent e)
        {
            ApplyTheme();
        }

        protected virtual void ApplyTheme()
        {

        }
    }
}
