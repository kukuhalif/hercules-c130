using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class InteractionToggleGroup : MonoBehaviour
    {
        [SerializeField] bool allowSwitchOff = false;
        [SerializeField] InteractionToggle[] toggles;

        private void Start()
        {
            if (toggles.Length <= 1)
                return;

            AddListener();
            Init();
        }

        private void OnDestroy()
        {
            RemoveListener();
        }

        public void Setup(InteractionToggle[] toggles)
        {
            this.toggles = toggles;

            AddListener();
            Init();
        }

        private void Init()
        {
            if (toggles.Length <= 1)
                return;

            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].AllowSwitchOff = allowSwitchOff;
            }
        }

        private void AddListener()
        {
            if (toggles.Length <= 1)
                return;

            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].OnChangeToggleEvent += OnChangeToggleListener;
            }
        }

        private void RemoveListener()
        {
            if (toggles.Length <= 1)
                return;

            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].OnChangeToggleEvent -= OnChangeToggleListener;
            }
        }

        private void OnChangeToggleListener(InteractionToggle toggle)
        {
            if (toggle.IsOn)
            {
                for (int i = 0; i < toggles.Length; i++)
                {
                    if (toggles[i] != toggle)
                        toggles[i].IsOn = false;
                }
            }
        }
    }
}
