using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Slider))]
    public class InteractionSlider : UIElement
    {
        protected override void Start()
        {
            base.Start();

            Slider slider = GetComponent<Slider>();
            slider.transition = Selectable.Transition.None;
        }

        protected override void ApplyTheme()
        {
            Slider slider = GetComponent<Slider>();

            ColorBlock sliderColor = slider.colors;

            sliderColor.normalColor = theme.sliderHandleDefaultColor;
            sliderColor.highlightedColor = theme.sliderHandleHighlightedColor;
            sliderColor.pressedColor = theme.sliderHandlePressedColor;
            sliderColor.selectedColor = theme.sliderHandleSelectedColor;
            sliderColor.disabledColor = theme.sliderHandleDisabledColor;

            slider.colors = sliderColor;

            Image fill = slider.fillRect.GetComponent<Image>();
            fill.color = theme.sliderFillColor;

            Image bg = slider.transform.GetComponentInFirstChild<Image>();
            bg.color = theme.sliderBgColor;
        }
    }
}

