using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] GameObject[] toEnabledUIs;

        private void Start()
        {
            StartCoroutine(EnableUI());
        }

        private IEnumerator EnableUI()
        {
            for (int i = 0; i < toEnabledUIs.Length; i++)
            {
                yield return null;
                toEnabledUIs[i].SetActive(true);
            }

            yield return null;
            EventManager.TriggerEvent(new InitializationUIDoneEvent());
        }
    }
}
