using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class InteractionToggle : UIElement, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
    {
        [SerializeField] bool defaultState;

        public delegate void OnChangeState(bool on);
        public event OnChangeState OnChangeStateEvent;

        public delegate void OnChangeToggle(InteractionToggle toggle);
        public event OnChangeToggle OnChangeToggleEvent;

        bool initialized;
        bool currentState;
        bool allowSwitchOff = true; // allow switch off by pointer click
        bool disableSFX = false;
        Image background;
        Image icon;
        TextMeshProUGUI text;
        Text defaultText;

        public bool IsOn
        {
            set
            {
                currentState = !value;
                ChangeState();
            }

            get => currentState;
        }

        public bool AllowSwitchOff
        {
            set => allowSwitchOff = value;
        }

        private void Awake()
        {
            background = GetComponent<Image>();
            icon = transform.GetComponentInFirstChild<Image>();
            text = transform.GetComponentInFirstChild<TextMeshProUGUI>();
            defaultText = transform.GetComponentInFirstChild<Text>();
        }

        protected override void Start()
        {
            base.Start();

            if (icon != null)
                icon.raycastTarget = false;

            if (text != null)
                text.raycastTarget = false;

            if (defaultText != null)
                defaultText.raycastTarget = false;

            currentState = defaultState;
        }

        private void OnDisable()
        {
            Highlight(false);
        }

        protected override void ApplyTheme()
        {
            if (initialized)
            {
                SetColor();
                Highlight(false);
            }
            else
            {
                currentState = defaultState;

                SetColor();
                Highlight(false);

                initialized = true;
            }
        }

        private void SetColor()
        {
            if (icon != null)
                icon.color = currentState ? theme.toggleIconSelectedColor : theme.toggleIconDefaultColor;

            if (text != null)
                text.color = currentState ? theme.toggleIconSelectedColor : theme.toggleIconDefaultColor;

            if (defaultText != null)
                defaultText.color = currentState ? theme.toggleIconSelectedColor : theme.toggleIconDefaultColor;

            if (background != null)
                background.color = currentState ? theme.toggleBgSelectedColor : theme.toggleBgDefaultColor;
        }

        private void Highlight(bool on)
        {
            if (background != null)
                background.color = on ? (currentState ? theme.toggleBgHighlightedOnColor : theme.toggleBgHighlightedOffColor) : (currentState ? theme.toggleBgSelectedColor : theme.toggleBgDefaultColor);
        }

        public void DisableSFX()
        {
            disableSFX = true;
        }

        public void ChangeState()
        {
            currentState = !currentState;

            SetColor();

            OnChangeStateEvent?.Invoke(currentState);
            OnChangeToggleEvent?.Invoke(this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            Highlight(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Highlight(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!allowSwitchOff && currentState)
                return;

            if (!disableSFX)
                AudioPlayer.PlaySFX(SFX.ButtonClick);
            ChangeState();
        }

        // entah mengapa ini harus ada biar clicknya jalan (kalo togglenya ada dalam graphic raycaster lain)

        public void OnPointerUp(PointerEventData eventData)
        {

        }

        public void OnPointerDown(PointerEventData eventData)
        {

        }
    }
}

