using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.UI
{
    public class FullscreenPanel : MonoBehaviour
    {
        [SerializeField] RectTransform fullscreenPanel;
        [SerializeField] RectTransform fullscreenPanelCalculateTaskbar;
        [SerializeField] RectTransform minimizeAreaPanel;

        private void Start()
        {
            VirtualTrainingUiUtility.SetupFullscreenPanel(fullscreenPanel, fullscreenPanelCalculateTaskbar, minimizeAreaPanel);
        }
    }
}
