using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public static class VirtualTrainingUiUtility
    {
        private class PanelData
        {
            public Vector2 anchorMin;
            public Vector2 anchorMax;
            public Vector2 pivot;
            public Vector2 anchoredPosition;
            public Vector2 sizeDelta;

            public PanelData(Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector2 anchoredPosition, Vector2 sizeDelta)
            {
                this.anchorMin = anchorMin;
                this.anchorMax = anchorMax;
                this.pivot = pivot;
                this.anchoredPosition = anchoredPosition;
                this.sizeDelta = sizeDelta;
            }

            public PanelData(PanelData data)
            {
                this.anchorMin = data.anchorMin;
                this.anchorMax = data.anchorMax;
                this.pivot = data.pivot;
                this.anchoredPosition = data.anchoredPosition;
                this.sizeDelta = data.sizeDelta;
            }
        }

        private class PanelState
        {
            public PanelData original;
            public PanelData windowed;
            public PanelData maximized;

            public PanelState(PanelData original, PanelData windowed, PanelData maximized)
            {
                if (original != null)
                    this.original = original;
                if (windowed != null)
                    this.windowed = windowed;
                if (maximized != null)
                    this.maximized = maximized;
            }
        }

        private static RectTransform FULLSCREEN_PANEL_IGNORE_TASKBAR;
        private static RectTransform FULLSCREEN_PANEL_CALCULATE_TASKBAR;
        private static RectTransform TASKBAR;
        private static RectTransform MINIMIZE_PANEL_AREA;

        private static Dictionary<RectTransform, PanelState> PANEL_DATAS;

        public static void SetupFullscreenPanel(RectTransform ignoreTaskbar, RectTransform calculateTaskbar, RectTransform minimizePanelArea)
        {
            FULLSCREEN_PANEL_IGNORE_TASKBAR = ignoreTaskbar;
            FULLSCREEN_PANEL_CALCULATE_TASKBAR = calculateTaskbar;
            MINIMIZE_PANEL_AREA = minimizePanelArea;
            PANEL_DATAS = new Dictionary<RectTransform, PanelState>();
        }

        public static void SetTaskbar(RectTransform taskbar)
        {
            TASKBAR = taskbar;
        }

        public static RectTransform GetFullscreenPanel(bool calculateTaskbar)
        {
            return calculateTaskbar ? FULLSCREEN_PANEL_CALCULATE_TASKBAR : FULLSCREEN_PANEL_IGNORE_TASKBAR;
        }

        public static Rect GetScreenBounds(bool calculateTaskbar)
        {
            if (VirtualTrainingCamera.CurrentCamera == null)
                return new Rect();

            if (TASKBAR == null)
                calculateTaskbar = false;

            RectTransform fullPanel = GetFullscreenPanel(calculateTaskbar);
            Rect screen = new Rect(0, calculateTaskbar ? TASKBAR.rect.height / 2f : 0f, fullPanel.rect.width, fullPanel.rect.height);
            return screen;
        }

        public static void SetOriginalState(RectTransform panel)
        {
            PanelData data = new PanelData(panel.anchorMin, panel.anchorMax, panel.pivot, panel.anchoredPosition, panel.sizeDelta);

            if (PANEL_DATAS.ContainsKey(panel))
            {
                PANEL_DATAS[panel].original = data;
            }
            else
            {
                PANEL_DATAS.Add(panel, new PanelState(data, null, null));
            }
        }

        public static void SavePanelState(RectTransform panel, bool windowed)
        {
            if (panel == null)
                return;

            PanelData data = new PanelData(panel.anchorMin, panel.anchorMax, panel.pivot, panel.anchoredPosition, panel.sizeDelta);

            if (PANEL_DATAS.ContainsKey(panel))
            {
                if (windowed)
                    PANEL_DATAS[panel].windowed = data;
                else
                    PANEL_DATAS[panel].maximized = data;
            }
            else
            {
                if (windowed)
                    PANEL_DATAS.Add(panel, new PanelState(null, data, null));
                else
                    PANEL_DATAS.Add(panel, new PanelState(null, null, data));
            }
        }

        public static void FullscreenPanel(RectTransform panel, bool calculateTaskbar, float duration, TweenCallback afterAction)
        {
            SavePanelState(panel, true);

            RectTransform reference = GetFullscreenPanel(calculateTaskbar);
            panel.pivot = reference.pivot;

            panel.DOAnchorMin(reference.anchorMin, duration);
            panel.DOAnchorMax(reference.anchorMax, duration);
            panel.DOAnchorPos(reference.anchoredPosition, duration);
            panel.DOSizeDelta(reference.sizeDelta, duration).OnComplete(afterAction);
        }

        public static void RestorePanel(RectTransform panel, bool fromWindowed, float duration)
        {
            if (!PANEL_DATAS.ContainsKey(panel))
                return;

            PanelData data = null;

            if (fromWindowed)
                data = PANEL_DATAS[panel].windowed;
            else
                data = PANEL_DATAS[panel].maximized;

            if (data == null)
                data = PANEL_DATAS[panel].original;

            panel.SetPivot(data.pivot);

            panel.DOAnchorMin(data.anchorMin, duration);
            panel.DOAnchorMax(data.anchorMax, duration);
            panel.DOAnchorPos(data.anchoredPosition, duration);
            panel.DOSizeDelta(data.sizeDelta, duration);
        }

        public static void MinimizePanel(RectTransform panel, bool windowed, float duration, TweenCallback afterAction)
        {
            SavePanelState(panel, windowed);

            panel.SetPivot(MINIMIZE_PANEL_AREA.pivot);

            panel.DOAnchorMin(MINIMIZE_PANEL_AREA.anchorMin, duration);
            panel.DOAnchorMax(MINIMIZE_PANEL_AREA.anchorMax, duration);
            panel.DOAnchorPos(MINIMIZE_PANEL_AREA.anchoredPosition, duration);
            panel.DOSizeDelta(MINIMIZE_PANEL_AREA.sizeDelta, duration).OnComplete(afterAction);
        }

        public static void ResetPanel(RectTransform panel, float duration)
        {
            if (!PANEL_DATAS.ContainsKey(panel))
                return;

            PanelData data = PANEL_DATAS[panel].original;
            PANEL_DATAS[panel].windowed = new PanelData(PANEL_DATAS[panel].original);

            panel.SetPivot(data.pivot);

            panel.DOAnchorMin(data.anchorMin, duration);
            panel.DOAnchorMax(data.anchorMax, duration);
            panel.DOAnchorPos(data.anchoredPosition, duration);
            panel.DOSizeDelta(data.sizeDelta, duration);
        }
    }
}
