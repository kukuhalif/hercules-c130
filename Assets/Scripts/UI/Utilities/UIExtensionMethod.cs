using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VirtualTraining.UI
{
    public static class UIExtensionMethod
    {
        public static bool RectContains(this RectTransform container, RectTransform item)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(container, item.position);
        }

        public static bool RectContains(this RectTransform container, Vector2 position)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(container, position);
        }

        public static void RefreshLayout(this RectTransform rect)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }

        public static void ConfinePanel(this RectTransform rect, bool calculateTaskbarheight)
        {
            return;

            Vector2 offset = rect.GetGUIElementOffset(calculateTaskbarheight);
            rect.anchoredPosition += offset;
        }

        public static Vector2 GetGUIElementOffset(this RectTransform rect, bool calculateTaskbarheight)
        {
            Rect screenBounds = VirtualTrainingUiUtility.GetScreenBounds(calculateTaskbarheight);

            Vector3[] objectCorners = new Vector3[4];
            rect.GetWorldCorners(objectCorners);

            Vector2 offset = new Vector2(0, 0);

            for (int i = 0; i < objectCorners.Length; i++)
            {
                if (objectCorners[i].x < screenBounds.xMin)
                {
                    offset.x = screenBounds.xMin - objectCorners[i].x;
                }
                if (objectCorners[i].x > screenBounds.xMax)
                {
                    offset.x = screenBounds.xMax - objectCorners[i].x;
                }
                if (objectCorners[i].y < screenBounds.yMin)
                {
                    offset.y = screenBounds.yMin - objectCorners[i].y;
                }
                if (objectCorners[i].y > screenBounds.yMax)
                {
                    offset.y = screenBounds.yMax - objectCorners[i].y;
                }
            }

            return offset;
        }

        public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
        {
            if (rectTransform == null) return;

            Vector2 size = rectTransform.rect.size;
            Vector2 deltaPivot = rectTransform.pivot - pivot;
            Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);
            rectTransform.pivot = pivot;
            rectTransform.localPosition -= deltaPosition;
        }
    }
}
