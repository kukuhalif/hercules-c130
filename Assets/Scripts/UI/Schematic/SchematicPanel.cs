using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class SchematicPanel : UIPanel
    {
        [SerializeField] RawImage ImageTexture;
        [SerializeField] RawImage VideoRawTexture;
        [SerializeField] VideoController videoController;

        List<Schematic> schematics;
        Schematic currentSchematic;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.AddListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.AddListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<SetSchematicDataEvent>(SetSchematicDataListener);
            EventManager.RemoveListener<ShowSchematicEvent>(ShowSchematicListener);
            EventManager.RemoveListener<ShowSchematicLinkEvent>(SchematicLinkListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            videoController.ReleaseTexture();
        }

        private void PlayMateriListener(MateriEvent e)
        {
            ClosePanel();
        }

        private void SetSchematicDataListener(SetSchematicDataEvent e)
        {
            currentSchematic = null;
            schematics = e.schematics;

            if (schematics == null || schematics.Count == 0)
                return;

            for (int i = 0; i < schematics.Count; i++)
            {
                if (schematics[i].showImmediately)
                {
                    ShowPanel(() =>
                    {
                        Show(schematics[i]);
                    });

                    break;
                }
            }
        }

        private void ShowSchematicListener(ShowSchematicEvent e)
        {
            ShowPanel(() =>
            {
                Show(e.schematic);
            });
        }

        private void SchematicLinkListener(ShowSchematicLinkEvent e)
        {
            for (int i = 0; i < schematics.Count; i++)
            {
                if (e.id == schematics[i].descriptionId)
                {
                    ShowPanel(() =>
                    {
                        Show(schematics[i]);
                    });
                    return;
                }
            }

            Debug.LogError("schematic link not found");
        }

        private void SetImageSize(int width, int height)
        {
            SetAspectRatio(width, height);
        }

        private void Show(Schematic schematic)
        {
            if (schematic.image != null)
            {
                SetImageSize(schematic.image.width, schematic.image.height);
                SetTitleText(schematic.image.name);
                videoController.SetActive(false);
                ImageTexture.gameObject.SetActive(true);
                VideoRawTexture.gameObject.SetActive(false);
                ImageTexture.texture = schematic.image;
                currentSchematic = schematic;

            }
            else if (schematic.video != null)
            {
                SetImageSize((int)schematic.video.width, (int)schematic.video.height);
                SetTitleText(schematic.video.name);
                videoController.SetActive(true);
                ImageTexture.gameObject.SetActive(false);
                VideoRawTexture.gameObject.SetActive(true);
                videoController.Play(schematic.video, schematic.isLooping);
                currentSchematic = schematic;
            }
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (currentSchematic != null)
                base.ShowAllPanelListener(e);
        }
    }
}

