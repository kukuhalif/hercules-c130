﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    public class UINodeButton : MonoBehaviour
    {
        public TroubleshootGenerator generateUI;
        public TroubleshootNode troubleshootNode;
        public TextMeshProUGUI textTitle;
        bool isInstantiated;
        InteractionButton button;
        private void Start()
        {
            button = GetComponent<InteractionButton>();
            button.OnClickEvent += ClickButton;
        }

        private void OnDestroy()
        {
            button.OnClickEvent -= ClickButton;
        }

        public void ClickButton()
        {
            if (troubleshootNode != null && isInstantiated == false)
            {
                isInstantiated = true;
                generateUI.GenerateButton(troubleshootNode);
            }
            generateUI.currentButtonActive = this.GetComponent<RectTransform>();

            EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshootNode.content.troubleshootdata));
            for (int i = 0; i < generateUI.uINodeButtons.Count; i++)
            {
                generateUI.uINodeButtons[i].ResetButton();
            }
            button.ForceOn(true);
            generateUI.uINodeButtons.Add(this);
        }

        public void ResetButton()
        {
            button.ForceOn(false);
        }
    }
}
