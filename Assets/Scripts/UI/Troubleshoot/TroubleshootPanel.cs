﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class TroubleshootPanel : UIPanel
    {
        public GameObject prefabCanvastTroubleshoot;
        GameObject currentCanvasT;
        bool isTroubleshootDataExist = false;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.AddListener<MateriEvent>(PlayMateriListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<TroubleshootUIEvent>(TroubleshootListener);
            EventManager.RemoveListener<MateriEvent>(PlayMateriListener);
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isTroubleshootDataExist)
                base.ShowAllPanelListener(e);
        }

        private void PlayMateriListener(MateriEvent e)
        {
            ClosePanel();
        }

        void TroubleshootListener(TroubleshootUIEvent e)
        {
            if (e.materi != null)
            {
                if (e.materi.troubleshootData.troubleshootGraph != null && currentCanvasT == null)
                {
                    currentCanvasT = Instantiate(prefabCanvastTroubleshoot, transform);
                    currentCanvasT.transform.SetSiblingIndex(1);
                    currentCanvasT.GetComponent<TroubleshootGenerator>().troubleshootData = e.materi.troubleshootData;
                    currentCanvasT.SetActive(true);
                    
                    isTroubleshootDataExist = true;
                }

                ShowPanel();
            }
            else
            {
                ResetTroubleshoot();
            }
        }

        public void ResetTroubleshoot()
        {
            if (currentCanvasT != null)
                Destroy(currentCanvasT);

            isTroubleshootDataExist = false;
        }
    }
}
