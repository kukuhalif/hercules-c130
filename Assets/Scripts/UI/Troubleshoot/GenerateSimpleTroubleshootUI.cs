using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;


namespace VirtualTraining.UI
{
    public partial class TroubleshootGenerator : MonoBehaviour
    {
        float rangeSnapping = 100;
        void GenerateSimplifiedButton(TroubleshootNode node)
        {
            for (int i = 0; i < node.GetOutputPort(outputNodeName).ConnectionCount; i++)
            {
                points.Clear();
                GameObject line = Instantiate(uiLineRenderer, canvasParent, false);
                line.transform.position = canvasParent.transform.position;
                line.transform.SetAsFirstSibling();
                UILineRenderer lineRenderer = line.GetComponent<UILineRenderer>();
                points.Add(ReverseYaxis(node.position) + offsideOutputNode);


                var noda = node.GetOutputPort(outputNodeName).GetConnection(i).node;
                TroubleshootNode nodeCurr = noda as TroubleshootNode;

                if (node.GetOutputPort(outputNodeName).GetReroutePoints(i).Count > 0)
                {
                    for (int j = 0; j < node.GetOutputPort(outputNodeName).GetReroutePoints(i).Count; j++)
                    {
                        Vector2 lineVector = ReverseYaxis(node.GetOutputPort(outputNodeName).GetReroutePoints(i)[j]);
                        lineVector = SnappingPoint(lineVector, ReverseYaxis(node.position), ReverseYaxis(node.GetOutputPort(outputNodeName).GetConnection(i).node.position));
                        points.Add(lineVector + offsideInputNode);
                        SizingContent(lineVector + offsideInputNode);
                    }
                }
                else
                    CheckNodePosition(node, nodeCurr);

                Vector2 parentNode = node.GetOutputPort(outputNodeName).GetConnection(i).node.position;
                parentNode = SnappingButton(parentNode,node, node.GetOutputPort(outputNodeName).GetConnection(i).node as TroubleshootNode);
                points.Add(ReverseYaxis(parentNode));

                RefreshLine(lineRenderer);
                InstantiateButton(node,nodeCurr, false);
                InstatiateText(nodeCurr, node);
            }
        }

        void CheckNodePosition(TroubleshootNode inputNode, TroubleshootNode outputNode)
        {
            float distanceX = Mathf.Abs(outputNode.position.x - inputNode.position.x);
            float distanceY = Mathf.Abs(outputNode.position.y - inputNode.position.y);

            if (distanceX > 100 && distanceY > 100)
            {
                if (troubleshootData.startPositionGraph == GraphStartPosition.Up || troubleshootData.startPositionGraph == GraphStartPosition.Down)
                {
                    float centerPositionY = (outputNode.position.y + inputNode.position.y) / 2;
                    Vector2 lineVector = new Vector2(inputNode.position.x, centerPositionY);
                    GeneratePoint(lineVector);

                    Vector2 lineVectorX = new Vector2(outputNode.position.x, centerPositionY);
                    GeneratePoint(lineVectorX);
                }
                else
                {
                    float centerPositionX = (outputNode.position.x + inputNode.position.x) / 2;
                    Vector2 lineVector = new Vector2(centerPositionX, inputNode.position.y);
                    GeneratePoint(lineVector);

                    Vector2 lineVectorX = new Vector2(centerPositionX, outputNode.position.y);
                    GeneratePoint(lineVectorX);
                }
            }
        }

        void GeneratePoint(Vector2 pointPosition)
        {
            pointPosition = ReverseYaxis(pointPosition);
            points.Add(pointPosition + offsideInputNode);
            SizingContent(pointPosition + offsideInputNode);
        }

        public Vector2 SnappingButton(Vector2 pointPosition, TroubleshootNode inputNode, TroubleshootNode outputNode)
        {
            float distanceX = Mathf.Abs(outputNode.position.x - inputNode.position.x);
            float distanceY = Mathf.Abs(outputNode.position.y - inputNode.position.y);
            if (distanceX < 100)
            {
                pointPosition = new Vector2(inputNode.position.x, outputNode.position.y);
                //GeneratePoint(pointPosition);
            }
            else if (distanceY < 100)
            {
                pointPosition = new Vector2(outputNode.position.x, inputNode.position.y);
                //GeneratePoint(pointPosition);
            }

            return pointPosition;
        }

        Vector2 SnappingPoint(Vector2 input, Vector2 inputNode, Vector2 outputNode)
        {
            if (Mathf.Abs(input.x - inputNode.x) < rangeSnapping)
                input.x = inputNode.x;
            if (Mathf.Abs(input.y - inputNode.y) < rangeSnapping)
                input.y = inputNode.y;
            if (Mathf.Abs(input.x - outputNode.x) < rangeSnapping)
                input.x = outputNode.x;
            if (Mathf.Abs(input.y - outputNode.y) < rangeSnapping)
                input.y = outputNode.y;

            return input;
        }
    }
}
