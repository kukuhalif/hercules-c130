﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using VirtualTraining.Core;


namespace VirtualTraining.UI
{
    public partial class TroubleshootGenerator : MonoBehaviour
    {
        public TroubleshootData troubleshootData;
        [SerializeField] Transform canvasParent;
        [SerializeField] Transform contentTransform;
        public float positionStart = 0.8f;
        [SerializeField] InteractionButton _buttonNode;
        TroubleshootNode _nodeRoot;
        [SerializeField] GameObject uiLineRenderer;
        public List<Vector2> points = new List<Vector2>();
        [SerializeField] Vector2 rootPosition;
        [SerializeField] Vector2 textOffset;
        Vector2 diffPosition;
        public string outputNodeName = "outputEmpty";
        public string inputNodeName = "inputEmpty";
        List<Vector2> positionStarts = new List<Vector2>();
        public float offsideX;
        public float offsideY;
        public Text addOnText;
        public Vector2 offsideInputNode;
        public Vector2 offsideOutputNode;
        public List<UINodeButton> uINodeButtons = new List<UINodeButton>();
        public RectTransform currentButtonActive;

        private void Start()
        {
            RectTransform rect = contentTransform.gameObject.GetComponent<RectTransform>();
            Vector2 positionUp = new Vector2(0, (rect.sizeDelta.y / 2) * positionStart); // up
            positionStarts.Add(positionUp);
            Vector2 positionDown = new Vector2(0, (rect.sizeDelta.y / 2) * -positionStart); // down
            positionStarts.Add(positionDown);
            Vector2 positionLeft = new Vector2((rect.sizeDelta.x / 2 * -positionStart), 0); // left
            positionStarts.Add(positionLeft);
            Vector2 positionRight = new Vector2((rect.sizeDelta.x / 2 * positionStart), 0); // right
            positionStarts.Add(positionRight);
            rootPosition = positionStarts[(int)troubleshootData.startPositionGraph];
            
            GenerateRoot();
        }

        public void GenerateRoot()
        {
            if (troubleshootData.troubleshootGraph != null)
            {
                for (int i = 0; i < troubleshootData.troubleshootGraph.nodes.Count; i++)
                {
                    if (troubleshootData.troubleshootGraph.nodes[i].GetInputPort(inputNodeName).ConnectionCount == 0 && troubleshootData.troubleshootGraph.nodes[i].GetOutputPort(outputNodeName).ConnectionCount > 0)
                    {
                        _nodeRoot = troubleshootData.troubleshootGraph.nodes[i] as TroubleshootNode;
                        //diffPosition = canvasParent.transform.TransformPoint(rootPosition) - canvasParent.transform.TransformPoint(ReverseYaxis(_nodeRoot.position));
                        Vector3 newVector = new Vector3(rootPosition.x, rootPosition.y, 0);
                        diffPosition = rootPosition- ReverseYaxis(_nodeRoot.position);
                        Debug.Log(rootPosition);
                        Debug.Log(canvasParent.transform.InverseTransformPoint(rootPosition));
                    }
                }
            }

            if (_nodeRoot != null)
            {
                InstantiateButton(_nodeRoot, _nodeRoot, true);
            }
        }

        public void GenerateButton(TroubleshootNode node)
        {
            if (troubleshootData.isSimplifiedMode)
                GenerateSimplifiedButton(node);
            else
                GenerateButtonBase(node);
        }

        void GenerateButtonBase(TroubleshootNode node)
        {
            for (int i = 0; i < node.GetOutputPort(outputNodeName).ConnectionCount; i++)
            {
                points.Clear();
                GameObject line = Instantiate(uiLineRenderer, canvasParent, false);
                line.transform.position = canvasParent.transform.position;
                line.transform.SetAsFirstSibling();
                UILineRenderer lineRenderer = line.GetComponent<UILineRenderer>();
                points.Add(ReverseYaxis(node.position) + offsideOutputNode);

                if (node.GetOutputPort(outputNodeName).GetReroutePoints(i).Count > 0)
                {
                    for (int j = 0; j < node.GetOutputPort(outputNodeName).GetReroutePoints(i).Count; j++)
                    {
                        Vector2 lineVector = ReverseYaxis(node.GetOutputPort(outputNodeName).GetReroutePoints(i)[j]);
                        //lineVector = new Vector2(lineVector.x + -8, lineVector.y);
                        lineVector = SnappingPoint(lineVector, ReverseYaxis(node.position), ReverseYaxis(node.GetOutputPort(outputNodeName).GetConnection(i).node.position));
                        points.Add(lineVector + offsideInputNode);
                        SizingContent(lineVector + offsideInputNode);
                    }
                }

                Vector2 parentNode = node.GetOutputPort(outputNodeName).GetConnection(i).node.position;
                parentNode = SnappingButton(parentNode, node, node.GetOutputPort(outputNodeName).GetConnection(i).node as TroubleshootNode);
                points.Add(ReverseYaxis(parentNode));

                RefreshLine(lineRenderer);
                var noda = node.GetOutputPort(outputNodeName).GetConnection(i).node;
                TroubleshootNode nodeCurr = noda as TroubleshootNode;
                InstantiateButton(node, nodeCurr, false);
                InstatiateText(nodeCurr, node);
            }
        }

        public void InstantiateButton(TroubleshootNode inputNode, TroubleshootNode node, bool isRoot)
        {
            InteractionButton buttonTemp;
            if (node.content.troubleshootdata.prefabCustomButton != null)
            {
                buttonTemp = Instantiate(node.content.troubleshootdata.prefabCustomButton.GetComponent<InteractionButton>(), canvasParent, false);
            }
            else
                buttonTemp = Instantiate(_buttonNode, canvasParent, false);

            buttonTemp.gameObject.SetActive(true);

            if (isRoot)
            {
                //buttonTemp.transform.position = canvasParent.transform.TransformPoint(rootPosition);
                buttonTemp.transform.localPosition = rootPosition;

                //RectTransform rect = buttonTemp.GetComponent<RectTransform>();
                //rect.anchoredPosition = rootPosition;
                Debug.Log(buttonTemp.transform.position);
            }
            else
            {
                Vector2 positionTemp = SnappingButton(node.position, inputNode, node);
                node.position = positionTemp;
                buttonTemp.transform.position = canvasParent.transform.TransformPoint(ReverseYaxis(positionTemp));
                //RectTransform rect = buttonTemp.GetComponent<RectTransform>();
                //rect.anchoredPosition = positionTemp;
            }

            buttonTemp.transform.SetAsLastSibling();
            UINodeButton uiNode = buttonTemp.GetComponent<UINodeButton>();

            if (!string.IsNullOrEmpty(node.content.troubleshootdata.description))
                uiNode.textTitle.text = node.content.troubleshootdata.description;
            else
                uiNode.textTitle.text = node.content.troubleshootdata.title;
            uiNode.troubleshootNode = node;
            uiNode.generateUI = this;
            SizingContent(buttonTemp.transform.localPosition);
        }

        private void RefreshLine(UILineRenderer lineRenderer)
        {
            lineRenderer.Points = points.ToArray();
            lineRenderer.SetAllDirty();
        }

        public Vector2 ReverseYaxis(Vector2 vector)
        {
            vector = new Vector2(vector.x * troubleshootData.sizeMultiplierX, (vector.y * -1) * troubleshootData.sizeMultiplierY);
            Vector2 newVector = vector + diffPosition;

            return newVector;
        }
        void SizingContent(Vector2 position)
        {
            RectTransform rect = contentTransform.gameObject.GetComponent<RectTransform>();
            float posx = position.x;
            float posy = position.y;

            if (posx < 0)
                posx = posx * -1;

            if (position.x < 0)
                posx = posx - canvasParent.localPosition.x;
            else
                posx = posx + canvasParent.localPosition.x;

            if ((rect.sizeDelta.x / 2 - offsideX) < posx)
            {
                float deltaPos = posx - (rect.sizeDelta.x / 2 - offsideX);
                rect.sizeDelta = new Vector2(rect.sizeDelta.x + deltaPos, rect.sizeDelta.y);
                if (position.x > 0)
                    deltaPos = deltaPos * -1;
                canvasParent.localPosition = new Vector2(canvasParent.localPosition.x + (deltaPos / 2), canvasParent.localPosition.y);
                rect.transform.localPosition = new Vector2((rect.transform.localPosition.x - (deltaPos / 2)), rect.transform.localPosition.y);
            }


            if (posy < 0)
                posy = posy * -1;

            if (position.y < 0)
                posy = posy - canvasParent.localPosition.y;
            else
                posy = posy + canvasParent.localPosition.y;

            if ((rect.sizeDelta.y / 2 - offsideY) < posy)
            {
                float deltaPos = posy - (rect.sizeDelta.y / 2 - offsideY);
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + deltaPos);
                if (position.y > 0)
                    deltaPos = deltaPos * -1;
                canvasParent.localPosition = new Vector2(canvasParent.localPosition.x, canvasParent.localPosition.y + (deltaPos / 2));
                rect.transform.localPosition = new Vector2(rect.transform.localPosition.x, rect.transform.localPosition.y - (deltaPos / 2));
            }
        }

        public void InstatiateText(TroubleshootNode node, TroubleshootNode nodeStart)
        {
            if (!string.IsNullOrEmpty(node.content.troubleshootdata.text))
            {
                for (int i = 0; i < nodeStart.GetOutputPort(outputNodeName).ConnectionCount; i++)
                {
                    if (nodeStart.GetOutputPort(outputNodeName).GetConnection(i).node == node)
                    {
                        Text TextAddOn = Instantiate(addOnText, canvasParent, false);
                        TextAddOn.text = node.content.troubleshootdata.text;
                        TextAddOn.gameObject.SetActive(true);
                        int reroutePointCount = nodeStart.GetOutputPort(outputNodeName).GetReroutePoints(i).Count;

                        if (reroutePointCount == 0)
                        {
                            Vector2 meanPosition = (node.position + nodeStart.position) / 2;
                            TextPositioning(TextAddOn, meanPosition);
                        }
                        else if (reroutePointCount == 1)
                        {
                            TextPositioning(TextAddOn, nodeStart.GetOutputPort(outputNodeName).GetReroutePoints(i)[0]);
                        }
                        else if (reroutePointCount % 2 != 0)
                        {
                            int x = (reroutePointCount - 1) / 2;
                            TextPositioning(TextAddOn, nodeStart.GetOutputPort(outputNodeName).GetReroutePoints(i)[x]);
                            Debug.Log("ganjil");
                        }
                        else
                        {
                            int x = reroutePointCount / 2;
                            int y = x - 1;
                            Debug.Log(x + " " + y);
                            Vector2 meanPosition = (nodeStart.GetOutputPort(outputNodeName).GetReroutePoints(i)[x] + nodeStart.GetOutputPort(outputNodeName).GetReroutePoints(i)[y]) / 2;
                            TextPositioning(TextAddOn, meanPosition);
                            Debug.Log("Genap");
                        }
                    }
                }
            }
        }

        void TextPositioning(Text TextAddOn, Vector2 reroutePoints)
        {
            TextAddOn.transform.localPosition = ReverseYaxis(reroutePoints) + textOffset;
        }

    }
}

