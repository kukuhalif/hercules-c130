﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    public class FigurePanel : UIPanel
    {
        [SerializeField] TMP_Text figureName;
        [SerializeField] InteractionButton prevFigureButton;
        [SerializeField] InteractionButton nextFigureButton;

        protected override void Awake()
        {
            base.Awake();
            EventManager.AddListener<PlayFigureEvent>(PlayFigureListener);
            EventManager.AddListener<FreePlayEvent>(FreePlayListener);
            prevFigureButton.OnClickEvent += PreviousFigure;
            nextFigureButton.OnClickEvent += NextFigure;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<PlayFigureEvent>(PlayFigureListener);
            EventManager.RemoveListener<FreePlayEvent>(FreePlayListener);
            prevFigureButton.OnClickEvent -= PreviousFigure;
            nextFigureButton.OnClickEvent -= NextFigure;
        }

        private void PlayFigureListener(PlayFigureEvent e)
        {
            if (e.figureCount == 0)
                ClosePanel();
            else
            {
                figureName.text = e.name + " (" + (e.currentFigure + 1).ToString() + " / " + e.figureCount.ToString() + ")";
                ShowPanel();
            }
        }

        private void FreePlayListener(FreePlayEvent e)
        {
            ClosePanel();
        }

        private void NextFigure()
        {
            EventManager.TriggerEvent(new NextFigureEvent());
        }

        private void PreviousFigure()
        {
            EventManager.TriggerEvent(new PrevFigureEvent());
        }
    }
}
