﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    public class MonitorCameraPanel : UIPanel
    {
        [SerializeField] RectTransform monitorPanel;
        [SerializeField] InteractionButton prevButton;
        [SerializeField] InteractionButton nextButton;
        [SerializeField] TextMeshProUGUI cameraName;

        int currentIndex;
        List<MonitorCamera> monitors;
        UIInteraction panelInteraction;

        protected override void Start()
        {
            base.Start();
            EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent += Prev;
            nextButton.OnClickEvent += Next;

            panelInteraction = monitorPanel.gameObject.AddComponent<UIInteraction>();
            panelInteraction.OnClickDown += OnLeftClickDown;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            prevButton.OnClickEvent -= Prev;
            nextButton.OnClickEvent -= Next;
            panelInteraction.OnClickDown -= OnLeftClickDown;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            cameraName.color = theme.genericTextColor;
        }

        private void OnLeftClickDown()
        {
            // trigger panel selection because body panel has 2 ui element (body & monitor panel)
            SetOnTop();

            RaycastHit hitInfo = new RaycastHit();
            if (VirtualTrainingCamera.RaycastFromMonitorCamera(ref hitInfo, VirtualTrainingInputSystem.PointerPosition, monitorPanel))
            {
                EventManager.TriggerEvent(new MonitorCameraInteractionEvent(hitInfo.transform.gameObject));
            }
        }

        private void MonitorCameraListener(MonitorCameraEvent e)
        {
            if (e.monitors == null || e.monitors.Count == 0)
            {
                monitors = null;
                ClosePanel();
                return;
            }

            monitors = e.monitors;
            currentIndex = 0;

            cameraName.text = monitors[0].displayName;

            ShowPanel();
            EventManager.TriggerEvent(new PlayMonitorCameraEvent(0, monitors[0]));
        }

        private void Next()
        {
            currentIndex++;

            if (currentIndex >= monitors.Count)
                currentIndex = 0;

            cameraName.text = monitors[currentIndex].displayName;

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        private void Prev()
        {
            currentIndex--;

            if (currentIndex < 0)
                currentIndex = monitors.Count - 1;

            cameraName.text = monitors[currentIndex].displayName;

            EventManager.TriggerEvent(new PlayMonitorCameraEvent(currentIndex, monitors[currentIndex]));
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (monitors != null)
                base.ShowAllPanelListener(e);
        }
    }
}
