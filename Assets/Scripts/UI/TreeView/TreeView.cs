using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIWidgets;
using TMPro;
using UnityEngine.EventSystems;

public class TreeView<TComponent, TItem> : TreeViewCustom<TComponent, TItem>
    where TComponent : TreeViewComponentBase<TItem>
{
    [SerializeField] TMP_InputField searchInputField;

    public override void Start()
    {
        base.Start();
        searchInputField.onValueChanged.AddListener(SearchSubmit);
        InitializeTree();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        searchInputField.onValueChanged.RemoveListener(SearchSubmit);
    }

    protected virtual void InitializeTree()
    {

    }

    protected virtual void ShowFilteredElement()
    {

    }

    protected bool IsSearching()
    {
        return !string.IsNullOrEmpty(searchInputField.text);
    }

    protected string GetSearchString()
    {
        return searchInputField.text;
    }

    private void SearchSubmit(string search)
    {
        Clear();

        if (string.IsNullOrEmpty(search))
        {
            InitializeTree();
        }
        else
        {
            ShowFilteredElement();
        }
    }
}
