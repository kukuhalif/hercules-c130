using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class AssistiveTouch : UIPanel
    {
        float pointerPositionTolerance = 0.5f;
        Vector2 lastPointerPosition;

        [SerializeField] InteractionButton button;
        [SerializeField] ObjectInteraction objectInteraction;

        protected override void Start()
        {
            base.Start();

            button.DisableSFX();

            VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
            button.OnClickEvent += OnClick;

            EventManager.AddListener<ShowStartingUIPanelEvent>(ShowListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
            button.OnClickEvent -= OnClick;
        }

        private void ShowListener(ShowStartingUIPanelEvent e)
        {
            ShowPanel();
            EventManager.RemoveListener<ShowStartingUIPanelEvent>(ShowListener);
        }

        private void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            lastPointerPosition = pointerPosition;
        }

        public void OnClick()
        {
            if (objectInteraction.isPanelActive)
                return;

            Vector2 pointerPos = VirtualTrainingInputSystem.PointerPosition;

            if (Vector2.Distance(lastPointerPosition, pointerPos) < pointerPositionTolerance)
            {
                objectInteraction.SetBodyPosition(BodyRect.anchoredPosition);
                objectInteraction.ShowPanel(null, false, false);

                ClosePanel();
            }
        }
    }
}
