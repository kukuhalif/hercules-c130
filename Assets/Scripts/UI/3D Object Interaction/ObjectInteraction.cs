﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    public class ObjectInteraction : UIPanel
    {
        [SerializeField] AssistiveTouch assistiveTouch;
        [SerializeField] RectTransform box;
        [SerializeField] TextMeshProUGUI featureText;
        [SerializeField] GameObject background;

        [SerializeField] InteractionToggle rotateMode;
        [SerializeField] InteractionToggle multipleSelectionMode;
        [SerializeField] InteractionToggle pullApartMode;
        [SerializeField] InteractionButton resetCamera;
        [SerializeField] InteractionButton resetObject;
        [SerializeField] InteractionToggle lockCameraMode;
        [SerializeField] InteractionToggle xRayMode;
        [SerializeField] InteractionToggle cutawayMode;
        [SerializeField] InteractionButton visible;
        [SerializeField] InteractionButton hide;
        [SerializeField] InteractionButton partObjectList;

        bool clickListenerAdded = false;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();

            rotateMode.OnChangeStateEvent += RotateMode_OnChangeStateEvent;
            multipleSelectionMode.OnChangeStateEvent += MultipleSelectionMode_OnChangeStateEvent;
            pullApartMode.OnChangeStateEvent += PullApartMode_OnChangeStateEvent;
            resetCamera.OnClickEvent += ResetCamera_OnChangeStateEvent;
            resetObject.OnClickEvent += ResetObject_OnChangeStateEvent;
            lockCameraMode.OnChangeStateEvent += LockCamera_OnChangeStateEvent;
            xRayMode.OnChangeStateEvent += XRayMode_OnChangeStateEvent;
            cutawayMode.OnChangeStateEvent += CutawayMode_OnChangeStateEvent;
            visible.OnClickEvent += Visible_OnChangeStateEvent;
            hide.OnClickEvent += Hide_OnChangeStateEvent;
            partObjectList.OnClickEvent += PartObjectList_OnChangeStateEvent;

            EventManager.AddListener<HideObjectEvent>(HideObjectListener);
            EventManager.AddListener<CutawayEvent>(CutawayListener);
            EventManager.AddListener<ObjectListPanelEvent>(ObjectListPanelListener);

            DefaultState();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            rotateMode.OnChangeStateEvent -= RotateMode_OnChangeStateEvent;
            multipleSelectionMode.OnChangeStateEvent -= MultipleSelectionMode_OnChangeStateEvent;
            pullApartMode.OnChangeStateEvent -= PullApartMode_OnChangeStateEvent;
            resetCamera.OnClickEvent -= ResetCamera_OnChangeStateEvent;
            resetObject.OnClickEvent -= ResetObject_OnChangeStateEvent;
            lockCameraMode.OnChangeStateEvent -= LockCamera_OnChangeStateEvent;
            xRayMode.OnChangeStateEvent -= XRayMode_OnChangeStateEvent;
            cutawayMode.OnChangeStateEvent -= CutawayMode_OnChangeStateEvent;
            visible.OnClickEvent -= Visible_OnChangeStateEvent;
            hide.OnClickEvent -= Hide_OnChangeStateEvent;
            partObjectList.OnClickEvent -= PartObjectList_OnChangeStateEvent;

            EventManager.RemoveListener<HideObjectEvent>(HideObjectListener);
            EventManager.RemoveListener<CutawayEvent>(CutawayListener);
            EventManager.RemoveListener<ObjectListPanelEvent>(ObjectListPanelListener);

            if (clickListenerAdded)
            {
                VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
                clickListenerAdded = false;
            }

        }

        private void DefaultState()
        {
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(true));
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionCutawayEvent(false));
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
        }

        protected override void ResetAllPanelListener(ResetAllUIPanelEvent e)
        {
            Hide();
        }

        #region toggle listener

        private void PartObjectList_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionShowPartObjectListEvent(!partObjectList.ForcedOn));
        }

        private void Hide_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(false));
        }

        private void Visible_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionVisibleEvent(true));
            visible.ForceOn(false);
        }

        private void CutawayMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionCutawayEvent(on));
        }

        private void XRayMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionXRayEvent(on));
        }

        private void LockCamera_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionLockCameraEvent(on));
        }

        private void ResetObject_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
        }

        private void ResetCamera_OnChangeStateEvent()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetCameraEvent());
        }

        private void PullApartMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionPullApartEvent(on));
        }

        private void MultipleSelectionMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionMultipleSelectionEvent(on));
        }

        private void RotateMode_OnChangeStateEvent(bool on)
        {
            EventManager.TriggerEvent(new ObjectInteractionRotateEvent(on));
        }

        private void HideObjectListener(HideObjectEvent e)
        {
            visible.ForceOn(true);
        }

        private void CutawayListener(CutawayEvent e)
        {
            cutawayMode.OnChangeStateEvent -= CutawayMode_OnChangeStateEvent;
            cutawayMode.IsOn = e.cutaway != null;
            cutawayMode.OnChangeStateEvent += CutawayMode_OnChangeStateEvent;
        }

        private void ObjectListPanelListener(ObjectListPanelEvent e)
        {
            partObjectList.ForceOn(e.open);
        }

        #endregion

        protected override void OnEnablePanel()
        {
            base.OnEnablePanel();
            VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
            clickListenerAdded = true;

            background.transform.position = box.transform.position;
            background.SetActive(true);

            featureText.transform.position = box.transform.position;
            featureText.gameObject.SetActive(true);
        }

        protected override void OnDisablePanel()
        {
            base.OnDisablePanel();

            VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
            clickListenerAdded = false;

            background.SetActive(false);

            featureText.text = "";
            featureText.gameObject.SetActive(false);
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            featureText.color = theme.genericTextColor;
        }

        private void Hide()
        {
            ClosePanel();
            assistiveTouch.ShowPanel(null, false);
        }

        private void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (featureText.rectTransform.RectContains(pointerPosition))
            {
                Hide();
                return;
            }
        }
    }
}
