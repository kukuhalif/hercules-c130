using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

namespace VirtualTraining.UI
{
    public class InteractionObjectText : MonoBehaviour, IPointerEnterHandler
    {
        [SerializeField] TextMeshProUGUI _featureText;

        public void OnPointerEnter(PointerEventData eventData)
        {
            _featureText.text = gameObject.name;
        }
    }
}
