using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class MaintenancePanel : UIPanel
    {
        [SerializeField] TreeViewMaintenance treeViewMaintenance;

        bool isMaintenanceDataExist = false;

        protected override void Start()
        {
            base.Start();

            EventManager.AddListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.RemoveListener<MaintenanceUIEvent>(MaintenanceUIEventListener);
        }

        protected override void ShowAllPanelListener(ShowAllUIPanelEvent e)
        {
            if (isMaintenanceDataExist)
                base.ShowAllPanelListener(e);
        }

        private void MaintenanceUIEventListener(MaintenanceUIEvent e)
        {
            if (e.materi == null)
            {
                isMaintenanceDataExist = false;
                ClosePanel();
                return;
            }

            isMaintenanceDataExist = true;

            // get maintenance tree root from maintenance tree element list
            MaintenanceTreeElement root = DatabaseManager.GetMaintenanceTree(e.materi.maintenanceData.treeElements);

            // get list of roots
            List<MaintenanceTreeElement> roots = new List<MaintenanceTreeElement>();

            // actual child is grand child from root
            for (int i = 0; i < root.children.Count; i++)
            {
                for (int ii = 0; ii < root.children[i].children.Count; ii++)
                {
                    roots.Add((MaintenanceTreeElement)root.children[i].children[ii]);
                }
            }

            treeViewMaintenance.GenerateTree(roots);
            ShowPanel();
        }
    }
}
