using System;
using System.ComponentModel;
using UIWidgets;
using UnityEngine;

namespace VirtualTraining.UI
{
    [Serializable]
    public class TreeViewManualItemData : ITreeViewManualItem
    {
        [SerializeField] string name;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewManualItemData"/> class.
        /// </summary>
        /// <param name="ManualName">Name.</param>
        public TreeViewManualItemData(string ManualName)
        {
            name = ManualName;
        }

        /// <summary>
        /// OnChange event.
        /// </summary>
        [Obsolete("Use PropertyChanged.")]
        public event OnChange OnChange = () => { };

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

        /// <summary>
        /// Notify property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
#pragma warning disable 0618
            OnChange();
#pragma warning restore 0618
        }

        /// <summary>
        /// Display item data using specified component.
        /// </summary>
        /// <param name="component">Component.</param>
        public void Display(TreeViewManualComponent component)
        {
            if (component.Icon != null)
            {
                component.Icon.sprite = null;
                component.Icon.color = Color.clear;
            }

            if (component.TextTMPro != null)
                component.TextTMPro.text = Name;
        }
    }
}