using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UIWidgets;
using Paroxe.PdfRenderer;

namespace VirtualTraining.UI
{
    public class ManualTreeNode : TreeNode<ITreeViewManualItem>
    {
        private bool isFolder;
        private PDFAsset pdf;

        public ManualTreeNode(PDFAsset pdf, bool isFolder, ITreeViewManualItem nodeItem, ObservableList<TreeNode<ITreeViewManualItem>> nodeNodes = null, bool nodeIsExpanded = false, bool nodeIsVisible = true) : base(nodeItem, nodeNodes, nodeIsExpanded, nodeIsVisible)
        {
            this.pdf = pdf;
            this.isFolder = isFolder;
        }

        public PDFAsset PDF { get => pdf; }
        public bool IsFolder { get => isFolder; }
    }
}