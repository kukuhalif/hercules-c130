using System;
using UnityEngine;
using System.ComponentModel;
using UIWidgets;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.UI
{
    /// <summary>
    /// TreeViewManual element component.
    /// </summary>
    public class TreeViewManualComponent : TreeViewComponentBase<ITreeViewManualItem>
    {
        [SerializeField] public TextMeshProUGUI TextTMPro;

        static ITreeViewManualItem selected;

        ITreeViewManualItem data;
        InteractionButton button;

        /// <summary>
        /// Item.
        /// </summary>
        public ITreeViewManualItem item
        {
            get
            {
                return data;
            }

            set
            {
                if (data != null)
                {
                    data.PropertyChanged -= UpdateView;
                }

                data = value;
                if (data != null)
                {
                    data.PropertyChanged += UpdateView;
                }

                UpdateView();
            }
        }

        protected override void ToggleNode()
        {
            base.ToggleNode();

            VirtualTraining.Core.AudioPlayer.PlaySFX(Node.IsExpanded ? SFX.ExpandTreeElement : SFX.CollapseTreeElement);
        }

        /// <summary>
        /// Set data.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="depth">Depth.</param>
        public override void SetData(TreeNode<ITreeViewManualItem> node, int depth)
        {
            base.SetData(node, depth);

            item = (node == null) ? null : node.Item;
        }

        /// <summary>
        /// Update view.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ev">Event.</param>
        protected virtual void UpdateView(object sender = null, PropertyChangedEventArgs ev = null)
        {
            if (item == null)
            {
                Icon.sprite = null;
                if (TextTMPro != null)
                    TextTMPro.text = string.Empty;
            }
            else
            {
                item.Display(this);
            }

            if (button == null)
                button = GetComponent<InteractionButton>();

            ManualTreeNode manualTreeNode = Node as ManualTreeNode;
            button.SetActive(!manualTreeNode.IsFolder);

            if (selected == null)
            {
                if (!manualTreeNode.IsFolder)
                {
                    if (Node.Clicked)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
            else
            {
                if (!manualTreeNode.IsFolder)
                {
                    if (selected == data)
                        ClickedOn();
                    else
                        ClickedOff();
                }
            }
        }

        /// <summary>
        /// Remove listeners.
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (data != null)
            {
                data.PropertyChanged -= UpdateView;
            }

            onClickItem.RemoveListener(OnClick);
            ClickedOff();
        }

        protected override void Start()
        {
            base.Start();

            button.DisableSFX();
            onClickItem.AddListener(OnClick);
        }

        private void ClickedOn()
        {
            if (button != null)
                button.ForceOn(true);
            EventManager.AddListener<OnClosedPdfPanelEvent>(ClosePDFListener);
            EventManager.AddListener<ShowPdfEvent>(OpenOtherPDFListener);
            if (Node != null)
                Node.Clicked = true;

            selected = data;
        }

        private void ClickedOff()
        {
            if (button != null)
                button.ForceOn(false);
            EventManager.RemoveListener<OnClosedPdfPanelEvent>(ClosePDFListener);
            EventManager.RemoveListener<ShowPdfEvent>(OpenOtherPDFListener);
            if (Node != null)
                Node.Clicked = false;
        }

        private void OnClick(ListViewItem e)
        {
            ToggleNode();

            if (selected == data)
                return;

            ManualTreeNode ManualTreeNode = Node as ManualTreeNode;
            EventManager.TriggerEvent(new ShowPdfEvent(ManualTreeNode.PDF));

            ClickedOn();
        }

        private void ClosePDFListener(OnClosedPdfPanelEvent e)
        {
            ClickedOff();
            selected = null;
        }

        private void OpenOtherPDFListener(ShowPdfEvent e)
        {
            ClickedOff();
            selected = null;
        }
    }
}