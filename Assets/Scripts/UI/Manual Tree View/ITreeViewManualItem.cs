using System.ComponentModel;

namespace VirtualTraining.UI
{
	/// <summary>
	/// TreeViewManualItem interface.
	/// </summary>
	public interface ITreeViewManualItem : INotifyPropertyChanged
	{
		/// <summary>
		/// Display item data using specified component.
		/// </summary>
		/// <param name="component">Component.</param>
		void Display(TreeViewManualComponent component);
	}
}