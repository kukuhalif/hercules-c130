using System;
using System.Collections;
using System.Collections.Generic;
using UIWidgets;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;

namespace VirtualTraining.UI
{
    public class TreeViewManual : TreeView<TreeViewManualComponent, ITreeViewManualItem>
    {
        // initialize filtered tree

        List<ManualTreeNode> allManuales = new List<ManualTreeNode>();

        protected override void InitializeTree()
        {
            Nodes = GetData();
        }

        protected override void ShowFilteredElement()
        {
            var rootNodes = new ObservableList<TreeNode<ITreeViewManualItem>>();
            for (int i = 0; i < allManuales.Count; i++)
            {
                if (allManuales[i].PDF.name.ToLower().Contains(GetSearchString().ToLower()))
                {
                    var child = allManuales[i];
                    rootNodes.Add(child);
                }
            }
            Nodes = rootNodes;
        }

        //---------------------------------------------

        // initialize complete Manual tree

        private ManualTreeNode CreateNode(PDFAsset pdf, bool isFolder, ITreeViewManualItem item, ObservableList<TreeNode<ITreeViewManualItem>> nodes)
        {
            var node = new ManualTreeNode(pdf, isFolder, item, nodes, false, true);
            allManuales.Add(node);
            return node;
        }

        private ObservableList<TreeNode<ITreeViewManualItem>> GetData()
        {
            allManuales.Clear();

            var rootNodes = new ObservableList<TreeNode<ITreeViewManualItem>>();
            List<PDFAsset> pdfs = DatabaseManager.GetPdfUi();

            for (int i = 0; i < pdfs.Count; i++)
            {
                AddChildNode(pdfs[i], rootNodes, new ObservableList<TreeNode<ITreeViewManualItem>>());
            }

            return rootNodes;
        }

        private void AddChildNode(PDFAsset pdf, ObservableList<TreeNode<ITreeViewManualItem>> currentNodes, ObservableList<TreeNode<ITreeViewManualItem>> childNodes)
        {
            // no folder here
            // actually no actual tree for manual

            currentNodes.Add(CreateNode(pdf, false, new TreeViewManualItemData(pdf.name), childNodes));
        }

        //---------------------------------------------
    }
}