﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VirtualTraining.Core;

namespace VirtualTraining.UI
{
    public class SettingPanel : UIPanel
    {
        [SerializeField] InteractionTab tab;
        [SerializeField] GameObject[] tabContents;

        [SerializeField] Image panelBg1;
        [SerializeField] Image panelBg2;

        [SerializeField] InteractionButton cancelButton;
        [SerializeField] InteractionButton resetButton;
        [SerializeField] InteractionButton applyButton;

        [SerializeField] TextMeshProUGUI[] allGenericText;

        [SerializeField] Slider rotateSpeedSlider;
        [SerializeField] Slider zoomSpeedSlider;
        [SerializeField] Slider dragSpeedSlider;
        [SerializeField] Slider cameraTransitionSpeedSlider;
        [SerializeField] Slider voiceVolumeSlider;
        [SerializeField] Slider sfxVolumeSlider;
        [SerializeField] Slider vrCanvasDistanceSlider;
        [SerializeField] Slider vrMenuHeightSlider;

        [SerializeField] TMP_Dropdown resolutionDropdown;
        [SerializeField] TMP_Dropdown qualityDropdown;

        [SerializeField] TMP_Dropdown fullscreenDropdown;
        [SerializeField] TMP_Dropdown showEnvirontmentObjectDropdown;

        SettingData defaultSetting;
        SettingData currentSetting;
        List<Resolution> resolutions = new List<Resolution>();

        bool firstCloseDone = false;

        protected override void Start()
        {
            base.Start();

            tab.OnChangeTabEvent += ChangeTabContentListener;

            cancelButton.OnClickEvent += CancelButtonCallback;
            resetButton.OnClickEvent += ResetButtonCallback;
            applyButton.OnClickEvent += ApplyButtonCallback;

            defaultSetting = DatabaseManager.GetDefaultSetting();
            currentSetting = new SettingData();

            ResolutionOptionInit();
            QualityOptionInit();

            SetMinMaxSetting();
            AddUIListener();

            Load();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            tab.OnChangeTabEvent -= ChangeTabContentListener;
            cancelButton.OnClickEvent -= CancelButtonCallback;
            resetButton.OnClickEvent -= ResetButtonCallback;
            applyButton.OnClickEvent -= ApplyButtonCallback;
        }

        protected override void ApplyTheme()
        {
            base.ApplyTheme();

            panelBg1.color = theme.panelContentColor;
            panelBg2.color = theme.panelTitleColor;

            for (int i = 0; i < allGenericText.Length; i++)
            {
                allGenericText[i].color = theme.genericTextColor;
            }
        }

        private void ChangeTabContentListener(int selected)
        {
            for (int i = 0; i < tabContents.Length; i++)
            {
                tabContents[i].SetActive(i == selected);
            }
        }

        private void CancelButtonCallback()
        {
            ClosePanel();
        }

        private void ResetButtonCallback()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Reset setting ?", ResetSetting, null));
        }

        private void ApplyButtonCallback()
        {
            EventManager.TriggerEvent(new ConfirmationPanelEvent("Apply setting ?", Apply, null));
        }

        private void Load()
        {
            currentSetting = SettingUtility.GetCurrentSetting();

            SetUI();
            Apply();
        }

        private void Apply()
        {
            SettingUtility.SetSettingData(currentSetting);

            Screen.SetResolution(resolutions[currentSetting.resolution].width, resolutions[currentSetting.resolution].height, currentSetting.fullscreen);
            QualitySettings.SetQualityLevel(currentSetting.quality);

            EventManager.TriggerEvent(new ApplySettingEvent(currentSetting));

            if (firstCloseDone)
                ClosePanel();
            else
            {
                ClosePanel(null, false, false, false);
                firstCloseDone = true;
            }
        }

        private void SetUI()
        {
            rotateSpeedSlider.value = currentSetting.rotateSpeed;
            zoomSpeedSlider.value = currentSetting.zoomSpeed;
            dragSpeedSlider.value = currentSetting.dragSpeed;
            cameraTransitionSpeedSlider.value = currentSetting.cameraTransitionSpeed;
            voiceVolumeSlider.value = currentSetting.voiceVolume;
            sfxVolumeSlider.value = currentSetting.sfxVolume;
            vrCanvasDistanceSlider.value = currentSetting.vrCanvasDistance;
            vrMenuHeightSlider.value = currentSetting.vrMenuHeight;
            resolutionDropdown.value = currentSetting.resolution;
            qualityDropdown.value = currentSetting.quality;
            fullscreenDropdown.value = currentSetting.fullscreen ? 0 : 1;
            showEnvirontmentObjectDropdown.value = currentSetting.showEnvirontmentObject ? 0 : 1;
        }

        private void ResetSetting()
        {
            string defaultData = JsonUtility.ToJson(defaultSetting);
            currentSetting = JsonUtility.FromJson<SettingData>(defaultData);

            SetUI();
            Apply();
        }

        private void ResolutionOptionInit()
        {
            List<string> resOpt = new List<string>();
            Resolution[] res = Screen.resolutions;
            for (int i = 0; i < res.Length; i++)
            {
                float ar = (float)Screen.resolutions[i].width / (float)Screen.resolutions[i].height;
                if (ar >= 1.7 && ar < 1.8)
                {
                    if (!resOpt.Contains(res[i].width + " x " + res[i].height))
                    {
                        resolutions.Add(res[i]);
                        resOpt.Add(res[i].width + " x " + res[i].height);
                    }
                }
            }

            resolutionDropdown.ClearOptions();
            resolutionDropdown.AddOptions(resOpt);

            // resolution default
            defaultSetting.resolution = resOpt.Count - 1;
        }

        private void QualityOptionInit()
        {
            List<string> qualities = new List<string>();
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                qualities.Add(QualitySettings.names[i]);
            }

            qualityDropdown.ClearOptions();
            qualityDropdown.AddOptions(qualities);

            // quality default
            defaultSetting.quality = qualities.Count - 1;
        }

        private void AddUIListener()
        {
            rotateSpeedSlider.onValueChanged.AddListener(ChangedRotateValue);
            zoomSpeedSlider.onValueChanged.AddListener(ChangedZoomValue);
            dragSpeedSlider.onValueChanged.AddListener(ChangedDragValue);
            cameraTransitionSpeedSlider.onValueChanged.AddListener(ChangedMoveCameraValue);
            voiceVolumeSlider.onValueChanged.AddListener(ChangedBGMValue);
            sfxVolumeSlider.onValueChanged.AddListener(ChangedSFXValue);
            vrCanvasDistanceSlider.onValueChanged.AddListener(ChangedVRCanvasDistanceValue);
            vrMenuHeightSlider.onValueChanged.AddListener(ChangedVRMenuHeightValue);
            resolutionDropdown.onValueChanged.AddListener(ChangedResolutionValue);
            qualityDropdown.onValueChanged.AddListener(ChangedQualityValue);
            fullscreenDropdown.onValueChanged.AddListener(ChangedFullscreenValue);
            showEnvirontmentObjectDropdown.onValueChanged.AddListener(ChangedShowBackgroundValue);
        }

        private void SetMinMaxSetting()
        {
            var minMax = DatabaseManager.GetMinMaxSetting();

            rotateSpeedSlider.minValue = minMax.rotateSpeed.x;
            rotateSpeedSlider.maxValue = minMax.rotateSpeed.y;

            zoomSpeedSlider.minValue = minMax.zoomSpeed.x;
            zoomSpeedSlider.maxValue = minMax.zoomSpeed.y;

            dragSpeedSlider.minValue = minMax.dragSpeed.x;
            dragSpeedSlider.maxValue = minMax.dragSpeed.y;

            cameraTransitionSpeedSlider.minValue = minMax.moveCameraSpeed.x;
            cameraTransitionSpeedSlider.maxValue = minMax.moveCameraSpeed.y;

            voiceVolumeSlider.minValue = minMax.bgmVolume.x;
            voiceVolumeSlider.maxValue = minMax.bgmVolume.y;

            sfxVolumeSlider.minValue = minMax.sfxVolume.x;
            sfxVolumeSlider.maxValue = minMax.sfxVolume.y;

            vrCanvasDistanceSlider.minValue = minMax.vrCanvasDistance.x;
            vrCanvasDistanceSlider.maxValue = minMax.vrCanvasDistance.y;

            vrMenuHeightSlider.minValue = minMax.vrMenuHeight.x;
            vrMenuHeightSlider.maxValue = minMax.vrMenuHeight.y;
        }

        private void ChangedRotateValue(float value)
        {
            currentSetting.rotateSpeed = value;
        }

        private void ChangedZoomValue(float value)
        {
            currentSetting.zoomSpeed = value;
        }

        private void ChangedDragValue(float value)
        {
            currentSetting.dragSpeed = value;
        }

        private void ChangedMoveCameraValue(float value)
        {
            currentSetting.cameraTransitionSpeed = value;
        }

        private void ChangedBGMValue(float value)
        {
            currentSetting.voiceVolume = value;
        }

        private void ChangedSFXValue(float value)
        {
            currentSetting.sfxVolume = value;
        }

        private void ChangedVRMenuHeightValue(float value)
        {
            currentSetting.vrCanvasDistance = value;
        }

        private void ChangedVRCanvasDistanceValue(float value)
        {
            currentSetting.vrCanvasDistance = value;
        }

        private void ChangedResolutionValue(int value)
        {
            currentSetting.resolution = value;
        }

        private void ChangedQualityValue(int value)
        {
            currentSetting.quality = value;
        }

        private void ChangedFullscreenValue(int value)
        {
            currentSetting.fullscreen = value == 0;
        }

        private void ChangedShowBackgroundValue(int value)
        {
            currentSetting.showEnvirontmentObject = value == 0;
        }
    }
}
