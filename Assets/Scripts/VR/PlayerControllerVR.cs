﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using VirtualTraining.Feature;
using UnityEngine.XR.Management;
using Valve.VR.Extras;
using VirtualTraining.Core;
using System;

public class PlayerControllerVR : CameraController
{
    public SteamVR_Action_Vector2 inputPosition;
    public SteamVR_Action_Vector2 inputRotation;
    public float speed;
    public Camera vrCamera;
    public SteamVR_Action_Boolean TurnUp;
    public SteamVR_Action_Boolean TurnDown;
    [SerializeField] float distance;

    // Start is called before the first frame update

    private void Awake()
    {
        //StartCoroutine(StartXR());
    }

    private void OnDestroy()
    {
        StopXR();
    }
    IEnumerator StartXR()
    {
        yield return XRGeneralSettings.Instance.Manager.InitializeLoader();
        if (XRGeneralSettings.Instance.Manager.activeLoader == null)
        {
            Debug.LogError("Initializing XR Failed. Check Editor or Player log for details.");
        }
        else
        {
            Debug.Log("Starting XR...");
            XRGeneralSettings.Instance.Manager.StartSubsystems();
            yield return null;
        }
    }

    void StopXR()
    {
        if (XRGeneralSettings.Instance.Manager.isInitializationComplete)
        {
            XRGeneralSettings.Instance.Manager.StopSubsystems();
            //Camera.main.ResetAspect();
            XRGeneralSettings.Instance.Manager.DeinitializeLoader();
        }
    }
    void Start()
    {
        VirtualTrainingCamera.SetCurrentCameraController(this, true);
        TurnUp = SteamVR_Input.GetBooleanAction("SnapTurnUp");
        TurnDown = SteamVR_Input.GetBooleanAction("SnapTurnDown");
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 direction = Player.instance.hmdTransform.TransformDirection(new Vector3(inputPosition.axis.x, 0, inputPosition.axis.y));
        transform.position += speed * Time.deltaTime * Vector3.ProjectOnPlane(direction, Vector3.up);

        if (inputRotation.axis.x > 0.6 || inputRotation.axis.x < -0.6f)
        {
            Vector3 rotation = new Vector3(0, inputRotation.axis.x, 0);
            transform.eulerAngles += speed * 40 * Time.deltaTime * rotation;
        }

        if (inputRotation.axis.y > 0.7f)
        {
            SteamVR_LaserPointer.distance += distance * Time.deltaTime;
        }
        else if (inputRotation.axis.y < -0.7f)
        {
            if (distance > 0)
                SteamVR_LaserPointer.distance -= distance * Time.deltaTime;
        }
    }

    public override Camera GetCamera()
    {
        return vrCamera;
    }

    public override void ResetCamera(Action arriveAction)
    {

    }

    public override void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
    {

    }

    public override void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, float fov, float orthographicSize, Action arriveAction)
    {

    }
}
