using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class MonitorCameraManager : MonoBehaviour
    {
        [SerializeField] float moveCameraSpeed = 5f;
        [SerializeField] float cameraArriveDifference = 0.001f;
        [SerializeField] Camera monitorCamera;

        MonitorCamera _destination;
        bool _arrive;

        private void Start()
        {
            EventManager.AddListener<MonitorCameraEvent>(MonitorCameraListener);
            EventManager.AddListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);

            VirtualTrainingCamera.SetCurrentMonitorCamera(monitorCamera);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MonitorCameraEvent>(MonitorCameraListener);
            EventManager.RemoveListener<PlayMonitorCameraEvent>(PlayMonitorCameraListener);
        }

        private void MonitorCameraListener(MonitorCameraEvent e)
        {
            if (e.monitors == null || e.monitors.Count == 0)
            {
                DisableCamera();
            }
        }

        private void PlayMonitorCameraListener(PlayMonitorCameraEvent e)
        {
            monitorCamera.gameObject.SetActive(true);
            _destination = e.destination;
            _arrive = false;
        }

        private void DisableCamera()
        {
            monitorCamera.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (!_arrive && monitorCamera.gameObject.activeInHierarchy)
            {
                monitorCamera.transform.position = Vector3.Lerp(monitorCamera.transform.position, _destination.destination.position, moveCameraSpeed * Time.deltaTime);
                monitorCamera.transform.rotation = Quaternion.Slerp(monitorCamera.transform.rotation, _destination.destination.rotation, moveCameraSpeed * Time.deltaTime);

                if (Vector3.Distance(transform.position, _destination.destination.position) < cameraArriveDifference &&
                    Quaternion.Angle(transform.rotation, _destination.destination.rotation) < cameraArriveDifference)
                {
                    _arrive = true;
                }
            }
        }
    }
}
