using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class AnimationPlayerScript : AnimationPlayerBase
{
    [System.Serializable]
    public class AnimationSliderData
    {
        public Vector3 startPosition;
        public Vector3 endPosition;
        public Vector3 startRotation;
        public Vector3 endRotation;
        public float duration;
        public float originalDuration;
        public float sliderValue;
    }

    public float totalDuration;
    float totalDurationTemp;
    float durationReduceValue;

    Vector3 startPositionTemp;
    Vector3 endPositionTemp;
    Vector3 startRotationTemp;
    Vector3 endRotationTemp;

    public float duration;
    //Vector3 currentPosition;
    //Vector3 currentRotation;
    Vector3 deltaPosition;
    Vector3 deltaRotation;

    float lerpValue;
    bool isAnimating;
    bool globalCoordinate;
    bool isReverse;

    float currentSlider;
    float currentLerpValue;
    public List<SADataKeyframe> scriptedAnimationDatas = new List<SADataKeyframe>();
    [SerializeField] List<AnimationSliderData> durationAnimations = new List<AnimationSliderData>();
    List<AnimationSliderData> durationAnimationsBackward = new List<AnimationSliderData>();
    float currentSpeed;
    GameObject newParent;
    GameObject newObject;
    Transform parentOld;
    Vector3 parentOldStartPosition;

    public void Setup(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputData, bool interaction)
    {
        SetVBBaseData(vbDataModel, vbOutputData, interaction);
        this.globalCoordinate = vbOutputData.scriptedAnimationElement.GetData().globalCoordinate;
        this.isReverse = vbOutputData.isReverseAnimation;
        scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animData;
        SetSliderPercentage(getVBDataModel.defaultValue);
        isAnimatedScript = true;
    }
    public void SetupOnStart(VirtualButtonDataModel vbDataModel, VBStartAnimationData vbStartData, bool interaction)
    {
        VirtualButtonOutputData vbOutputData = new VirtualButtonOutputData();
        vbOutputData.scriptedAnimationElement = vbStartData.scriptedAnimationElement;
        vbOutputData.ObjectAnimation = vbStartData.ObjectAnimation;
        vbOutputData.isReverseAnimation = vbStartData.isReverseAnimation;
        vbOutputData.isDisabledFinished = vbStartData.isDisabledFinished;
        SetVBBaseData(vbDataModel, vbOutputData, interaction);
        this.globalCoordinate = vbOutputData.scriptedAnimationElement.GetData().globalCoordinate;
        this.isReverse = vbOutputData.isReverseAnimation;
        scriptedAnimationDatas = vbOutputData.scriptedAnimationElement.GetData().animData;
        SetSliderPercentage(getVBDataModel.defaultValue);
        isAnimatedScript = true;
    }

    public void SetupAnimation(AnimationFigureData animationFigureData)
    {
        this.globalCoordinate = animationFigureData.scriptedAnimationElement.GetData().globalCoordinate;
        this.isReverse = animationFigureData.isReverseAnimation;
        scriptedAnimationDatas = animationFigureData.scriptedAnimationElement.GetData().animData;
        for (int j = 0; j < scriptedAnimationDatas.Count; j++)
        {
            totalDuration += scriptedAnimationDatas[j].duration;
        }
        isAnimatedScript = true;
        VirtualButtonOutputData vbOutputData = new VirtualButtonOutputData();
        vbOutputData.isDisabledFinished = animationFigureData.isDisabledFinished;
        SetupVBBase(vbOutputData);
    }

    public float GetTotalDuration()
    {
        totalDuration = 0;
        for (int j = 0; j < scriptedAnimationDatas.Count; j++)
        {
            totalDuration += scriptedAnimationDatas[j].duration;
        }

        return totalDuration;
    }

    public void SetupAnimationScriptTools(bool isCoordinate)
    {
        this.globalCoordinate = isCoordinate;
        SetupVBBase();
    }

    private void Start()
    {
        StartAnimator(false);
        if (scriptedAnimationDatas != null)
            SetupAnimationScript();
    }

    // Update is called once per frame
    void Update()
    {
        Animating();
        if (newParent != null && parentOld != null)
        {
            Vector3 parentDeltaPosition = parentOld.transform.position - parentOldStartPosition;
            if (parentDeltaPosition != Vector3.zero)
            {
                newParent.transform.position = newParent.transform.position + (parentOld.transform.position - parentOldStartPosition);
                parentOldStartPosition = parentOld.transform.position;
            }
        }
    }

    public override void Animating()
    {
        if (isAnimating)
        {
            PlayAnimation();
            if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
            {
                if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter || getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation
                    || getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
                {
                    if (isAnimating)
                    {
                        //if (isReverse)
                        //    PlayAnimationSlider(durationAnimationsBackward);
                        //else
                        PlayAnimationSlider(durationAnimations);
                    }
                }
                else
                {
                    //if (isReverse)
                    //    PlayAnimationSlider(durationAnimationsBackward);
                    //else
                    PlayAnimationSlider(durationAnimations);
                }
            }
            else
            {
                //if (isReverse)
                //    PlayAnimationSlider(durationAnimationsBackward);
                //else
                PlayAnimationSlider(durationAnimations);
            }
        }
    }

    public override void StartPlayAnimation(float maxValue, float speed)
    {
        maxSliderValue = maxValue;

        currentLerpValue = sliderValue;
        totalDurationTemp = totalDuration * (Mathf.Abs(maxValue - currentLerpValue));
        lerpValue = 0;
        isAnimating = true;
        currentSpeed = speed;
        //Debug.Log(maxValue + " " + speed);
    }

    void SetupAnimationScript()
    {
        if (totalDuration == 0)
        {
            for (int i = 0; i < scriptedAnimationDatas.Count; i++)
            {
                totalDuration += scriptedAnimationDatas[i].duration;
            }
        }
        totalDurationTemp = totalDuration;

        for (int j = 0; j < scriptedAnimationDatas.Count; j++)
        {
            durationReduceValue += scriptedAnimationDatas[j].duration;

            if (j == 0)
            {
                if (newObject != null)
                {
                    if (globalCoordinate)
                    {
                        startPositionTemp = newObject.transform.position;
                        startRotationTemp = newObject.transform.eulerAngles;
                    }
                    else
                    {
                        startPositionTemp = newObject.transform.localPosition;
                        startRotationTemp = newObject.transform.localEulerAngles;
                    }
                }
                else
                {
                    if (globalCoordinate)
                    {
                        startPositionTemp = gameObject.transform.position;
                        startRotationTemp = gameObject.transform.eulerAngles;
                    }
                    else
                    {
                        startPositionTemp = gameObject.transform.localPosition;
                        startRotationTemp = gameObject.transform.localEulerAngles;
                    }
                }

                if (isReverse)
                {
                    endRotationTemp += startRotationTemp + (scriptedAnimationDatas[j].deltaRotation * -1);
                    endPositionTemp += startPositionTemp + (scriptedAnimationDatas[j].deltaPosition *-1);
                }
                else
                {
                    endRotationTemp += startRotationTemp + scriptedAnimationDatas[j].deltaRotation;
                    endPositionTemp += startPositionTemp + scriptedAnimationDatas[j].deltaPosition;
                }
                    
            }
            else if (j > 0)
            {
                if (isReverse)
                {
                    startPositionTemp += scriptedAnimationDatas[j - 1].deltaPosition * -1;
                    startRotationTemp += scriptedAnimationDatas[j - 1].deltaRotation * -1;
                    endRotationTemp += scriptedAnimationDatas[j].deltaRotation * -1;
                    endPositionTemp += scriptedAnimationDatas[j].deltaPosition * -1;
                }
                else
                {
                    startPositionTemp += scriptedAnimationDatas[j - 1].deltaPosition;
                    startRotationTemp += scriptedAnimationDatas[j - 1].deltaRotation;

                    endRotationTemp += scriptedAnimationDatas[j].deltaRotation;
                    endPositionTemp += scriptedAnimationDatas[j].deltaPosition;
                }
                sliderValueTemp += scriptedAnimationDatas[j].duration / totalDuration;
            }

            MakeAnimationDataTemp(durationAnimations, durationReduceValue, startPositionTemp, endPositionTemp, startRotationTemp, endRotationTemp, sliderValueTemp, scriptedAnimationDatas[j].duration);
        }
        //if (isReverse)
        //{
        //    durationReduceValue = 0;
        //    sliderValueTemp = 0;
        //    for (int i = durationAnimations.Count - 1; i >= 0; i--)
        //    {
        //        durationReduceValue += scriptedAnimationDatas[i].duration;
        //        if (i < durationAnimations.Count - 1)
        //            sliderValueTemp += scriptedAnimationDatas[i].duration / totalDuration;

        //        MakeAnimationDataTemp(durationAnimationsBackward, durationReduceValue, durationAnimations[i].endPosition, durationAnimations[i].startPosition, durationAnimations[i].endRotation, durationAnimations[i].startRotation, sliderValueTemp, durationAnimations[i].originalDuration);
        //    }
        //}
        if (transform.localEulerAngles.x != 0 || transform.localEulerAngles.y != 0 || transform.localEulerAngles.z != 0)
            InstatntiateRotationButton();
    }
    void MakeAnimationDataTemp(List<AnimationSliderData> animData, float duration, Vector3 startPosition, Vector3 endPosition, Vector3 startRotation, Vector3 endRotation, float SliderValue, float originalDuration)
    {
        AnimationSliderData animationSliderBackward = new AnimationSliderData();
        animationSliderBackward.duration = duration;
        animationSliderBackward.startPosition = startPosition;
        animationSliderBackward.endPosition = endPosition;
        animationSliderBackward.startRotation = startRotation;
        animationSliderBackward.endRotation = endRotation;
        animationSliderBackward.sliderValue = SliderValue;
        animationSliderBackward.originalDuration = originalDuration;

        animData.Add(animationSliderBackward);
    }

    void PlayAnimationSlider(List<AnimationSliderData> animationDatas)
    {
        float x = sliderValue * totalDuration;

        for (int i = 0; i < animationDatas.Count; i++)
        {
            if (x <= animationDatas[i].duration)
            {
                deltaPosition = animationDatas[i].endPosition;
                currentPosition = animationDatas[i].startPosition;
                deltaRotation = animationDatas[i].endRotation;
                currentRotation = animationDatas[i].startRotation;
                duration = animationDatas[i].duration;

                float h = 0;

                if (i > 0)
                {
                    h = animationDatas[i - 1].duration / totalDuration;
                    currentSlider = (sliderValue - h) * (totalDuration / (animationDatas[i].duration - animationDatas[i - 1].duration));
                }
                else
                {
                    currentSlider = sliderValue / (duration / totalDuration);
                }
                break;
            }
        }
        if (newObject != null)
        {
            LerpTransform(newObject);

            transform.position = newObject.transform.position;
            transform.eulerAngles = newObject.transform.eulerAngles;
        }
        else
        {
            LerpTransform(this.gameObject);
        }
    }

    void LerpTransform(GameObject objectTransform)
    {
        if (globalCoordinate)
        {
            if (isReverse)
            {
                deltaPosition = deltaPosition * -1;
            }
            objectTransform.transform.position = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
            objectTransform.transform.eulerAngles = Vector3.Lerp(currentRotation, deltaRotation, currentSlider);
        }
        else
        {
            Vector3 currPositionTemp = currentPosition - getStartPosition;
            Vector3 deltaPositionTemp = deltaPosition - getStartPosition;

            //objectTransform.transform.localPosition = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);
            if (newParent != null)
                objectTransform.transform.localPosition = Vector3.Lerp(currPositionTemp, deltaPositionTemp, currentSlider);
            else
                objectTransform.transform.localPosition = Vector3.Lerp(currentPosition, deltaPosition, currentSlider);

            Vector3 currEulerTemp = currentRotation - getStartRotation;
            Vector3 deltaEulerTemp = deltaRotation - getStartRotation;
            //objectTransform.transform.localEulerAngles = Vector3.Lerp(currEulerTemp, deltaEulerTemp, currentSlider);
            objectTransform.transform.localEulerAngles = Vector3.Lerp(currentRotation, deltaRotation, currentSlider);
        }
    }

    private void PlayAnimation()
    {
        if (lerpValue < 1)
        {
            lerpValue += (Time.deltaTime * currentSpeed) / totalDurationTemp;
            SetSliderPercentage(Mathf.Lerp(currentLerpValue, maxSliderValue, lerpValue));
        }
        else
        {
            Debug.Log(lerpValue);
            lerpValue = 0;
            isAnimating = false;
            if (getVBOutputData.isDisabledFinished)
            {
                gameObject.SetActive(false);
            }
        }
    }

    void InstatntiateRotationButton()
    {
        newParent = new GameObject("parent rotation temp");
        newParent.transform.position = transform.position;
        parentOldStartPosition = transform.parent.position;

        Vector3 localEuler = transform.localEulerAngles;

        transform.localEulerAngles = Vector3.zero;
        Vector3 eulerZero = transform.eulerAngles;

        newParent.transform.eulerAngles = eulerZero;
        //newParent.transform.position = transform.parent.position;

        //newObject = Instantiate(this.gameObject);
        //newObject.transform.SetParent(newParent.transform);
        //AnimationPlayerScript animScript = newObject.GetComponent<AnimationPlayerScript>();
        //newObject.transform.localPosition = Vector3.zero;
        //newObject.transform.localEulerAngles = Vector3.zero;
        //animScript.enabled = false;
        //MeshRenderer mesh = GetComponent<MeshRenderer>();
        //MeshCollider col = newObject.GetComponent<MeshCollider>();
        //if (mesh != null)
        //    mesh.enabled = false;

        //if (col != null)
        //    col.enabled = false;
        if (VirtualTrainingSceneManager.GameObjectRoot.gameObject != null)
            newParent.transform.SetParent(VirtualTrainingSceneManager.GameObjectRoot.gameObject.transform);

        parentOld = transform.parent;
        transform.SetParent(newParent.transform);
        transform.localEulerAngles = localEuler;
        //transform.localEulerAngles = Vector3.zero;
    }

    public override void StopAnimation()
    {
        base.StopAnimation();
        isAnimating = false;
        Debug.Log("stop");
    }

    public override void ResetAnimation()
    {
        base.ResetAnimation();
        isAnimating = false;
        Debug.Log("reset");
    }

    public override void DestroyItself()
    {
        SetSliderPercentage(0);
        if (newParent != null)
        {
            transform.SetParent(parentOld);
            Destroy(newParent.gameObject);
        }
        transform.localPosition = getStartPosition;
        transform.localEulerAngles = getStartRotation;

        if (VirtualTrainingSceneManager.GameObjectRoot.gameObject != null)
        {
            GameObject objectRoot = VirtualTrainingSceneManager.GameObjectRoot.gameObject;
            if (objectRoot != null)
            {
                objectRoot.SetActive(false);
                objectRoot.SetActive(true);
            }
        }

        //MeshRenderer mesh = GetComponent<MeshRenderer>();
        //MeshCollider col = GetComponent<MeshCollider>();
        //if (mesh != null)
        //    mesh.enabled = true;
        //if (col != null)
        //    col.enabled = true;


        base.DestroyItself();
    }
}
