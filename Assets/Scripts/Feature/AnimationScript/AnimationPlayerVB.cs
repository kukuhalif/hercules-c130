using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class AnimationPlayerVB : AnimationPlayerBase
{
    bool isAnimating;
    bool isDragAnimating;
    float t;
    float duration;
    // Start is called before the first frame update
    void Start()
    {
        t = 0;
    }

    public float getDuration()
    {
        float durationTemp = 0;
        if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
            (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
        {
            CalculateDurationSequence();
        }
        else
            durationTemp = CalculateDurationOnce();

        return durationTemp;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDragAnimating && getvirtualButtonBase != null)
        {
            t += Time.deltaTime * getvirtualButtonOnAnimation.speedVirtualButton / 5;
            VirtualButtonDrag vbDrag = getvirtualButtonBase as VirtualButtonDrag;
            float x = Mathf.Lerp(getvirtualButtonOnAnimation.startValue, getvirtualButtonOnAnimation.endValue, t);
            float y = Mathf.Lerp(getvirtualButtonOnAnimation.startValueY, getvirtualButtonOnAnimation.endValueY, t);
            vbDrag.SetDeltaPositionAnimation(x, y);
        }
        CheckSliderValue();
    }
    void CheckSliderValue()
    {
        if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
        (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
        {

        }
        else
        {
            for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
            {
                if(i == 0)
                {
                    SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                }
                else
                {
                    if(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue < sliderValue)
                    {
                        SetSliderPercentage(getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue);
                    }
                }
            }
        }
    }
    public override void StartPlayAnimation(float maxValue, float speed)
    {
        maxSliderValue = maxValue;

        switch (getvirtualButtonBase.virtualButtonDatas.vbType)
        {
            case VirtualButtonType.Click:
                getvirtualButtonBase.virtualButtonDatas.isBack = true;
                VirtualButtonClick vbClick = getvirtualButtonBase as VirtualButtonClick;
                vbClick.maxAnimationValue = maxValue;
                break;
            case VirtualButtonType.Drag:
                isDragAnimating = true;
                break;
            case VirtualButtonType.Press:

                break;
        }
        if ((getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click) ||
                (getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
        {
            StartCoroutine(TriggerVirtualButtonSequence());
        }
        else
            getvirtualButtonBase.TriggerVirtualButton();
    }

    IEnumerator TriggerVirtualButtonSequence()
    {
        for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
        {
            getvirtualButtonBase.TriggerVirtualButton();

            if ((getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampParameter) ||
                (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation) ||
                (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.TimeStampState))
            {
                yield return null;
            }
            else
            {
                if (getvirtualButtonBase.animationPlayerContainerInteraction[i] != null)
                    yield return new WaitUntil(() => getvirtualButtonBase.animationPlayerContainerInteraction[i].sliderValue >= 0.96f);
                else
                    yield return null;
            }
        }
    }

    public override void StopAnimation()
    {
        base.StopAnimation();
        getvirtualButtonBase.StopVirtualButton();
    }
    public override void Animating()
    {
        if (isAnimating)
        {
            Debug.Log("Animating");
        }
    }
    public override void ResetAnimation()
    {
        base.ResetAnimation();
    }
    public override void DestroyItself()
    {
        //base.DestroyItself();
        //getvirtualButtonBase.DestroyThis();
    }

    float CalculateDurationOnce()
    {
        duration = 0;
        Debug.Log(getvirtualButtonBase.name);
        for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
        {
            switch (getvirtualButtonBase.virtualButtonDatas.vbType)
            {
                case VirtualButtonType.Click:
                    if (i == 0)
                    {
                        duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }
                    else
                    {
                        if (GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]) > duration)
                            duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }

                    break;
                case VirtualButtonType.Drag:
                    if (i == 0)
                    {
                        duration = (getvirtualButtonOnAnimation.endValue - getvirtualButtonOnAnimation.startValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                    }
                    else
                    {
                        float durationTemp = (getvirtualButtonOnAnimation.endValue - getvirtualButtonOnAnimation.startValue) * 5 / getvirtualButtonOnAnimation.speedVirtualButton;
                        if (durationTemp > duration)
                        {
                            duration = durationTemp;
                        }
                    }
                    break;
                case VirtualButtonType.Press:
                    if (i == 0)
                    {
                        duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }
                    else
                    {
                        if (GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]) > duration)
                            duration = GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                    }
                    break;
            }
        }
        return duration; 
    }

    float GetDuration(AnimationPlayerBase animBase)
    {
        float durationTemp = 0;
        if (animBase.isAnimatedScript)
        {
            AnimationPlayerScript script = animBase as AnimationPlayerScript;
            durationTemp = script.GetTotalDuration();
            Debug.Log(script.totalDuration);
        }
        else
        {
            durationTemp = animBase.getAnimator.GetCurrentAnimatorStateInfo(0).length;
        }
        return durationTemp;
    }

    float GetDurationOnParameter(AnimationPlayerBase animBase)
    {
        float durationTemp = 0;
        for (int i = 0; i < animBase.getVBOutputData.parameterDatas.Count; i++)
        {
            float valueTemp = (animBase.getVBOutputData.parameterDatas[i].minimalValue + animBase.getVBOutputData.parameterDatas[i].maximalValue) * animBase.getVBDataModel.defaultValue;
            //float durationss = (animBase.getVBOutputData.parameterDatas[i].maximalValue - valueTemp) * animBase.getVBOutputData.speed
        }
        return durationTemp;
    }

    void CalculateDurationSequence()
    {
        if (getvirtualButtonBase.virtualButtonDatas.isSequentialClick && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Click)
        {
            if (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation ||
                getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                for (int i = 0; i < getvirtualButtonBase.animationPlayerContainerInteraction.Count; i++)
                {
                    duration += GetDuration(getvirtualButtonBase.animationPlayerContainerInteraction[i]);
                }
            }
            else if (getvirtualButtonBase.virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
            {

            }
        }
        else if ((getvirtualButtonBase.virtualButtonDatas.isSequentialDrag && getvirtualButtonBase.virtualButtonDatas.vbType == VirtualButtonType.Drag))
        {

        }
    }
}

