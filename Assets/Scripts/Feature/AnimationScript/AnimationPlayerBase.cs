using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public abstract class AnimationPlayerBase : MonoBehaviour
{
    VirtualButtonDataModel virtualButtonDataModel;
    VirtualButtonOutputData vbOutputData;

    Animator animator;
    float sliderPercentage;
    Vector3 startPosition;
    Vector3 startRotation;
    public Vector3 currentPosition;
    public Vector3 currentRotation;
    bool isInteraction;

    protected float maxSliderValue;
    protected float sliderValueTemp;

    public bool isBack;
    public bool setCurrentParam;

    public float sliderValue { get => sliderPercentage; }
    public Animator getAnimator { get => animator; }
    public VirtualButtonDataModel getVBDataModel { get => virtualButtonDataModel; }
    public VirtualButtonOutputData getVBOutputData { get => vbOutputData; }
    public bool isInteractionObject { get => isInteraction; }
    public Vector3 getStartPosition { get => startPosition; }
    public Vector3 getStartRotation { get => startRotation; }
    public bool isAnimatedScript;

    VirtualButtonOnAnimation virtualButtonOnAnimation;
    public VirtualButtonOnAnimation getvirtualButtonOnAnimation { get => virtualButtonOnAnimation; }
    bool isVirtualButton;
    VirtualButtonBase virtualbuttonBase;

    public VirtualButtonBase getvirtualButtonBase { get => virtualbuttonBase; }
    public abstract void Animating();

    public abstract void StartPlayAnimation(float maxValue, float speed);

    public void SetSliderPercentage(float value)
    {
        if (value < 0)
            value = 0;
        else if (value > 1)
            value = 1;
        sliderPercentage = value;
    }

    public void SetMinMaxValue(float min, float max)
    {
        if (vbOutputData != null)
        {
            vbOutputData.minimalValue = min;
            vbOutputData.maximalValue = max;
        }
    }

    protected void SetVBBaseData(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputData, bool interaction)
    {
        virtualButtonDataModel = vbDataModel;
        this.vbOutputData = vbOutputData;
        isInteraction = interaction;
    }


    public void SetupVBBase(VirtualButtonOutputData vbOutputData = null)
    {
        virtualButtonDataModel = new VirtualButtonDataModel();
        if (vbOutputData != null)
            this.vbOutputData = vbOutputData;
        else
            this.vbOutputData = new VirtualButtonOutputData();

        if (isAnimatedScript == false)
            animator = GetComponent<Animator>();
    }

    public void SetupVBinAnimation(VirtualButtonOnAnimation vbOnAnimation,VirtualButtonBase vbBase, bool isVirtualButton)
    {
        this.isVirtualButton = isVirtualButton;
        virtualButtonOnAnimation = vbOnAnimation;
        virtualbuttonBase = vbBase;
        this.vbOutputData = new VirtualButtonOutputData();
    }

    protected void StartAnimator(bool condition)
    {
        animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.enabled = condition;
        }
        else
            Debug.Log("its animation script");

        startPosition = gameObject.transform.localPosition;
        startRotation = gameObject.transform.localEulerAngles;
        currentPosition = startPosition;
        currentRotation = startRotation;
    }

    public virtual void DestroyItself()
    {
        sliderPercentage = 0;
        gameObject.SetActive(true);
        Destroy(this);
    }

    public virtual void ResetAnimation()
    {
        sliderPercentage = 0;
        transform.localPosition = startPosition;
        transform.localEulerAngles = startRotation;
        gameObject.SetActive(true);
    }

    public virtual void StopAnimation()
    {

    }
}
