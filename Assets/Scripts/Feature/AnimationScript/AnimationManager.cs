﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEngine.UI;
using VirtualTraining.UI;
namespace VirtualTraining.Feature
{
    public class AnimationManager : MonoBehaviour
    {
        public List<AnimationPlayerBase> animationPlayers = new List<AnimationPlayerBase>();

        public List<GameObject> gameObjectSequence = new List<GameObject>();
        [SerializeField] public static bool isSequenceAnimation;
        [Range(0, 1)]
        public float sliderValue;
        public bool isPlaying;
        bool isStartAnimation;

        float loopValue = 0;
        [SerializeField] GameObject canvasDescription;
        public Slider slider;
        [SerializeField] InteractionButton playPauseButton;
        [SerializeField] InteractionButton beginAnimationButton;
        [SerializeField] InteractionButton lastAnimationButton;
        [SerializeField] GameObject toolsDP;
        [Range(0, 1)]
        [SerializeField] float forwardPreviousValue = 0.1f;
        float differenceValue;
        float maxDuration;
        public List<int> sequentialValues = new List<int>();
        int animationCount = 0;
        bool isAnimate;

        // Start is called before the first frame update
        void Start()
        {
            EventManager.AddListener<AnimationPlayEvent>(StartAnimationListener);
            EventManager.AddListener<AnimationSequenceDone>(PlayAnimationSequence);
            playPauseButton.OnClickEvent += PlayAnimation;
            beginAnimationButton.OnClickEvent += BeginAnimation;
            lastAnimationButton.OnClickEvent += LastAnimation;
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<AnimationPlayEvent>(StartAnimationListener);
            EventManager.RemoveListener<AnimationSequenceDone>(PlayAnimationSequence);
            playPauseButton.OnClickEvent -= PlayAnimation;
            beginAnimationButton.OnClickEvent -= BeginAnimation;
            lastAnimationButton.OnClickEvent -= LastAnimation;
        }

        void StartAnimationListener(AnimationPlayEvent e)
        {
            if (e.animationData == null)
            {
                ResetAnimation();
                toolsDP.SetActive(false);
                animationCount = 0;
                isAnimate = false;
                return;
            }
            toolsDP.SetActive(true);
            
            if (e.animationData.isVirtualButton && e.animationData.virtualButton.virtualButtonElementType.GetData() != null)
            {
                e.animationData.virtualButton.virtualButtonElementType.GetData().defaultValue = e.animationData.virtualButton.startValue;
                VirtualButtonBase vbBase = GameObject.Find("Virtual Button").GetComponent<VirtualButtonManager>().InitVBAnimation(e.animationData.virtualButton.virtualButtonElementType.GetData());
                AnimationPlayerVB animBase = vbBase.gameObject.AddComponent<AnimationPlayerVB>();
                animBase.SetupVBinAnimation(e.animationData.virtualButton, vbBase, true);
                animationPlayers.Add(animBase);
                sequentialValues.Add(e.animationData.sequentialQueue);
                animationCount++;
            }
            else
            {
                if (e.animationData.isAnimatedScript && e.animationData.gameObjectInteraction != null)
                {
                    if (e.animationData.gameObjectInteraction.gameObject != null)
                    {
                        AnimationPlayerScript animScript = e.animationData.gameObjectInteraction.gameObject.AddComponent<AnimationPlayerScript>();
                        animScript.SetupAnimation(e.animationData);

                        animationPlayers.Add(animScript);
                        sequentialValues.Add(e.animationData.sequentialQueue);
                        animationCount++;
                    }
                    else
                        Debug.Log("game object interaction kosong");

                }
                else if (e.animationData.isAnimatedScript == false && e.animationData.animatorController != null)
                {
                    if (e.animationData.animatorController.gameObject != null)
                    {
                        AnimationPlayerManual animManual = e.animationData.animatorController.gameObject.AddComponent<AnimationPlayerManual>();
                        animManual.SetupAnimation(e.animationData.animationName);

                        animationPlayers.Add(animManual);
                        sequentialValues.Add(e.animationData.sequentialQueue);
                        animationCount++;
                    }
                    else
                        Debug.Log("game object animator kosong");
                }
            }

        }
        public void PlayAnimationSequence(AnimationSequenceDone e)
        {
            StartCoroutine(PlaySequenceDelay());
        }

        IEnumerator PlaySequenceDelay()
        {
            yield return new WaitUntil(() => animationCount == sequentialValues.Count);
            if (animationPlayers.Count > 0)
            {
                if (isSequenceAnimation)
                {
                    canvasDescription.SetActive(true);
                    StartCoroutine(PlaySequenceAnimation());
                }
                else
                {
                    for (int i = 0; i < animationPlayers.Count; i++)
                    {
                        animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
                    }
                }
                isPlaying = true;
                sliderValue = 0;
                differenceValue = slider.maxValue - slider.minValue;
                loopValue = 0;
            }
            isAnimate = true;
        }

        IEnumerator PlaySequenceAnimation()
        {
            isStartAnimation = true;
            CalculateMaxDuration();
            int maxSequenceValueTemp = 0;
            for (int i = 0; i < sequentialValues.Count; i++)
            {
                if (sequentialValues[i] > maxSequenceValueTemp)
                    maxSequenceValueTemp = sequentialValues[i];
            }
            //for (int i = 0; i < animationPlayers.Count; i++)
            //{
            //    animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
            //    if (animationPlayers[i] != null)
            //        yield return new WaitUntil(() => animationPlayers[i].sliderValue >= 0.96f);
            //}
            for (int i = 0; i < maxSequenceValueTemp+1; i++)
            {
                for (int j = 0; j < sequentialValues.Count; j++)
                {
                    if (sequentialValues[j] == i)
                    {
                        animationPlayers[j].StartPlayAnimation(1, animationPlayers[j].getVBOutputData.speed);
                    }
                }

                for (int k = 0; k < sequentialValues.Count; k++)
                {
                    if (sequentialValues[k] == i)
                    {
                        if (animationPlayers[k] != null)
                            yield return new WaitUntil(() => animationPlayers[k].sliderValue >= 0.96f);
                    }
                    Debug.Log(k +" "+i +" " +maxSequenceValueTemp);
                }
            }
        }

        void CalculateMaxDuration()
        {
            maxDuration = 0;
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                // get duration virtual button
                if (animationPlayers[i].getvirtualButtonBase != null)
                {
                    //return;
                    AnimationPlayerVB animVB = animationPlayers[i] as AnimationPlayerVB;
                    maxDuration += animVB.getDuration();
                    Debug.Log(animVB.getDuration());
                }
                else
                {
                    if (animationPlayers[i].isAnimatedScript)
                    {
                        AnimationPlayerScript script = animationPlayers[i] as AnimationPlayerScript;
                        maxDuration += script.totalDuration;
                        Debug.Log(maxDuration);
                    }
                    else
                    {
                        maxDuration += animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length;
                    }
                }
            }
        }

        private void Update()
        {
            if (isAnimate)
            {
                PlayAnimationUI();
                CheckSlider();
            }
        }

        void PlayAnimationUI()
        {
            if (animationPlayers.Count > 0)
            {
                if (isPlaying == true)
                {
                    if (isStartAnimation == false)
                    {
                        for (int i = 0; i < animationPlayers.Count; i++)
                        {
                            animationPlayers[i].StartPlayAnimation(1, animationPlayers[i].getVBOutputData.speed);
                        }
                        isStartAnimation = true;
                    }

                    if (sliderValue >= 1)
                    {
                        isPlaying = false;
                        loopValue = 0;
                    }
                    CheckAnimation();
                }
                else
                {
                    if (isSequenceAnimation)
                    {
                        if (animationPlayers.Count > 0)
                        {
                            float valueTemp = 0;
                            for (int i = 0; i < animationPlayers.Count; i++)
                            {
                                // check virtual button in animation
                                if (animationPlayers[i].getvirtualButtonBase != null)
                                {
                                    AnimationPlayerVB manual = animationPlayers[i] as AnimationPlayerVB;
                                    valueTemp += manual.getDuration() / maxDuration;
                                    continue;
                                }

                                if (animationPlayers[i].isAnimatedScript == false)
                                    valueTemp += animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length / maxDuration;
                                else
                                {
                                    AnimationPlayerScript script = animationPlayers[i] as AnimationPlayerScript;
                                    valueTemp += script.totalDuration / maxDuration;
                                }

                                if (sliderValue < valueTemp)
                                {
                                    if (animationPlayers[i].isAnimatedScript == false && !animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).loop)
                                    {
                                        float h = valueTemp - (animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length / maxDuration);
                                        animationPlayers[i].SetSliderPercentage((sliderValue - h) / (animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length / maxDuration));
                                        break;
                                    }
                                    else if (animationPlayers[i].isAnimatedScript == true)
                                    {
                                        AnimationPlayerScript script = animationPlayers[i] as AnimationPlayerScript;
                                        float h = valueTemp - (script.totalDuration / maxDuration);
                                        animationPlayers[i].SetSliderPercentage((sliderValue - h) / (script.totalDuration / maxDuration));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < animationPlayers.Count; i++)
                        {
                            animationPlayers[i].SetSliderPercentage(sliderValue);
                        }
                    }
                    if (isStartAnimation)
                    {
                        for (int i = 0; i < animationPlayers.Count; i++)
                        {
                            animationPlayers[i].StopAnimation();
                        }
                        isStartAnimation = false;
                    }
                }
            }
        }

        void CheckAnimation()
        {
            if (isSequenceAnimation)
            {
                float y = 0;
                float value = 0;
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    // check virtual button in animation
                    if (animationPlayers[i].getvirtualButtonBase != null)
                    {
                        AnimationPlayerVB manual = animationPlayers[i] as AnimationPlayerVB;
                        y += animationPlayers[i].sliderValue * (manual.getDuration() / maxDuration) ;
                        value += manual.getDuration();
                    }
                    else
                    {
                        if (animationPlayers[i].isAnimatedScript == false && animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).loop)
                        {
                            float length = animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length / maxDuration;

                            if (loopValue <= (animationPlayers[i].sliderValue * length) - (value / maxDuration))
                            {
                                loopValue = animationPlayers[i].sliderValue * length;
                            }
                            value += animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length;
                        }
                        else
                        {
                            if (animationPlayers[i].isAnimatedScript == false)
                            {
                                y += animationPlayers[i].sliderValue / animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length / maxDuration;
                                value += animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).length;
                            }
                            else
                            {
                                AnimationPlayerScript script = animationPlayers[i] as AnimationPlayerScript;
                                y += animationPlayers[i].sliderValue * (script.totalDuration / maxDuration);
                                value += script.totalDuration;
                            }
                        }
                    }
        
                }
                sliderValue = y + loopValue;
            }
            else
            {
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    if(i == 0)
                    {
                        if (animationPlayers[i].getAnimator != null && !animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).loop)
                            sliderValue = animationPlayers[i].sliderValue;
                        else if (animationPlayers[i].isAnimatedScript == true)
                            sliderValue = animationPlayers[i].sliderValue;
                    }
                    else
                    {
                        if (animationPlayers[i].sliderValue < sliderValue)
                        {
                            if (animationPlayers[i].getAnimator != null && !animationPlayers[i].getAnimator.GetCurrentAnimatorStateInfo(0).loop)
                                sliderValue = animationPlayers[i].sliderValue;
                            else if (animationPlayers[i].isAnimatedScript == true)
                                sliderValue = animationPlayers[i].sliderValue;
                        }
                    }
                }
            }
        }

        void ResetAnimation()
        {
            sequentialValues.Clear();
            StopAllCoroutines();
            for (int i = 0; i < animationPlayers.Count; i++)
            {
                animationPlayers[i].DestroyItself();
            }
            animationPlayers.Clear();
        }

        void CheckSlider()
        {
            if (animationPlayers.Count > 0)
            {
                if (isPlaying)
                    slider.value = sliderValue * differenceValue;
                else
                    sliderValue = slider.value / differenceValue;

                if (isPlaying == false && slider.value >= slider.maxValue - 0.05f)
                {
                    slider.value = slider.maxValue / differenceValue;
                    CheckPlayPauseIcon();
                }
            }
        }
        public void ClickHandle()
        {
            isPlaying = false;
        }

        public void PlayAnimation()
        {
            if (slider.value >= slider.maxValue)
            {
                slider.value = slider.minValue;
                sliderValue = 0;
                for (int i = 0; i < animationPlayers.Count; i++)
                {
                    animationPlayers[i].SetSliderPercentage(0);
                }
                StartCoroutine(delayStart());

                if (isSequenceAnimation)
                    isStartAnimation = true;
            }
            isPlaying = !isPlaying;

            CheckPlayPauseIcon();
        }

        IEnumerator delayStart()
        {
            yield return new WaitForSeconds(0.1f);
            if (isSequenceAnimation)
            {
                isStartAnimation = true;
                StartCoroutine(PlaySequenceAnimation());
            }

            isPlaying = true;
        }

        void CheckPlayPauseIcon()
        {
            if (isPlaying)
            {
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().pause);
            }
            else
            {
                playPauseButton.SetIcon(DatabaseManager.GetUITexture().play);
            }
        }

        public void ForwardAnimation()
        {
            ClickHandle();
            slider.value += (slider.maxValue - slider.minValue) * forwardPreviousValue;
            if (slider.value > slider.maxValue)
                slider.value = slider.maxValue;
        }

        public void PreviousAnimation()
        {
            ClickHandle();
            slider.value -= (slider.maxValue - slider.minValue) * forwardPreviousValue;
            if (slider.value < slider.minValue)
                slider.value = slider.minValue;
        }

        public void BeginAnimation()
        {
            ClickHandle();
            slider.value = slider.minValue;
        }

        public void LastAnimation()
        {
            ClickHandle();
            slider.value = slider.maxValue;
        }
    }
}
