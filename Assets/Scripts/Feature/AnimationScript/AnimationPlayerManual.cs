using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

public class AnimationPlayerManual : AnimationPlayerBase
{
    string animName;
    bool isAnimatorStatePlay;
    bool isAnimatorParameterPlay;
    float sliderPercentageTemp;

    float currentSliderX;
    float currentSliderY;
    float startParameterX;
    float startParameterY;

    float speedParameter;
    bool isStart;
    public float getSliderPercentageTemp { get => sliderPercentageTemp; }
    List<float> parameterStarts = new List<float>();

    public void Setup(VirtualButtonDataModel vbDataModel, VirtualButtonOutputData vbOutputData, bool interaction)
    {
        SetVBBaseData(vbDataModel, vbOutputData, interaction);
        SetAnimName(vbOutputData.animationState);
    }
    public void SetupOnStart(VirtualButtonDataModel vbDataModel, VBStartAnimationData vbStartData, bool interaction, bool isStart)
    {
        VirtualButtonOutputData vbOutputData = new VirtualButtonOutputData();
        vbOutputData.animationState = vbStartData.animationState;
        SetVBBaseData(vbDataModel, vbOutputData, interaction);
        SetAnimName(vbOutputData.animationState);
        this.isStart = isStart;
    }

    public void SetAnimName(string animState)
    {
        if (!string.IsNullOrEmpty(animState))
            animName = animState;
    }

    public void SetupAnimation(string animState)
    {
        SetupVBBase();
        animName = animState;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartAnimator(true);
        SetSliderPercentage(getVBDataModel.defaultValue);

        if (getAnimator != null)
        {
            if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
            {
                if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
                {
                    SetupParameterAll();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                {
                    SetupParameterOnce();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
                {
                    if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                        SetupParameterOnce();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                        SetupParameterAll();
                }
            }
            else //bukan sequential
            {
                SetupParameterOnce();
            }


            if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
            {
                if (getVBOutputData.feedBackManualType == FeedbackManualType.State && !string.IsNullOrEmpty(animName))
                    getAnimator.SetFloat("speed", 0);
            }
            else //bukan sequential
            {
                if (!string.IsNullOrEmpty(animName))
                    getAnimator.SetFloat("speed", 0);
            }

            if (isStart)
            {
                StartPlayAnimation(1, getVBOutputData.speed);
            }
                
        }
    }

    void SetupParameterOnce()
    {
        float deltaValue = Mathf.Lerp(getVBOutputData.minimalValue, getVBOutputData.maximalValue, getVBDataModel.defaultValue);
        if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
        {
            startParameterX = getAnimator.GetFloat(getVBOutputData.animationParameter);
            getAnimator.SetFloat(getVBOutputData.animationParameter, deltaValue);
        }

        if (!string.IsNullOrEmpty(getVBOutputData.animationParameterY))
        {
            startParameterY = getAnimator.GetFloat(getVBOutputData.animationParameterY);
            getAnimator.SetFloat(getVBOutputData.animationParameterY, deltaValue);
        }
    }

    void SetupParameterAll()
    {
        for (int i = 0; i < getVBOutputData.parameterDatas.Count; i++)
        {
            float deltaValue = Mathf.Lerp(getVBOutputData.parameterDatas[i].minimalValue, getVBOutputData.parameterDatas[i].maximalValue, getVBDataModel.defaultValue);
            if (!string.IsNullOrEmpty(getVBOutputData.parameterDatas[i].parameter))
            {
                parameterStarts.Add(getAnimator.GetFloat(getVBOutputData.parameterDatas[i].parameter));
                getAnimator.SetFloat(getVBOutputData.parameterDatas[i].parameter, deltaValue);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isAnimatorParameterPlay)
            AnimatingParameter();
        if (isAnimatorStatePlay)
        {
            if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
            {
                if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    Animating();
                }
            }
            else
            {
                Animating();
            }
        }

        if (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.direction != DragDirection.Both && !getVBDataModel.isSequentialDrag)
        {
            if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
            {
                float x = Mathf.Lerp(getVBOutputData.minimalValue, getVBOutputData.maximalValue, sliderValue);
                getAnimator.SetFloat(getVBOutputData.animationParameter, x);
            }
        }
    }

    public override void Animating()
    {
        if (!string.IsNullOrEmpty(animName))
        {
            if (getAnimator.GetCurrentAnimatorStateInfo(0).IsName(animName))
                SetSliderPercentage(getAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);

            if (getAnimator.GetCurrentAnimatorStateInfo(0).loop == true)
            {
                if (sliderValueTemp > maxSliderValue)
                {
                    if (sliderValue < maxSliderValue)
                    {
                        SetSliderPercentage(Mathf.Round(sliderValue * 10f) / 10f);
                        isAnimatorStatePlay = false;
                        SetSliderPercentage(maxSliderValue);
                    }
                }
                else
                {
                    if (sliderValue > maxSliderValue)
                    {
                        SetSliderPercentage(0);
                        getAnimator.Play(animName, -1, 0f);
                    }
                }
            }
            else
            {
                SetSliderPercentage(Mathf.Round(sliderValue * 100f) / 100f);

                if (sliderValueTemp > maxSliderValue)
                {
                    if (sliderValue < maxSliderValue)
                    {
                        getAnimator.SetFloat("speed", 0);
                        isAnimatorStatePlay = false;
                        SetSliderPercentage(maxSliderValue);
                    }
                }
                else
                {
                    if (sliderValue > maxSliderValue)
                    {
                        getAnimator.SetFloat("speed", 0);
                        isAnimatorStatePlay = false;
                        SetSliderPercentage(maxSliderValue);
                    }
                }
            }
        }
    }

    public override void StartPlayAnimation(float maxValue,float speed)
    {
        if (getAnimator == null)
            return;

        maxSliderValue = maxValue;
        sliderValueTemp = sliderValue;
        if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
        {
            if (getVBOutputData.feedBackManualType == FeedbackManualType.State && !string.IsNullOrEmpty(animName))
            {
                StartAnimationManual(speed);
            }
        }
        else //bukan sequential
        {
            if (!string.IsNullOrEmpty(animName))
                StartAnimationManual(speed);
        }

        if ((!string.IsNullOrEmpty(getVBOutputData.animationParameter) || getVBOutputData.parameterDatas.Count >0))
        {
            StartParameter(speed);
        }
    }

    void StartAnimationManual(float speed)
    {
        if (sliderValue <= maxSliderValue)
            getAnimator.SetFloat("speed", speed);
        else if (sliderValue >= maxSliderValue)
            getAnimator.SetFloat("speed", -speed);
        else
            getAnimator.SetFloat("speed", 0);

        if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
        {
            if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
            {
                getAnimator.Play(animName, -1, sliderValue);
            }
        }
        else
            getAnimator.Play(animName, -1, sliderValue);

        if (isStart)
            getAnimator.Play(animName, -1, sliderValue);
        isAnimatorStatePlay = true;
    }

    void StartParameter(float speed)
    {
        if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
        {
            if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
            {
                SetSliderPercetageTempAll();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter)
            {
                SetSliderPercetageTempOnce();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    SetSliderPercetageTempOnce();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
            {
                if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    SetSliderPercetageTempAll();
            }
        }
        else //bukan sequential
        {
            SetSliderPercetageTempOnce();
        }

        if (sliderPercentageTemp <= maxSliderValue)
            speedParameter = speed;
        else if (sliderPercentageTemp >= maxSliderValue)
            speedParameter = -speed;
        else
            speedParameter = 0;
    }

    void SetSliderPercetageTempOnce()
    {
        sliderPercentageTemp = (getAnimator.GetFloat(getVBOutputData.animationParameter) - getVBOutputData.minimalValue) / (getVBOutputData.maximalValue - getVBOutputData.minimalValue);
        isAnimatorParameterPlay = true;
    }

    void SetSliderPercetageTempAll()
    {
        float parameterPercentage;
        for (int i = 0; i <getVBOutputData.parameterDatas.Count; i++)
        {
            parameterPercentage = (getAnimator.GetFloat(getVBOutputData.parameterDatas[i].parameter) - getVBOutputData.parameterDatas[i].minimalValue) / (getVBOutputData.parameterDatas[i].maximalValue - getVBOutputData.parameterDatas[i].minimalValue);
            if (parameterPercentage < sliderPercentageTemp)
            {
                sliderPercentageTemp = parameterPercentage;
            }
        }
        isAnimatorParameterPlay = true;
    }

    void AnimatingParameter()
    {
        if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
        {
            if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
            {
                PlayAllParameterList();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter)
            {
                PlayOnceParameter();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    PlayOnceParameter();
            }
            else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
            {
                if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    PlayAllParameterList();
            }
        }
        else //bukan sequential
        {
            PlayOnceParameter();
        }
    }

    void PlayAllParameterList()
    {
        if (speedParameter > 0)
        {
            if (sliderPercentageTemp >= maxSliderValue)
            {
                isAnimatorParameterPlay = false;
                speedParameter = 0;
                sliderPercentageTemp = maxSliderValue;
            }
        }
        else if (speedParameter < 0)
        {
            if (sliderPercentageTemp <= maxSliderValue)
            {
                isAnimatorParameterPlay = false;
                speedParameter = 0;
                sliderPercentageTemp = maxSliderValue;
            }
        }
        sliderPercentageTemp += Time.deltaTime * speedParameter;

        if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
        {
            if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
            {
                SetSliderPercentage(sliderPercentageTemp);
            }
        }
        else
        {
            if (string.IsNullOrEmpty(animName))
                SetSliderPercentage(sliderPercentageTemp);
            else
            {
                if (sliderPercentageTemp < sliderValue)
                    SetSliderPercentage(sliderPercentageTemp);
            }

        }

        for (int i = 0; i < getVBOutputData.parameterDatas.Count; i++)
        {
            float x = Mathf.Lerp(getVBOutputData.parameterDatas[i].minimalValue, getVBOutputData.parameterDatas[i].maximalValue, sliderPercentageTemp);
            getAnimator.SetFloat(getVBOutputData.parameterDatas[i].parameter, x);
        }
        
    }
    void PlayOnceParameter()
    {
        if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
        {
            sliderPercentageTemp += Time.deltaTime * speedParameter;

            if (sliderPercentageTemp < sliderValue)
                SetSliderPercentage(sliderPercentageTemp);

            float x = Mathf.Lerp(getVBOutputData.minimalValue, getVBOutputData.maximalValue, sliderPercentageTemp);
            getAnimator.SetFloat(getVBOutputData.animationParameter, x);

            if (speedParameter > 0)
            {
                if (sliderPercentageTemp > maxSliderValue)
                {
                    isAnimatorParameterPlay = false;
                    speedParameter = 0;
                    sliderPercentageTemp = maxSliderValue;
                }
            }
            else if (speedParameter < 0)
            {
                if (sliderPercentageTemp < maxSliderValue)
                {
                    isAnimatorParameterPlay = false;
                    speedParameter = 0;
                    sliderPercentageTemp = maxSliderValue;
                }
            }
        }
    }

    public void StartPlayAnimationSequentialState(float maxValue, float startSlider)
    {
        maxSliderValue = maxValue;
        sliderValueTemp = startSlider;

        if (!string.IsNullOrEmpty(animName))
        {
            if (startSlider <= maxSliderValue)
                getAnimator.SetFloat("speed", getVBOutputData.speed);
            else if (startSlider >= maxSliderValue)
                getAnimator.SetFloat("speed", -getVBOutputData.speed);
            else
                getAnimator.SetFloat("speed", 0);


            getAnimator.Play(animName, -1, startSlider);
        }
        SetSliderPercentage(startSlider);
        isAnimatorStatePlay = true;
    }

    public void SetBothDrag(float x, float y)
    {
        if (getAnimator != null)
        {
            //getAnimator.Play(animName);
            if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
            {
                if (setCurrentParam == false)
                    currentSliderX = getAnimator.GetFloat(getVBOutputData.animationParameter);

                x = currentSliderX + x;
                if (x < getVBOutputData.maximalValue && x > getVBOutputData.minimalValue)
                    getAnimator.SetFloat(getVBOutputData.animationParameter, x);
            }
            if (!string.IsNullOrEmpty(getVBOutputData.animationParameterY))
            {
                if (setCurrentParam == false)
                    currentSliderY = getAnimator.GetFloat(getVBOutputData.animationParameterY);

                y = currentSliderY + y;
                if (y < getVBOutputData.maximalValue && y > getVBOutputData.minimalValue)
                    getAnimator.SetFloat(getVBOutputData.animationParameterY, y);
            }
            setCurrentParam = true;
        }
    }

    public void SetElasticBothDrag(float speed)
    {
        if (getAnimator != null)
        {
            getAnimator.Play(animName);
            if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
            {
                float XDefaultValue = getVBOutputData.minimalValue + getVBDataModel.defaultValue * (getVBOutputData.maximalValue - getVBOutputData.minimalValue);
                float x = Mathf.Lerp(getAnimator.GetFloat(getVBOutputData.animationParameter), XDefaultValue, speed * Time.deltaTime);
                getAnimator.SetFloat(getVBOutputData.animationParameter, x);
            }
            if (!string.IsNullOrEmpty(getVBOutputData.animationParameterY))
            {
                float YDefaultValue = getVBOutputData.minimalValue + getVBDataModel.defaultValue * (getVBOutputData.maximalValue - getVBOutputData.minimalValue);
                float y = Mathf.Lerp(getAnimator.GetFloat(getVBOutputData.animationParameterY), YDefaultValue, speed * Time.deltaTime);
                getAnimator.SetFloat(getVBOutputData.animationParameterY, y);
            }
        }
    }

    public override void StopAnimation()
    {
        base.StopAnimation();
        if (getAnimator != null && !string.IsNullOrEmpty(animName))
            getAnimator.SetFloat("speed", 0);
    }

    public override void ResetAnimation()
    {
        base.ResetAnimation();
        if (getAnimator != null)
            getAnimator.Play(animName, -1, sliderValue);
    }

    public override void DestroyItself()
    {
        base.DestroyItself();
        if (getAnimator != null)
        {
            getAnimator.Play(animName, -1, sliderValue);
            if ((getVBDataModel.vbType == VirtualButtonType.Click && getVBDataModel.isSequentialClick) || (getVBDataModel.vbType == VirtualButtonType.Drag && getVBDataModel.isSequentialDrag))
            {
                if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
                {
                    ResetParameterAll();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayParameter)
                {
                    ResetParameterOnce();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.PlayState)
                {
                    if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                        ResetParameterOnce();
                }
                else if (getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation || getVBDataModel.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    if (getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                        ResetParameterAll();
                }
            }
            else //bukan sequential
            {
                ResetParameterOnce();
            }

            getAnimator.Rebind();
            getAnimator.Update(0);
            getAnimator.enabled = false;
        }
    }

    void ResetParameterOnce()
    {
        if (!string.IsNullOrEmpty(getVBOutputData.animationParameter))
            getAnimator.SetFloat(getVBOutputData.animationParameter, startParameterX);
        if (!string.IsNullOrEmpty(getVBOutputData.animationParameterY))
            getAnimator.SetFloat(getVBOutputData.animationParameterY, startParameterY);
    }

    void ResetParameterAll()
    {
        for (int i = 0; i < getVBOutputData.parameterDatas.Count; i++)
        {
            if (!string.IsNullOrEmpty(getVBOutputData.parameterDatas[i].parameter))
            {
                getAnimator.SetFloat(getVBOutputData.animationParameter, parameterStarts[i]);
            }
        }
    }
}
