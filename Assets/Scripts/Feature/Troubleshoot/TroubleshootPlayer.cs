﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class TroubleshootPlayer : MonoBehaviour
    {
        private TroubleshootDataModel troubleshootData;

        void Start()
        {
            EventManager.AddListener<TroubleshootPlayEvent>(Play);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<TroubleshootPlayEvent>(Play);
        }
        private void Play(TroubleshootPlayEvent e)
        {
            if (e.troubleshootData == null)
            {
                ResetTroubleshoot();
                return;
            }
            ResetMateriAction();
            troubleshootData = e.troubleshootData;
            PlayTroubleshoot();
        }


        void ResetTroubleshoot()
        {
            VirtualTrainingCamera.ResetCameraPosition(ResetMateriAction);
        }

        void PlayTroubleshoot()
        {
            VirtualTrainingCamera.MoveCamera(troubleshootData.figure.cameraDestination, OnPlayMateriArriveAction);
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(OverrideTroubleshoot(troubleshootData).figure);
        }
        TroubleshootDataModel OverrideTroubleshoot(TroubleshootDataModel troubleshootDataModel)
        {
            TroubleshootDataModel overrideMonitorData = troubleshootDataModel.figure.cameraOverrideTroubleshoot.GetData();
            TroubleshootDataModel overridePartObjectData = troubleshootDataModel.figure.partObjectOverrideTroubleshoot.GetData();
            if (overrideMonitorData != null)
            {
                troubleshootDataModel.figure.monitorCameras = overrideMonitorData.figure.monitorCameras;
                troubleshootDataModel.figure.cameraDestination = overrideMonitorData.figure.cameraDestination;
            }

            if (overridePartObjectData != null)
            {
                troubleshootDataModel.figure.partObjects = overrideMonitorData.figure.partObjects;
            }

            return troubleshootDataModel;
        }

        private void PlayFigure(Figure figure)
        {
            VirtualTrainingCamera.MoveCamera(figure.cameraDestination, OnArriveFigureCamera);
        }
        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(troubleshootData.figure.cutaway));
            EventManager.TriggerEvent(new HelperEvent(troubleshootData.figure.callouts));
            EventManager.TriggerEvent(new HelperEvent(troubleshootData.figure.helpers));
            EventManager.TriggerEvent(new PartObjectEvent(troubleshootData.figure.partObjects));
            EventManager.TriggerEvent(new MonitorCameraEvent(troubleshootData.figure.monitorCameras));
            PlayVirtualButton();
            PlayAnimation();
        }

        private void ResetMateriAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new PartObjectEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new MaintenanceUIEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new AnimationPlayEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
        }

        void PlayVirtualButton()
        {
            for (int i = 0; i < troubleshootData.figure.virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(troubleshootData.figure.virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), troubleshootData.figure.virtualButtonElement[i].overrideVirtualButton));
            }
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void PlayAnimation()
        {
            AnimationManager.isSequenceAnimation = troubleshootData.figure.isSequenceAnimation;

            for (int i = 0; i < troubleshootData.figure.animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(troubleshootData.figure.animationFigureDatas[i]));
            }

            EventManager.TriggerEvent(new AnimationSequenceDone());
        }
    }
}
