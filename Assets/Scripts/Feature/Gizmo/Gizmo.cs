using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class Gizmo : MonoBehaviour
    {
        enum Mode
        {
            Translate,
            Rotate
        }

        enum GizmoAxis
        {
            X,
            Y,
            Z,
            XY,
            XZ,
            YZ,
            XYRotate,
            XZRotate,
            YZRotate,
            none
        };

        [SerializeField] float size;

        [SerializeField] LayerMask layer;

        [SerializeField] GameObject xAxis;
        [SerializeField] GameObject yAxis;
        [SerializeField] GameObject zAxis;

        [SerializeField] GameObject xyPlane;
        [SerializeField] GameObject xzPlane;
        [SerializeField] GameObject yzPlane;

        [SerializeField] GameObject xyRotate;
        [SerializeField] GameObject xzRotate;
        [SerializeField] GameObject yzRotate;

        [SerializeField] Collider sphere;

        Mode currentMode;
        GizmoAxis selectedAxis;
        Vector3 rotationAxis = Vector3.zero;
        Vector3 hitvector, mousePos, lookCamera, startDrag, startPos, startDragRot, lookHitPoint;
        float sphereRadius, rayDistance, distance;
        bool dragging, rotating;
        GameObject rotationParent;
        Ray ray;
        RaycastHit hit;
        Plane dragplane;
        Transform gizmoParent;

        private void Start()
        {
            sphereRadius = sphere.bounds.extents.x;
            rotationParent = new GameObject("gizmo rotation parent");
            rotationParent.transform.SetParent(gizmoParent);
            currentMode = Mode.Translate;
            ChangeMode();
        }

        private void OnEnable()
        {
            VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
            VirtualTrainingInputSystem.OnStartRightClick += OnStartRightClick;
            VirtualTrainingInputSystem.OnLeftClickDown += OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick += OnLeftClickUp;
        }

        private void OnDisable()
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
            VirtualTrainingInputSystem.OnStartRightClick -= OnStartRightClick;
            VirtualTrainingInputSystem.OnLeftClickDown += OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick += OnLeftClickUp;
        }

        private void Update()
        {
            float distance = Vector3.Distance(VirtualTrainingCamera.CameraTransform.position, transform.position);
            transform.localScale = new Vector3(size, size, size) * distance;
        }

        public void SetParent(Transform parent)
        {
            gizmoParent = parent;
            transform.SetParent(gizmoParent);
        }

        public void Enable(Vector3 pos, Quaternion rot)
        {
            transform.position = pos;
            transform.rotation = rot;
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        private GameObject Raycast(ref RaycastHit hitInfo)
        {
            if (VirtualTrainingCamera.Raycast(ref hitInfo, layer))
            {
                return hitInfo.transform.gameObject;
            }

            return null;
        }

        private void OnLeftClickUp(Vector2 pointerPosition, GameObject raycastedUI)
        {
            SetFalse();
        }

        private void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;

            GameObject result = Raycast(ref hit);

            if (result == null)
                return;

            if (result == xAxis)
            {
                selectedAxis = GizmoAxis.X;
                dragplane.SetNormalAndPosition(transform.up, transform.position);
            }
            else if (result == yAxis)
            {
                selectedAxis = GizmoAxis.Y;
                dragplane.SetNormalAndPosition(transform.forward, transform.position);
            }
            else if (result == zAxis)
            {
                selectedAxis = GizmoAxis.Z;
                dragplane.SetNormalAndPosition(transform.up, transform.position);
            }
            else if (result == xyPlane)
            {
                selectedAxis = GizmoAxis.XY;
                dragplane.SetNormalAndPosition(transform.forward, transform.position);
            }
            else if (result == xzPlane)
            {
                selectedAxis = GizmoAxis.XZ;
                dragplane.SetNormalAndPosition(transform.up, transform.position);
            }
            else if (result == yzPlane)
            {
                selectedAxis = GizmoAxis.YZ;
                dragplane.SetNormalAndPosition(transform.right, transform.position);
            }
            else if (result == xyRotate)
            {
                selectedAxis = GizmoAxis.XYRotate;
                rotationAxis = -transform.forward;
                lookHitPoint = hit.point - transform.position - Vector3.Project(hit.point - transform.position, transform.forward);
                //Debug.DrawLine(transform.position, transform.position + lookHitPoint, Color.cyan, 10.0f);
                dragplane.SetNormalAndPosition(lookHitPoint, hit.point);
                //DrawPlane(lookHitPoint, transform.position + lookHitPoint, 10.0f);
                rotating = true;
            }
            else if (result == xzRotate)
            {
                selectedAxis = GizmoAxis.XZRotate;
                rotationAxis = -transform.up;
                lookHitPoint = hit.point - transform.position - Vector3.Project(hit.point - transform.position, transform.up);
                //Debug.DrawLine(transform.position, transform.position + lookHitPoint, Color.cyan, 10.0f);
                dragplane.SetNormalAndPosition(lookHitPoint, hit.point);
                //DrawPlane(lookHitPoint, transform.position + lookHitPoint, 10.0f);
                rotating = true;
            }
            else if (result == yzRotate)
            {
                selectedAxis = GizmoAxis.YZRotate;
                rotationAxis = -transform.right;
                lookHitPoint = hit.point - transform.position - Vector3.Project(hit.point - transform.position, transform.right);
                //Debug.DrawLine(transform.position, transform.position + lookHitPoint, Color.cyan, 10.0f);
                dragplane.SetNormalAndPosition(lookHitPoint, hit.point);
                //DrawPlane(lookHitPoint, transform.position + lookHitPoint, 10.0f);
                rotating = true;
            }
            else if (result == sphere.gameObject)
            {
                selectedAxis = GizmoAxis.none;
                lookCamera = VirtualTrainingCamera.CameraTransform.position - transform.position;
                startDragRot = transform.position + lookCamera.normalized * sphereRadius;
                dragplane.SetNormalAndPosition(lookCamera.normalized, startDragRot);
                //DrawPlane(lookCamera.normalized, startDragRot, 10.0f);
                rotating = true;
            }
            else
            {
                SetFalse();
                return;
            }

            if (rotating)
            {
                if (dragplane.Raycast(ray, out rayDistance))
                {
                    startDragRot = ray.GetPoint(rayDistance);
                }

                rotationParent.transform.position = transform.position;
            }

            hitvector = hit.point - transform.position;

            distance = hit.distance;
            Vector2 inputPos = VirtualTrainingInputSystem.PointerPosition;
            startDrag = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(new Vector3(inputPos.x, inputPos.y, distance));
            startPos = transform.position;
            dragging = true;

            if (dragging || rotating)
            {
                VirtualTrainingCamera.IsMovementEnabled = false;
            }

            StartCoroutine(SetParent());
        }

        IEnumerator SetParent()
        {
            yield return null;
            transform.SetParent(rotationParent.transform);
        }

        private void OnLeftClickDown(Vector2 axisValue)
        {
            if (dragging || rotating)
            {
                ray = VirtualTrainingCamera.CurrentCamera.ScreenPointToRay(Input.mousePosition);

                if (dragplane.Raycast(ray, out rayDistance))
                {
                    mousePos = ray.GetPoint(rayDistance);
                }

                Vector2 inputPos = VirtualTrainingInputSystem.PointerPosition;
                Vector3 onDrag = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(new Vector3(inputPos.x, inputPos.y, distance));
                Vector3 translation = onDrag - startDrag;
                Vector3 projectedTranslation = Vector3.zero;

                if (dragging)
                {
                    switch (selectedAxis)
                    {
                        case GizmoAxis.X:
                            {
                                projectedTranslation = Vector3.Project(translation, transform.right);
                                transform.position = startPos + projectedTranslation.normalized * translation.magnitude;
                                break;
                            }
                        case GizmoAxis.Y:
                            {
                                projectedTranslation = Vector3.Project(translation, transform.up);
                                transform.position = startPos + projectedTranslation.normalized * translation.magnitude;
                                break;
                            }
                        case GizmoAxis.Z:
                            {
                                projectedTranslation = Vector3.Project(translation, transform.forward);
                                transform.position = startPos + projectedTranslation.normalized * translation.magnitude;
                                break;
                            }
                        case GizmoAxis.XY:
                        case GizmoAxis.XZ:
                        case GizmoAxis.YZ:
                            {
                                transform.position = mousePos - hitvector;
                                break;
                            }

                    }
                }

                if (rotating)
                {
                    translation = mousePos - startDragRot;
                    //Debug.DrawLine(startDragRot, mousePos, Color.white, 6.0f);

                    Vector3 rotationAxis2 = Vector3.zero;

                    switch (selectedAxis)
                    {
                        case GizmoAxis.XYRotate:
                            {
                                projectedTranslation = translation - Vector3.Project(translation, rotationAxis);
                                //Debug.DrawLine(startDragRot, startDragRot + projectedTranslation, Color.yellow, 1.0f);
                                rotationAxis2 = Vector3.Cross(projectedTranslation, lookHitPoint);
                                //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis2.normalized, Color.magenta, 1.0f);
                                //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis.normalized, Color.cyan, 1.0f);
                                break;
                            }
                        case GizmoAxis.XZRotate:
                            {
                                projectedTranslation = translation - Vector3.Project(translation, rotationAxis);
                                //Debug.DrawLine(startDragRot, startDragRot + projectedTranslation, Color.yellow, 1.0f);
                                rotationAxis2 = Vector3.Cross(projectedTranslation, lookHitPoint);
                                //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis2.normalized, Color.magenta, 1.0f);
                                //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis.normalized, Color.magenta, 1.0f);
                                break;
                            }
                        case GizmoAxis.YZRotate:
                            {
                                projectedTranslation = translation - Vector3.Project(translation, rotationAxis);
                                //Debug.DrawLine(startDragRot, startDragRot + projectedTranslation, Color.yellow, 1.0f);
                                rotationAxis2 = Vector3.Cross(projectedTranslation, lookHitPoint);

                                //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis.normalized, Color.gray, 1.0f);
                                break;
                            }
                        case GizmoAxis.none:
                            {
                                rotationAxis2 = rotationAxis;
                                projectedTranslation = translation;
                                rotationAxis = Vector3.Cross(translation, lookCamera);
                                break;
                            }
                    }
                    float angle;
                    Quaternion delta;

                    //Debug.DrawLine(transform.position, transform.position + 5 * rotationAxis.normalized, Color.red, 1.0f);
                    angle = -Mathf.Rad2Deg * projectedTranslation.magnitude / sphereRadius;
                    delta = Quaternion.AngleAxis(angle, rotationAxis2);

                    rotationParent.transform.rotation = delta;
                }
            }
        }

        private void SetFalse()
        {
            transform.SetParent(gizmoParent);

            rotating = false;
            dragging = false;

            VirtualTrainingCamera.IsMovementEnabled = true;
        }

        private void OnStartRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            bool hit = false;
            RaycastHit hitInfo = new RaycastHit();
            GameObject result = Raycast(ref hitInfo);

            if (result == null)
                return;

            if (result == xAxis)
                hit = true;
            else if (result == yAxis)
                hit = true;
            else if (result == zAxis)
                hit = true;
            else if (result == xyPlane)
                hit = true;
            else if (result == xzPlane)
                hit = true;
            else if (result == yzPlane)
                hit = true;
            else if (result == xyRotate)
                hit = true;
            else if (result == xzRotate)
                hit = true;
            else if (result == yzRotate)
                hit = true;
            else if (result == sphere.gameObject)
                hit = true;

            if (hit)
            {
                currentMode = currentMode == Mode.Rotate ? Mode.Translate : Mode.Rotate;
                ChangeMode();
            }
        }

        private void ChangeMode()
        {
            if (currentMode == Mode.Translate)
            {
                sphere.gameObject.SetActive(false);

                xyRotate.SetActive(false);
                xzRotate.SetActive(false);
                yzRotate.SetActive(false);

                xAxis.SetActive(true);
                yAxis.SetActive(true);
                zAxis.SetActive(true);

                xyPlane.SetActive(true);
                xzPlane.SetActive(true);
                yzPlane.SetActive(true);
            }
            else
            {
                sphere.gameObject.SetActive(true);

                xyRotate.SetActive(true);
                xzRotate.SetActive(true);
                yzRotate.SetActive(true);

                xAxis.SetActive(false);
                yAxis.SetActive(false);
                zAxis.SetActive(false);

                xyPlane.SetActive(false);
                xzPlane.SetActive(false);
                yzPlane.SetActive(false);
            }
        }
    }
}
