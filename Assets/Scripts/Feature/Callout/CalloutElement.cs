﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    [RequireComponent(typeof(CalloutText))]
    public class CalloutElement : MonoBehaviour
    {
        Callout _theParent;

        [SerializeField]
        Transform _textBox;
        float _minBoxScale = 7e-05f, _maxBoxScale = 1f;

        [SerializeField]
        Transform _helperParent;

        [SerializeField]
        List<GameObject> _points = new List<GameObject>();
        [SerializeField]
        List<LineRenderer> _lines = new List<LineRenderer>();

        public bool inversePivot = false;
        public bool ignoreX, ignoreY, ignoreZ;
        [SerializeField] float _ignoreXOffset, _ignoreYOffset, _ignoreZOffset;
        [SerializeField] bool _ignoreAll;
        [SerializeField] bool _setupIgnoreAll;

        public float lineLengthAdd;

        [HideInInspector]
        [System.NonSerialized]
        public float cameraDistance = 0.005f;

        Vector3 _direction;
        [SerializeField]
        [HideInInspector]
        Vector3 _ignoreAllOffset = new Vector3();

        Transform _cameraTransform;

        private void Start()
        {
            if (gameObject.layer == VirtualTrainingCamera.MonitorCameraLayer)
                _cameraTransform = VirtualTrainingCamera.MonitorCameraTransform;
            else
                _cameraTransform = VirtualTrainingCamera.CameraTransform;

#if UNITY_EDITOR
#else
_setupIgnoreAll = false;
#endif
        }

        private void OnEnable()
        {
            if (_theParent == null)
            {
                _theParent = GetComponentInParent<Callout>();
            }

            if (_helperParent == null)
            {
                GameObject helper = new GameObject("TextAndLine");
                _helperParent = helper.transform;
                _helperParent.SetParent(transform);
            }

            if (_textBox == null)
            {
                Object obj = Instantiate(Resources.Load("Callout/CalloutTextBox"));
                _textBox = (obj as GameObject).transform;
                _textBox.name = "TextBox";
                _textBox.SetParent(_helperParent);
            }

            //get last position for ignore all position
            if (_ignoreAll)
            {
                _ignoreAllOffset = _textBox.position - GetMeanVector(_points);
            }
        }

        private void OnDisable()
        {
            //get last position for ignore all position
            if (_ignoreAll)
            {
                _ignoreAllOffset = _textBox.position - GetMeanVector(_points);
            }
        }

        public void Setup(Callout parent)
        {
            _theParent = parent;
        }

        public void CreateCalloutPoint()
        {
            GameObject point = new GameObject("Point " + (_points.Count + 1).ToString(), typeof(CalloutPoint));
            point.transform.SetParent(transform);
            point.transform.position = new Vector3(0, 0, 0);
            _points.Add(point);

            Object obj = Instantiate(Resources.Load("Callout/CalloutLine"));
            GameObject line = obj as GameObject;
            line.name = "line " + _points.Count;
            line.transform.SetParent(_helperParent);
            _lines.Add(line.GetComponent<LineRenderer>());
            SetText();
        }

        void SetText()
        {
#if UNITY_EDITOR
            CalloutText ct = GetComponent<CalloutText>();
            ct.GetTextComponent();
            ct.SetText();
#endif
        }

        public void DeletePointAndLine(int index)
        {
            DestroyImmediate(_points[index]);
            DestroyImmediate(_lines[index]);

            _points.RemoveAt(index);
            _lines.RemoveAt(index);

            SetText();
        }

        public GameObject GetPoint(int index)
        {
            return _points[index];
        }

        public void SetPoint(int index, Vector3 position)
        {
            _points[index].transform.position = position;
        }

        public void SetPoint(GameObject po, int index)
        {
            _points[index] = po;
        }

        public LineRenderer GetLine(int index)
        {
            return _lines[index];
        }

        public void SetLine(LineRenderer li, int index)
        {
            _lines[index] = li;
        }

        public int PointCount()
        {
            return _points.Count;
        }

        private Vector3 GetMeanVector(List<GameObject> positions)
        {
            if (positions.Count == 0)
                return Vector3.zero;
            float x = 0f;
            float y = 0f;
            float z = 0f;
            foreach (GameObject pos in positions)
            {
                x += pos.transform.position.x;
                y += pos.transform.position.y;
                z += pos.transform.position.z;
            }
            return new Vector3(x / positions.Count, y / positions.Count, z / positions.Count);
        }

        private void LateUpdate()
        {
            // if pivot not setup yet then abort !
            if (_theParent.pivotPoint == null)
                return;

            // get average points position
            Vector3 averagePoint = GetMeanVector(_points);

            // only on play mode
            if (Application.isPlaying)
            {
                // set size berdasarkan jarak dari kamera game
                cameraDistance = Vector3.Distance(_cameraTransform.position, averagePoint) * _theParent.zoomScale;
                cameraDistance = Mathf.Clamp(cameraDistance, _theParent.minScale, _theParent.maxScale);
            }

            // set texbox position
            if (_points.Count > 0)
            {
                if (_ignoreAll)
                {
                    // if ignore all x y and z
                    if (_setupIgnoreAll)
                        _ignoreAllOffset = _textBox.position - GetMeanVector(_points);
                    else
                        _textBox.position = GetMeanVector(_points) + _ignoreAllOffset;
                }
                else
                {
                    // get direction of text box
                    _direction = _theParent.pivotPoint.position - averagePoint;

                    if (ignoreX)
                        _direction.x = _ignoreXOffset;
                    if (ignoreY)
                        _direction.y = _ignoreYOffset;
                    if (ignoreZ)
                        _direction.z = _ignoreZOffset;

                    // normalize magnitude (length)
                    _direction.Normalize();

                    if (inversePivot)
                        _textBox.position = averagePoint + _direction * ((_theParent.lineLengthScale + lineLengthAdd) * cameraDistance);
                    else
                        _textBox.position = averagePoint - _direction * ((_theParent.lineLengthScale + lineLengthAdd) * cameraDistance);
                }
            }

            float lineWidth = cameraDistance * _theParent.boxScaleConstant * _theParent.lineWidthConstant;

            for (int i = 0; i < _lines.Count; i++)
            {
                if (_points[i] != null && _lines[i] != null)
                    _lines[i].SetPosition(0, _points[i].transform.position);

                if (_lines[i] != null)
                {
                    _lines[i].SetPosition(1, _textBox.position);
                    _lines[i].startWidth = lineWidth;
                    _lines[i].endWidth = lineWidth;
                }
            }

            float boxScale = cameraDistance * _theParent.boxScaleConstant;
            boxScale = Mathf.Clamp(boxScale, _minBoxScale, _maxBoxScale);
            _textBox.localScale = new Vector3(boxScale, boxScale, boxScale);
        }
    }
}