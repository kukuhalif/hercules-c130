﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VirtualTraining.Feature
{
    public class CalloutText : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField]
        bool _dontUseObjectName;
        [HideInInspector]
        public string content = "Enter text ...";
        List<Text> _texts = new List<Text>();
        List<TMP_Text> _tmTexts = new List<TMP_Text>();

        private void OnEnable()
        {
            GetTextComponent();
        }

        private void Update()
        {
            SetText();
        }

        public void GetTextComponent()
        {
            _texts.Clear();
            _tmTexts.Clear();
            GetText<Text>(transform, _texts);
            GetText<TMP_Text>(transform, _tmTexts);
        }

        public void SetText()
        {
            if (!_dontUseObjectName)
                content = gameObject.name;
            for (int i = 0; i < _texts.Count; i++)
            {
                _texts[i].text = content;
            }
            for (int i = 0; i < _tmTexts.Count; i++)
            {
                _tmTexts[i].text = content;
            }
        }

        void GetText<T>(Transform obj, List<T> textList)
        {
            T[] texts = obj.GetComponents<T>();
            for (int i = 0; i < texts.Length; i++)
            {
                textList.Add(texts[i]);
            }
            for (int i = 0; i < obj.childCount; i++)
            {
                GetText<T>(obj.GetChild(i), textList);
            }
        }
#endif
    }

}