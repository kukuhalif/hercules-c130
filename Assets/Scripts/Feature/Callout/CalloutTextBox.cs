﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class CalloutTextBox : MonoBehaviour
    {
        [SerializeField]
        Transform _camTransform;

        private void OnEnable()
        {
            if (gameObject.layer == VirtualTrainingCamera.MonitorCameraLayer)
                _camTransform = VirtualTrainingCamera.MonitorCameraTransform;
            else
                _camTransform = VirtualTrainingCamera.CameraTransform;
        }

        private void LateUpdate()
        {
            if (_camTransform == null)
                return;

            Vector3 v = _camTransform.position - transform.position;
            v.x = v.z = 0.0f;
            transform.LookAt(_camTransform.position - v);
            transform.Rotate(0, 180, 0);
            transform.rotation = (_camTransform.rotation);
        }
    }
}