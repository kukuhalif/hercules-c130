﻿using UnityEngine;

namespace VirtualTraining.Feature
{
    public class CalloutPoint : MonoBehaviour
    {
        [SerializeField] bool _disableReparent;
#if UNITY_EDITOR
        [HideInInspector][System.NonSerialized] public bool reparentCalloutPoint = false;
        Transform _originalParent;
#endif

        // set calout point parent only when play in build
        private void Start()
        {
            ReparentToTarget();
        }

        public void ReparentToOriginalParent()
        {
#if UNITY_EDITOR
            transform.SetParent(_originalParent);
#endif
        }

        public void ReparentToTarget()
        {
#if UNITY_EDITOR
            if (reparentCalloutPoint)
            {

                _originalParent = transform.parent;
#endif
                if (_disableReparent)
                    return;

                bool attached = false;
                float size = 0.001f;
                while (!attached && size <= 15f)
                {
                    Collider[] cols = Physics.OverlapSphere(transform.position, size);
                    if (cols.Length > 0)
                    {
                        transform.SetParent(cols[0].transform, true);
                        attached = true;
                    }
                    else
                    {
                        size += 0.001f;
                    }
                }
#if UNITY_EDITOR
            }
#endif
        }
    }
}