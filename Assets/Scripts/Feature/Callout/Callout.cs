﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using TMPro;

namespace VirtualTraining.Feature
{
    public class Callout : MonoBehaviour
    {
        [System.NonSerialized]
        public bool created = false;
        public Transform pivotPoint;

        // -1 = main camera
        // else, second monitor index
        public int secondMonitorIndex = -1;

        // ini angka cantik
        public float lineLengthScale = 10f;
        public float boxScaleConstant = 0.035f;
        public float lineWidthConstant = 2.5f;

        // ini angka cantik
        [HideInInspector]
        [System.NonSerialized]
        public float minScale = 0.0001f;
        [HideInInspector]
        [System.NonSerialized]
        public float maxScale = 0.5f;
        [HideInInspector]
        [System.NonSerialized]
        public float zoomScale = 0.01f;

        [System.NonSerialized]
        public CalloutElement copySource = null;

        [System.NonSerialized]
        CalloutPoint[] points;

        private void Start()
        {
            EventManager.AddListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);
            points = transform.GetAllComponentsInChilds<CalloutPoint>();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<PlayMonitorCameraEvent>(OnOpenMonitorCamera);
        }

        public void Destroy()
        {
            for (int i = 0; i < points.Length; i++)
            {
                Destroy(points[i].gameObject);
            }
            Destroy(gameObject);
        }

        void OnOpenMonitorCamera(PlayMonitorCameraEvent e)
        {
            if (secondMonitorIndex == -1)
                return;

            gameObject.SetActive(secondMonitorIndex == e.index + 1);
        }

        public void CreatePivot()
        {
            if (pivotPoint == null)
            {
                GameObject pivot = new GameObject("pivot");
                pivotPoint = pivot.transform;
                pivotPoint.SetParent(transform);
            }
        }

        public void On(bool on)
        {
            gameObject.SetActive(on);
            if (on)
            {
                created = true;
            }
        }

        public void SetMargin(float top, float bottom, float left, float right)
        {
            var txts = transform.GetAllComponentsInChilds<TextMeshProUGUI>();
            foreach (var txt in txts)
            {
                txt.margin = new Vector4(left, top, right, bottom);
            }
        }
    }

}