﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class PartObjectManager : InitializationQueue
    {
        class ObjectData
        {
            GameObject obj;
            bool respondHideAll = true;
            bool respondXRayAll = true;
            public List<GameObject> childs = new List<GameObject>();

            int defaultLayer;
            Renderer renderer;
            Material[] defaultMaterials;
            Collider collider = null;
            List<Color> defaultEmissions = new List<Color>();

            public ObjectData(GameObject obj)
            {
                this.obj = obj;

                defaultLayer = obj.layer;

                renderer = obj.GetComponent<Renderer>();
                if (renderer != null)
                {
                    defaultMaterials = renderer.materials;
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        defaultEmissions.Add(defaultMaterials[i].GetColor("_EmissionColor"));
                    }
                }

                collider = obj.GetComponent<Collider>();

                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    GetChilds(obj.transform.GetChild(i).gameObject);
                }

                hideAllEvent += HideAllListener;
                xrayAllEvent += XrayAllListener;
            }

            ~ObjectData()
            {
                hideAllEvent -= HideAllListener;
                xrayAllEvent -= XrayAllListener;
            }

            public void Reset()
            {
                respondHideAll = true;
                respondXRayAll = true;
            }

            private void XrayAllListener(bool xray, Material mat)
            {
                if (respondXRayAll && xray)
                {
                    Xray(true, mat);
                }
                else if (!xray)
                {
                    Xray(false);
                }
            }

            private void HideAllListener(bool hide)
            {
                if (respondHideAll && hide)
                {
                    Hide(true);
                }
                else if (!hide)
                {
                    Hide(false);
                }

            }

            private void GetChilds(GameObject obj)
            {
                childs.Add(obj);

                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    GetChilds(obj.transform.GetChild(i).gameObject);
                }
            }

            public void Solo()
            {
                respondHideAll = false;
            }

            public void Hide(bool hide)
            {
                if (renderer != null)
                {
                    renderer.enabled = !hide;
                }

                if (collider != null)
                {
                    collider.enabled = !hide;
                }
            }

            public void Highlight(bool setColor, Color color)
            {
                respondXRayAll = false;

                if (setColor && renderer != null)
                {
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        renderer.materials[i].SetFloat("_Highlight", 1);
                        renderer.materials[i].SetColor("_HighlightColor", color);
                    }
                }
            }

            public void ResetHighlight()
            {
                if (renderer != null)
                {
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        renderer.materials[i].SetFloat("_Highlight", 0);
                    }
                }
            }

            public void ResetLayer()
            {
                obj.layer = defaultLayer;
            }

            public void Xray(bool xRay, Material xrayMat = null)
            {
                if (renderer != null)
                {
                    Material[] mats = renderer.materials;
                    if (xRay)
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            mats[i] = xrayMat;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < defaultMaterials.Length; i++)
                        {
                            mats[i] = defaultMaterials[i];
                        }
                    }

                    renderer.materials = mats;
                }

                if (collider != null)
                {
                    collider.enabled = !xRay;
                }
            }

            public void IgnoreCutaway(bool ignore)
            {
                if (renderer != null)
                {
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        renderer.materials[i].SetFloat("_IgnoreCutaway", ignore ? 1 : 0);
                    }
                }
            }

            public void Blink(Color color, float speed)
            {
                respondHideAll = false;
                respondXRayAll = false;

                if (renderer != null)
                {
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        renderer.materials[i].SetFloat("_Blink", 1);
                        renderer.materials[i].SetColor("_EmissionColor", color);
                        renderer.materials[i].SetFloat("_BlinkSpeed", speed);
                    }
                }
            }

            public void Blink()
            {
                if (renderer != null)
                {
                    for (int i = 0; i < defaultEmissions.Count; i++)
                    {
                        renderer.materials[i].SetFloat("_Blink", 0);
                        renderer.materials[i].SetColor("_EmissionColor", defaultEmissions[i]);
                    }
                }
            }

            public void Fresnel(Color color, bool blink, float blinkSpeed)
            {
                respondHideAll = false;
                respondXRayAll = false;

                if (renderer != null)
                {
                    for (int i = 0; i < defaultMaterials.Length; i++)
                    {
                        renderer.materials[i].SetFloat("_Fresnel", 1);
                        renderer.materials[i].SetColor("_EmissionColor", color);

                        if (blink)
                        {
                            renderer.materials[i].SetFloat("_Blink", 1);
                            renderer.materials[i].SetFloat("_BlinkSpeed", blinkSpeed);
                        }
                    }
                }
            }

            public void ResetFresnel()
            {
                if (renderer != null)
                {
                    for (int i = 0; i < defaultEmissions.Count; i++)
                    {
                        renderer.materials[i].SetFloat("_Fresnel", 0);
                        renderer.materials[i].SetColor("_EmissionColor", defaultEmissions[i]);
                        renderer.materials[i].SetFloat("_Blink", 0);
                    }
                }
            }
        }

        class CurrentPartObject
        {
            public GameObject target;
            public PartObjectAction action;
            public bool noRecursive;

            public CurrentPartObject(GameObject target, PartObjectAction action)
            {
                this.target = target;
                this.action = action;
            }
        }

        public delegate void HideAll(bool hide);
        public static event HideAll hideAllEvent;

        public delegate void XrayAll(bool xray, Material mat);
        public static event XrayAll xrayAllEvent;

        Color defaultHighlightColor;
        Color defaultSelectColor;
        Color defaultBlinkColor;
        Color defaultFresnelColor;
        Material[] xRayMaterials;
        int index;
        Dictionary<GameObject, ObjectData> objectDataLookup = new Dictionary<GameObject, ObjectData>();
        List<CurrentPartObject> currentEvent = new List<CurrentPartObject>();
        List<CurrentPartObject> currentFresnel = new List<CurrentPartObject>();
        bool hideAll, xrayAll;
        int highlightXrayMat;

        public override void Initialize(GameObject obj)
        {
            ObjectData objData = new ObjectData(obj);
            objectDataLookup.Add(obj, objData);
        }

        private void Start()
        {
            defaultHighlightColor = DatabaseManager.GetDefaultHighlightColor();
            defaultSelectColor = DatabaseManager.GetDefaultSelectedColor();
            defaultBlinkColor = DatabaseManager.GetDefaultBlinkColor();
            defaultFresnelColor = DatabaseManager.GetDefaultFresnelColor();
            xRayMaterials = DatabaseManager.GetXrayMaterials().ToArray();

            EventManager.AddListener<PartObjectEvent>(PartObjectListener);
            EventManager.AddListener<FresnelObjectsEvent>(FresnelListener);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<PartObjectEvent>(PartObjectListener);
            EventManager.RemoveListener<FresnelObjectsEvent>(FresnelListener);
        }

        private void FresnelListener(FresnelObjectsEvent e)
        {
            if (e.partObjects == null)
            {
                ResetFresnel();
                return;
            }

            for (int i = 0; i < e.partObjects.Count; i++)
            {
                if (e.partObjects[i].target == null)
                {
                    Debug.LogError("ada part object yang kosong disini, cek di virtual button");
                    continue;
                }

                CurrentPartObject newObj = new CurrentPartObject(e.partObjects[i].target.gameObject, PartObjectAction.Fresnel);
                newObj.noRecursive = e.partObjects[i].fresnelNoRecursive;

                Color fc = e.partObjects[i].useFresnelDefaultColor ? defaultFresnelColor : e.partObjects[i].fresnelColor;

                objectDataLookup[e.partObjects[i].target.gameObject].Fresnel(fc, e.partObjects[i].fresnelWithBlink, e.partObjects[i].blinkSpeed);
                if (!e.partObjects[i].fresnelNoRecursive)
                {
                    List<GameObject> childs = objectDataLookup[e.partObjects[i].target.gameObject].childs;
                    for (index = 0; index < childs.Count; index++)
                    {
                        objectDataLookup[childs[index]].Fresnel(fc, e.partObjects[i].fresnelWithBlink, e.partObjects[i].blinkSpeed);
                    }
                }

                currentFresnel.Add(newObj);
            }
        }

        private void PartObjectListener(PartObjectEvent e)
        {
            if (e.objects == null)
            {
                ResetObjects();
            }
            else
            {
                for (int i = 0; i < e.objects.Count; i++)
                {
                    if (e.objects[i].target == null)
                    {
                        Debug.LogError("ada part object yang kosong disini, cek di materi editor");
                        continue;
                    }

                    Execute(e.objects[i]);
                }

                if (hideAll)
                    hideAllEvent.Invoke(true);
                if (xrayAll)
                    xrayAllEvent.Invoke(true, xRayMaterials[highlightXrayMat]);

                StartCoroutine(Culling());
            }
        }

        private IEnumerator Culling()
        {
            yield return null;
            List<GameObject> culls = new List<GameObject>();
            for (int i = 0; i < currentEvent.Count; i++)
            {
                if (currentEvent[i].action == PartObjectAction.Blink ||
                    currentEvent[i].action == PartObjectAction.Solo ||
                    currentEvent[i].action == PartObjectAction.Highlight ||
                    currentEvent[i].action == PartObjectAction.Fresnel ||
                    currentEvent[i].action == PartObjectAction.Select)

                    culls.Add(currentEvent[i].target);
            }
            EventManager.TriggerEvent(new PartObjectCullingEvent(culls));
        }

        private void ResetFresnel()
        {
            for (int i = 0; i < currentFresnel.Count; i++)
            {
                objectDataLookup[currentFresnel[i].target].ResetFresnel();
                if (!currentFresnel[i].noRecursive)
                {
                    for (index = 0; index < objectDataLookup[currentFresnel[i].target].childs.Count; index++)
                    {
                        objectDataLookup[objectDataLookup[currentFresnel[i].target].childs[index]].ResetFresnel();
                    }
                }
            }

            currentFresnel.Clear();
        }


        private void ResetObjects()
        {
            if (hideAll)
                hideAllEvent.Invoke(false);
            if (xrayAll)
                xrayAllEvent.Invoke(false, null);

            hideAll = false;
            xrayAll = false;

            for (int i = 0; i < currentEvent.Count; i++)
            {
                ResetObject(currentEvent[i]);
            }

            currentEvent.Clear();
        }

        private void ResetObject(CurrentPartObject partObject)
        {
            ObjectData data = objectDataLookup[partObject.target.gameObject];
            data.Reset();

            switch (partObject.action)
            {
                // reset solo tidak perlu ngapa ngapain

                case PartObjectAction.Disable:

                    partObject.target.gameObject.SetActive(true);

                    break;
                case PartObjectAction.Highlight:

                    data.ResetHighlight();
                    if (!partObject.noRecursive)
                    {
                        for (index = 0; index < data.childs.Count; index++)
                        {
                            objectDataLookup[data.childs[index]].ResetHighlight();
                        }
                    }

                    break;
                case PartObjectAction.Xray:

                    data.Xray(false);
                    for (index = 0; index < data.childs.Count; index++)
                    {
                        objectDataLookup[data.childs[index]].Xray(false);
                    }

                    break;
                case PartObjectAction.Enable:

                    partObject.target.gameObject.SetActive(false);

                    break;
                case PartObjectAction.Select:

                    data.ResetHighlight();
                    if (!partObject.noRecursive)
                    {
                        for (index = 0; index < data.childs.Count; index++)
                        {
                            objectDataLookup[data.childs[index]].ResetHighlight();
                        }
                    }

                    break;
                case PartObjectAction.Blink:

                    data.Blink();
                    if (!partObject.noRecursive)
                    {
                        for (index = 0; index < data.childs.Count; index++)
                        {
                            objectDataLookup[data.childs[index]].Blink();
                        }
                    }

                    break;
                case PartObjectAction.SetLayer:

                    data.ResetLayer();

                    for (index = 0; index < data.childs.Count; index++)
                    {
                        objectDataLookup[data.childs[index]].ResetLayer();
                    }

                    break;
                case PartObjectAction.IgnoreCutaway:

                    data.IgnoreCutaway(false);

                    for (index = 0; index < data.childs.Count; index++)
                    {
                        objectDataLookup[data.childs[index]].IgnoreCutaway(false);
                    }

                    break;
                case PartObjectAction.Fresnel:

                    data.ResetFresnel();
                    if (!partObject.noRecursive)
                    {
                        for (index = 0; index < data.childs.Count; index++)
                        {
                            objectDataLookup[data.childs[index]].ResetFresnel();
                        }
                    }

                    break;
            }
        }

        private void Execute(PartObject partObject)
        {
            CurrentPartObject newCurrent = new CurrentPartObject(partObject.target.gameObject, partObject.action);
            Color color;
            switch (partObject.action)
            {
                case PartObjectAction.Solo:

                    hideAll = true;
                    objectDataLookup[partObject.target.gameObject].Solo();
                    for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                    {
                        objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Solo();
                    }

                    break;
                case PartObjectAction.Disable:

                    partObject.target.gameObject.SetActive(false);

                    break;
                case PartObjectAction.Highlight:

                    xrayAll = true;
                    highlightXrayMat = partObject.xrayIndex;

                    color = partObject.useDefaultHighlightColor ? defaultHighlightColor : partObject.highlightColor;

                    objectDataLookup[partObject.target.gameObject].Highlight(partObject.setHighlightColor, partObject.highlightColor);

                    if (!partObject.highlightNoRecursive)
                    {
                        for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                        {
                            objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Highlight(partObject.setHighlightColor, color);
                        }
                    }

                    newCurrent.noRecursive = partObject.highlightNoRecursive;

                    break;
                case PartObjectAction.Xray:

                    objectDataLookup[partObject.target.gameObject].Xray(true, xRayMaterials[partObject.xrayIndex]);
                    for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                    {
                        objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Xray(true, xRayMaterials[partObject.xrayIndex]);
                    }

                    break;
                case PartObjectAction.Enable:

                    partObject.target.gameObject.SetActive(true);

                    break;
                case PartObjectAction.Select:

                    color = partObject.useDefaultHighlightColor ? defaultSelectColor : partObject.highlightColor;

                    objectDataLookup[partObject.target.gameObject].Highlight(partObject.setHighlightColor, partObject.highlightColor);

                    if (!partObject.highlightNoRecursive)
                    {
                        for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                        {
                            objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Highlight(partObject.setHighlightColor, color);
                        }
                    }

                    newCurrent.noRecursive = partObject.highlightNoRecursive;

                    break;
                case PartObjectAction.Blink:

                    color = partObject.useDefaultBlinkColor ? defaultBlinkColor : partObject.blinkColor;

                    objectDataLookup[partObject.target.gameObject].Blink(color, partObject.blinkSpeed);

                    if (!partObject.blinkNoRecursive)
                    {
                        for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                        {
                            objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Blink(color, partObject.blinkSpeed);
                        }
                    }

                    newCurrent.noRecursive = partObject.blinkNoRecursive;

                    break;
                case PartObjectAction.SetLayer:

                    partObject.target.gameObject.layer = partObject.layerOption == LayerOption.MainCamera ? VirtualTrainingCamera.MainCameraLayer : VirtualTrainingCamera.MonitorCameraLayer;

                    for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                    {
                        objectDataLookup[partObject.target.gameObject].childs[index].layer = partObject.target.gameObject.layer;
                    }

                    break;
                case PartObjectAction.IgnoreCutaway:

                    objectDataLookup[partObject.target.gameObject].IgnoreCutaway(true);

                    for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                    {
                        objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].IgnoreCutaway(true);
                    }

                    break;
                case PartObjectAction.Fresnel:

                    color = partObject.useFresnelDefaultColor ? defaultFresnelColor : partObject.fresnelColor;

                    objectDataLookup[partObject.target.gameObject].Fresnel(color, partObject.fresnelWithBlink, partObject.blinkSpeed);

                    if (!partObject.fresnelNoRecursive)
                    {
                        for (index = 0; index < objectDataLookup[partObject.target.gameObject].childs.Count; index++)
                        {
                            objectDataLookup[objectDataLookup[partObject.target.gameObject].childs[index]].Fresnel(color, partObject.fresnelWithBlink, partObject.blinkSpeed);
                        }
                    }

                    newCurrent.noRecursive = partObject.fresnelNoRecursive;

                    break;
            }

            currentEvent.Add(newCurrent);
        }
    }
}
