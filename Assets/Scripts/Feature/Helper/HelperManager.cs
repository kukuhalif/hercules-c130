﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class HelperManager : MonoBehaviour
    {
        [SerializeField] Transform helperParent;
        List<GameObject> helpers = new List<GameObject>();

        private void Start()
        {
            EventManager.AddListener<HelperEvent>(Show);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<HelperEvent>(Show);
        }

        public void Show(HelperEvent e)
        {
            if (e.helpers == null)
            {
                Hide();
                return;
            }

            for (int i = 0; i < e.helpers.Count; i++)
            {
                if (e.helpers[i] == null)
                {
                    Debug.LogError("ada callout / helper yang kosong disini, cek di materi editor");
                }
                else
                {
                    GameObject cp = Instantiate(e.helpers[i], helperParent);
                    this.helpers.Add(cp);
                }
            }
        }

        public void Hide()
        {
            for (int i = 0; i < helpers.Count; i++)
            {
                Destroy(helpers[i]);
            }
            helpers.Clear();
        }
    }
}