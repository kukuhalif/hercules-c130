﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class PcCameraController : CameraController
    {
        // setting
        float rotateSpeed = 5f;
        float cameraTransitionSpeed = 5f;
        float zoomSpeed = 2f;
        float dragSpeed = 5f;
        float cameraArriveDifference;
        [SerializeField] Transform cameraParent;

        // state
        /// <summary>
        /// rotate or drag mode
        /// </summary>
        bool isRotateMode = true;
        bool isLocked = false;
        bool isRotateFromView = false;

        // const
        const float DRAG_CONST = 0.0015f;

        // cache
        Camera mainCamera;
        Transform anchor;
        Transform cameraTarget;
        List<PartObject> partObjects = new List<PartObject>();
        GameObject lastFocusedObj;

        // default
        Vector3 defaultPosition;
        Quaternion defaultRotation;
        float defaultFOV;
        float defaultOrthographicSize;

        // camera destination
        Vector3 destinationPosition;
        Quaternion destinationRotation;
        bool isOrthographic;
        float destinationFOV;
        float destinationOrthographicSize;
        Action arriveAction;

        // state
        bool isRotating;
        bool isDragging;
        bool isDraggingZoom;
        bool isMoving;
        bool crossProductZoomX;
        bool crossProductZoomY;
        bool crossProductZoomZ;
        Vector2 pointerDeltaPosition;
        Vector2 pointerAxisPosition;
        Vector2 rotateFromViewOffsetAxisPosition;
        Vector2 dragSmoothDelta;
        float scrollValue;
        float dragDistance;
        float lastPinchDistance;
        float currentDragSpeed;
        bool[] isTouchOverUI;
        bool isZoomTouchValid;
        bool isDragTouchValid;

        private void Start()
        {
            // get camera
            mainCamera = GetComponent<Camera>();

            // set current camera
            VirtualTrainingCamera.SetCurrentCameraController(this, false);

            // set default value
            defaultPosition = destinationPosition = transform.position;
            defaultRotation = destinationRotation = transform.rotation;
            defaultFOV = destinationFOV = mainCamera.fieldOfView;
            defaultOrthographicSize = mainCamera.orthographicSize;

            // add input listener
            VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
            VirtualTrainingInputSystem.OnLeftClickDown += OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
            VirtualTrainingInputSystem.OnMovePointer += OnMovePointer;
            VirtualTrainingInputSystem.OnStartMiddleClick += OnStartMiddleClick;
            VirtualTrainingInputSystem.OnEndMiddleClick += OnEndMiddleClick;
            VirtualTrainingInputSystem.OnStartRightClick += OnStartRightClick;
            VirtualTrainingInputSystem.OnEndRightClick += OnEndRightClick;
            VirtualTrainingInputSystem.OnMouseScroll += OnMouseScroll;
            VirtualTrainingInputSystem.OnTouchAdded += OnTouchAdded;
            VirtualTrainingInputSystem.OnTouchRemoved += OnTouchRemoved;

            // create camera anchor
            GameObject anchor = new GameObject("camera anchor");
            anchor.transform.SetParent(cameraParent);
            this.anchor = anchor.transform;
            this.anchor.position = transform.position;
            this.anchor.rotation = transform.rotation;
            this.anchor.position = FocusPosition();

            // initialize touch (3 fingers)
            isTouchOverUI = new bool[3];

            // add interaction object event listener
            EventManager.AddListener<ObjectInteractionRotateEvent>(RotateModeListener);
            EventManager.AddListener<ObjectInteractionResetCameraEvent>(ResetCameraPositionListener);
            EventManager.AddListener<ObjectInteractionLockCameraEvent>(LockCameraListener);

            // setting listener
            EventManager.AddListener<ApplySettingEvent>(ApplySettingListener);

            // part object listener
            EventManager.AddListener<PartObjectEvent>(PartObjectListener);

            // set camera arrive difference
            cameraArriveDifference = DatabaseManager.GetCameraArriveDifference();
        }

        private void OnDestroy()
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
            VirtualTrainingInputSystem.OnLeftClickDown -= OnLeftClickDown;
            VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
            VirtualTrainingInputSystem.OnMovePointer -= OnMovePointer;
            VirtualTrainingInputSystem.OnStartMiddleClick -= OnStartMiddleClick;
            VirtualTrainingInputSystem.OnEndMiddleClick -= OnEndMiddleClick;
            VirtualTrainingInputSystem.OnStartRightClick -= OnStartRightClick;
            VirtualTrainingInputSystem.OnEndRightClick -= OnEndRightClick;
            VirtualTrainingInputSystem.OnMouseScroll -= OnMouseScroll;
            VirtualTrainingInputSystem.OnTouchAdded -= OnTouchAdded;
            VirtualTrainingInputSystem.OnTouchRemoved -= OnTouchRemoved;

            EventManager.RemoveListener<ObjectInteractionRotateEvent>(RotateModeListener);
            EventManager.RemoveListener<ObjectInteractionResetCameraEvent>(ResetCameraPositionListener);
            EventManager.RemoveListener<ObjectInteractionLockCameraEvent>(LockCameraListener);
            EventManager.RemoveListener<ApplySettingEvent>(ApplySettingListener);
            EventManager.RemoveListener<PartObjectEvent>(PartObjectListener);
        }

        private void PartObjectListener(PartObjectEvent e)
        {
            partObjects = e.objects;
        }

        private void ApplySettingListener(ApplySettingEvent e)
        {
            rotateSpeed = e.data.rotateSpeed;
            zoomSpeed = e.data.zoomSpeed;
            dragSpeed = e.data.dragSpeed;
            cameraTransitionSpeed = e.data.cameraTransitionSpeed;
        }

        private void RotateModeListener(ObjectInteractionRotateEvent e)
        {
            isRotateMode = e.on;
        }

        private void ResetCameraPositionListener(ObjectInteractionResetCameraEvent e)
        {
            MoveCamera(destinationPosition, destinationRotation, cameraTarget, isOrthographic, destinationFOV, destinationOrthographicSize, null);
        }

        private void LockCameraListener(ObjectInteractionLockCameraEvent e)
        {
            isLocked = e.on;
            if (isLocked)
            {
                OnEndLeftClick(Vector2.zero, null);
                OnEndMiddleClick(Vector2.zero, null);
                OnEndRightClick(Vector2.zero, null);
                lastPinchDistance = 0f;
            }
        }

        public override Camera GetCamera()
        {
            return mainCamera;
        }

        private void OnTouchAdded(bool isFingerOverUI)
        {
            if (isLocked)
                return;

            int touchCount = VirtualTrainingInputSystem.TouchCount;

            // reset touch over ui states if this touch is first touch
            if (touchCount == 1)
            {
                for (int i = 0; i < isTouchOverUI.Length; i++)
                {
                    isTouchOverUI[i] = false;
                }
            }

            // reset touch input validation state
            isZoomTouchValid = false;
            isDragTouchValid = false;

            // set touch state
            if (touchCount < 3)
                isTouchOverUI[touchCount] = isFingerOverUI;

            if (touchCount > 1)
            {
                OnEndLeftClick(Vector2.zero, null);
                OnEndMiddleClick(Vector2.zero, null);

                // for zoom
                if (touchCount == 2 && !isTouchOverUI[0] && !isTouchOverUI[1])
                {
                    isZoomTouchValid = true;
                }
                else
                    isZoomTouchValid = false;

                // for drag
                if (touchCount == 3 && !isTouchOverUI[0] && !isTouchOverUI[1] && !isTouchOverUI[2])
                {
                    isDragTouchValid = true;
                    InitDrag();
                }
                else
                    isDragTouchValid = false;
            }
        }

        private void OnTouchRemoved(bool isFingerOverUI)
        {
            lastPinchDistance = 0f;
            OnEndLeftClick(Vector2.zero, null);
            OnEndMiddleClick(Vector2.zero, null);
        }

        private void InitRotate()
        {
            isRotating = true;

            anchor.position = FocusPosition();
            transform.SetParent(anchor, true);

            rotateFromViewOffsetAxisPosition.x = transform.rotation.eulerAngles.y;
            rotateFromViewOffsetAxisPosition.y = -transform.rotation.eulerAngles.x;
        }

        private void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            if (isRotateMode)
                InitRotate();
            else
                InitDrag();
        }

        private void OnLeftClickDown(Vector2 axisValue)
        {
            pointerAxisPosition = axisValue;
        }

        private void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (isRotating)
            {
                isRotating = false;
                transform.SetParent(cameraParent, true);
            }

            if (!isRotateMode)
            {
                isDragging = false;
            }
        }

        private void InitDrag()
        {
            isDragging = true;
            pointerDeltaPosition = Vector2.zero;
            dragSmoothDelta = Vector2.zero;

            anchor.position = FocusPosition();
            dragDistance = Vector3.Distance(anchor.position, transform.position);
            if (dragDistance < 0.01f)
                dragDistance = 0.01f;
            currentDragSpeed = 0f;
        }

        private void OnStartMiddleClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            InitDrag();
        }

        private void OnEndMiddleClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isRotating = false;
            isDragging = false;
        }

        private void OnStartRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            isDraggingZoom = true;
        }

        private void OnEndRightClick(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isDraggingZoom = false;
        }

        private void OnMouseScroll(float value, GameObject raycastedUI)
        {
            if (raycastedUI != null || isLocked)
                return;

            scrollValue = value;
        }

        private void OnMovePointer(Vector2 deltaPosition)
        {
            pointerDeltaPosition = deltaPosition;
        }

        private Vector3 FocusPosition()
        {
            RaycastHit hitInfo = new RaycastHit();
            if (VirtualTrainingCamera.Raycast(ref hitInfo))
            {
                lastFocusedObj = hitInfo.transform.gameObject;
                return hitInfo.point;
            }

            if (partObjects != null && partObjects.Count > 0)
            {
                int nearestIdx = -1;
                int nearestHighlightIdx = -1;
                float minDistance = float.MaxValue;

                for (int i = 0; i < partObjects.Count; i++)
                {
                    if (partObjects[i].target.gameObject != null &&
                        (partObjects[i].action == PartObjectAction.Solo ||
                        partObjects[i].action == PartObjectAction.Highlight ||
                        partObjects[i].action == PartObjectAction.Enable ||
                        partObjects[i].action == PartObjectAction.Blink ||
                        partObjects[i].action == PartObjectAction.SetLayer ||
                        partObjects[i].action == PartObjectAction.IgnoreCutaway ||
                        partObjects[i].action == PartObjectAction.Fresnel
                        ))
                    {
                        float distance = Vector3.Distance(transform.position, partObjects[i].target.gameObject.transform.position);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            nearestIdx = i;

                            // prioritize highlight
                            if (partObjects[i].action == PartObjectAction.Highlight)
                                nearestHighlightIdx = i;
                        }
                    }
                }

                if (nearestIdx == -1)
                {
                    if (lastFocusedObj == null)
                        return Vector3.zero;

                    return lastFocusedObj.transform.position;
                }

                return partObjects[nearestHighlightIdx == -1 ? nearestIdx : nearestHighlightIdx].target.gameObject.transform.position;
            }

            if (lastFocusedObj == null)
                return Vector3.zero;

            return lastFocusedObj.transform.position;
        }

        private float CalculatePinchZoom(UnityEngine.InputSystem.EnhancedTouch.Touch touchZero, UnityEngine.InputSystem.EnhancedTouch.Touch touchOne, float zoomDistance)
        {
            float currentDistance = Vector2.Distance(touchZero.screenPosition, touchOne.screenPosition);

            float result = 0f;

            if (currentDistance > lastPinchDistance)
            {
                result = -Time.deltaTime * zoomDistance;
            }
            else if (currentDistance < lastPinchDistance)
            {
                result = Time.deltaTime * zoomDistance;
            }

            lastPinchDistance = currentDistance;

            return result;
        }

        private float CalculateZoomValue(float zoomDistance)
        {
            if (VirtualTrainingInputSystem.TouchCount == 2)
            {
                return CalculatePinchZoom(VirtualTrainingInputSystem.GetTouch(0), VirtualTrainingInputSystem.GetTouch(1), zoomDistance);
            }
            else if (isDraggingZoom)
            {
                return pointerDeltaPosition.y * (zoomDistance * Time.deltaTime);
            }
            else if (scrollValue != 0f)
            {
                if (scrollValue > 0)
                    return -Time.deltaTime * zoomDistance;
                else if (scrollValue < 0)
                    return Time.deltaTime * zoomDistance;
            }

            return 0f;
        }

        private bool IsCrossProductDifferent()
        {
            Vector3 crossIn = Vector3.Cross(anchor.position, transform.position);

            if (crossIn.x < 0 && crossProductZoomX)
                return true;

            if (crossIn.y < 0 && crossProductZoomY)
                return true;

            if (crossIn.z < 0 && crossProductZoomZ)
                return true;

            if (crossIn.x > 0 && !crossProductZoomX)
                return true;

            if (crossIn.y > 0 && !crossProductZoomY)
                return true;

            if (crossIn.z > 0 && !crossProductZoomZ)
                return true;

            return false;
        }

        public override void ResetCamera(Action arriveAction)
        {
            destinationPosition = defaultPosition;
            destinationRotation = defaultRotation;
            isOrthographic = false;

            MoveCamera(defaultPosition, defaultRotation, null, false, defaultFOV, defaultOrthographicSize, arriveAction);
        }

        public override void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
        {
            MoveCamera(cameraDestination.position, cameraDestination.rotation, cameraDestination.target.gameObject == null ? null : cameraDestination.target.gameObject.transform, cameraDestination.isOrthographic, cameraDestination.fov, cameraDestination.orthographicSize, arriveAction);
        }

        public override void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, float fov, float orthographicSize, Action arriveAction)
        {
            isMoving = true;

            if (rotation.w == 0f && rotation.x == 0f && rotation.y == 0f && rotation.z == 0f)
                rotation = Quaternion.identity;

            OnEndLeftClick(Vector2.zero, null);
            OnEndMiddleClick(Vector2.zero, null);
            OnEndRightClick(Vector2.zero, null);

            this.cameraTarget = cameraTarget;

            if (cameraTarget == null)
            {
                destinationPosition = position;
                destinationRotation = rotation;
            }
            else
            {
                destinationPosition = cameraTarget.position;
                destinationRotation = cameraTarget.rotation;
            }

            this.isOrthographic = isOrthographic;
            destinationFOV = fov;
            destinationOrthographicSize = orthographicSize;
            this.arriveAction = arriveAction;
        }

        private void LateUpdate()
        {
            if (isMoving)
            {
                mainCamera.orthographic = false;
                float moveSpeed = cameraTransitionSpeed * Time.deltaTime;

                transform.position = Vector3.Lerp(transform.position, destinationPosition, moveSpeed);
                transform.rotation = Quaternion.Slerp(transform.rotation, destinationRotation, moveSpeed);
                mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView, destinationFOV, moveSpeed);
                mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, destinationOrthographicSize, moveSpeed);

                if (Vector3.Distance(transform.position, destinationPosition) < cameraArriveDifference &&
                    Quaternion.Angle(transform.rotation, destinationRotation) < cameraArriveDifference &&
                    Mathf.Abs(mainCamera.fieldOfView - destinationFOV) < cameraArriveDifference &&
                    Mathf.Abs(mainCamera.orthographicSize - destinationOrthographicSize) < cameraArriveDifference)
                {
                    isMoving = false;
                    mainCamera.orthographic = isOrthographic;

                    anchor.rotation = transform.rotation;

                    pointerAxisPosition.x = transform.rotation.eulerAngles.y;
                    pointerAxisPosition.y = -transform.rotation.eulerAngles.x;

                    arriveAction?.Invoke();
                }

                return;
            }

            if (cameraTarget != null)
            {
                transform.position = cameraTarget.position;
                transform.rotation = cameraTarget.rotation;

                return;
            }

            if (VirtualTrainingCamera.IsMovementEnabled)
            {
                // zoom
                if (isDraggingZoom || scrollValue != 0f || isZoomTouchValid)
                {
                    if (transform.parent != anchor)
                        transform.SetParent(cameraParent, true);

                    float zoomDistance = Vector3.Distance(anchor.position, transform.position);
                    if (zoomDistance < 0.05f)
                        zoomDistance = 0.05f;
                    else if (zoomDistance > 5f)
                        zoomDistance = 5f;
                    float zoomValue = CalculateZoomValue(zoomDistance) * zoomSpeed;

                    if (zoomValue != 0f)
                    {
                        if (mainCamera.orthographic) // orthographic zoom
                        {
                            mainCamera.orthographicSize += zoomValue;
                            if (mainCamera.orthographicSize <= 0.1f)
                                mainCamera.orthographicSize = 0.1f;
                        }
                        else if (isRotateFromView) // fov zoom
                        {
                            mainCamera.fieldOfView += zoomValue * 10f;
                            if (mainCamera.fieldOfView <= 0.1f)
                                mainCamera.fieldOfView = 0.1f;
                        }
                        else // position zoom
                        {
                            bool minDistanceReached = false;
                            // zoom in min handling
                            if (zoomValue < 0)
                            {
                                // if camera position is in front of anchor / min zoom
                                if (IsCrossProductDifferent())
                                {
                                    minDistanceReached = true;
                                }
                            }
                            else
                            {
                                Vector3 cross = Vector3.Cross(anchor.position, transform.position);
                                crossProductZoomX = cross.x > 0;
                                crossProductZoomY = cross.y > 0;
                                crossProductZoomZ = cross.z > 0;
                            }

                            if (!minDistanceReached)
                                transform.Translate(0f, 0f, -zoomValue);
                        }
                    }
                }
                else if (isDragging || isDragTouchValid) // drag
                {
                    if (isRotateMode)
                        OnEndLeftClick(Vector2.zero, null);

                    Vector2 dragInput = new Vector2(-pointerDeltaPosition.x * DRAG_CONST * dragDistance, -pointerDeltaPosition.y * DRAG_CONST * dragDistance);
                    currentDragSpeed = Mathf.Lerp(currentDragSpeed, dragSpeed, Time.deltaTime * dragSpeed);
                    dragSmoothDelta = Vector2.Lerp(dragSmoothDelta, dragInput, Time.deltaTime * currentDragSpeed);
                    transform.Translate(dragSmoothDelta, Space.Self);
                }
                else if (isRotating) // orbit
                {
                    pointerAxisPosition += rotateFromViewOffsetAxisPosition;

                    if (isRotateFromView)
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(-pointerAxisPosition.y, pointerAxisPosition.x, 0f), rotateSpeed * Time.deltaTime);
                    else
                        anchor.rotation = Quaternion.Slerp(anchor.rotation, Quaternion.Euler(-pointerAxisPosition.y, pointerAxisPosition.x, 0f), rotateSpeed * Time.deltaTime);
                }
            }
        }
    }
}
