using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VirtualTraining.Feature
{
    public class CullingManager : MonoBehaviour
    {
#if UNITY_EDITOR
        [MenuItem("Virtual Training Shortcut/Enable All Culled Object &K", false)]
        private static void EnabledCulledObject()
        {
            CullingManager cm = FindObjectOfType<CullingManager>();
            if (cm == null)
                return;

            cm.EnableAllCulled(true);
        }

        [MenuItem("Virtual Training Shortcut/Disable All Culled Object &L", false)]
        private static void DisabledCulledObject()
        {
            CullingManager cm = FindObjectOfType<CullingManager>();
            if (cm == null)
                return;

            cm.EnableAllCulled(false);
        }

        [MenuItem("Virtual Training Shortcut/Enable All Culled Object &K", true)]
        static bool EnabledCulledObjectValidation()
        {
            return Application.isPlaying;
        }

        [MenuItem("Virtual Training Shortcut/Disable All Culled Object &L", true)]
        static bool DisableCulledObjectValidation()
        {
            return Application.isPlaying;
        }
#endif

        private List<CullingDataModel> cullings = new List<CullingDataModel>();

        private void Start()
        {
            EventManager.AddListener<AddMovedObjectEvent>(AddMovedObjectListener);
            EventManager.AddListener<PartObjectCullingEvent>(PartObjectListener);

            List<CullingTreeElement> datas = DatabaseManager.GetCullingDatas();
            for (int i = 0; i < datas.Count; i++)
            {
                if (datas[i].data != null && datas[i].data.covers.Count > 0 && datas[i].data.culleds.Count > 0)
                {
                    CullingDataModel newCdm = new CullingDataModel();

                    for (int co = 0; co < datas[i].data.covers.Count; co++)
                    {
                        Collider[] collider = datas[i].data.covers[co].gameObject.transform.GetAllComponentsInChilds<Collider>();
                        for (int re = 0; re < collider.Length; re++)
                        {
                            if (collider[re] != null)
                            {
                                GameObjectType got = new GameObjectType(collider[re].GetComponent<GameObjectReference>().Id);
                                newCdm.covers.Add(got);
                            }
                        }
                    }

                    for (int cu = 0; cu < datas[i].data.culleds.Count; cu++)
                    {
                        Collider[] collider = datas[i].data.culleds[cu].gameObject.transform.GetAllComponentsInChilds<Collider>();
                        for (int re = 0; re < collider.Length; re++)
                        {
                            if (collider[re] != null)
                            {
                                GameObjectType got = new GameObjectType(collider[re].GetComponent<GameObjectReference>().Id);
                                newCdm.culleds.Add(got);
                            }
                        }
                    }

                    cullings.Add(newCdm);
                }
            }

            if (cullings.Count == 0)
            {
                Destroy(gameObject);
                return;
            }

            EnableAllCulled(false);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<AddMovedObjectEvent>(AddMovedObjectListener);
            EventManager.RemoveListener<PartObjectCullingEvent>(PartObjectListener);
        }

        private void AddMovedObjectListener(AddMovedObjectEvent e)
        {
            if (e.movedObject == null)
            {
                EnableAllCulled(false);
                return;
            }

            EnableCulled(e.movedObject);
        }

        private void PartObjectListener(PartObjectCullingEvent e)
        {
            EnableAllCulled(false);

            for (int i = 0; i < e.shows.Count; i++)
            {
                EnableCulledPartObject(e.shows[i]);
            }
        }

        private void EnableCulledPartObject(GameObject show)
        {
            for (int i = 0; i < cullings.Count; i++)
            {
                for (int c = 0; c < cullings[i].culleds.Count; c++)
                {
                    if (cullings[i].culleds[c].gameObject == show)
                    {
                        cullings[i].culleds[c].gameObject.SetActive(true);
                    }
                }
            }
        }

        private void EnableCulled(GameObject movedObj)
        {
            for (int i = 0; i < cullings.Count; i++)
            {
                for (int c = 0; c < cullings[i].covers.Count; c++)
                {
                    if (cullings[i].covers[c].gameObject == movedObj)
                    {
                        for (int d = 0; d < cullings[i].culleds.Count; d++)
                        {
                            cullings[i].culleds[d].gameObject.SetActive(true);
                        }
                        continue;
                    }
                }
            }
        }

        private void EnableAllCulled(bool enable)
        {
            for (int i = 0; i < cullings.Count; i++)
            {
                for (int c = 0; c < cullings[i].culleds.Count; c++)
                {
                    cullings[i].culleds[c].gameObject.SetActive(enable);
                }
            }
        }
    }
}
