﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using Valve.VR.Extras;
using DG.Tweening;

namespace VirtualTraining.Feature
{
    public class ObjectSelector : InitializationQueue
    {
        class ObjectData
        {
            public Transform transform;
            public Vector3 defaultPosition = new Vector3();
            public Vector3 offsetPosition = new Vector3();
            public float distanceToAnchor;
            public List<GameObject> childs = new List<GameObject>();
            public Vector3[] stayPositions;
            private Renderer renderer;

            private float resetTweenDuration;

            public ObjectData(GameObject obj, float resetTweenDuration)
            {
                this.resetTweenDuration = resetTweenDuration;

                defaultPosition = obj.transform.position;
                transform = obj.transform;

                Transform[] childs = obj.transform.GetAllChilds();
                for (int i = 0; i < childs.Length; i++)
                {
                    Renderer render = childs[i].GetComponent<Renderer>();
                    if (render != null)
                        this.childs.Add(childs[i].gameObject);
                }

                stayPositions = new Vector3[childs.Length];
                renderer = obj.GetComponent<Renderer>();
            }

            public void SetSelectedMaterial(Color selectedColor)
            {
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    renderer.materials[i].SetFloat("_Selected", 1);
                    renderer.materials[i].SetColor("_SelectedColor", selectedColor);
                }
            }

            public void ResetSelectedMaterial()
            {
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    renderer.materials[i].SetFloat("_Selected", 0);
                }
            }

            public void ResetPosition()
            {
                transform.DOMove(defaultPosition, resetTweenDuration);
            }

            public void ChildsStay()
            {
                for (int i = 0; i < childs.Count; i++)
                {
                    childs[i].transform.position = stayPositions[i];
                }
            }
        }

        // setting
        bool isPullApartActive;
        bool isMultipleSelectActive;
        Color selectedColor;

        // state
        bool isLeftClickDown;

        // cache
        List<GameObject> selectedObjects = new List<GameObject>();
        List<GameObject> hidedObjects = new List<GameObject>();
        public List<GameObject> movedObjects = new List<GameObject>();
        Dictionary<GameObject, ObjectData> objectLookup = new Dictionary<GameObject, ObjectData>();

        private void Awake()
        {
            VirtualTrainingInputSystem.OnStartLeftClick += StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick += EndLeftClickListener;
        }

        private void Start()
        {
            EventManager.AddListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.AddListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.AddListener<ObjectInteractionResetObjectEvent>(ResetPositionListener);
            EventManager.AddListener<ObjectInteractionVisibleEvent>(VisibleListener);
            selectedColor = DatabaseManager.GetDefaultSelectedColor();
        }

        private void OnDestroy()
        {
            VirtualTrainingInputSystem.OnStartLeftClick -= StartLeftClickListener;
            VirtualTrainingInputSystem.OnEndLeftClick -= EndLeftClickListener;

            EventManager.RemoveListener<ObjectInteractionMultipleSelectionEvent>(MultipleSelectionListener);
            EventManager.RemoveListener<ObjectInteractionPullApartEvent>(PullApartListener);
            EventManager.RemoveListener<ObjectInteractionResetObjectEvent>(ResetPositionListener);
            EventManager.RemoveListener<ObjectInteractionVisibleEvent>(VisibleListener);
        }

        private void MultipleSelectionListener(ObjectInteractionMultipleSelectionEvent e)
        {
            isMultipleSelectActive = e.on;
        }

        private void PullApartListener(ObjectInteractionPullApartEvent e)
        {
            isPullApartActive = e.on;
        }

        private void ResetPositionListener(ObjectInteractionResetObjectEvent e)
        {
            ResetObjectPosition();
            ClearSelectedColor();
            selectedObjects.Clear();
        }

        private void VisibleListener(ObjectInteractionVisibleEvent e)
        {
            if (e.visible)
            {
                for (int i = 0; i < hidedObjects.Count; i++)
                {
                    hidedObjects[i].SetActive(true);
                }
                hidedObjects.Clear();
                EventManager.TriggerEvent(new AddMovedObjectEvent());
            }
            else
            {
                for (int i = 0; i < selectedObjects.Count; i++)
                {
                    selectedObjects[i].SetActive(false);
                    hidedObjects.Add(selectedObjects[i]);
                    EventManager.TriggerEvent(new AddMovedObjectEvent(selectedObjects[i]));
                }

                if (hidedObjects.Count > 0)
                {
                    EventManager.TriggerEvent(new HideObjectEvent());
                }
            }
        }

        public override void Initialize(GameObject obj)
        {
            if (obj.GetComponent<Renderer>() != null)
            {
                ObjectData objData = new ObjectData(obj, DatabaseManager.GetResetObjectPositionDuration());
                objectLookup.Add(obj, objData);
            }
        }

        private void Update()
        {
            // pull apart
            if (isLeftClickDown && isPullApartActive && selectedObjects.Count > 0)
            {
                for (int i = 0; i < selectedObjects.Count; i++)
                {
                    // get selected object data
                    ObjectData objectData = objectLookup[selectedObjects[i].gameObject];
                    Vector3 pointerWorldPosition;
                    // set object position from pointer position
                    if (VirtualTrainingCamera.IsVR)
                    {
                        pointerWorldPosition = SteamVR_LaserPointer.hand.transform.position;
                        float x = SteamVR_LaserPointer.distance * objectData.distanceToAnchor;
                        selectedObjects[i].transform.position = pointerWorldPosition + SteamVR_LaserPointer.hand.transform.forward * x;
                    }
                    else
                    {
                        Vector2 pointerPosition = VirtualTrainingInputSystem.PointerPosition;
                        Vector3 pointerScreenPosition = new Vector3(pointerPosition.x, pointerPosition.y, objectData.distanceToAnchor);
                        pointerWorldPosition = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(pointerScreenPosition);
                        selectedObjects[i].transform.position = pointerWorldPosition + objectData.offsetPosition;
                    }

                    // set childs position to stay
                    for (int p = 0; p < objectData.childs.Count; p++)
                    {
                        if (!selectedObjects.Contains(objectData.childs[p]))
                            objectData.ChildsStay();
                    }
                }
            }
        }

        private void StartLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            if (raycastedUI != null)
                return;

            GameObjectReference gor = null;
            isLeftClickDown = true;

            if (isPullApartActive || isMultipleSelectActive)
            {
                RaycastHit hitInfo = new RaycastHit();

                if (VirtualTrainingCamera.Raycast(ref hitInfo))
                {
                    gor = hitInfo.transform.GetComponent<GameObjectReference>();

                    if (gor != null)
                    {
                        if (!isMultipleSelectActive)
                        {
                            ClearSelectedColor();
                            selectedObjects.Clear();
                        }

                        if (!selectedObjects.Contains(gor.gameObject))
                        {
                            selectedObjects.Add(gor.gameObject);
                        }

                        if (!movedObjects.Contains(gor.gameObject))
                        {
                            movedObjects.Add(gor.gameObject);
                        }

                    }

                    // update information for all selected objects
                    foreach (var selectedObject in selectedObjects)
                    {
                        SelectObject(selectedObject.gameObject);
                    }
                }
                else
                {
                    ClearSelectedColor();
                    selectedObjects.Clear();
                }
            }
            else
            {
                ClearSelectedColor();
                selectedObjects.Clear();
            }

            if (isLeftClickDown && isPullApartActive && selectedObjects.Count > 0)
                VirtualTrainingCamera.IsPulling = true;

            if (isPullApartActive && gor != null)
                EventManager.TriggerEvent(new AddMovedObjectEvent(gor.gameObject));
        }

        private void ClearSelectedColor()
        {
            for (int i = 0; i < selectedObjects.Count; i++)
            {
                objectLookup[selectedObjects[i]].ResetSelectedMaterial();
            }
        }

        private void ResetObjectPosition()
        {
            for (int i = 0; i < movedObjects.Count; i++)
            {
                if (objectLookup.ContainsKey(movedObjects[i]))
                {
                    ObjectData data = objectLookup[movedObjects[i]];
                    data.ResetPosition();
                    for (int j = 0; j < data.childs.Count; j++)
                    {
                        objectLookup[data.childs[j]].ResetPosition();
                    }
                }
            }

            movedObjects.Clear();
            StartCoroutine(ResetMovedObject());
        }

        private IEnumerator ResetMovedObject()
        {
            yield return new WaitForSeconds(DatabaseManager.GetResetObjectPositionDuration());
            EventManager.TriggerEvent(new AddMovedObjectEvent());
        }

        private void SelectObject(GameObject obj)
        {
            if (objectLookup.ContainsKey(obj))
            {
                // calculate offset & distance to camera
                float distanceToAnchor;
                Vector3 pointerWorldPosition;
                Vector2 pointerPosition = VirtualTrainingInputSystem.PointerPosition;

                if (VirtualTrainingCamera.IsVR)
                {
                    SteamVR_LaserPointer.distance = 1;
                    distanceToAnchor = Vector3.Distance(SteamVR_LaserPointer.hand.transform.position, obj.transform.position);
                    pointerWorldPosition = SteamVR_LaserPointer.hand.transform.position;
                }
                else
                {
                    distanceToAnchor = Vector3.Distance(VirtualTrainingCamera.CameraPosition, obj.transform.position);
                    Vector3 pointerScreenPosition = new Vector3(pointerPosition.x, pointerPosition.y, distanceToAnchor);
                    pointerWorldPosition = VirtualTrainingCamera.CurrentCamera.ScreenToWorldPoint(pointerScreenPosition);
                }

                Vector3 offsetPosition = obj.transform.position - pointerWorldPosition;

                // set object data
                ObjectData selectedObject = objectLookup[obj];

                selectedObject.distanceToAnchor = distanceToAnchor;
                selectedObject.offsetPosition = offsetPosition;

                for (int i = 0; i < selectedObject.childs.Count; i++)
                {
                    selectedObject.stayPositions[i] = selectedObject.childs[i].transform.position;
                }

                selectedObject.SetSelectedMaterial(selectedColor);
            }
        }

        private void EndLeftClickListener(Vector2 pointerPosition, GameObject raycastedUI)
        {
            isLeftClickDown = false;
            VirtualTrainingCamera.IsPulling = false;
        }
    }
}
