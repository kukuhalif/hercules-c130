using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

public class VirtualButtonDrag : VirtualButtonBase
{
    bool SetParamDrag;
    protected Vector2 pointerStart;
    protected Vector3 startPositionPointer;
    public float sliderCurrentValue;
    protected float pointerDeltaPosition;
    float x;
    float y;
    bool isPlaySequent;
    float valueSequential;
    int valueTemp;
    int sequentialValue;
    float deltaPositionAnimationX;
    float deltaPositionAnimationY;

    public List<ParameterSequentialData> parameterSequentialDatasInteraction = new List<ParameterSequentialData>();
    public List<ParameterSequentialData> parameterSequentialDatasFeedback = new List<ParameterSequentialData>();
    protected override void SetupVirtualButton()
    {
        base.SetupVirtualButton();
        valueTemp = 0;
        if (virtualButtonDatas.isSequentialDrag)
        {
            SetupAnimationPlayerBase(animationPlayerContainerInteraction, parameterSequentialDatasInteraction);
            SetupAnimationFeedback(animationPlayerContainerFeedback);
        }
    }

    void SetupAnimationPlayerBase(List<AnimationPlayerBase> animationPlayerBases, List<ParameterSequentialData> parameterSequentialDatas)
    {
        for (int i = 0; i < animationPlayerBases.Count; i++)
        {
            if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
            {
                for (int j = 0; j < animationPlayerBases[i].getVBOutputData.parameterDatas.Count; j++)
                {
                    ParameterSequentialData paramSequentData = new ParameterSequentialData();
                    paramSequentData.animationPlayerBase = animationPlayerBases[i];
                    paramSequentData.parameterDatasInteraction = animationPlayerBases[i].getVBOutputData.parameterDatas[j];
                    parameterSequentialDatas.Add(paramSequentData);

                }
            }
            else if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                {
                    ParameterSequentialData paramSequentData = new ParameterSequentialData();
                    paramSequentData.animationPlayerBase = animationPlayerBases[i];
                    paramSequentData.animationState = animationPlayerBases[i].getVBOutputData.animationDatas[l].animationState;
                    parameterSequentialDatas.Add(paramSequentData);
                }
            }
        }
    }

    void SetupAnimationFeedback(List<AnimationPlayerBase> animationPlayerBases)
    {
        for (int i = 0; i < animationPlayerBases.Count; i++)
        {
            if (animationPlayerBases[i].getVBOutputData.isAnimationScript)
            {
                ParameterSequentialData paramSequentData = new ParameterSequentialData();
                paramSequentData.animationPlayerBase = animationPlayerBases[i];
                parameterSequentialDatasFeedback.Add(paramSequentData);
            }
            else
            {
                AnimationPlayerManual animManual = animationPlayerBases[i] as AnimationPlayerManual;
                if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                {
                    for (int j = 0; j < animManual.getVBOutputData.parameterDatas.Count; j++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.parameterDatasInteraction = animManual.getVBOutputData.parameterDatas[j];
                        parameterSequentialDatasFeedback.Add(paramSequentData);

                    }
                }
                else
                {
                    for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.animationState = animManual.getVBOutputData.animationDatas[l].animationState;
                        parameterSequentialDatasFeedback.Add(paramSequentData);
                    }
                }
            }
        }
    }

    public void SetDeltaPositionAnimation(float valueX, float valueY)
    {
        deltaPositionAnimationX = valueX;
        deltaPositionAnimationY = valueY;
    }

    private void Update()
    {
        if (leftClickDown)
            PlayVBDrag();

        if ((virtualButtonDatas.vbType == VirtualButtonType.Click && !virtualButtonDatas.isSequentialClick) || (virtualButtonDatas.vbType == VirtualButtonType.Drag && !virtualButtonDatas.isSequentialDrag))
            ElasticVBDrag();

        if (virtualButtonDatas.isSequentialDrag)
            CheckSliderSequentData();
    }

    public override void TriggerVirtualButton()
    {
        pointerStart = VirtualTrainingInputSystem.PointerPosition;
        if (clickTargetCurrent != null)
            startPositionPointer = new Vector3(VirtualTrainingInputSystem.PointerPosition.x, VirtualTrainingInputSystem.PointerPosition.y, Vector3.Distance(Camera.main.transform.position, clickTargetCurrent.transform.position));
        sliderCurrentValue = sliderValue;
        leftClickDown = true;
        SetParamDrag = false;
        sequentialValue = 1;
    }

    void PlayVBDrag()
    { 
        Vector3 screenPos;
        Vector3 deltaPos = Vector3.zero;

        if(clickTargetCurrent != null)
        {
            screenPos = new Vector3(VirtualTrainingInputSystem.PointerPosition.x, VirtualTrainingInputSystem.PointerPosition.y, Vector3.Distance(Camera.main.transform.position, clickTargetCurrent.transform.position));
            deltaPos = Camera.main.ScreenToWorldPoint(screenPos) - Camera.main.ScreenToWorldPoint(startPositionPointer);
        }

        if (virtualButtonDatas.direction == DragDirection.Horizontal)
        {
            if (deltaPositionAnimationX == 0)
            {
                x = CheckX(deltaPos);
                pointerDeltaPosition = x / virtualButtonDatas.deltaPositionX;
            }
            else
            {
                x = (deltaPositionAnimationX * virtualButtonDatas.deltaPositionX) + virtualButtonDatas.deltaPositionX;
                pointerDeltaPosition = deltaPositionAnimationX;
            }
        }
        else if (virtualButtonDatas.direction == DragDirection.Vertical)
        {
            if (deltaPositionAnimationY == 0)
            {
                y = CheckY(deltaPos);
                pointerDeltaPosition = y / virtualButtonDatas.deltaPositionY;
            }
            else
            {
                y = (deltaPositionAnimationY * virtualButtonDatas.deltaPositionY) + virtualButtonDatas.deltaPositionY;
                pointerDeltaPosition = deltaPositionAnimationY;
            }
        }
        else if (virtualButtonDatas.direction == DragDirection.Both)
        {
            x = CheckX(deltaPos);
            x = x / virtualButtonDatas.deltaPositionX;
            y = CheckY(deltaPos);
            y = y / virtualButtonDatas.deltaPositionY;
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                AnimationPlayerManual animationManual = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                animationManual.SetBothDrag(x, y);
                if (SetParamDrag == false)
                {
                    animationManual.setCurrentParam = false;
                }
            }
            StartCoroutine(PlayBothFeedback(x, y));

            SetParamDrag = true;
            pointerDeltaPosition = 1;
        }

        if (IsInvert)
            pointerDeltaPosition = pointerDeltaPosition * -1;

        if (sliderValue >= 0)
        {
            if (virtualButtonDatas.isSequentialDrag)
            {
                PlaySequentialDrag();
                Debug.Log("play sequential");
            }
            else
            {
                sliderValue = sliderCurrentValue + pointerDeltaPosition;
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].SetSliderPercentage(sliderValue);

                    if (!string.IsNullOrEmpty(animationPlayerContainerInteraction[i].getVBOutputData.animationState) || animationPlayerContainerInteraction[i].getVBOutputData.isAnimationScript)
                        animationPlayerContainerInteraction[i].StartPlayAnimation(sliderValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }
                StartCoroutine(PlayDelayAnimationFeedbackDrag(sliderValue));
            }
        }
        if (sliderValue <= 0)
            sliderValue = 0;
        if (sliderValue > 1)
            sliderValue = 1;
    }
    IEnumerator PlayDelayAnimationFeedbackDrag(float sliderValue)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            animationPlayerContainerFeedback[i].SetSliderPercentage(sliderValue);

            if (virtualButtonDatas.isElastic)
                animationPlayerContainerFeedback[i].StartPlayAnimation(sliderValue, virtualButtonDatas.elasticSpeed);
            else if(!string.IsNullOrEmpty(animationPlayerContainerFeedback[i].getVBOutputData.animationState) || animationPlayerContainerFeedback[i].getVBOutputData.isAnimationScript)
                animationPlayerContainerFeedback[i].StartPlayAnimation(sliderValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
        }
    }
    void PlaySequentialDrag()
    {
        switch (virtualButtonDatas.sequencePlayMethod)
        {
            case SequencePlayMethod.TimeStampParameter:
                SequentialDragTimeStamp();
                break;
            case SequencePlayMethod.TimeStampScriptedAnimation:
                SequentialDragTimeStamp();
                break;
            case SequencePlayMethod.TimeStampState:
                SequentialDragTimeStamp();
                break;
            case SequencePlayMethod.PlayParameter:
                PlaySequentialManual(true);
                break;
            case SequencePlayMethod.PlayScriptedAnimation:
                PlaySequentialAnimationScript();
                break;
            case SequencePlayMethod.PlayState:
                PlaySequentialManual(false);
                break;
        }
    }

    void SequentialDragTimeStamp()
    {
        for (int i = 0; i < virtualButtonDatas.sequentialDragDatas.Count; i++)
        {
            if (pointerDeltaPosition > 0)
            {
                if (pointerDeltaPosition + sliderCurrentValue >= virtualButtonDatas.sequentialDragDatas[i].durations)
                {
                    if (valueSequential < virtualButtonDatas.sequentialDragDatas[i].durations)
                        isPlaySequent = false;

                    PlayAllAnimationPlayer(i);
                }
            }
            else if (pointerDeltaPosition < 0)
            {
                if (pointerDeltaPosition + sliderCurrentValue < 0)
                {
                    if (valueSequential > 0)
                        isPlaySequent = false;
                    PlayAllAnimationPlayer(-1);
                }
                else if (pointerDeltaPosition + sliderCurrentValue <= virtualButtonDatas.sequentialDragDatas[i].durations)
                {
                    if (valueSequential > virtualButtonDatas.sequentialDragDatas[i].durations)
                        isPlaySequent = false;

                    PlayAllAnimationPlayer(i);
                    break;
                }
            }
        }
    }

    void CheckSliderSequentData()
    {
        for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
        {
            sliderValue = animationPlayerContainerInteraction[i].sliderValue;
            if (pointerDeltaPosition > 0)
            {
                if (animationPlayerContainerInteraction[i].sliderValue < sliderValue)
                    sliderValue = animationPlayerContainerInteraction[i].sliderValue;
            }
            else
            {
                if (animationPlayerContainerInteraction[i].sliderValue > sliderValue)
                    sliderValue = animationPlayerContainerInteraction[i].sliderValue;
            }
        }
    }

    void PlayAllAnimationPlayer(int valueTemp)
    {
        if (isPlaySequent == false)
        {
            if (valueTemp == -1)
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(0, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }
                valueSequential = 0;
                PlayEffect(0);
                StartCoroutine(PlayDelaySequentialFeedback(0));
            }
            else
            {
                valueSequential = virtualButtonDatas.sequentialDragDatas[valueTemp].durations;
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].StartPlayAnimation(valueSequential, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                }
                PlayEffect(valueSequential);
                float valueSequential2 = virtualButtonDatas.sequentialDragDatas[valueTemp].durationSecond;

                if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferenceValue)
                    StartCoroutine(PlayDelaySequentialFeedback(valueSequential2));
                else
                    StartCoroutine(PlayDelaySequentialFeedback(valueSequential));
            }

            isPlaySequent = true;
        }
    }

    IEnumerator PlayDelaySequentialFeedback(float maxValue)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            yield return new WaitUntil(() => sliderValue == maxValue);
        else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            animationPlayerContainerFeedback[i].StartPlayAnimation(maxValue, animationPlayerContainerFeedback[i].getVBOutputData.speed);
        }
    }

    void PlaySequentialManual(bool isParameter)
    {
        float deltaPosition;

        if (virtualButtonDatas.direction == DragDirection.Horizontal)
            deltaPosition = virtualButtonDatas.deltaPositionX;
        else
            deltaPosition = virtualButtonDatas.deltaPositionY;

        if (pointerDeltaPosition < 0)
        {
            if (valueTemp > 0 && x < -deltaPosition * (sequentialValue))
            {
                valueTemp--;
                PlayAnimationManual(valueTemp, 0, isParameter, 1);
                sequentialValue++;

            }
        }
        else
        {
            if (valueTemp < parameterSequentialDatasInteraction.Count && x > deltaPosition * (sequentialValue))
            {
                PlayAnimationManual(valueTemp, 1, isParameter, 0);
                valueTemp++;
                sequentialValue++;
            }
        }
    }

    void PlayAnimationManual(int valueTemp, int maxValue, bool isParameter, float startSlider = 0)
    {
        AnimationPlayerManual animManual = parameterSequentialDatasInteraction[valueTemp].animationPlayerBase as AnimationPlayerManual;
        if (isParameter)
        {
            animManual.getVBOutputData.animationParameter = parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.parameter;
            animManual.SetMinMaxValue(parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.minimalValue, parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.maximalValue);
            animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
        }
        else
        {
            animManual.SetAnimName(parameterSequentialDatasInteraction[valueTemp].animationState);
            animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
        }
        StartCoroutine(DelayPlayAnimationManualFeedback(animManual, maxValue, valueTemp, startSlider));
    }

    IEnumerator DelayPlayAnimationManualFeedback(AnimationPlayerBase animBase, float maxValue, int valueTemp, float startSlider = 0)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
        {
            if (animBase.getVBOutputData.isAnimationScript)
                yield return new WaitUntil(() => animBase.sliderValue == maxValue);
            else
            {
                AnimationPlayerManual animManual = animBase as AnimationPlayerManual;
                yield return new WaitUntil(() => animManual.getSliderPercentageTemp == maxValue);
            }
        }
        else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        if (valueTemp < parameterSequentialDatasFeedback.Count)
        {
            if (parameterSequentialDatasFeedback[valueTemp].animationPlayerBase.getVBOutputData.isAnimationScript)
            {
                AnimationPlayerScript animScript = parameterSequentialDatasFeedback[valueTemp].animationPlayerBase as AnimationPlayerScript;
                animScript.StartPlayAnimation(maxValue, animScript.getVBOutputData.speed);
            }
            else
            {
                AnimationPlayerManual animManual = parameterSequentialDatasFeedback[valueTemp].animationPlayerBase as AnimationPlayerManual;
                if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                {
                    Debug.Log("parameter " + valueTemp + " " + maxValue);
                    animManual.getVBOutputData.animationParameter = parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.parameter;
                    animManual.SetMinMaxValue(parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.minimalValue, parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.maximalValue);
                    animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
                }
                else
                {
                    Debug.Log("state");
                    animManual.SetAnimName(parameterSequentialDatasFeedback[valueTemp].animationState);
                    animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
                }
            }
        }
    }

    void PlaySequentialAnimationScript()
    {
        if (pointerDeltaPosition < 0)
        {
            if (valueTemp > 0 && x < -virtualButtonDatas.deltaPositionX * (sequentialValue))
            {
                valueTemp--;
                animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 0, valueTemp, 1));
                sequentialValue++;
            }
        }
        else
        {
            if (valueTemp < animationPlayerContainerInteraction.Count && x > virtualButtonDatas.deltaPositionX * (sequentialValue))
            {
                animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp, 0));
                valueTemp++;
                sequentialValue++;
            }
        }
    }

    IEnumerator PlayBothFeedback(float x, float y)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            AnimationPlayerManual animationManual = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
            animationManual.SetBothDrag(x, y);
            if (SetParamDrag == false)
            {
                animationManual.setCurrentParam = false;
            }
        }
    }

    void ElasticVBDrag()
    {
        if (virtualButtonDatas.isElastic == true && leftClickDown == false && !virtualButtonDatas.isSequentialDrag)
        {
            if (virtualButtonDatas.direction == DragDirection.Both)
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    AnimationPlayerManual animManual = animationPlayerContainerInteraction[i] as AnimationPlayerManual;
                    animManual.SetElasticBothDrag(virtualButtonDatas.elasticSpeed);
                }
                StartCoroutine(DelayElasticDragBoth());

            }
            else
            {
                sliderValue = Mathf.Lerp(sliderValue, virtualButtonDatas.defaultValue, virtualButtonDatas.elasticSpeed * Time.deltaTime);
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].SetSliderPercentage(sliderValue);
                    animationPlayerContainerInteraction[i].StartPlayAnimation(sliderValue, virtualButtonDatas.elasticSpeed);
                }
                StartCoroutine(PlayDelayAnimationFeedbackDrag(sliderValue));

            }
        }
        else
        {
            if(leftClickDown == false)
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    animationPlayerContainerInteraction[i].StopAnimation();
                }
                StartCoroutine(StopAnimationFeedback());
            }
        }
    }
    IEnumerator StopAnimationFeedback()
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            animationPlayerContainerFeedback[i].StopAnimation();
        }
    }
    IEnumerator DelayElasticDragBoth()
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);


        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            AnimationPlayerManual animManual = animationPlayerContainerFeedback[i] as AnimationPlayerManual;
            animManual.SetElasticBothDrag(virtualButtonDatas.elasticSpeed);
        }
    }

    protected float CheckX(Vector3 deltaPos)
    {
        if (virtualButtonDatas.deltaPositionX > 0)
        {
            if (deltaPos.x > 0)
                x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x);
            else
                x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x) * -1;
        }
        else
        {
            if (deltaPos.x < 0)
                x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x) * -1;
            else
                x = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.x - pointerStart.x);
        }
        return x;
    }

    protected float CheckY(Vector3 deltaPos)
    {
        if (virtualButtonDatas.deltaPositionY > 0)
        {
            if (deltaPos.y > 0)
                y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y);
            else
                y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y) * -1;
        }
        else
        {
            if (deltaPos.y < 0)
                y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y) * -1;
            else
                y = Mathf.Abs(VirtualTrainingInputSystem.PointerPosition.y - pointerStart.y);
        }

        return y;
    }

    public override void PlayVirtualButton()
    {
        Debug.Log("play virtual button");
    }

    public override void StopVirtualButton()
    {
        Debug.Log("play virtual button");
    }
}
