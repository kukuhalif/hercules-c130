using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public abstract class ModifierBase : MonoBehaviour
    {
        [SerializeField]
        VirtualbuttonModifierData modifierData;

        public VirtualbuttonModifierData ModifierData { get => modifierData; }

        public void SetModifierData(VirtualbuttonModifierData modifier)
        {
            modifierData = modifier;
        }
        public virtual void DestroyModifier()
        {
            Destroy(this);
        }
    }
}
