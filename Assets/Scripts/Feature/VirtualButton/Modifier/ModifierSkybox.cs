﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class ModifierSkybox : ModifierBase
    {
        VirtualButtonBase virtualButtonBase;
        public float sliderValue;

        float currentExposure;
        float startExposure;
        float startambientIntensity;
        float currentIntensityMultiplier;
        bool isChanging;
        bool isReset;
        Vector3 DestinationRotation;
        Vector3 StartRotation;

        float currentSlider;
        // Start is called before the first frame update
        void Start()
        {
            currentExposure = RenderSettings.skybox.GetFloat("_Exposure");
            currentIntensityMultiplier = RenderSettings.ambientIntensity;
            startExposure = currentExposure;
            startambientIntensity = currentIntensityMultiplier;
            virtualButtonBase = GetComponent<VirtualButtonBase>();
            if (ModifierData.defaultCondition == DefaultCondition.On)
            {
                if (ModifierData.skyBoxType == VBSKyboxType.Click)
                {
                    isChanging = true;
                }
            }
            if (ModifierData.light.gameObject != null)
            {
                StartRotation = ModifierData.light.gameObject.transform.localEulerAngles;
                DestinationRotation = new Vector3(ModifierData.lightTransformX, ModifierData.lightTransformY, ModifierData.lightTransformZ);
            }
        }

        void ChangeSkybox()
        {
            for (int i = 0; i < ModifierData.modifierConditionValues.Count; i++)
            {
                if (ModifierData.modifierConditionValues[i].minValue <= virtualButtonBase.sliderValue && ModifierData.modifierConditionValues[i].maxValue >= virtualButtonBase.sliderValue)
                {
                    if (ModifierData.skyBoxType == VBSKyboxType.Click)
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            isChanging = true;
                        else
                            isChanging = false;
                        isReset = false;
                    }
                }
                else
                {
                    if (ModifierData.skyBoxType == VBSKyboxType.Click)
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            isChanging = false;
                        else
                            isChanging = true;
                    }
                    else
                        ResetSkybox();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            ChangeSkybox();
            if (ModifierData.skyBoxType == VBSKyboxType.Click)
            {
                ChanginsKyboxClick();
            }
            else
            {
                currentExposure = Mathf.Lerp(startExposure, ModifierData.dayExposure, virtualButtonBase.sliderValue);
                currentIntensityMultiplier = Mathf.Lerp(startambientIntensity, ModifierData.dayIntensityMultiplier, virtualButtonBase.sliderValue);
                if (ModifierData.light.gameObject != null)
                {
                    ModifierData.light.gameObject.transform.localEulerAngles = Vector3.Lerp(StartRotation, DestinationRotation, virtualButtonBase.sliderValue);
                }

                RenderSettings.skybox.SetFloat("_Exposure", currentExposure);
                RenderSettings.ambientIntensity = currentIntensityMultiplier;
            }
        }
        void ChanginsKyboxClick()
        {
            if (isChanging)
            {
                if (currentSlider < 1)
                {
                    currentSlider += (Time.deltaTime * ModifierData.speed) / 1;
                    currentExposure = Mathf.Lerp(currentExposure, ModifierData.dayExposure, currentSlider);
                    currentIntensityMultiplier = Mathf.Lerp(startambientIntensity, ModifierData.dayIntensityMultiplier, currentSlider);
                    if (ModifierData.light.gameObject != null)
                    {
                        ModifierData.light.gameObject.transform.localEulerAngles = Vector3.Lerp(StartRotation, DestinationRotation, currentSlider);
                    }
                }

                RenderSettings.skybox.SetFloat("_Exposure", currentExposure);
                RenderSettings.ambientIntensity = currentIntensityMultiplier;
            }
            else
            {
                if (currentSlider > 0)
                {
                    currentSlider -= (Time.deltaTime * ModifierData.speed) / 1;
                    currentExposure = Mathf.Lerp(startExposure, currentExposure, currentSlider);
                    currentIntensityMultiplier = Mathf.Lerp(startambientIntensity, currentIntensityMultiplier, currentSlider);
                    if (ModifierData.light.gameObject != null)
                    {
                        ModifierData.light.gameObject.transform.localEulerAngles = Vector3.Lerp(DestinationRotation, StartRotation, currentSlider);
                    }
                }
                RenderSettings.skybox.SetFloat("_Exposure", currentExposure);
                RenderSettings.ambientIntensity = currentIntensityMultiplier;
            }
        }
        public void ResetSkybox()
        {
            if (isReset == false)
            {
                RenderSettings.skybox.SetFloat("_Exposure", startExposure);
                RenderSettings.ambientIntensity = startambientIntensity;
                if (ModifierData.light.gameObject != null)
                {
                    ModifierData.light.gameObject.transform.localEulerAngles = StartRotation;
                }

                isReset = true;
            }
        }
        public override void DestroyModifier()
        {
            base.DestroyModifier();
            ResetSkybox();
        }
    }
}
