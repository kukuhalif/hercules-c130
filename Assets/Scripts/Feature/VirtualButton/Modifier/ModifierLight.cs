﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class ModifierLight : ModifierBase
    {

        List<Color> startColors = new List<Color>();
        List<Color> startEmmisionColor = new List<Color>();

        float timeElapsed;
        public float sliderValue;
        float timeElapsedColor;
        bool intesityChanging;
        bool colorChanged;

        public Material[] gameObjectMaterials;
        public List<Material> materials = new List<Material>();
        bool isStartBlink;
        bool isStartColorChanging;
        float intensity;
        public VirtualButtonBase virtualButtonBase;

        void Start()
        {
            virtualButtonBase = GetComponent<VirtualButtonBase>();
            CheckMaterial();
        }

        void CheckMaterial()
        {
            for (int i = 0; i < ModifierData.interactionObjectDatas.Count; i++)
            {
                materials.AddRange(ModifierData.interactionObjectDatas[i].gameObject.gameObject.GetComponent<MeshRenderer>().materials);
                if (ModifierData.interactionObjectDatas[i].gameObject.gameObject != null)
                {
                    if (ModifierData.defaultCondition == DefaultCondition.Off)
                        ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(false);
                    else
                        ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(true);
                }
            }
            gameObjectMaterials = materials.ToArray();


            for (int i = 0; i < gameObjectMaterials.Length; i++)
            {
                startColors.Add(gameObjectMaterials[i].color);
                startEmmisionColor.Add(gameObjectMaterials[i].GetColor("_EmissionColor"));
            }

            if (ModifierData.defaultCondition == DefaultCondition.On)
                StartModifierLight();

        }
        void Update()
        {
            sliderValue = Mathf.Round(virtualButtonBase.sliderValue * 10f) / 10f;
            if (isStartBlink)
                IntensityChanging();
            if (isStartColorChanging)
                ColorChanging();

            if (ModifierData.isBlink || ModifierData.isChangeColor)
            {
                for (int i = 0; i < ModifierData.modifierConditionValues.Count; i++)
                {
                    if (sliderValue >= ModifierData.modifierConditionValues[i].minValue && sliderValue <= ModifierData.modifierConditionValues[i].maxValue)
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            StartModifierLight();
                        else
                            StopModifierLight();
                    }
                    else
                    {
                        if (ModifierData.defaultCondition == DefaultCondition.Off)
                            StopModifierLight();
                        else
                            StartModifierLight();
                    }
                }
            }
        }

        public void StartModifierLight()
        {
            if (ModifierData.isBlink)
                isStartBlink = true;
            if (ModifierData.isChangeColor)
                isStartColorChanging = true;
        }

        public void StopModifierLight()
        {
            if (ModifierData.isBlink)
                isStartBlink = false;
            if (ModifierData.isChangeColor)
                isStartColorChanging = false;

            if (gameObjectMaterials != null)
            {
                for (int i = 0; i < gameObjectMaterials.Length; i++)
                {
                    gameObjectMaterials[i].color = startColors[i];
                    gameObjectMaterials[i].SetColor("_EmissionColor", startEmmisionColor[i]);
                }
            }
        }

        void IntensityChanging()
        {
            if (ModifierData.isBlink)
            {
                if (timeElapsed <= ModifierData.delayTimeBlink && intesityChanging == false)
                {
                    intensity = Mathf.Lerp(ModifierData.minIntensity, ModifierData.maxIntensity, timeElapsed / ModifierData.delayTimeBlink);
                    for (int i = 0; i < gameObjectMaterials.Length; i++)
                    {
                        gameObjectMaterials[i].SetColor("_EmissionColor", new Color(gameObjectMaterials[i].color.r, gameObjectMaterials[i].color.g, gameObjectMaterials[i].color.b, 1.0F) * intensity);
                    }

                    timeElapsed += Time.deltaTime;

                    if (timeElapsed > ModifierData.delayTimeBlink)
                    {
                        intesityChanging = true;
                        timeElapsed = 0;
                    }
                }
                else if (timeElapsed <= ModifierData.delayTimeBlink && intesityChanging == true)
                {
                    intensity = Mathf.Lerp(ModifierData.maxIntensity, ModifierData.minIntensity, timeElapsed / ModifierData.delayTimeBlink);
                    for (int i = 0; i < gameObjectMaterials.Length; i++)
                    {
                        gameObjectMaterials[i].SetColor("_EmissionColor", new Color(gameObjectMaterials[i].color.r, gameObjectMaterials[i].color.g, gameObjectMaterials[i].color.b, 1.0F) * intensity);
                    }
                    timeElapsed += Time.deltaTime;

                    if (timeElapsed > ModifierData.delayTimeBlink)
                    {
                        intesityChanging = false;
                        timeElapsed = 0;
                    }
                }
            }
            else
            {
                intensity = Mathf.Lerp(ModifierData.minIntensity, ModifierData.maxIntensity, sliderValue);
                for (int i = 0; i < gameObjectMaterials.Length; i++)
                {
                    gameObjectMaterials[i].SetColor("_EmissionColor", new Color(gameObjectMaterials[i].color.r, gameObjectMaterials[i].color.g, gameObjectMaterials[i].color.b, 1.0F) * intensity);
                }
            }
        }

        void ColorChanging()
        {
            if (ModifierData.isChangeColor)
            {
                if (timeElapsedColor <= ModifierData.delayTimeColor && colorChanged == false)
                {
                    for (int i = 0; i < gameObjectMaterials.Length; i++)
                    {
                        gameObjectMaterials[i].color = Color.Lerp(ModifierData.color1, ModifierData.color2, timeElapsedColor / ModifierData.delayTimeColor);
                    }
                    timeElapsedColor += Time.deltaTime;

                    if (timeElapsedColor > ModifierData.delayTimeColor)
                    {
                        timeElapsedColor = 0;
                        colorChanged = true;
                    }
                }
                else if (timeElapsedColor <= ModifierData.delayTimeColor && colorChanged == true)
                {
                    for (int i = 0; i < gameObjectMaterials.Length; i++)
                    {
                        gameObjectMaterials[i].color = Color.Lerp(ModifierData.color2, ModifierData.color1, timeElapsedColor / ModifierData.delayTimeColor);
                    }
                    timeElapsedColor += Time.deltaTime;

                    if (timeElapsedColor > ModifierData.delayTimeColor)
                    {
                        timeElapsedColor = 0;
                        colorChanged = false;
                    }
                }
            }
            else
            {
                for (int i = 0; i < gameObjectMaterials.Length; i++)
                {
                    gameObjectMaterials[i].color = Color.Lerp(ModifierData.color1, ModifierData.color2, sliderValue);
                }
            }
        }

        public override void DestroyModifier()
        {
            base.DestroyModifier();
            StopModifierLight();
            for (int i = 0; i < ModifierData.interactionObjectDatas.Count; i++)
            {
                if (ModifierData.interactionObjectDatas[i].gameObject.gameObject != null)
                {
                    if (ModifierData.defaultCondition == DefaultCondition.Off)
                    {
                        ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(false);
                    }
                    else
                        ModifierData.interactionObjectDatas[i].gameObject.gameObject.SetActive(true);
                }
            }
        }
    }
}
