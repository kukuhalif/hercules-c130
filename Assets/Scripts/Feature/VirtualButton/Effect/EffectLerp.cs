﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class EffectLerp : MonoBehaviour
    {
        public VirtualButtonEffectData vbEffectData;
        public Animator animator;
        public float sliderValue;
        float speedTemp;
        float currentValue;
        float startValue;
        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            if (animator != null)
                animator.enabled = true;
            if (!string.IsNullOrEmpty(vbEffectData.parameter))
                currentValue = animator.GetFloat(vbEffectData.parameter);
            startValue = currentValue;
        }
        void Calculate()
        {
            if (!string.IsNullOrEmpty(vbEffectData.parameter))
            {
                speedTemp = Mathf.Lerp(vbEffectData.minValue, vbEffectData.maxValue, sliderValue);
                animator.SetFloat(vbEffectData.parameter, Mathf.Lerp(currentValue, speedTemp, Time.deltaTime * vbEffectData.speedAcc));
                currentValue = animator.GetFloat(vbEffectData.parameter);
            }
        }

        // Update is called once per frame
        void Update()
        {
            Calculate();
        }

        public void DestroyLerp()
        {
            if (animator != null)
            {
                animator.SetFloat(vbEffectData.parameter, startValue);
                //animator.Play(vbEffectData.animationState, -1, 0);
                //animator.Play("Default", -1, 0);
            }
            StartCoroutine(DelayDestroy());
        }

        IEnumerator DelayDestroy()
        {
            yield return new WaitForSeconds(.2f);
            if (animator != null)
                animator.enabled = false;

            DestroyImmediate(this);
        }
    }
}
