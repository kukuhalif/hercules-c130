using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;
public abstract class VirtualButtonBase : MonoBehaviour
{
    bool isInvert;
    public VirtualButtonDataModel virtualButtonDatas;
    protected bool leftClickDown;
    public GameObject clickTargetCurrent;
    public float sliderValue;
    [SerializeField] List<GameObjectType> fresnelObjects = new List<GameObjectType>();
    public bool IsInvert { get => isInvert; }

    public List<VBStartAnimationData> vbStartAnimationDatas = new List<VBStartAnimationData>();
    public List<ClickTargetData> clickTargetDatas = new List<ClickTargetData>();
    public List<AnimationPlayerBase> animationPlayerContainerInteraction = new List<AnimationPlayerBase>();
    public List<AnimationPlayerBase> animationPlayerContainerFeedback = new List<AnimationPlayerBase>();
    public List<AnimationPlayerBase> animationOnStarts = new List<AnimationPlayerBase>();
    public List<EffectLerp> effectLerpContainer = new List<EffectLerp>();
    public List<ModifierBase> modifierSetActiveContainer = new List<ModifierBase>();

    // Start is called before the first frame update
    void Start()
    {
        fresnelObjects.Clear();
        VirtualTrainingInputSystem.OnStartLeftClick += OnStartLeftClick;
        VirtualTrainingInputSystem.OnEndLeftClick += OnEndLeftClick;
        EventManager.AddListener<MonitorCameraInteractionEvent>(InteractionEventListener);
        for (int i = 0; i < virtualButtonDatas.clickTargetDatas.Count; i++)
        {
            fresnelObjects.Add(virtualButtonDatas.clickTargetDatas[i].gameObject);
        }

        EventManager.TriggerEvent (new FresnelObjectsEvent(fresnelObjects,true,true));
        SetupVirtualButton();
    }

    private void OnDestroy()
    {
        VirtualTrainingInputSystem.OnStartLeftClick -= OnStartLeftClick;
        VirtualTrainingInputSystem.OnEndLeftClick -= OnEndLeftClick;
        EventManager.RemoveListener<MonitorCameraInteractionEvent>(InteractionEventListener);
        EventManager.TriggerEvent(new FresnelObjectsEvent());
        for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
        {
            if (animationPlayerContainerInteraction[i] != null)
                animationPlayerContainerInteraction[i].DestroyItself();
        }
        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            if (animationPlayerContainerFeedback[i] != null)
                animationPlayerContainerFeedback[i].DestroyItself();
        }
        for (int k = 0; k < animationOnStarts.Count; k++)
        {
            if (animationOnStarts[k] != null)
                animationOnStarts[k].DestroyItself();
        }
        for (int j = 0; j < effectLerpContainer.Count; j++)
        {
            if (effectLerpContainer[j] != null)
                effectLerpContainer[j].DestroyLerp();
        }

        for (int e = 0; e < modifierSetActiveContainer.Count; e++)
        {
            if (modifierSetActiveContainer[e] != null)
                modifierSetActiveContainer[e].DestroyModifier();
        }
    }

    public abstract void TriggerVirtualButton();

    public abstract void PlayVirtualButton();
    public abstract void StopVirtualButton();

    protected virtual void OnStartLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
    {
        if (raycastedUI != null)
            return;

        RaycastHit hitInfo = new RaycastHit();
        if (VirtualTrainingCamera.Raycast(ref hitInfo))
        {
            for (int i = 0; i < clickTargetDatas.Count; i++)
            {
                if (clickTargetDatas[i].gameObject.gameObject == hitInfo.transform.gameObject)
                {
                    VirtualTrainingCamera.IsMovementEnabled = false;
                    clickTargetCurrent = hitInfo.transform.gameObject;
                    isInvert = clickTargetDatas[i].isInvert;
                    TriggerVirtualButton();
                    break;
                }
            }
        }
    }
    protected virtual void OnEndLeftClick(Vector2 pointerPosition, GameObject raycastedUI)
    {
        leftClickDown = false;
        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            animationPlayerContainerFeedback[i].setCurrentParam = false;
        }
        for (int j = 0; j < animationPlayerContainerInteraction.Count; j++)
        {
            animationPlayerContainerInteraction[j].setCurrentParam = false;
        }

        VirtualTrainingCamera.IsMovementEnabled = true;
    }

    protected virtual void InteractionEventListener(MonitorCameraInteractionEvent e)
    {
        if (e.interactionObject == null)
            return;
        else
        {
            for (int i = 0; i < clickTargetDatas.Count; i++)
            {
                if (clickTargetDatas[i].gameObject.gameObject == e.interactionObject)
                {
                    VirtualTrainingCamera.IsMovementEnabled = false;
                    clickTargetCurrent = e.interactionObject.gameObject;
                    TriggerVirtualButton();
                    break;
                }
            }
        }
    }
    protected virtual void SetupVirtualButton()
    {
        if (virtualButtonDatas == null)
            return;

        clickTargetDatas = virtualButtonDatas.clickTargetDatas;
        vbStartAnimationDatas = virtualButtonDatas.vbStartAnimations;

        for (int a = 0; a < virtualButtonDatas.vbOutputInteraction.Count; a++)
        {
            SetAnimationPlayerData(virtualButtonDatas, true, virtualButtonDatas.vbOutputInteraction[a]);
        }

        for (int b = 0; b < virtualButtonDatas.vbOutputFeedback.Count; b++)
        {
            SetAnimationPlayerData(virtualButtonDatas, false, virtualButtonDatas.vbOutputFeedback[b]);
        }

        for (int c = 0; c < virtualButtonDatas.vbStartAnimations.Count; c++)
        {
            SetAnimationOnStart(virtualButtonDatas, virtualButtonDatas.vbStartAnimations[c]);
        }

        for (int d = 0; d < virtualButtonDatas.vbEffectDatas.Count; d++)
        {
            setEffectLerp(virtualButtonDatas.vbEffectDatas[d]);
        }

        for (int e = 0; e < virtualButtonDatas.vbModifierData.Count; e++)
        {
            setModifier(virtualButtonDatas.vbModifierData[e]);
        }
    }

    void SetAnimationOnStart(VirtualButtonDataModel dataModel, VBStartAnimationData vbOnStart)
    {
        Transform transformObject;
        if (vbOnStart.isAnimationScript)
        {
            if (vbOnStart.ObjectAnimation.gameObject != null)
            {
                if (vbOnStart.scriptedAnimationElement.GetData() != null)
                {
                    transformObject = vbOnStart.ObjectAnimation.gameObject.transform;
                    AnimationPlayerScript animScript = transformObject.gameObject.AddComponent<AnimationPlayerScript>();
                    animScript.SetupOnStart(dataModel, vbOnStart, false);
                    animationOnStarts.Add(animScript);
                    animScript.StartPlayAnimation(1, animScript.getVBOutputData.speed);
                }
                else
                    Debug.Log("Data Animation Script Kosong");

            }
            else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
        }
        else
        {
            if (vbOnStart.animatorController.gameObject != null)
            {
                transformObject = vbOnStart.animatorController.gameObject.transform;

                AnimationPlayerManual animScript = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                animScript.SetupOnStart(dataModel, vbOnStart, false,true);
                animationOnStarts.Add(animScript);
            }
            else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
        }

    }

    void SetAnimationPlayerData(VirtualButtonDataModel dataModel, bool isInteractionObject, VirtualButtonOutputData vbOutputData)
    {
        Transform transformObject;
        if (vbOutputData.isAnimationScript)
        {
            if (vbOutputData.ObjectAnimation.gameObject != null)
            {
                if (vbOutputData.scriptedAnimationElement.GetData() != null)
                {
                    transformObject = vbOutputData.ObjectAnimation.gameObject.transform;
                    AnimationPlayerScript animScript = transformObject.gameObject.AddComponent<AnimationPlayerScript>();
                    animScript.Setup(dataModel, vbOutputData, isInteractionObject);

                    if (isInteractionObject == true)
                        animationPlayerContainerInteraction.Add(animScript);
                    else
                        animationPlayerContainerFeedback.Add(animScript);
                }
                else
                    Debug.Log("Data Animation Script Kosong");

            }
            else
            {
                if (isInteractionObject)
                    Debug.Log("Data Interaction Object Animasi Manual Kosong");
                else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
            }
        }
        else
        {
            if (vbOutputData.animatorController.gameObject != null)
            {
                transformObject = vbOutputData.animatorController.gameObject.transform;

                AnimationPlayerManual animScript = transformObject.gameObject.AddComponent<AnimationPlayerManual>();
                animScript.Setup(dataModel, vbOutputData, isInteractionObject);

                if (isInteractionObject == true)
                    animationPlayerContainerInteraction.Add(animScript);
                else
                    animationPlayerContainerFeedback.Add(animScript);
            }
            else
            {
                if (isInteractionObject)
                    Debug.Log("Data Interaction Object Animasi Manual Kosong");
                else
                    Debug.Log("Data Feedback Object Animasi Manual Kosong");
            }
        }
    }

    void setEffectLerp(VirtualButtonEffectData effectData)
    {
        if(effectData.animator != null)
        {
            EffectLerp effectLerp = effectData.animator.gameObject.AddComponent<EffectLerp>();
            effectLerp.vbEffectData = effectData;
            effectLerpContainer.Add(effectLerp);
        }
    }
    void setModifier(VirtualbuttonModifierData modifierData)
    {
        switch (modifierData.vbModifierType)
        {
            case VirtualbuttonModifierType.SetActiveOrNonActive:
                ModifierSetActive modifierSetActive = this.gameObject.AddComponent<ModifierSetActive>();
                modifierSetActive.SetModifierData(modifierData);
                modifierSetActiveContainer.Add(modifierSetActive);
                Debug.Log("set active");
                break;
            case VirtualbuttonModifierType.Light:
                ModifierLight modifierLight = this.gameObject.AddComponent<ModifierLight>();
                modifierLight.SetModifierData(modifierData);
                modifierSetActiveContainer.Add(modifierLight);
                Debug.Log("light");
                break;
            case VirtualbuttonModifierType.Skybox:
                ModifierSkybox modifierSkybox = this.gameObject.AddComponent<ModifierSkybox>();
                modifierSkybox.SetModifierData(modifierData);
                modifierSetActiveContainer.Add(modifierSkybox);
                Debug.Log("skybox");
                break;
        }
    }

    protected void PlayEffect(float value)
    {
        for (int i = 0; i < effectLerpContainer.Count; i++)
        {
            effectLerpContainer[i].sliderValue = value;
        }
    }

    public void DestroyThis()
    {
        DestroyImmediate(this.gameObject);
    }
}
