using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

[System.Serializable]
public class ParameterSequentialData
{
    public AnimationPlayerBase animationPlayerBase;
    public ParameterData parameterDatasInteraction;
    public string animationState;

}

public class VirtualButtonClick : VirtualButtonBase
{
    int valueTemp;
    bool isBackSequential;
    public List<ParameterSequentialData> parameterSequentialDatasInteraction = new List<ParameterSequentialData>();
    public List<ParameterSequentialData> parameterSequentialDatasFeedback = new List<ParameterSequentialData>();
    float sliderValueTemp;
    bool invertTemp;
    public float maxAnimationValue = 1;
    public float minAnimationValue = 0;
    void PlayVBClick()
    {
        VirtualTrainingCamera.IsMovementEnabled = false;

        if (virtualButtonDatas.isSequentialClick)
        {
            if (IsInvert != invertTemp)
            {
                invertTemp = IsInvert;
                if (isBackSequential)
                {
                    if (IsInvert)
                        valueTemp++;
                    else
                    {
                        if (valueTemp > 0)
                            valueTemp--;
                        else
                            isBackSequential = !isBackSequential;
                    }
                }
                else
                {
                    if (IsInvert)
                        valueTemp--;
                    else
                        valueTemp++;
                }
            }

            PlayAllAnimationPlayer();
        }
        else
        {
            if (IsInvert)
                virtualButtonDatas.isBack = !virtualButtonDatas.isBack;

            virtualButtonDatas.isBack = !virtualButtonDatas.isBack;
            if (animationPlayerContainerInteraction.Count > 0)
            {
                for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
                {
                    if (animationPlayerContainerInteraction[i].getVBDataModel.isBack)
                        animationPlayerContainerInteraction[i].StartPlayAnimation(minAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
                    else
                        animationPlayerContainerInteraction[i].StartPlayAnimation(maxAnimationValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);

                    Debug.Log("click " + virtualButtonDatas.isBack);
                }
            }

            if (animationPlayerContainerInteraction.Count > 0)
                StartCoroutine(DelayTransitionFeedback(animationPlayerContainerInteraction[0].getVBDataModel.isBack));
        }
    }

    protected override void SetupVirtualButton()
    {
        base.SetupVirtualButton();
        valueTemp = 0;
        if (virtualButtonDatas.isSequentialClick)
        {
            SetupAnimationPlayerBase(animationPlayerContainerInteraction, parameterSequentialDatasInteraction);
            SetupAnimationFeedback(animationPlayerContainerFeedback);
        }
    }

    void SetupAnimationPlayerBase(List<AnimationPlayerBase> animationPlayerBases, List<ParameterSequentialData> parameterSequentialDatas)
    {
        for (int i = 0; i < animationPlayerBases.Count; i++)
        {
            if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayParameter)
            {
                for (int j = 0; j < animationPlayerBases[i].getVBOutputData.parameterDatas.Count; j++)
                {
                    ParameterSequentialData paramSequentData = new ParameterSequentialData();
                    paramSequentData.animationPlayerBase = animationPlayerBases[i];
                    paramSequentData.parameterDatasInteraction = animationPlayerBases[i].getVBOutputData.parameterDatas[j];
                    parameterSequentialDatas.Add(paramSequentData);

                }
            }
            else if (virtualButtonDatas.sequencePlayMethod == SequencePlayMethod.PlayState)
            {
                for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                {
                    ParameterSequentialData paramSequentData = new ParameterSequentialData();
                    paramSequentData.animationPlayerBase = animationPlayerBases[i];
                    paramSequentData.animationState = animationPlayerBases[i].getVBOutputData.animationDatas[l].animationState;
                    parameterSequentialDatas.Add(paramSequentData);
                }
            }
        }
    }

    void SetupAnimationFeedback(List<AnimationPlayerBase> animationPlayerBases)
    {
        for (int i = 0; i < animationPlayerBases.Count; i++)
        {
            if (animationPlayerBases[i].getVBOutputData.isAnimationScript)
            {
                ParameterSequentialData paramSequentData = new ParameterSequentialData();
                paramSequentData.animationPlayerBase = animationPlayerBases[i];
                parameterSequentialDatasFeedback.Add(paramSequentData);
            }
            else
            {
                AnimationPlayerManual animManual = animationPlayerBases[i] as AnimationPlayerManual;
                if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                {
                    for (int j = 0; j < animManual.getVBOutputData.parameterDatas.Count; j++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.parameterDatasInteraction = animManual.getVBOutputData.parameterDatas[j];
                        parameterSequentialDatasFeedback.Add(paramSequentData);

                    }
                }
                else
                {
                    for (int l = 0; l < animationPlayerBases[i].getVBOutputData.animationDatas.Count; l++)
                    {
                        ParameterSequentialData paramSequentData = new ParameterSequentialData();
                        paramSequentData.animationPlayerBase = animationPlayerBases[i];
                        paramSequentData.animationState = animManual.getVBOutputData.animationDatas[l].animationState;
                        parameterSequentialDatasFeedback.Add(paramSequentData);
                    }
                }
            }
        }
    }

    IEnumerator DelayTransitionFeedback(bool isBack)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
        {
            if (isBack)
                yield return new WaitUntil(() => sliderValue >= 0.96f);
            else
                yield return new WaitUntil(() => sliderValue <= 0.05f);
        }
        else if (virtualButtonDatas.transitionMethod == TransitionMethod.Direct)
            yield return new WaitForSeconds(0);
        else
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);

        if (animationPlayerContainerFeedback.Count > 0)
        {
            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                animationPlayerContainerFeedback[i].isBack = isBack;

                if (animationPlayerContainerFeedback[i].isBack)
                    animationPlayerContainerFeedback[i].StartPlayAnimation(0, animationPlayerContainerFeedback[i].getVBOutputData.speed);
                else
                    animationPlayerContainerFeedback[i].StartPlayAnimation(1, animationPlayerContainerFeedback[i].getVBOutputData.speed);
            }
        }
    }

    public override void TriggerVirtualButton()
    {
        Debug.Log("virtual button click");
        PlayVBClick();
    }

    private void Update()
    {
        if (animationPlayerContainerFeedback.Count > 0)
        {
            for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
            {
                if (i == 0)
                    sliderValueTemp = animationPlayerContainerFeedback[i].sliderValue;

                if (sliderValueTemp >= animationPlayerContainerFeedback[i].sliderValue)
                    sliderValueTemp = animationPlayerContainerFeedback[i].sliderValue;
            }
        }
        else
        {
            for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
            {
                if (i == 0)
                    sliderValueTemp = animationPlayerContainerInteraction[i].sliderValue;

                if (sliderValueTemp >= animationPlayerContainerInteraction[i].sliderValue)
                    sliderValueTemp = animationPlayerContainerInteraction[i].sliderValue;
            }
        }

        sliderValue = sliderValueTemp;
    }
    void PlayAllAnimationPlayer()
    {
        switch (virtualButtonDatas.sequencePlayMethod)
        {
            case SequencePlayMethod.TimeStampParameter:
                SequentialClickTimeStamp();
                break;
            case SequencePlayMethod.TimeStampScriptedAnimation:
                SequentialClickTimeStamp();
                break;
            case SequencePlayMethod.TimeStampState:
                SequentialClickTimeStamp();
                break;
            case SequencePlayMethod.PlayParameter:
                PlaySequentialManual(true);
                break;
            case SequencePlayMethod.PlayScriptedAnimation:
                PlaySequentialAnimationScript();
                break;
            case SequencePlayMethod.PlayState:
                PlaySequentialManual(false);
                break;
        }
    }
    void SequentialClickTimeStamp()
    {
        if (IsInvert)
        {
            SequentialInvert();
        }
        else
        {
            if (isBackSequential)
            {
                if (valueTemp > 0)
                {
                    valueTemp--;
                    PlayAnimationList(valueTemp);
                }
                else
                {
                    isBackSequential = false;
                    PlayAnimationList(-1);
                }
            }
            else
            {
                if (valueTemp < virtualButtonDatas.sequentialClickDatas.Count)
                {
                    PlayAnimationList(valueTemp);
                    valueTemp++;
                }
                else
                {
                    PlayAnimationList(valueTemp - 2);
                    valueTemp = valueTemp - 2;
                    isBackSequential = true;
                }
            }
        }
    }

    void SequentialInvert()
    {
        if (isBackSequential == false)
        {
            if (valueTemp > 0)
            {
                valueTemp = valueTemp - 1;
                PlayAnimationList(valueTemp);
            }
            else
            {
                isBackSequential = true;
                PlayAnimationList(-1);
            }
        }
        else
        {
            if (valueTemp < virtualButtonDatas.sequentialClickDatas.Count)
            {
                PlayAnimationList(valueTemp);
                valueTemp++;
            }
            else
            {
                PlayAnimationList(valueTemp - 2);
                valueTemp = valueTemp - 2;
                isBackSequential = false;
            }
        }
    }
    void PlayAnimationList(int value)
    {
        for (int i = 0; i < animationPlayerContainerInteraction.Count; i++)
        {
            if (value <= -1)
            {
                animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.defaultValue, animationPlayerContainerInteraction[i].getVBOutputData.speed);
            }
            else
                animationPlayerContainerInteraction[i].StartPlayAnimation(virtualButtonDatas.sequentialClickDatas[value].durations, animationPlayerContainerInteraction[i].getVBOutputData.speed);

            Debug.Log(value);
        }

        if (value <= -1)
            StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.defaultValue));
        else
        {
            if (virtualButtonDatas.vbPlayStampValueType == VBPlayStampValueType.DifferenceValue)
                StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durationSecond));
            else
                StartCoroutine(PlayDelayAnimationSequence(virtualButtonDatas.sequentialClickDatas[value].durations));
        }

    }
    IEnumerator PlayDelayAnimationSequence(float value)
    {
        sliderValue = Mathf.Round(sliderValue * 10) / 10;
        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
            yield return new WaitUntil(() => sliderValue == value);
        else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        for (int i = 0; i < animationPlayerContainerFeedback.Count; i++)
        {
            animationPlayerContainerFeedback[i].StartPlayAnimation(value, animationPlayerContainerFeedback[i].getVBOutputData.speed);
        }
    }

    void PlaySequentialAnimationScript()
    {
        if (IsInvert)
        {
            SequentialAnimationScriptInvert();
        }
        else
        {
            if (isBackSequential)
            {
                if (valueTemp >= 0)
                {
                    animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 0, 1));
                    valueTemp--;
                }
                else
                {
                    animationPlayerContainerInteraction[0].StartPlayAnimation(1, animationPlayerContainerInteraction[0].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 1, 0));
                    valueTemp = valueTemp + 2;
                    isBackSequential = false;
                }
            }
            else
            {
                if (valueTemp < animationPlayerContainerInteraction.Count)
                {
                    animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp));
                    valueTemp++;
                }
                else
                {
                    animationPlayerContainerInteraction[valueTemp - 1].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp - 1].getVBOutputData.speed);
                    StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp - 1], 1, 0));
                    valueTemp = valueTemp - 2;
                    isBackSequential = true;
                }
            }
        }
    }

    void SequentialAnimationScriptInvert()
    {
        if (isBackSequential == false)
        {
            if (valueTemp >= 0)
            {
                animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 0, valueTemp));
                valueTemp--;
            }
            else
            {
                animationPlayerContainerInteraction[0].StartPlayAnimation(1, animationPlayerContainerInteraction[0].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[0], 1, 0));
                valueTemp = valueTemp + 2;
                isBackSequential = true;
            }
        }
        else
        {
            if (valueTemp < animationPlayerContainerInteraction.Count)
            {
                animationPlayerContainerInteraction[valueTemp].StartPlayAnimation(1, animationPlayerContainerInteraction[valueTemp].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp], 1, valueTemp));
                valueTemp++;
            }
            else
            {
                animationPlayerContainerInteraction[valueTemp - 1].StartPlayAnimation(0, animationPlayerContainerInteraction[valueTemp - 1].getVBOutputData.speed);
                StartCoroutine(DelayPlayAnimationManualFeedback(animationPlayerContainerInteraction[valueTemp - 1], 1, valueTemp - 1));
                valueTemp = valueTemp - 2;
                isBackSequential = false;
            }
        }
    }

    void PlaySequentialManual(bool isParameter)
    {
        if (IsInvert)
        {
            SequentialManualInvert(isParameter);
        }
        else
        {
            if (isBackSequential)
            {
                if (valueTemp >= 0)
                {
                    PlayAnimationManual(valueTemp, 0, isParameter, 1);
                    valueTemp--;
                }
                else
                {
                    PlayAnimationManual(0, 1, isParameter, 0);
                    valueTemp = valueTemp + 2;
                    isBackSequential = false;
                }
            }
            else
            {
                if (valueTemp < parameterSequentialDatasInteraction.Count)
                {
                    PlayAnimationManual(valueTemp, 1, isParameter, 0);
                    valueTemp++;
                }
                else
                {
                    PlayAnimationManual(valueTemp - 1, 0, isParameter, 1);
                    valueTemp = valueTemp - 2;
                    isBackSequential = true;
                }
            }
        }
    }

    void SequentialManualInvert(bool isParameter)
    {
        if (isBackSequential == false)
        {
            if (valueTemp >= 0)
            {
                PlayAnimationManual(valueTemp, 0, isParameter, 1);
                valueTemp--;
            }
            else
            {
                PlayAnimationManual(0, 1, isParameter, 0);
                valueTemp = valueTemp + 2;
                isBackSequential = true;
            }
        }
        else
        {
            if (valueTemp < parameterSequentialDatasInteraction.Count)
            {
                PlayAnimationManual(valueTemp, 1, isParameter, 0);
                valueTemp++;
            }
            else
            {
                PlayAnimationManual(valueTemp - 1, 0, isParameter, 1);
                valueTemp = valueTemp - 2;
                isBackSequential = false;
            }
        }
    }

    void PlayAnimationManual(int valueTemp, int maxValue, bool isParameter, float startSlider = 0)
    {
        AnimationPlayerManual animManual = parameterSequentialDatasInteraction[valueTemp].animationPlayerBase as AnimationPlayerManual;
        if (isParameter)
        {
            animManual.getVBOutputData.animationParameter = parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.parameter;
            animManual.SetMinMaxValue(parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.minimalValue, parameterSequentialDatasInteraction[valueTemp].parameterDatasInteraction.maximalValue);
            animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
        }
        else
        {
            animManual.SetAnimName(parameterSequentialDatasInteraction[valueTemp].animationState);
            animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
        }
        StartCoroutine(DelayPlayAnimationManualFeedback(animManual, maxValue, valueTemp, startSlider));
    }

    IEnumerator DelayPlayAnimationManualFeedback(AnimationPlayerBase animBase, float maxValue, int valueTemp, float startSlider = 0)
    {
        if (virtualButtonDatas.transitionMethod == TransitionMethod.AfterAnimation)
        {
            if (animBase.getVBOutputData.isAnimationScript)
                yield return new WaitUntil(() => animBase.sliderValue == maxValue);
            else
            {
                AnimationPlayerManual animManual = animBase as AnimationPlayerManual;
                yield return new WaitUntil(() => animManual.getSliderPercentageTemp == maxValue);
            }
        }
        else if (virtualButtonDatas.transitionMethod == TransitionMethod.Followthrough)
            yield return new WaitForSeconds(virtualButtonDatas.followThroughDelay);
        else
            yield return new WaitForSeconds(0);

        if (valueTemp < parameterSequentialDatasFeedback.Count)
        {
            if (parameterSequentialDatasFeedback[valueTemp].animationPlayerBase.getVBOutputData.isAnimationScript)
            {
                AnimationPlayerScript animScript = parameterSequentialDatasFeedback[valueTemp].animationPlayerBase as AnimationPlayerScript;
                animScript.StartPlayAnimation(maxValue, animScript.getVBOutputData.speed);
            }
            else
            {
                AnimationPlayerManual animManual = parameterSequentialDatasFeedback[valueTemp].animationPlayerBase as AnimationPlayerManual;
                if (animManual.getVBOutputData.feedBackManualType == FeedbackManualType.Parameter)
                {
                    animManual.getVBOutputData.animationParameter = parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.parameter;
                    animManual.SetMinMaxValue(parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.minimalValue, parameterSequentialDatasFeedback[valueTemp].parameterDatasInteraction.maximalValue);
                    animManual.StartPlayAnimation(maxValue, animManual.getVBOutputData.speed);
                }
                else
                {
                    animManual.SetAnimName(parameterSequentialDatasFeedback[valueTemp].animationState);
                    animManual.StartPlayAnimationSequentialState(maxValue, startSlider);
                }
            }
        }
    }

    public override void PlayVirtualButton()
    {
        Debug.Log("play virtual button");
    }

    public override void StopVirtualButton()
    {
        Debug.Log("play virtual button");
    }
}
