﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Feature
{
    [CustomEditor(typeof(FlowManager))]
    [CanEditMultipleObjects]
    public class FlowManagerEditor : Editor
    {
        float recordPositionRate = 0.05f;
        float followerScale = 1f;
        FlowFollower followerTemplate = null;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            FlowManager manager = target as FlowManager;

            if (Application.isPlaying)
            {
                if (GUILayout.Button(manager.IsEditMode ? "Play" : "Edit Mode"))
                {
                    manager.IsEditMode = !manager.IsEditMode;
                    if (manager.IsEditMode)
                        manager.EditMode();
                    else
                        manager.Play();
                }

                if (manager.IsEditMode)
                {
                    if (GUILayout.Button("Add FlowLeader Component"))
                    {
                        if (EditorUtility.DisplayDialog("Add flow component", "Add flow to mesh renderer / pointer object ?", "yes", "no"))
                            manager.AddFlowEntityComponent();
                    }

                    if (GUILayout.Button("Remove All FlowLeader Component"))
                    {
                        if (EditorUtility.DisplayDialog("Remove flow component", "Remove all flow leader component ?", "yes", "no"))
                            manager.RemoveFlowEntityComponent();
                    }

                    if (GUILayout.Button("Bake Movement"))
                    {
                        if (EditorUtility.DisplayDialog("Bake", "Bake movement ?", "yes", "no"))
                            manager.BakeMovement();
                    }
                }
            }

            GUILayout.Space(10f);

            EditorGUILayout.LabelField("Set value for all flow leaders section");

            recordPositionRate = EditorGUILayout.Slider("Record Position Rate", recordPositionRate, 0.05f, 0.5f);
            followerScale = EditorGUILayout.FloatField("Follower Scale", followerScale);
            followerTemplate = EditorGUILayout.ObjectField("Follower Template", followerTemplate, typeof(FlowFollower), true) as FlowFollower;

            if (GUILayout.Button("set value to all flow leaders"))
            {
                if (EditorUtility.DisplayDialog("flow editor", "are you sure to set record position rate, follower scale and follower template to all flow leaders ?", "yes", "no"))
                {
                    List<FlowLeader> flowLeaders = manager.GetFlowLeaders();

                    for (int i = 0; i < flowLeaders.Count; i++)
                    {
                        flowLeaders[i].RecordPositionRate = recordPositionRate;
                        flowLeaders[i].FollowerScale = followerScale;
                        flowLeaders[i].FollowerTemplate = followerTemplate;
                    }
                }
            }
        }
    }
}

#endif