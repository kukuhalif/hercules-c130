﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Feature
{
    public class FlowLeader : MonoBehaviour
    {
        [Range(0.05f, 0.5f)]
        [FormerlySerializedAs("_recordPositionRate")]
        [SerializeField] float recordPositionRate = 0.05f;

        [FormerlySerializedAs("_speedMultiplier")]
        [SerializeField] float speedMultiplier = 1f;

        [FormerlySerializedAs("_followerScale")]
        [SerializeField] float followerScale = 1f;

        [FormerlySerializedAs("_followerParent")]
        [SerializeField] Transform followerParent;

        [FormerlySerializedAs("_followerTemplate")]
        [SerializeField] FlowFollower followerTemplate;

        List<Vector3> _positions = new List<Vector3>();
        List<Quaternion> _rotations = new List<Quaternion>();

        int currentFollower;
        int followerCount;

        [FormerlySerializedAs("_followers")]
        [SerializeField] List<FlowFollower> followers = new List<FlowFollower>();

        [HideInInspector] MeshRenderer currentRenderer;

        public float RecordPositionRate { get => recordPositionRate; set => recordPositionRate = value; }
        public float FollowerScale { get => followerScale; set => followerScale = value; }
        public FlowFollower FollowerTemplate { get => followerTemplate; set => followerTemplate = value; }

        private void Awake()
        {
            currentRenderer = GetComponent<MeshRenderer>();
        }

        private void Start()
        {
            if (followerParent == null)
            {
                GameObject fp = new GameObject();
                followerParent = fp.transform;
                followerParent.SetParent(transform.parent);
                followerParent.position = new Vector3();
                followerParent.gameObject.name = "follower parent -> " + gameObject.name;
            }
        }

        public void Bake()
        {
            if (currentRenderer != null)
                currentRenderer.enabled = true;
            _positions.Clear();
            _rotations.Clear();
            while (followers.Count > 0)
            {
                DestroyImmediate(followers[followers.Count - 1].gameObject);
                followers.RemoveAt(followers.Count - 1);
            }
            InvokeRepeating("RecordAndCreateFollower", 0, recordPositionRate);
        }

        void RecordAndCreateFollower()
        {
            _positions.Add(transform.position);
            _rotations.Add(transform.rotation);

            FlowFollower newFollower = Instantiate(followerTemplate);
            newFollower.transform.position = transform.position;
            newFollower.transform.rotation = transform.rotation;
            newFollower.transform.localScale = new Vector3(followerScale, followerScale, followerScale);
            newFollower.transform.parent = followerParent;
            newFollower.gameObject.name = _positions.Count.ToString();
            followers.Add(newFollower);
        }

        public void StopRecording()
        {
            CancelInvoke("RecordAndCreateFollower");
            RemoveEndPosition();
            RemoveTooClosePosition();
            RemoveInvalidFollower();
            SetFollowersData();
        }

        void RemoveEndPosition()
        {
            int tailPosition = _positions.Count;
            for (int i = 1; i < _positions.Count; i++)
            {
                if (_positions[i - 1] == _positions[i])
                {
                    tailPosition = i;
                    break;
                }
            }

            _positions.RemoveRange(tailPosition, _positions.Count - tailPosition);
            _rotations.RemoveRange(tailPosition, _positions.Count - tailPosition);
        }

        void RemoveTooClosePosition()
        {
            float averageDistance = 0f;
            float totalDistance = 0;

            for (int i = 0; i < _positions.Count - 1; i++)
            {
                totalDistance += Vector3.Distance(_positions[i], _positions[i + 1]);
            }

            averageDistance = totalDistance / _positions.Count;

            for (int i = _positions.Count - 1; i > 0; i--)
            {
                if (Vector3.Distance(_positions[i], _positions[i - 1]) < averageDistance / 2)
                {
                    Debug.Log(gameObject.name + " remove index " + i);
                    _positions.RemoveAt(i);
                    _rotations.RemoveAt(i);
                }
            }
        }

        void RemoveInvalidFollower()
        {
            List<FlowFollower> removed = new List<FlowFollower>();
            for (int i = 0; i < followers.Count; i++)
            {
                if (i >= _positions.Count - 1)
                {
                    removed.Add(followers[i]);
                }
            }

            for (int i = 0; i < removed.Count; i++)
            {
                followers.Remove(removed[i]);
                DestroyImmediate(removed[i].gameObject);
            }
        }

        void SetFollowersData()
        {
            for (int i = 0; i < followers.Count; i++)
            {
                followers[i].SetData(_positions[i], _positions[i + 1], _rotations[i], _rotations[i + 1]);
            }
        }

        public void StartPosition()
        {
            followerCount = followers.Count;
            for (currentFollower = 0; currentFollower < followerCount; currentFollower++)
            {
                followers[currentFollower].StartPosition(GetSpeedMultiplier, GetFollowerScale);
            }
            if (currentRenderer != null)
                currentRenderer.enabled = false;
        }

        float GetFollowerScale()
        {
            return followerScale;
        }

        float GetSpeedMultiplier()
        {
            return speedMultiplier;
        }

        public void Play(float deltaTime)
        {
            for (currentFollower = 0; currentFollower < followerCount; currentFollower++)
            {
                followers[currentFollower].Play(deltaTime);
            }
        }
    }
}