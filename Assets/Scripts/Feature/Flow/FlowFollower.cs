﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Feature
{
    public class FlowFollower : MonoBehaviour
    {
        [FormerlySerializedAs("_startPos")]
        [SerializeField] Vector3 startPos;

        [FormerlySerializedAs("_endPos")]
        [SerializeField] Vector3 endPos;

        [FormerlySerializedAs("_startRot")]
        [SerializeField] Quaternion startRot;

        [FormerlySerializedAs("_endRot")]
        [SerializeField] Quaternion endRot;

        float _lerpPos;
        float _speedMultiplier;
        Func<float> _GetScale;
        Func<float> _GetSpeedMultiplier;

        public void SetData(Vector3 startPos, Vector3 endPos, Quaternion startRot, Quaternion endRot)
        {
            this.startPos = startPos;
            this.endPos = endPos;
            this.startRot = startRot;
            this.endRot = endRot;
        }

        public void StartPosition(Func<float> getSpeedMultiplier, Func<float> getScale)
        {
            _GetScale = getScale;
            _GetSpeedMultiplier = getSpeedMultiplier;
            transform.localScale = new Vector3(_GetScale(), _GetScale(), _GetScale());
            _speedMultiplier = _GetSpeedMultiplier();
            _lerpPos = 0f;
            transform.position = startPos;
            transform.rotation = startRot;
        }

        public void Play(float deltaTime)
        {
#if UNITY_EDITOR
            _speedMultiplier = _GetSpeedMultiplier();
            transform.localScale = new Vector3(_GetScale(), _GetScale(), _GetScale());
#endif

            transform.position = Vector3.Lerp(startPos, endPos, _lerpPos);
            transform.rotation = Quaternion.Slerp(startRot, endRot, _lerpPos);

            _lerpPos += (deltaTime * _speedMultiplier);

            if (_lerpPos >= 1f)
            {
                _lerpPos = 0f;
            }
        }
    }
}