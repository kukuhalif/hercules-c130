﻿using UnityEngine;
using VirtualTraining.Core;


namespace VirtualTraining.Feature
{
    public class MaintenancePlayer : MonoBehaviour
    {
        private MaintenanceDataModel maintenanceDataModel;

        void Start()
        {
            EventManager.AddListener<MaintenancePlayEvent>(Play);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MaintenancePlayEvent>(Play);
        }

        private void Play(MaintenancePlayEvent e)
        {
            if (e.maintenanceData == null)
            {
                ResetMaintenance();
                return;
            }

            ResetMaintenanceAction();
            maintenanceDataModel = e.maintenanceData;
            PlayMaintenance();
        }


        void ResetMaintenance()
        {
            VirtualTrainingCamera.ResetCameraPosition(ResetMaintenanceAction);
        }

        void PlayMaintenance()
        {
            VirtualTrainingCamera.MoveCamera(maintenanceDataModel.figure.cameraDestination, OnPlayMateriArriveAction);
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(OverrideMaintenance(maintenanceDataModel).figure);
        }

        MaintenanceDataModel OverrideMaintenance(MaintenanceDataModel maintenanceDataModel)
        {
            MaintenanceDataModel overrideMonitorData = maintenanceDataModel.figure.cameraOverrideMaintenance.GetData();
            MaintenanceDataModel overridePartObjectData = maintenanceDataModel.figure.partObjectOverrideMaintenance.GetData();
            if (overrideMonitorData != null)
            {
                maintenanceDataModel.figure.monitorCameras = overrideMonitorData.figure.monitorCameras;
                maintenanceDataModel.figure.cameraDestination = overrideMonitorData.figure.cameraDestination;
            }

            if(overridePartObjectData != null)
            {
                maintenanceDataModel.figure.partObjects = overrideMonitorData.figure.partObjects;
            }

            return maintenanceDataModel;
        }

        private void PlayFigure(Figure figure)
        {
            VirtualTrainingCamera.MoveCamera(figure.cameraDestination, OnArriveFigureCamera);
        }
        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(maintenanceDataModel.figure.cutaway));
            EventManager.TriggerEvent(new HelperEvent(maintenanceDataModel.figure.callouts));
            EventManager.TriggerEvent(new HelperEvent(maintenanceDataModel.figure.helpers));
            EventManager.TriggerEvent(new PartObjectEvent(maintenanceDataModel.figure.partObjects));
            EventManager.TriggerEvent(new MonitorCameraEvent(maintenanceDataModel.figure.monitorCameras));
            PlayVirtualButton();
            PlayAnimation();
        }

        private void ResetMaintenanceAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new PartObjectEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new AnimationPlayEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
        }

        void PlayVirtualButton()
        {
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            for (int i = 0; i < maintenanceDataModel.figure.virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(maintenanceDataModel.figure.virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), maintenanceDataModel.figure.virtualButtonElement[i].overrideVirtualButton));
            }
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void PlayAnimation()
        {
            AnimationManager.isSequenceAnimation = maintenanceDataModel.figure.isSequenceAnimation;

            for (int i = 0; i < maintenanceDataModel.figure.animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(maintenanceDataModel.figure.animationFigureDatas[i]));
            }

            EventManager.TriggerEvent(new AnimationSequenceDone());
        }
    }
}
