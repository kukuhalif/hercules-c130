﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class MateriPlayer : MonoBehaviour
    {
        private MateriTreeElement materiTreeElement;
        private int figureIndex;
        private List<Figure> figures = new List<Figure>();

        private void Start()
        {
            EventManager.AddListener<InializationDoneEvent>(InitializationDone);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<MateriEvent>(Play);
            EventManager.RemoveListener<NextFigureEvent>(NextFigure);
            EventManager.RemoveListener<PrevFigureEvent>(PreviousFigure);
            EventManager.RemoveListener<FreePlayEvent>(FreePlay);
        }

        private void InitializationDone(InializationDoneEvent e)
        {
            EventManager.AddListener<MateriEvent>(Play);
            EventManager.AddListener<NextFigureEvent>(NextFigure);
            EventManager.AddListener<PrevFigureEvent>(PreviousFigure);
            EventManager.AddListener<FreePlayEvent>(FreePlay);
        }

        private void Play(MateriEvent e)
        {
            if (e.materiTreeElement.MateriData.figures.Count == 0)
            {
                Debug.LogError("figure list empty");
                return;
            }

            ResetMateriAction();

            materiTreeElement = e.materiTreeElement;

            // play first figure
            figureIndex = 0;
            figures = materiTreeElement.MateriData.figures;
            PlayMateri();
        }

        private void FreePlay(FreePlayEvent e)
        {
            ResetMateri();
        }

        private void OnPlayMateriArriveAction()
        {
            PlayFigure(figures[figureIndex], false);

            EventManager.TriggerEvent(new DisableBackgroundEvent(materiTreeElement.MateriData.disableBackground));
            EventManager.TriggerEvent(new SetSchematicDataEvent(materiTreeElement.MateriData.schematics));
        }

        private void PlayMateri()
        {
            EventManager.TriggerEvent(new DescriptionPanelEvent(materiTreeElement));

            VirtualTrainingCamera.MoveCamera(figures[figureIndex].cameraDestination, OnPlayMateriArriveAction);
        }

        private void PlayFigure(Figure figure, bool moveCamera)
        {
            EventManager.TriggerEvent(new PlayFigureEvent(figures[figureIndex].name, figureIndex, figures.Count));

            if (moveCamera)
                VirtualTrainingCamera.MoveCamera(figure.cameraDestination, OnArriveFigureCamera);
            else
                OnArriveFigureCamera();
        }

        private void OnArriveFigureCamera()
        {
            EventManager.TriggerEvent(new CutawayEvent(figures[figureIndex].cutaway));
            EventManager.TriggerEvent(new HelperEvent(figures[figureIndex].callouts));
            EventManager.TriggerEvent(new HelperEvent(figures[figureIndex].helpers));
            EventManager.TriggerEvent(new PartObjectEvent(figures[figureIndex].partObjects));
            EventManager.TriggerEvent(new MonitorCameraEvent(figures[figureIndex].monitorCameras));
            PlayVirtualButton();
            PlayAnimation();
        }

        private void ResetFigureAction()
        {
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new PartObjectEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new AnimationPlayEvent());
        }

        private void ResetMateriAction()
        {
            EventManager.TriggerEvent(new ObjectInteractionResetObjectEvent());
            EventManager.TriggerEvent(new CutawayEvent());
            EventManager.TriggerEvent(new HelperEvent());
            EventManager.TriggerEvent(new PartObjectEvent());
            EventManager.TriggerEvent(new TroubleshootUIEvent());
            EventManager.TriggerEvent(new MaintenanceUIEvent());
            EventManager.TriggerEvent(new DisableBackgroundEvent());
            EventManager.TriggerEvent(new SetSchematicDataEvent());
            EventManager.TriggerEvent(new ShowPdfEvent());
            EventManager.TriggerEvent(new MonitorCameraEvent());
            EventManager.TriggerEvent(new VirtualButtonEventDestroy());
            EventManager.TriggerEvent(new AnimationPlayEvent());
        }

        private void ResetMateri()
        {
            VirtualTrainingCamera.ResetCameraPosition(ResetMateriAction);
        }

        private void NextFigure(NextFigureEvent e)
        {
            if (figures.Count <= 1)
                return;

            ResetFigureAction();

            figureIndex++;

            if (figureIndex >= figures.Count)
                figureIndex = 0;

            PlayFigure(figures[figureIndex], true);
        }

        private void PreviousFigure(PrevFigureEvent e)
        {
            if (figures.Count <= 1)
                return;

            ResetFigureAction();

            figureIndex--;

            if (figureIndex < 0)
                figureIndex = figures.Count - 1;

            PlayFigure(figures[figureIndex], true);
        }

        void PlayVirtualButton()
        {
            for (int i = 0; i < figures[figureIndex].virtualButtonElement.Count; i++)
            {
                EventManager.TriggerEvent(new VirtualButtonEvent(figures[figureIndex].virtualButtonElement[i].virtualButtonElementType.GetDataRecursive(), figures[figureIndex].virtualButtonElement[i].overrideVirtualButton));
            }
            EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
        }

        void PlayAnimation()
        {
            AnimationManager.isSequenceAnimation = figures[figureIndex].isSequenceAnimation;

            for (int i = 0; i < figures[figureIndex].animationFigureDatas.Count; i++)
            {
                EventManager.TriggerEvent(new AnimationPlayEvent(figures[figureIndex].animationFigureDatas[i]));
            }

            EventManager.TriggerEvent(new AnimationSequenceDone());
        }
    }
}
