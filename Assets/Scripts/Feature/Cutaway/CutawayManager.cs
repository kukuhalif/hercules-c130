﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class CutawayManager : MonoBehaviour
    {
        Gizmo gizmo;
        private CutawayState currentState;
        private Matrix4x4 matrix;
        private GameObject cornerGizmoHelper;
        Cutaway defaultBound;

        [SerializeField] Transform gizmoParent;

        [SerializeField] LineRenderer[] lines;

        [SerializeField] Transform[] planePoints;
        [SerializeField] Transform[] boxPoints;
        [SerializeField] Transform[] cornerPoints;

        private void Start()
        {
            EventManager.AddListener<CutawayEvent>(CutawayEventListener);
            EventManager.AddListener<ObjectInteractionCutawayEvent>(ObjectInteractionListener);

            cornerGizmoHelper = new GameObject("corner gizmo helper");
            cornerGizmoHelper.transform.SetParent(transform);
            cornerGizmoHelper.transform.localPosition = Vector3.zero;
            cornerGizmoHelper.transform.localRotation = Quaternion.identity;

            for (int i = 0; i < lines.Length; i++)
            {
                lines[i].startWidth = 0.05f;
                lines[i].endWidth = 0.05f;
            }

            gizmo = Instantiate(Resources.Load<Gizmo>("Gizmo/Gizmo"));
            gizmo.SetParent(gizmoParent);
            gizmo.Disable();

            defaultBound = new Cutaway();
            var defaultBounds = GetBounds(VirtualTrainingSceneManager.GameObjectRoot);

            defaultBound.position = defaultBounds.center;
            defaultBound.scale = defaultBounds.size;
            defaultBound.rotation = Quaternion.identity.eulerAngles;
            defaultBound.state = CutawayState.Plane;
        }

        private void OnDisable()
        {
            Disable();
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<CutawayEvent>(CutawayEventListener);
            EventManager.RemoveListener<ObjectInteractionCutawayEvent>(ObjectInteractionListener);
        }

        private void OnApplicationQuit()
        {
            Disable();
        }

        private void CutawayEventListener(CutawayEvent e)
        {
            if (e.cutaway == null)
                Disable();
            else
                Enable(e.cutaway);
        }

        private void ObjectInteractionListener(ObjectInteractionCutawayEvent e)
        {
            if (e.on)
                Enable(defaultBound);
            else
                Disable();
        }

        public void BoundSetup()
        {
            var defaultBounds = GetBounds(VirtualTrainingSceneManager.GameObjectRoot);

            transform.position = defaultBounds.center;
            transform.localScale = defaultBounds.size;
            transform.rotation = Quaternion.identity;
        }

        private Bounds GetBounds(GameObject go)
        {
            Quaternion quat = go.transform.rotation;//object axis AABB

            Bounds bounds = new Bounds();

            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
            if (renderers.Length > 0)
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    if (i == 0)
                    {
                        bounds = renderers[i].bounds;
                    }
                    else
                    {
                        bounds.Encapsulate(renderers[i].bounds);
                    }
                }
            }

            Vector3 localCentre = go.transform.InverseTransformPoint(bounds.center);
            go.transform.rotation = quat;
            bounds.center = go.transform.TransformPoint(localCentre);

            return bounds;
        }

        private void SetObject(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            transform.position = position;
            transform.rotation = rotation;
            transform.localScale = scale;
        }

        private void Update()
        {
            if (currentState == CutawayState.Plane)
            {
                GizmoFollow();

                Shader.SetGlobalVector("_SectionPoint", transform.position);
                Shader.SetGlobalVector("_SectionPlane", transform.forward);

                DrawDebug();
            }
            else if (currentState == CutawayState.Box || currentState == CutawayState.Corner)
            {
                GizmoFollow();

                matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
                Shader.SetGlobalMatrix("_WorldToBoxMatrix", matrix.inverse);
                Shader.SetGlobalVector("_SectionScale", transform.localScale);

                DrawDebug();
            }
        }

        private void GizmoFollow()
        {
            transform.position = gizmo.transform.position;
            transform.rotation = gizmo.transform.rotation;
        }

        private void Disable()
        {
            currentState = CutawayState.None;

            Shader.DisableKeyword("CLIP_PLANE");
            Shader.SetGlobalInt("_CLIP_PLANE", 0);

            Shader.DisableKeyword("CLIP_BOX");
            Shader.SetGlobalInt("_CLIP_BOX", 0);

            Shader.DisableKeyword("CLIP_CORNER");
            Shader.SetGlobalInt("_CLIP_CORNER", 0);

            Shader.EnableKeyword("CLIP_NONE");

            if (gizmo != null)
                gizmo.Disable();

            for (int i = 0; i < lines.Length; i++)
            {
                lines[i].enabled = false;
            }
        }

        private void SetCutaway(CutawayState state)
        {
            currentState = state;

            switch (currentState)
            {
                case CutawayState.Plane:

                    Shader.DisableKeyword("CLIP_NONE");
                    Shader.EnableKeyword("CLIP_PLANE");
                    Shader.SetGlobalInt("_CLIP_PLANE", 1);

                    break;
                case CutawayState.Box:

                    Shader.DisableKeyword("CLIP_NONE");
                    Shader.EnableKeyword("CLIP_BOX");
                    Shader.SetGlobalInt("_CLIP_BOX", 1);

                    break;
                case CutawayState.Corner:

                    Shader.DisableKeyword("CLIP_NONE");
                    Shader.EnableKeyword("CLIP_CORNER");
                    Shader.SetGlobalInt("_CLIP_CORNER", 1);

                    break;
            }
        }

        private void Enable(Cutaway cutaway)
        {
            Disable();
            if (cutaway.state == CutawayState.None)
                return;

            Quaternion rotation = Quaternion.Euler(cutaway.rotation);

            gizmo.Enable(cutaway.position, rotation);

            SetObject(cutaway.position, rotation, cutaway.scale);
            SetCutaway(cutaway.state);

            if (currentState == CutawayState.Plane)
            {
                lines[0].enabled = true;
            }
            else if (currentState == CutawayState.Box)
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    lines[i].enabled = true;
                }
            }
            else if (currentState == CutawayState.Corner)
            {
                for (int i = 0; i < 3; i++)
                {
                    lines[i].enabled = true;
                }
            }
        }

        private void DrawDebug()
        {
            if (currentState == CutawayState.Plane)
            {
                for (int i = 0; i < 4; i++)
                {
                    lines[0].SetPosition(i, planePoints[i].position);
                }
            }
            else if (currentState == CutawayState.Box)
            {
                for (int i = 0; i < 4; i++)
                {
                    lines[0].SetPosition(i, boxPoints[i].position);
                }

                for (int i = 4; i < 8; i++)
                {
                    lines[1].SetPosition(i - 4, boxPoints[i].position);
                }

                for (int i = 8; i < 12; i++)
                {
                    lines[2].SetPosition(i - 8, boxPoints[i].position);
                }

                for (int i = 12; i < 16; i++)
                {
                    lines[3].SetPosition(i - 12, boxPoints[i].position);
                }
            }
            else if (currentState == CutawayState.Corner)
            {
                for (int i = 0; i < 4; i++)
                {
                    lines[0].SetPosition(i, cornerPoints[i].position);
                }

                for (int i = 4; i < 8; i++)
                {
                    lines[1].SetPosition(i - 4, cornerPoints[i].position);
                }

                for (int i = 8; i < 12; i++)
                {
                    lines[2].SetPosition(i - 8, cornerPoints[i].position);
                }
            }
        }

        //#if UNITY_EDITOR
        //        private void OnDrawGizmos()
        //        {
        //            Gizmos.color = Color.red;

        //            if (_currentState == CutawayState.Box)
        //            {
        //                Gizmos.matrix = Matrix4x4.TRS(transform.localPosition, transform.localRotation, transform.localScale);
        //                Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        //            }
        //            else if (_currentState == CutawayState.Corner)
        //            {
        //                if (_cornerGizmoHelper == null)
        //                    return;

        //                _cornerGizmoHelper.transform.localPosition = new Vector3(0.5f, 0.5f, 0.5f);
        //                Gizmos.matrix = Matrix4x4.TRS(_cornerGizmoHelper.transform.position, _cornerGizmoHelper.transform.rotation, transform.localScale);
        //                Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        //            }
        //            else if (_currentState == CutawayState.Plane)
        //            {
        //                Gizmos.matrix = Matrix4x4.TRS(transform.localPosition, transform.localRotation, new Vector3(transform.localScale.x, transform.localScale.y, 0));
        //                Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        //            }
        //        }
        //#endif
    }
}
