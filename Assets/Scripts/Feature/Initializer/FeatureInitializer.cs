﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Feature
{
    public class FeatureInitializer : MonoBehaviour
    {
        [SerializeField] InitializationQueue[] queue;

        private void Start()
        {
            if (VirtualTrainingSceneManager.GameObjectRoot == null)
            {
                Debug.LogError("game object root null");
                return;
            }
            Debug.Log("start initialization");
            StartCoroutine(Initialization());
        }

        private IEnumerator Initialization()
        {
            GameObject[] childs = VirtualTrainingSceneManager.GameObjectRoot.GetAllChilds();

            Debug.Log("start initiazing " + childs.Length + " objects");

            int counter = 0;

            for (int i = 0; i < childs.Length; i++)
            {
                for (int q = 0; q < queue.Length; q++)
                {
                    queue[q].Initialize(childs[i]);
                }

                // disable animator
                Animator anim = childs[i].GetComponent<Animator>();
                if (anim != null)
                    anim.enabled = false;

                if (counter > 500)
                {
                    counter = 0;
                    yield return null;
                }

                counter++;
            }

            Debug.Log("initialization finished");
            EventManager.TriggerEvent(new InializationDoneEvent());
            yield return null;
            Destroy(gameObject);
        }
    }
}
