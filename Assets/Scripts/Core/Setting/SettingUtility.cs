using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public static class SettingUtility
    {
        public static SettingData GetCurrentSetting()
        {
            string prefix = VirtualTrainingSceneManager.ProjectName;
            string json = "";
            SettingData data = null;

            if (PlayerPrefs.HasKey(prefix + " setting"))
            {
                json = PlayerPrefs.GetString(prefix + " setting");
                data = JsonUtility.FromJson<SettingData>(json);
            }

            if (data == null)
            {
                data = DatabaseManager.GetDefaultSetting();
            }

            return data;
        }

        public static void SetSettingData(SettingData data)
        {
            string prefix = VirtualTrainingSceneManager.ProjectName;
            string json = JsonUtility.ToJson(data);

            PlayerPrefs.SetString(prefix + " setting", json);
        }
    }
}