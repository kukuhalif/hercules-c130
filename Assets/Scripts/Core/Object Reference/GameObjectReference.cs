﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [DisallowMultipleComponent]
    public class GameObjectReference : MonoBehaviour
    {
        private static Dictionary<int, GameObjectReference> REFERENCE_LOOKUP = new Dictionary<int, GameObjectReference>();
        [FormerlySerializedAs("_id")]
        [SerializeField] private int id;

        public int Id { get => id; }

        public static GameObjectReference GetReference(int id)
        {
            return REFERENCE_LOOKUP[id];
        }

        public static void ClearGameobjectReferenceLookup()
        {
            REFERENCE_LOOKUP.Clear();
        }

        private void Awake()
        {
            if (Application.isPlaying)
                REFERENCE_LOOKUP.Add(Id, this);
        }

        public void SetId(int id)
        {
            this.id = id;
        }
    }
}