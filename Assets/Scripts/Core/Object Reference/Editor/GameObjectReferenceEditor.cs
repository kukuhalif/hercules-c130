﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Core
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(GameObjectReference))]
    public class GameObjectReferenceEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GameObjectReference gameObjectData = (GameObjectReference)target;
            GUILayout.Label("object Id : " + gameObjectData.Id.ToString());
        }
    }
}

#endif