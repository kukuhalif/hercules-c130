﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public class GameObjectType
    {
        [FormerlySerializedAs("_id")]
        [SerializeField] private int id = 0;
        [NonSerialized] private GameObjectReference reference;

        public GameObjectType()
        {

        }

        public GameObjectType(int id)
        {
            Id = id;
        }

        public GameObjectType(object obj)
        {
            GameObject go = obj as GameObject;
            if (go != null)
            {
                gameObject = go;
            }
        }

        private GameObjectReference gameObjectReference
        {
            get
            {
                if (id == 0)
                    return null;

                if (reference == null)
                {
                    if (Application.isPlaying)
                    {
                        // get reference from cached data
                        reference = GameObjectReference.GetReference(id);
                        return reference;
                    }
                    else
                    {
                        // expensive operation, only available in editor mode
                        var gameObjectReferences = GameObject.FindObjectsOfType<GameObjectReference>(true);
                        foreach (var gameObjectReference in gameObjectReferences)
                        {
                            if (gameObjectReference != null && gameObjectReference.Id == id)
                            {
                                reference = gameObjectReference;
                                return reference;
                            }
                        }
                    }
                }
                return reference;
            }
            set
            {
                if (value == null)
                {
                    Id = 0;
                }
                else
                {
                    if (value != null)
                    {
                        reference = value;
                        id = value.Id;
                    }
                    else
                    {
                        Id = 0;
                    }
                }
            }
        }

        public int Id
        {
            get => id;
            set
            {
                id = value;
                reference = null;
            }
        }

        public GameObject gameObject
        {
            get
            {
                if (gameObjectReference == null)
                    return null;
                else
                    return gameObjectReference.gameObject;
            }
            set
            {
#if UNITY_EDITOR
                // set only available in editor mode
                if (value == null)
                    gameObjectReference = null;
                else
                {
                    var gameObjectReferenceTemp = value.GetComponent<GameObjectReference>();
                    if (gameObjectReferenceTemp == null)
                    {
                        gameObjectReference = null;

                        Debug.LogWarning("GameObjectReference Component is null");
                    }
                    gameObjectReference = gameObjectReferenceTemp;
                }
#endif
            }
        }
    }
}