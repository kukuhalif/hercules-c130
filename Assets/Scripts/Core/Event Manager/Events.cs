﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paroxe.PdfRenderer;
using UnityEngine.Video;

namespace VirtualTraining.Core
{
    public class MateriEvent : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;

        public MateriEvent(MateriTreeElement materiTreeElement)
        {
            this.materiTreeElement = materiTreeElement;
        }
    }

    public class NextFigureEvent : VirtualTrainingEvent
    {

    }

    public class PrevFigureEvent : VirtualTrainingEvent
    {

    }

    public class PlayMateriFromDesciptionPanel : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;

        public PlayMateriFromDesciptionPanel(MateriTreeElement materiTreeElement)
        {
            this.materiTreeElement = materiTreeElement;
        }
    }

    public class FreePlayEvent : VirtualTrainingEvent
    {

    }

    public class PlayFigureEvent : VirtualTrainingEvent
    {
        public string name;
        public int currentFigure;
        public int figureCount;

        public PlayFigureEvent(string name, int currentFigure, int figureCount)
        {
            this.name = name;
            this.currentFigure = currentFigure;
            this.figureCount = figureCount;
        }
    }

    public class CloseFigureEvent : VirtualTrainingEvent
    {

    }

    public class PartObjectEvent : VirtualTrainingEvent
    {
        public List<PartObject> objects;

        public PartObjectEvent(List<PartObject> objects)
        {
            this.objects = objects;
        }

        public PartObjectEvent()
        {
            objects = null;
        }
    }

    public class FresnelObjectsEvent : VirtualTrainingEvent
    {
        public List<PartObject> partObjects;

        public FresnelObjectsEvent()
        {

        }

        public FresnelObjectsEvent(List<GameObjectType> targets, Color color, bool recursive = true, bool withBlink = false)
        {
            partObjects = new List<PartObject>();
            for (int i = 0; i < targets.Count; i++)
            {
                PartObject poj = new PartObject();
                poj.action = PartObjectAction.Fresnel;
                poj.useFresnelDefaultColor = false;
                poj.fresnelColor = color;
                poj.fresnelNoRecursive = !recursive;
                poj.fresnelWithBlink = withBlink;
                poj.target = targets[i];
                partObjects.Add(poj);
            }
        }

        public FresnelObjectsEvent(List<GameObjectType> targets, bool recursive = true, bool withBlink = false)
        {
            partObjects = new List<PartObject>();
            for (int i = 0; i < targets.Count; i++)
            {
                PartObject poj = new PartObject();
                poj.action = PartObjectAction.Fresnel;
                poj.useFresnelDefaultColor = true;
                poj.fresnelNoRecursive = !recursive;
                poj.fresnelWithBlink = withBlink;
                poj.target = targets[i];
                partObjects.Add(poj);
            }
        }
    }

    public class VirtualButtonEvent : VirtualTrainingEvent
    {
        public List<VirtualButtonDataModel> virtualButtonDatas;
        public List<OverrideVariableVirtualButton> overrideData;

        public VirtualButtonEvent(List<VirtualButtonDataModel> virtualButtonDatas, List<OverrideVariableVirtualButton> overrideData = null)
        {
            this.virtualButtonDatas = virtualButtonDatas;
            this.overrideData = overrideData;
        }
    }

    public class VirtualButtonEventInstantiate : VirtualTrainingEvent
    {
    }

    public class VirtualButtonEventDestroy : VirtualTrainingEvent
    {

    }

    public class HelperEvent : VirtualTrainingEvent
    {
        public List<GameObject> helpers;

        public HelperEvent(List<GameObject> helpers)
        {
            this.helpers = helpers;
        }

        public HelperEvent()
        {
            helpers = null;
        }
    }

    public class MonitorCameraEvent : VirtualTrainingEvent
    {
        public List<MonitorCamera> monitors;

        public MonitorCameraEvent(List<MonitorCamera> monitors)
        {
            this.monitors = monitors;
        }

        public MonitorCameraEvent()
        {
            monitors = null;
        }
    }

    public class PlayMonitorCameraEvent : VirtualTrainingEvent
    {
        public int index;
        public MonitorCamera destination;

        public PlayMonitorCameraEvent(int index, MonitorCamera destination)
        {
            this.index = index;
            this.destination = destination;
        }
    }

    public class CutawayEvent : VirtualTrainingEvent
    {
        public Cutaway cutaway;

        public CutawayEvent(Cutaway cutaway)
        {
            this.cutaway = cutaway;
        }

        public CutawayEvent()
        {
            cutaway = null;
        }
    }

    public class InializationDoneEvent : VirtualTrainingEvent
    {

    }

    public class InitializationUIDoneEvent : VirtualTrainingEvent
    {

    }

    public class TroubleshootUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public TroubleshootUIEvent(Materi data)
        {
            materi = data;
        }
        public TroubleshootUIEvent()
        {

        }
    }

    public class TroubleshootPlayEvent : VirtualTrainingEvent
    {
        public TroubleshootDataModel troubleshootData;

        public TroubleshootPlayEvent(TroubleshootDataModel data)
        {
            troubleshootData = data;
        }

        public TroubleshootPlayEvent()
        {

        }
    }

    public class MaintenanceUIEvent : VirtualTrainingEvent
    {
        public Materi materi;
        public MaintenanceUIEvent(Materi data)
        {
            materi = data;
        }
        public MaintenanceUIEvent()
        {

        }
    }

    public class MaintenancePlayEvent : VirtualTrainingEvent
    {
        public MaintenanceDataModel maintenanceData;

        public MaintenancePlayEvent(MaintenanceDataModel data)
        {
            maintenanceData = data;
        }

        public MaintenancePlayEvent()
        {

        }
    }

    public class AnimationPlayEvent : VirtualTrainingEvent
    {
        public AnimationFigureData animationData;

        public AnimationPlayEvent(AnimationFigureData data)
        {
            animationData = data;
        }

        public AnimationPlayEvent()
        {
            animationData = null;
        }
    }

    public class AnimationSequenceDone : VirtualTrainingEvent
    {

    }

    public class DescriptionPanelEvent : VirtualTrainingEvent
    {
        public MateriTreeElement materiTreeElement;

        public DescriptionPanelEvent(MateriTreeElement materiTreeElement)
        {
            this.materiTreeElement = materiTreeElement;
        }

        public DescriptionPanelEvent()
        {
            materiTreeElement = null;
        }
    }

    public class ShowPdfEvent : VirtualTrainingEvent
    {
        public PdfData pdfData;
        public PDFAsset pdfAsset;

        public ShowPdfEvent(PdfData pdfData)
        {
            this.pdfData = pdfData;
        }

        public ShowPdfEvent(PDFAsset pdfAsset)
        {
            this.pdfAsset = pdfAsset;
        }

        public ShowPdfEvent()
        {
            pdfData = null;
            pdfAsset = null;
        }
    }

    public class OnClosedPdfPanelEvent : VirtualTrainingEvent
    {

    }

    public class ShowPdfLinkEvent : VirtualTrainingEvent
    {
        public string id;

        public ShowPdfLinkEvent(string id)
        {
            this.id = id;
        }
    }

    public class MinimizeUIPanelEvent : VirtualTrainingEvent
    {
        public string panelName;
        public Action maximizeCallback;

        public MinimizeUIPanelEvent(string panelName, Action maximizeCallback)
        {
            this.panelName = panelName;
            this.maximizeCallback = maximizeCallback;
        }
    }

    public class ShowUIPanelEvent : VirtualTrainingEvent
    {
        public string name;

        public ShowUIPanelEvent(string name)
        {
            this.name = name;
        }
    }

    public class CloseUIPanelEvent : VirtualTrainingEvent
    {
        public string name;

        public CloseUIPanelEvent(string name)
        {
            this.name = name;
        }
    }

    public class SetSchematicDataEvent : VirtualTrainingEvent
    {
        public List<Schematic> schematics;

        public SetSchematicDataEvent(List<Schematic> schematics)
        {
            this.schematics = schematics;
        }

        public SetSchematicDataEvent()
        {
            schematics = null;
        }
    }

    public class ShowSchematicEvent : VirtualTrainingEvent
    {
        public Schematic schematic;

        public ShowSchematicEvent(Schematic schematic)
        {
            this.schematic = schematic;
        }
    }

    public class ShowSchematicLinkEvent : VirtualTrainingEvent
    {
        public string id;

        public ShowSchematicLinkEvent(string id)
        {
            this.id = id;
        }
    }

    public class DisableBackgroundEvent : VirtualTrainingEvent
    {
        public bool disable;

        public DisableBackgroundEvent(bool disable)
        {
            this.disable = disable;
        }

        public DisableBackgroundEvent()
        {
            disable = false;
        }
    }

    public class MonitorCameraInteractionEvent : VirtualTrainingEvent
    {
        public GameObject interactionObject;

        public MonitorCameraInteractionEvent(GameObject interactionObject)
        {
            this.interactionObject = interactionObject;
        }
    }

    public class ObjectInteractionRotateEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionRotateEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionMultipleSelectionEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionMultipleSelectionEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionPullApartEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionPullApartEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionResetCameraEvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionResetObjectEvent : VirtualTrainingEvent
    {

    }

    public class ObjectInteractionLockCameraEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionLockCameraEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionXRayEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionXRayEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionCutawayEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionCutawayEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectInteractionVisibleEvent : VirtualTrainingEvent
    {
        public bool visible;

        public ObjectInteractionVisibleEvent(bool visible)
        {
            this.visible = visible;
        }
    }

    public class ObjectInteractionShowPartObjectListEvent : VirtualTrainingEvent
    {
        public bool on;

        public ObjectInteractionShowPartObjectListEvent(bool on)
        {
            this.on = on;
        }
    }

    public class ObjectListPanelEvent : VirtualTrainingEvent
    {
        public bool open;

        public ObjectListPanelEvent(bool open)
        {
            this.open = open;
        }
    }

    public class HideObjectEvent : VirtualTrainingEvent
    {

    }

    public class SwitchUIThemeEvent : VirtualTrainingEvent
    {
        public int index;

        public SwitchUIThemeEvent(int index)
        {
            this.index = index;
        }
    }

    public class ApplyUIThemesEvent : VirtualTrainingEvent
    {

    }

    public class ShowAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class MinimizeAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class CloseAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class ResetAllUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class ConfirmationPanelEvent : VirtualTrainingEvent
    {
        public string text;
        public string yesText;
        public string noText;
        public Action onYesCallback;
        public Action onNoCallback;

        public ConfirmationPanelEvent(string text, Action onYesCallback, Action onNoCallback, string yesText = "Yes", string noText = "No")
        {
            this.text = text;
            this.yesText = yesText;
            this.noText = noText;
            this.onYesCallback = onYesCallback;
            this.onNoCallback = onNoCallback;
        }
    }

    public class CloseConfirmationPanelEvent : VirtualTrainingEvent
    {

    }

    public class ApplySettingEvent : VirtualTrainingEvent
    {
        public SettingData data;

        public ApplySettingEvent(SettingData data)
        {
            this.data = data;
        }
    }

    public class ShowStartingUIPanelEvent : VirtualTrainingEvent
    {

    }

    public class ShowLoadingScreenEvent : VirtualTrainingEvent
    {
        public List<VideoClip> clips;
        public bool hideUI;
        public Func<float> progress;
        public Func<string> label;
        public Action endLoadingCallback;
        public bool waitVideo;
        public bool animatePanel;

        public ShowLoadingScreenEvent(List<VideoClip> clips, bool hideUI, bool animatePanel, Func<float> progress, Func<string> label, Action endLoadingCallback, bool waitVideo)
        {
            this.clips = clips;
            this.hideUI = hideUI;
            this.progress = progress;
            this.label = label;
            this.endLoadingCallback = endLoadingCallback;
            this.waitVideo = waitVideo;
            this.animatePanel = animatePanel;
        }
    }

    public class ObjectListToggleStateChangedEvent : VirtualTrainingEvent
    {

    }

    public class AddMovedObjectEvent : VirtualTrainingEvent
    {
        public GameObject movedObject;

        public AddMovedObjectEvent(GameObject movedObject)
        {
            this.movedObject = movedObject;
        }

        public AddMovedObjectEvent()
        {
            movedObject = null;
        }
    }

    public class PartObjectCullingEvent : VirtualTrainingEvent
    {
        public List<GameObject> shows = new List<GameObject>();

        public PartObjectCullingEvent(List<GameObject> shows)
        {
            this.shows = shows;
        }
    }
}
