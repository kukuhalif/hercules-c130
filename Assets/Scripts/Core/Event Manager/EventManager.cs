﻿using System;
using System.Collections.Generic;

namespace VirtualTraining.Core
{
    public abstract class VirtualTrainingEvent { }

    public class EventManager
    {
        public delegate void DelegateEvent<T>(T evt);
        private static Dictionary<Type, Delegate> DELEGATES = new Dictionary<Type, Delegate>();

        /// <summary>
        /// Assign/add delegate function into the dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="del"></param>
        public static void AddListener<T>(DelegateEvent<T> del) where T : VirtualTrainingEvent
        {
            Delegate tempDelegate;
            if (DELEGATES.TryGetValue(typeof(T), out tempDelegate))
            {
                DelegateEvent<T> method = tempDelegate as DelegateEvent<T>;
                DELEGATES[typeof(T)] = method += del;
            }
            else
                DELEGATES[typeof(T)] = del;
        }

        /// <summary>
        /// Call delegate function accordingly.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="evt"></param>
        public static void TriggerEvent<T>(T evt)
        {
            Delegate tempDelegate;
            if (DELEGATES.TryGetValue(typeof(T), out tempDelegate))
            {
                DelegateEvent<T> del = tempDelegate as DelegateEvent<T>;
                del(evt);
            }
        }

        /// <summary>
        /// Remove delegate function from the dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="del"></param>
        public static void RemoveListener<T>(DelegateEvent<T> del) where T : VirtualTrainingEvent
        {
            Delegate tempDelegate;
            if (DELEGATES.TryGetValue(typeof(T), out tempDelegate))
            {
                DelegateEvent<T> method = tempDelegate as DelegateEvent<T>;
                method -= del;

                if (method != null)
                    DELEGATES[typeof(T)] = method;
                else
                    DELEGATES.Remove(typeof(T));
            }
        }

        public static void RemoveAllListeners()
        {
            DELEGATES.Clear();
        }
    }

}
