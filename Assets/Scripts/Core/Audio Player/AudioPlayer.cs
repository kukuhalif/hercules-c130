﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public enum SFX
    {
        ButtonClick,
        MinimizePanel,
        ShowPanel,
        ClosePanel,
        WindowedPanel,
        MaximizePanel,
        ExpandTreeElement,
        CollapseTreeElement
    }

    public static class AudioPlayer
    {
        private static AudioSourcePlayer AUDIO_SOURCE_PLAYER;

        public static void Initialize(AudioSourcePlayer audioSourcePlayer)
        {
            AUDIO_SOURCE_PLAYER = audioSourcePlayer;
        }

        public static void PlaySFX(SFX sfx)
        {
            if (AUDIO_SOURCE_PLAYER == null)
            {
                Debug.LogError("audio player null");
                return;
            }

            AUDIO_SOURCE_PLAYER.PlaySFX(sfx);
        }

        public static void SetSFXVolume(float volume)
        {
            if (AUDIO_SOURCE_PLAYER == null)
            {
                Debug.LogError("audio player null");
                return;
            }

            AUDIO_SOURCE_PLAYER.SetSFXVolume(volume);
        }
    }
}
