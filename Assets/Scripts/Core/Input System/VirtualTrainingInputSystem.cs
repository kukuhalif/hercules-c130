﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.EventSystems;
using Valve.VR;

namespace VirtualTraining.Core
{
    public class VirtualTrainingInputSystem : MonoBehaviour
    {
        PlayerInput playerInput;

        public delegate void OnInputAction(Vector2 pointerPosition, GameObject raycastedUI);
        public delegate void OnInputAxisAction(Vector2 axisValue);

        Vector2 leftClickStartAxisValue;
        bool leftClickDown;
        public static event OnInputAction OnStartLeftClick;
        public static event OnInputAxisAction OnLeftClickDown;
        public static event OnInputAction OnEndLeftClick;

        Vector2 rightClickStartAxisValue;
        bool rightClickDown;
        public static event OnInputAction OnStartRightClick;
        public static event OnInputAxisAction OnRightClickDown;
        public static event OnInputAction OnEndRightClick;

        Vector2 middleClickStartAxisValue;
        bool _middleClickDown;
        public static event OnInputAction OnStartMiddleClick;
        public static event OnInputAxisAction OnMiddleClickDown;
        public static event OnInputAction OnEndMiddleClick;

        public delegate void OnMouseScrollAction(float value, GameObject raycastedUI);
        public static event OnMouseScrollAction OnMouseScroll;

        public delegate void OnMoveAction(Vector2 deltaPosition);
        public static event OnMoveAction OnMovePointer;

        public delegate void OnTouched(bool isFingerOverUI);
        public static event OnTouched OnTouchAdded;
        public static event OnTouched OnTouchRemoved;

        private static List<Finger> ACTIVE_FINGERS = new List<Finger>();
        public static int TouchCount { get => ACTIVE_FINGERS.Count; }
        public SteamVR_Action_Boolean grabObject;

        private GameObject raycastedUI;
        bool isListenerAdded;

        private void Awake()
        {
            playerInput = GetComponent<PlayerInput>();
        }

        private void Start()
        {
            EventManager.AddListener<ConfirmationPanelEvent>(ConfirmationPanelListener);
            EventManager.AddListener<CloseConfirmationPanelEvent>(CloseConfirmationPanelListener);
            AddInputHandleListener();
            grabObject = SteamVR_Input.GetBooleanAction("GrabObject");
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ConfirmationPanelEvent>(ConfirmationPanelListener);
            EventManager.RemoveListener<CloseConfirmationPanelEvent>(CloseConfirmationPanelListener);
            RemoveInputHandleListener();
        }

        private void AddInputHandleListener()
        {
            if (!isListenerAdded)
            {
                UnityEngine.InputSystem.EnhancedTouch.EnhancedTouchSupport.Enable();
                playerInput.onActionTriggered += HandleAction;
                UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += Touch_onFingerDown;
                UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += Touch_onFingerUp;

                isListenerAdded = true;
            }
        }

        private void RemoveInputHandleListener()
        {
            if (isListenerAdded)
            {
                UnityEngine.InputSystem.EnhancedTouch.EnhancedTouchSupport.Disable();
                playerInput.onActionTriggered -= HandleAction;
                UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= Touch_onFingerDown;
                UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= Touch_onFingerUp;

                isListenerAdded = false;
            }
        }

        private void ConfirmationPanelListener(ConfirmationPanelEvent e)
        {
            RemoveInputHandleListener();
        }

        private void CloseConfirmationPanelListener(CloseConfirmationPanelEvent e)
        {
            AddInputHandleListener();
        }

        private void Touch_onFingerDown(Finger obj)
        {
            ACTIVE_FINGERS.Add(obj);
            OnTouchAdded?.Invoke(IsTouchOverUI(obj.screenPosition));
        }

        private void Touch_onFingerUp(Finger obj)
        {
            ACTIVE_FINGERS.Remove(obj);
            OnTouchRemoved?.Invoke(IsTouchOverUI(obj.screenPosition));
        }

        public static UnityEngine.InputSystem.EnhancedTouch.Touch GetTouch(int index)
        {
            return ACTIVE_FINGERS[index].currentTouch;
        }

        public static Vector2 PointerPosition
        {
            get
            {
                if (ACTIVE_FINGERS.Count > 0)
                    return ACTIVE_FINGERS[0].screenPosition;
                return Input.mousePosition;
            }
        }

        private bool IsPointerOverUI()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            if (results.Count > 0)
                raycastedUI = results[0].gameObject;
            else
                raycastedUI = null;

            return results.Count > 0;
        }

        private bool IsTouchOverUI(Vector2 screenPosition)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = screenPosition;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            if (results.Count > 0)
                raycastedUI = results[0].gameObject;
            else
                raycastedUI = null;

            return results.Count > 0;
        }

        private void Update()
        {
            if (leftClickDown)
                OnLeftClickDown?.Invoke(PointerPosition - leftClickStartAxisValue);

            if (rightClickDown)
                OnRightClickDown?.Invoke(PointerPosition - rightClickStartAxisValue);

            if (_middleClickDown)
                OnMiddleClickDown?.Invoke(PointerPosition - middleClickStartAxisValue);

            if (grabObject != null && grabObject.changed)
            {
                if (!IsPointerOverUI() && grabObject.stateDown)
                    LeftClickDown();
                else if (grabObject.stateUp)
                    LeftClickUp();
            }
        }

        private void HandleAction(InputAction.CallbackContext context)
        {
            switch (context.action.name)
            {
                case "PointerDeltaPosition":

                    OnMovePointer?.Invoke(context.action.ReadValue<Vector2>());

                    break;

                case "MouseScroll":

                    IsPointerOverUI();
                    OnMouseScroll?.Invoke(context.action.ReadValue<float>(), raycastedUI);

                    break;

                case "LeftClick":

                    if (context.phase == InputActionPhase.Started)
                    {
                        LeftClickDown();
                    }
                    else if (context.phase == InputActionPhase.Canceled)
                    {
                        LeftClickUp();
                    }

                    break;

                case "RightClick":

                    if (context.phase == InputActionPhase.Started)
                    {
                        rightClickStartAxisValue = PointerPosition;
                        IsPointerOverUI();
                        OnStartRightClick?.Invoke(rightClickStartAxisValue, raycastedUI);
                        rightClickDown = true;
                    }
                    else if (context.phase == InputActionPhase.Canceled)
                    {
                        IsPointerOverUI();
                        OnEndRightClick?.Invoke(PointerPosition, raycastedUI);
                        rightClickDown = false;
                    }

                    break;

                case "MiddleClick":

                    if (context.phase == InputActionPhase.Started)
                    {
                        middleClickStartAxisValue = PointerPosition;
                        IsPointerOverUI();
                        OnStartMiddleClick?.Invoke(middleClickStartAxisValue, raycastedUI);
                        _middleClickDown = true;
                    }
                    else if (context.phase == InputActionPhase.Canceled)
                    {
                        IsPointerOverUI();
                        OnEndMiddleClick?.Invoke(PointerPosition, raycastedUI);
                        _middleClickDown = false;
                    }

                    break;
            }
        }

        void LeftClickDown()
        {
            leftClickStartAxisValue = PointerPosition;
            IsPointerOverUI();
            OnStartLeftClick?.Invoke(leftClickStartAxisValue, raycastedUI);
            leftClickDown = true;
        }

        void LeftClickUp()
        {
            IsPointerOverUI();
            OnEndLeftClick?.Invoke(PointerPosition, raycastedUI);
            leftClickDown = false;
        }
    }
}