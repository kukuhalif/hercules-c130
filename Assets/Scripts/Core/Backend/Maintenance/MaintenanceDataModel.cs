﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MaintenanceDataModel
    {
        public string title;
        public string description;
        public bool hideDescriptionPanel;
        public bool showDescriptionPopup;
        public bool disableBackground;
        public Figure figure;
        public List<Schematic> schematics;
        public string descriptionPopUp;

        public MaintenanceDataModel()
        {
            title = "New Maintenace Data";
            figure = new Figure();
            schematics = new List<Schematic>();
            description = "";
            hideDescriptionPanel = false;
            showDescriptionPopup = false;
            disableBackground = false;
            schematics = new List<Schematic>();
            descriptionPopUp = "";
        }
    }
}
