﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MaintenanceData
    {
        [SerializeField] public List<MaintenanceTreeElement> treeElements = new List<MaintenanceTreeElement>();
    }

    //[CreateAssetMenu(fileName = "New Maintenance Database", menuName = "Virtual Training/Maintenance Database")]
    public class MaintenanceTreeAsset : ScriptableObjectBase<MaintenanceData>
    {
        [FormerlySerializedAs("_maintenanceData")]
        [SerializeField] MaintenanceData maintenanceData;

        public MaintenanceData MaintenanceData
        {
            get
            {
                for (int i = 0; i < maintenanceData.treeElements.Count; i++)
                {
                    if (maintenanceData.treeElements[i].depth == -1)
                    {
                        maintenanceData.treeElements[i].name = name;
                        break;
                    }
                }
                return maintenanceData;
            }
        }

        void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (maintenanceData == null)
                maintenanceData = new MaintenanceData();

            if (maintenanceData.treeElements.Count == 0)
            {
                var root = new MaintenanceTreeElement("Root", -1, 0);
                maintenanceData.treeElements.Add(root);
            }
        }

        public override MaintenanceData GetData()
        {
            return MaintenanceData;
        }

        public override void SetData(MaintenanceData data)
        {
            maintenanceData = data;
        }
    }
}
