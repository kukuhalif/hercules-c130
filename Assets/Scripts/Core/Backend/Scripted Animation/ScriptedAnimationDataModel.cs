﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    //[Serializable]
    //public enum TypeAnimation { Move, Rotate }

    [Serializable]
    public class ScriptedAnimationDataModel
    {
        public List<SADataKeyframe> animData = new List<SADataKeyframe>();
        public bool globalCoordinate;
        public ScriptedAnimationDataModel(string name)
        {
            animData = new List<SADataKeyframe>();
            globalCoordinate = false;
        }

        public ScriptedAnimationDataModel()
        {
            animData = new List<SADataKeyframe>();
            globalCoordinate = false;
        }
    }

    [Serializable]
    public class SADataKeyframe
    {
        public string nameSA;
        public float duration;
        //public TypeAnimation type;
        [NonSerialized] public bool isStartRecording;
        //public bool globalCoordinate;
        //public bool isInverse;
        public Vector3 deltaPosition;
        public Vector3 deltaRotation;

        public SADataKeyframe()
        {
            duration = 1;
        }
    }
}
