﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;


public class AnimationController : MonoBehaviour
{
    bool isLoop;
    Transform _transform;
    public List<AnimationScript> animationScripts = new List<AnimationScript>();
    public Animator anim;
 

    private void Start()
    {
        _transform = GetComponent<Transform>();
    }

    public void Play(string state)
    {
        for (int i = 0; i < animationScripts.Count; i++)
        {
            if(state == animationScripts[i].state)
            {
                //animationScripts[i].Play(_transform);
                break;
            }
        }
        anim.Play(state);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Play("anim1");
        }
    }

}
