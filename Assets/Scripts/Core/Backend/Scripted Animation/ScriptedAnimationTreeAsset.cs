﻿using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class ScriptedAnimationData
    {
        [SerializeField] public List<ScriptedAnimationTreeElement> treeElements = new List<ScriptedAnimationTreeElement>();
    }

    [CreateAssetMenu(fileName = "New Scripted Animation Database", menuName = "Virtual Training/Scripted Animation Database")]
    public class ScriptedAnimationTreeAsset : ScriptableObjectBase<ScriptedAnimationData>
    {
        [SerializeField] ScriptedAnimationData _scriptedAnimationData;
        [SerializeField] ScriptedAnimationData scriptedAnimationData;

        public ScriptedAnimationData ScriptedAnimationData
        {
            get
            {
                for (int i = 0; i < _scriptedAnimationData.treeElements.Count; i++)
                {
                    if (_scriptedAnimationData.treeElements[i].depth == -1)
                    {
                        _scriptedAnimationData.treeElements[i].name = name;
                        break;
                    }
                }
                return _scriptedAnimationData;
            }
        }

        void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (_scriptedAnimationData == null)
                _scriptedAnimationData = new ScriptedAnimationData();

            if (_scriptedAnimationData.treeElements.Count == 0)
            {
                var root = new ScriptedAnimationTreeElement("Root", -1, 0);
                _scriptedAnimationData.treeElements.Add(root);
            }
        }

        public override ScriptedAnimationData GetData()
        {
            return ScriptedAnimationData;
        }

        public override void SetData(ScriptedAnimationData data)
        {
            _scriptedAnimationData = data;
        }
    }
}
