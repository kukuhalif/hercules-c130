﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class AnimationSetting
    {
        public enum Timing { Append, Join, Insert }
        public enum Type { Move, Rotate }

        public Timing timing;
        public Type type;
        public bool globalCoordinate;
        public float duration;
        public float insertEnterTime;
        public Vector3 value;

        //public void AddToSequence(Transform target, ref Vector3 currentPosition, ref Quaternion currentRotation, Sequence sequence)
        //{
        //    Tweener tweener = null;
        //    if (globalCoordinate)
        //    {
        //        if (type == Type.Move)
        //        {
        //            currentPosition += value;
        //            tweener = target.DOMove(currentPosition, duration);
        //        }
        //        else if (type == Type.Rotate)
        //        {
        //            currentRotation.eulerAngles += value;
        //            tweener = target.DORotate(currentRotation.eulerAngles, duration);
        //        }
        //    }
        //    else
        //    {
        //        if (type == Type.Move)
        //        {
        //            currentPosition += value;
        //            tweener = target.DOLocalMove(currentPosition, duration);
        //        }
        //        else if (type == Type.Rotate)
        //        {
        //            currentRotation.eulerAngles += value;
        //            tweener = target.DOLocalRotate(currentRotation.eulerAngles, duration);
        //        }
        //    }

        //    switch (timing)
        //    {
        //        case Timing.Append:
        //            sequence.Append(tweener);
        //            break;
        //        case Timing.Join:
        //            sequence.Join(tweener);
        //            break;
        //        case Timing.Insert:
        //            sequence.Insert(insertEnterTime, tweener);
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }
    [System.Serializable]
    [CreateAssetMenu(fileName = "Animation Script", menuName = "Virtual Training/Animated Script")]
    public class AnimationScript : ScriptableObject
    {
        public string state;
        public List<AnimationSetting> AnimationList = new List<AnimationSetting>();  
        Vector3 originalPosition, currentPosition;
        Quaternion originalRotation, currentRotation;
        //public void Play(Transform transform)
        //{
        //    currentPosition = transform.position;
        //    currentRotation = transform.rotation;
        //    Sequence sequence = DOTween.Sequence();

        //    for (int i = 0; i < AnimationList.Count; i++)
        //    {
        //        AnimationList[i].AddToSequence(transform, ref currentPosition, ref currentRotation, sequence);
        //    }
        //}
    }
}
