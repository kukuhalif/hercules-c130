﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace VirtualTraining.Core
{
    [Serializable]
    public class AnimatorControllerType : GameObjectType
    {
        [NonSerialized] Animator animator;

        public Animator animationController
        {
            get
            {
                if(animator != null)
                {
                    return animator;
                }
                else
                {
                    if (gameObject != null)
                    {
                        animator = gameObject.GetComponent<Animator>();
                        return animator;
                    }
                    else
                        return null;

                }
            }
            set
            {
                if(value == null)
                {
                    animator = null;
                    gameObject = null;
                }
                else
                {
                    animator = value;
                    gameObject = animator.gameObject;
                }
            }
        }
    }
}
