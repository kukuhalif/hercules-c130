using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class MateriData
    {
        [SerializeField] public List<MateriTreeElement> treeElements = new List<MateriTreeElement>();
    }

    [CreateAssetMenu(fileName = "New Materi Database", menuName = "Virtual Training/Materi Database")]
    public class MateriTreeAsset : ScriptableObjectBase<MateriData>
    {
        [FormerlySerializedAs("_materiData")]
        [SerializeField] MateriData materiData;

        public MateriData MateriData
        {
            get
            {
                for (int i = 0; i < materiData.treeElements.Count; i++)
                {
                    if (materiData.treeElements[i].depth == -1)
                    {
                        materiData.treeElements[i].name = name;
                        break;
                    }
                }
                return materiData;
            }
        }

        void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (materiData == null)
                materiData = new MateriData();

            if (materiData.treeElements.Count == 0)
            {
                var root = new MateriTreeElement("Root", -1, 0, MateriElementType.Materi);
                materiData.treeElements.Add(root);
            }
        }

        public override MateriData GetData()
        {
            return MateriData;
        }

        public override void SetData(MateriData data)
        {
            materiData = data;
        }
    }
}
