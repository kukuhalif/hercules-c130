﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Paroxe.PdfRenderer;

namespace VirtualTraining.Core
{
    public enum MateriElementType
    {
        Materi,
        Maintenance,
        Troubleshoot
    }

    public enum PartObjectAction
    {
        Solo,
        Disable,
        Highlight,
        Xray,
        Enable,
        Select,
        Blink,
        SetLayer,
        IgnoreCutaway,
        Fresnel
    };

    public enum LayerOption
    {
        MainCamera,
        MonitorCamera
    }

    public enum CutawayState
    {
        None,
        Plane,
        Box,
        Corner
    }

    public enum OverrideVirtualButton
    {
        DefaultValue,
    }


    [Serializable]
    public class PartObject
    {
        public GameObjectType target;
        public PartObjectAction action;

        public bool useDefaultHighlightColor;
        public bool setHighlightColor;
        public Color highlightColor;
        public bool highlightNoRecursive;

        public Color blinkColor;
        public bool useDefaultBlinkColor;
        public bool blinkNoRecursive;
        public float blinkSpeed;

        public int xrayIndex;
        public LayerOption layerOption;

        public bool fresnelNoRecursive;
        public bool fresnelWithBlink;
        public bool useFresnelDefaultColor;
        public Color fresnelColor;

        public PartObject(object obj) : this()
        {
            target = new GameObjectType();
            GameObject gameObject = obj as GameObject;
            GameObjectReference gor = gameObject.GetComponent<GameObjectReference>();
            if (gor != null)
                target.Id = gor.Id;
        }

        public PartObject()
        {
            target = new GameObjectType();
            action = PartObjectAction.Solo;
            useDefaultHighlightColor = true;
            setHighlightColor = false;
            highlightColor = Color.white;
            highlightNoRecursive = false;
            blinkColor = Color.white;
            useDefaultBlinkColor = true;
            blinkNoRecursive = false;
            blinkSpeed = 1f;
            xrayIndex = 0;
            layerOption = LayerOption.MainCamera;
            useFresnelDefaultColor = true;
            fresnelColor = Color.white;
        }
    }

    [Serializable]
    public class CameraDestination
    {
        public Vector3 position;
        public Quaternion rotation;
        public bool isOrthographic;
        public float orthographicSize;
        public float fov;
        public GameObjectType target;

        public CameraDestination()
        {
            position = Vector3.one;
            rotation = Quaternion.identity;
            isOrthographic = false;
            orthographicSize = 5f;
            fov = 50f;
            target = new GameObjectType();
        }

        public CameraDestination(CameraDestination obj) : this()
        {
            this.position = obj.position;
            this.rotation = obj.rotation;
            this.isOrthographic = obj.isOrthographic;
            this.orthographicSize = obj.orthographicSize;
            this.fov = obj.fov;
            if (obj.target != null && obj.target.gameObject != null)
                this.target.Id = obj.target.Id;
        }

        public override string ToString()
        {
            if (target.gameObject == null)
                return "pos : " + position + ", " + (isOrthographic ? ("orthographic, size : " + orthographicSize) : ("perspective, fov : " + fov));

            return "pos : " + target.gameObject.transform.position + ", " + (isOrthographic ? ("orthographic, size : " + orthographicSize) : ("perspective, fov : " + fov));
        }
    }

    [Serializable]
    public class MonitorCamera
    {
        public string displayName;
        public CameraDestination destination;

        public MonitorCamera()
        {
            displayName = "new monitor camera";
            destination = new CameraDestination();
        }
    }

    [Serializable]
    public class Schematic
    {
        public VideoClip video;
        public Texture2D image;
        public bool isLooping;
        public bool showImmediately;
        public string descriptionId;

        public Schematic(object param) : this()
        {
            VideoClip vid = param as VideoClip;
            Texture2D tex = param as Texture2D;

            if (vid != null)
                video = vid;
            else if (tex != null)
                image = tex;
        }

        public Schematic()
        {
            isLooping = true;
            showImmediately = false;
            descriptionId = "";
        }

        public Schematic(VideoClip video, Texture2D image, bool isLooping, bool showImmediately, string descriptionId) : this()
        {
            this.video = video;
            this.image = image;
            this.isLooping = isLooping;
            this.showImmediately = showImmediately;
            this.descriptionId = descriptionId;
        }
    }

    [Serializable]
    public class Cutaway
    {
        public CutawayState state;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;

        public Cutaway()
        {
            state = CutawayState.None;
            position = new Vector3();
            rotation = new Vector3();
            scale = Vector3.one;
        }
    }

    [Serializable]
    public class Figure
    {
        public string name;
        public CameraDestination cameraDestination;
        public Cutaway cutaway;
        public List<MonitorCamera> monitorCameras;
        public List<PartObject> partObjects;
        public List<GameObject> callouts;
        public List<GameObject> helpers;
        public List<VirtualButtonFigureData> virtualButtonElement;
        public List<AnimationFigureData> animationFigureDatas;
        public bool isSequenceAnimation;
        public bool isOverridePartObject;
        public bool isOverrideCamera;
        public MaintenanceElementType cameraOverrideMaintenance;
        public MaintenanceElementType partObjectOverrideMaintenance;
        public TroubleshootNodeType cameraOverrideTroubleshoot;
        public TroubleshootNodeType partObjectOverrideTroubleshoot;

        public Figure()
        {
            name = "new figure";
            cameraDestination = new CameraDestination();
            cutaway = new Cutaway();
            monitorCameras = new List<MonitorCamera>();
            partObjects = new List<PartObject>();
            callouts = new List<GameObject>();
            helpers = new List<GameObject>();
            virtualButtonElement = new List<VirtualButtonFigureData>();
            animationFigureDatas = new List<AnimationFigureData>();
            isSequenceAnimation = false;
            isOverridePartObject = true;
            isOverrideCamera = true;
            cameraOverrideMaintenance = new MaintenanceElementType();
            partObjectOverrideMaintenance = new MaintenanceElementType();
            cameraOverrideTroubleshoot = new TroubleshootNodeType();
            partObjectOverrideTroubleshoot = new TroubleshootNodeType();
        }
    }

    [Serializable]
    public class PdfData
    {
        [NonSerialized] public PDFAsset pdfAsset;
        public int index;
        public int page;

        public PdfData()
        {
            index = 0;
            page = 0;
        }
    }

    [Serializable]
    public class TroubleshootData
    {
        public TroubleshootGraph troubleshootGraph;
        public float sizeMultiplierX;
        public float sizeMultiplierY;
        public bool isSimplifiedMode;
        public GraphStartPosition startPositionGraph;
        public string CompleteNameMateri;

        public TroubleshootData()
        {
            sizeMultiplierY = 1;
            sizeMultiplierX = 1;
            isSimplifiedMode = true;
        }
    }

    [Serializable]
    public class Materi
    {
        public string name;
        public MateriElementType elementType;

        public string description;
        public bool hideDescriptionPanel;
        public bool showDescriptionPopup;
        public bool disableBackground;
        public List<Figure> figures;
        public List<Schematic> schematics;
        public List<PdfData> pdfs;
        public TroubleshootData troubleshootData;
        public MaintenanceData maintenanceData;

        public Materi(string name, MateriElementType elementType) : this()
        {
            this.name = name;
            this.elementType = elementType;
        }

        public Materi()
        {
            name = "new materi";
            elementType = MateriElementType.Materi;
            description = "";
            hideDescriptionPanel = false;
            showDescriptionPopup = false;
            disableBackground = false;
            schematics = new List<Schematic>();
            pdfs = new List<PdfData>();
            pdfs.Add(new PdfData());
            figures = new List<Figure>();
            figures.Add(new Figure());
            maintenanceData = new MaintenanceData();
            troubleshootData = new TroubleshootData();
            if (maintenanceData.treeElements.Count == 0)
            {
                var root = new MaintenanceTreeElement("Root", -1, 0);
                maintenanceData.treeElements.Add(root);
            }
        }
    }


    [Serializable]
    public class AnimationFigureData
    {
        public GameObjectType gameObjectInteraction;
        public ScriptedAnimationElementType scriptedAnimationElement;
        public string animationName;
        public AnimatorControllerType animatorController;
        public bool isReverseAnimation;
        public bool isDisabledFinished;
        public bool isAnimatedScript;
        public int sequentialQueue;
        public bool isSequential;
        public bool isInit;
        public bool isVirtualButton;
        public VirtualButtonOnAnimation virtualButton;

        public AnimationFigureData()
        {
            scriptedAnimationElement = new ScriptedAnimationElementType();
            gameObjectInteraction = new GameObjectType();
            animatorController = new AnimatorControllerType();
            virtualButton = new VirtualButtonOnAnimation();
        }
    }

    [Serializable]
    public class VirtualButtonFigureData
    {
        public VirtualButtonElementType virtualButtonElementType;
        public bool isOverrideVirtualButton;
        public List<OverrideVariableVirtualButton> overrideVirtualButton = new List<OverrideVariableVirtualButton>();

        public VirtualButtonFigureData()
        {
            virtualButtonElementType = new VirtualButtonElementType();
            overrideVirtualButton = new List<OverrideVariableVirtualButton>();
        }
    }

    [Serializable]
    public class OverrideVariableVirtualButton
    {
        public OverrideVirtualButton OverrideVirtualButton;
        public float speed;
        public float defaultValue;
    }

    [Serializable]
    public class VirtualButtonOnAnimation
    {
        public VirtualButtonElementType virtualButtonElementType;
        public float startValue;
        public float endValue;
        public float speedVirtualButton;
        public float startValueY;
        public float endValueY;

        public VirtualButtonOnAnimation()
        {
            virtualButtonElementType = new VirtualButtonElementType();
            endValue = 1;
            endValueY = 1;
            speedVirtualButton = 1;
        }
    }

}