using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [Serializable]
    public class CullingTreeElement : TreeElement
    {
        [SerializeField] CullingDataModel culling;

        public CullingDataModel data { get => culling; }

        public CullingTreeElement(string name, int depth, int id) : base(name, depth, id)
        {
            culling = new CullingDataModel();
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(data);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            culling = JsonUtility.FromJson<CullingDataModel>(data) as CullingDataModel;
        }
    }
}
