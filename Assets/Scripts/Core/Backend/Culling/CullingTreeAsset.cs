using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class CullingData
    {
        [SerializeField] public List<CullingTreeElement> treeElements = new List<CullingTreeElement>();
    }

    [CreateAssetMenu(fileName = "New Culling Database", menuName = "Virtual Training/Culling Database")]
    public class CullingTreeAsset : ScriptableObjectBase<CullingData>
    {
        [SerializeField] CullingData culling;

        private void Awake()
        {
            Initialization();
        }

        public override CullingData GetData()
        {
            return culling;
        }

        public override void Initialization()
        {
            if (culling == null)
                culling = new CullingData();

            if (culling.treeElements.Count == 0)
            {
                var root = new CullingTreeElement("Root", -1, 0);
                culling.treeElements.Add(root);
            }
        }

        public override void SetData(CullingData data)
        {
            culling = data;
        }
    }
}
