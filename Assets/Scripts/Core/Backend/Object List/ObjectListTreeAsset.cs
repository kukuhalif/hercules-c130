﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace VirtualTraining.Core
{

    [System.Serializable]
    public class ObjectListData
    {
        [SerializeField] public List<ObjectListTreeElement> treeElements = new List<ObjectListTreeElement>();
    }

    [CreateAssetMenu(fileName = "New Object List Database", menuName = "Virtual Training/Object List Database")]
    public class ObjectListTreeAsset : ScriptableObjectBase<ObjectListData>
    {
        [SerializeField] ObjectListData objectlistData;

        public ObjectListData ObjectListData
        {
            get
            {
                for (int i = 0; i < objectlistData.treeElements.Count; i++)
                {
                    if (objectlistData.treeElements[i].depth == -1)
                    {
                        objectlistData.treeElements[i].name = name;
                        break;
                    }
                }
                return objectlistData;
            }
        }

        void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (objectlistData == null)
                objectlistData = new ObjectListData();

            if (objectlistData.treeElements.Count == 0)
            {
                var root = new ObjectListTreeElement("Root", -1, 0);
                objectlistData.treeElements.Add(root);
            }
        }

        public override ObjectListData GetData()
        {
            return ObjectListData;
        }

        public override void SetData(ObjectListData data)
        {
            objectlistData = data;
        }
    }
}

