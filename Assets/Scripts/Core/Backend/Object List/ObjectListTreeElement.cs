﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    public enum ObjectListAction
    {
        None,
        Solo,
        Highlight,
        Hide,
        Xray
    }

    [Serializable]
    public class ObjectListTreeElement : TreeElement
    {
        [SerializeField] private ObjectListDataModel objectListData;
        [NonSerialized] public ObjectListAction action = ObjectListAction.None;

        public ObjectListDataModel ObjectListData { get => objectListData; }

        public ObjectListTreeElement(string name, int depth, int id) : base(name, depth, id)
        {
            objectListData = new ObjectListDataModel(name);
        }

        public override string GetDuplicateJsonData()
        {
            return JsonUtility.ToJson(objectListData);
        }

        public override void SetDuplicatedJsonData(string data)
        {
            objectListData = JsonUtility.FromJson<ObjectListDataModel>(data) as ObjectListDataModel;
        }
    }
}
