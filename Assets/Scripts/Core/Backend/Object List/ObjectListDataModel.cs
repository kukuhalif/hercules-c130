﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VirtualTraining.Core
{
    [Serializable]
    public class ObjectListDataModel 
    {
        public string name;
        public List<GameObjectType> objects;

        public ObjectListDataModel(string name)
        {
            this.name = name;
            objects = new List<GameObjectType>();
        }
    }
}

