﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public enum GraphStartPosition
    {
        Up,
        Down,
        Left,
        Right,
    }

    [System.Serializable]
    public class ContentTroubleshootNodeGraph
    {
        // Start is called before the first frame update
        //HideInInspector] [SerializeField] public MateriItem content;
        [SerializeField] public TroubleshootDataModel troubleshootdata;
        [SerializeField] public GraphStartPosition startPositionGraph;
    }
}
