﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using System;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class Empty
    {

    }

    [Serializable]
    public class TroubleshootNode : NodeReferenceBase
    {
        [Input] public Empty inputEmpty;
        [Output] public Empty outputEmpty;


        [HideInInspector]public ContentTroubleshootNodeGraph content;

        // Use this for initialization
        protected override void Init()
        {
            base.Init();
        }

        // Return the correct value of an output port when requested
        public override object GetValue(NodePort port)
        {
            return null; // Replace this
        }

    }
}

