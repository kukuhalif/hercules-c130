using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [Serializable]
    public class LightControlType : GameObjectType
    {
        [NonSerialized] Light light;
        public Light lightController
        {
            get
            {
                if (light != null)
                {
                    return light;
                }
                else
                {
                    if (gameObject != null)
                    {
                        light = gameObject.GetComponent<Light>();
                        return light;
                    }
                    else
                        return null;

                }
            }
            set
            {
                if (value == null)
                {
                    light = null;
                    gameObject = null;
                }
                else
                {
                    light = value;
                    gameObject = light.gameObject;
                }
            }
        }
    }
}
