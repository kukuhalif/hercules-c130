﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace VirtualTraining.Core
{
    [Serializable]
    public class TroubleshootDataModel 
    {
        public string title;
        public string text;
        public Figure figure;
        public GameObject prefabCustomButton;
        public bool isPopUpDescription;
        public string description;
        public string popUpDescription;

        public List<Schematic> schematics = new List<Schematic>();
        public GraphStartPosition startPositionGraph;
        public float sizeMultiplierX;
        public float sizeMultiplierY;
        public bool isSimplifiedMode;
        public int indexNode;

        public TroubleshootDataModel()
        {
            title = "New Troubleshoot Node";
            figure = new Figure();
            schematics = new List<Schematic>();
            sizeMultiplierY = 1;
            sizeMultiplierX = 1;
        }
    }
}
