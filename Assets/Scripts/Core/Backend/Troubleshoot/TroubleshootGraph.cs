﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using System;

namespace VirtualTraining.Core
{
    //[Serializable, CreateAssetMenu(fileName = "New Toubleshoot Graph", menuName = "Virtual Training/Troubleshoot Graph")]
    [Serializable]
    public class TroubleshootGraph : NodeGraph
    {
        TroubleshootNode nodeRoot;
        public void GetRoot()
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].GetInputPort("inputEmpty").ConnectionCount == 0)
                {
                    nodeRoot = nodes[i] as TroubleshootNode;
                }
            }
        }
    }
}

