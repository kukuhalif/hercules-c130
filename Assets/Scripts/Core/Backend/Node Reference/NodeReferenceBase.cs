using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using System;


namespace VirtualTraining.Core
{
    public abstract class NodeReferenceBase : Node
    {
        [HideInInspector]
        public int id = 0;
        [HideInInspector]
        public string CompleteName;

        protected override void Init()
        {
            //if (content != null)
            //    name = content.troubleshootdata.title;
            id = GenerateElementIdCallback();
            base.Init();
        }

        private int GenerateElementIdCallback()
        {
            if (id == 0)
            {
                string date = System.DateTime.Now.ToString("MM/dd/yyyy") + System.DateTime.Now.ToString("hh:mm:ss");
                int instanceId = GetInstanceID();
                int random = UnityEngine.Random.Range(-10000, 10000);

                string st = instanceId.ToString() + date + random.ToString();
                id = Animator.StringToHash(st);
            }
            return id;
        }

    }
}
