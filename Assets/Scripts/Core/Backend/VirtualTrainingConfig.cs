﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;
using UnityEngine.Serialization;
using Paroxe.PdfRenderer;

namespace VirtualTraining.Core
{
    [Serializable]
    public class SFXData
    {
        public SFX sfx;
        public AudioClip clip;

        public SFXData(object obj)
        {
            AudioClip clip = obj as AudioClip;
            this.clip = clip;
        }

        public SFXData()
        {

        }
    }

    public enum IconEnum
    {
        // anim
        AnimDelayPlay,
        AnimDisablePlaybar,
        AnimManualPlay,
        AnimPlayLoop,
        AnimPlaySequentially,

        // generic icon
        EditAdd,
        EditClear,
        EditCopy,
        EditCut,
        EditDelete,
        EditDuplicate,
        EditFavourite,
        EditFilter,
        EditInsert,
        EditMultipleSelect,
        EditPaste,
        EditPosition,
        EditRemove,

        // icon Set
        IconSet,

        // list editor
        ListFoldoutClose,
        ListFoldoutOpen,
        ListEditorGrabListElement,

        // part object
        PartObjectBlink,
        PartObjectHide,
        PartObjectHighlight,
        PartObjectIgnoreCutaway,
        PartObjectSelect,
        PartObjectSetLayer,
        PartObjectShow,
        PartObjectSolo,
        PartObjectXRay,

        // path
        PathMateri,

        // text
        TextCapitalize,
        TextSentenceCase,
        TextSmallCase,
    };

    [Serializable]
    public class EditorIcon
    {
        public IconEnum category;
        public Texture2D icon;

        public EditorIcon(IconEnum category, Texture2D icon)
        {
            this.category = category;
            this.icon = icon;
        }

        public EditorIcon()
        {

        }

        public EditorIcon(object obj)
        {
            Texture2D texture2D = obj as Texture2D;
            if (texture2D != null)
            {
                icon = texture2D;
            }
        }
    }

    [Serializable]
    public class SettingData
    {
        public float rotateSpeed;
        public float zoomSpeed;
        public float dragSpeed;
        public float cameraTransitionSpeed;
        public float voiceVolume;
        public float sfxVolume;
        public bool fullscreen;
        public int resolution;
        public bool showEnvirontmentObject;
        public int quality;
        public float vrCanvasDistance;
        public float vrMenuHeight;
    }

    [Serializable]
    public class MinMaxSettingData
    {
        public Vector2 rotateSpeed;
        public Vector2 zoomSpeed;
        public Vector2 dragSpeed;
        public Vector2 moveCameraSpeed;
        public Vector2 bgmVolume;
        public Vector2 sfxVolume;
        public Vector2 vrCanvasDistance;
        public Vector2 vrMenuHeight;

        public MinMaxSettingData()
        {
            rotateSpeed = new Vector2(1, 10);
            zoomSpeed = new Vector2(0.5f, 5f);
            dragSpeed = new Vector2(1, 10);
            moveCameraSpeed = new Vector2(1, 10);
            bgmVolume = new Vector2(0, 1);
            sfxVolume = new Vector2(0, 1);
            vrCanvasDistance = new Vector2(0, 10);
            vrMenuHeight = new Vector2(0, 10);
        }
    }

    [Serializable]
    public class UITheme
    {
        public string name;

        public Color taskbarBgColor;

        public Color buttonBgDefaultColor;
        public Color buttonBgHighlightColor;
        public Color buttonIconDefaultColor;
        public Color buttonIconPressedColor;
        public Color buttonBgDisabledColor;
        public Color buttonIconDisabledColor;

        public Color toggleBgDefaultColor;
        public Color toggleBgSelectedColor;
        public Color toggleBgHighlightedOnColor;
        public Color toggleBgHighlightedOffColor;
        public Color toggleIconDefaultColor;
        public Color toggleIconSelectedColor;

        public Color panelTitleColor;
        public Color panelTitleDraggedColor;
        public Color panelContentColor;
        public Color panelTitleTextColor;
        public Color panelTitleTextDraggedColor;

        public Color backgroundColor;

        public Color genericTextColor;

        public Color sliderHandleDefaultColor;
        public Color sliderHandleHighlightedColor;
        public Color sliderHandlePressedColor;
        public Color sliderHandleSelectedColor;
        public Color sliderHandleDisabledColor;
        public Color sliderFillColor;
        public Color sliderBgColor;

        public Color scrollbarHandleDefaultColor;
        public Color scrollbarHandleHighlightedColor;
        public Color scrollbarHandlePressedColor;
        public Color scrollbarHandleSelectedColor;
        public Color scrollbarHandleDisabledColor;
        public Color scrollbarBgColor;

        public Color inputFieldTextNormalColor;
        public Color inputFieldTextHighlightColor;
        public Color inputFieldTextPressedColor;
        public Color inputFieldTextSelectedColor;
        public Color inputFieldTextDisabledColor;
        public Color inputFieldBgColor;

        public Color dropdownTextNormalColor;
        public Color dropdownTextHighlightColor;
        public Color dropdownTextPressedColor;
        public Color dropdownTextSelectedColor;
        public Color dropdownTextDisabledColor;

        public Color TextLinkNormalColor;
        public Color TextLinkHoverColor;
        public Color TextLinkPressedColor;
        public Color TextLinkAfterColor;
    }

    [Serializable]
    public class UITexture
    {
        public Sprite play;
        public Sprite pause;
        public Sprite minimizePanel;
        public Sprite windowedPanel;
        public Sprite maximizePanel;
        public Sprite closePanel;
        public Sprite dropdownArrow;
        public Sprite checkmark;
    }

    [Serializable]
    public class ProjectDetails
    {
        public string name;
        public string subName;
        public Sprite icon;
        public PDFAsset helpPDF;
        public string about;

        public ProjectDetails()
        {
            name = "";
            subName = "";
            about = "";
        }
    }

    [Serializable]
    public class CursorTexture
    {
        public Texture2D link;
        public Texture2D horizontalResize;
        public Texture2D VerticalResize;
        public Texture2D slashResize;
        public Texture2D backSlashResize;
    }

    [Serializable]
    public class ConfigData
    {
        [SerializeField] public List<Material> xRayMaterials;
        [SerializeField] public Color highlightColor;
        [SerializeField] public Color selectedColor;
        [SerializeField] public Color blinkColor;
        [SerializeField] public Color fresnelColor;
        [SerializeField] public bool isVRMode;
        [SerializeField] public List<string> PC_Scenes;
        [SerializeField] public List<string> VR_Scenes;
        [SerializeField] public string quizScene;
        [SerializeField] public List<SFXData> sfxs;
        [SerializeField] public SettingData defaultSetting;
        [SerializeField] public MinMaxSettingData minMaxSetting;
        [SerializeField] public List<EditorIcon> editorIcons;
        [SerializeField] public List<UITheme> uiThemes;
        [NonSerialized] public int selectedTheme;
        [SerializeField] public CursorTexture cursorTexture;
        [SerializeField] public UITexture uiTexture;
        [SerializeField] public float cameraArriveDifference;
        [SerializeField] public float maxRaycastDistance;
        [SerializeField] public float resetObjectPositionDuration;
        [SerializeField] public List<VideoClip> appsLoadingVideos;
        [SerializeField] public List<VideoClip> quizLoadingVideos;

#if UNITY_EDITOR
        [SerializeField] public List<SceneAsset> PC_SceneAssets;
        [SerializeField] public List<SceneAsset> VR_SceneAssets;
        [SerializeField] public SceneAsset quizSceneAsset;
#endif

        public ConfigData()
        {
            highlightColor = Color.green;
            selectedColor = Color.yellow;
            fresnelColor = Color.red;
            PC_Scenes = new List<string>();
            VR_Scenes = new List<string>();
            sfxs = new List<SFXData>();
            defaultSetting = new SettingData();
            minMaxSetting = new MinMaxSettingData();
            editorIcons = new List<EditorIcon>();
            uiThemes = new List<UITheme>();
            cursorTexture = new CursorTexture();
            uiTexture = new UITexture();
            appsLoadingVideos = new List<VideoClip>();
            quizLoadingVideos = new List<VideoClip>();
            cameraArriveDifference = 0.001f;
            maxRaycastDistance = 1000f;
            resetObjectPositionDuration = 1f;

#if UNITY_EDITOR
            PC_SceneAssets = new List<SceneAsset>();
            VR_SceneAssets = new List<SceneAsset>();
#endif
        }
    }

    [CreateAssetMenu(fileName = "Config", menuName = "Virtual Training/Config File")]
    public class VirtualTrainingConfig : ScriptableObjectBase<ConfigData>
    {
        [FormerlySerializedAs("_configData")]
        [SerializeField] ConfigData configData;

        private void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (configData == null)
                configData = new ConfigData();
        }

        public override ConfigData GetData()
        {
            return configData;
        }

        public override void SetData(ConfigData data)
        {
            configData = data;
        }
    }
}