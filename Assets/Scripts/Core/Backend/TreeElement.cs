using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public abstract class TreeElement
    {
        [FormerlySerializedAs("_isFolder")]
        public bool isFolder;
        [FormerlySerializedAs("_id")]
        public int id;
        [FormerlySerializedAs("_name")]
        public string name;
        [FormerlySerializedAs("_depth")]
        public int depth;

        [NonSerialized] public TreeElement parent;
        [NonSerialized] public List<TreeElement> children;

        public string CompleteName
        {
            get
            {
                if (parent == null)
                    return "";

                List<string> names = new List<string>();

                TreeElement currentParent = this;

                while (currentParent.parent != null)
                {
                    names.Add(currentParent.name);
                    currentParent = currentParent.parent;
                }

                string completeName = "";

                for (int i = names.Count - 1; i >= 0; i--)
                {
                    if (i > 0)
                        completeName += names[i] + "/";
                    else
                        completeName += names[i];
                }

                return completeName;
            }
        }

        public bool hasChildren
        {
            get { return children != null && children.Count > 0; }
        }

        public TreeElement()
        {
        }

        public TreeElement(string name, int depth, int id)
        {
            this.name = name;
            this.id = id;
            this.depth = depth;
        }

        public abstract string GetDuplicateJsonData();

        public abstract void SetDuplicatedJsonData(string data);
    }

}


