﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [System.Serializable]
    public class VirtualButtonData
    {
        [SerializeField] public List<VirtualButtonTreeElement> treeElements = new List<VirtualButtonTreeElement>();
    }

    [CreateAssetMenu(fileName = "New Virtual Button Database", menuName = "Virtual Training/Virtual Button Database")]
    public class VirtualButtonTreeAsset : ScriptableObjectBase<VirtualButtonData>
    {
        [SerializeField] VirtualButtonData _virtualbuttonData;
        [SerializeField] VirtualButtonData virtualbuttonData;

        public VirtualButtonData VirtualButtonData
        {
            get
            {
                for (int i = 0; i < _virtualbuttonData.treeElements.Count; i++)
                {
                    if (_virtualbuttonData.treeElements[i].depth == -1)
                    {
                        _virtualbuttonData.treeElements[i].name = name;
                        break;
                    }
                }
                return _virtualbuttonData;
            }
        }
        void Awake()
        {
            Initialization();
        }

        public override void Initialization()
        {
            if (_virtualbuttonData == null)
                _virtualbuttonData = new VirtualButtonData();

            if (_virtualbuttonData.treeElements.Count == 0)
            {
                var root = new VirtualButtonTreeElement("Root", -1, 0);
                _virtualbuttonData.treeElements.Add(root);
            }
        }

        public override VirtualButtonData GetData()
        {
            return VirtualButtonData;
        }

        public override void SetData(VirtualButtonData data)
        {
            _virtualbuttonData = data;
        }
    }


}
