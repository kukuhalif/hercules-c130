﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    public enum VirtualButtonType { Click, Drag, Press };
    public enum VirtualbuttonModifierType { SetActiveOrNonActive, Light, Skybox };
    public enum SetActiveInteraction { Click, ByValue };
    public enum DragDirection { Horizontal, Vertical, Both };
    public enum EffectType { ParameterLerp };
    public enum TransitionMethod
    {
        Direct,
        Followthrough,
        AfterAnimation,
    };
    public enum SequencePlayMethod
    {
        TimeStampState,
        TimeStampParameter,
        TimeStampScriptedAnimation,
        PlayState,
        PlayParameter,
        PlayScriptedAnimation,
    }
    public enum ParameterType { X, Y };
    public enum FeedbackManualType { State, Parameter };
    public enum ElasticType { Both, Interaction, Feedback };
    public enum DefaultCondition { Off, On };
    public enum VBSKyboxType { Click, Value };
    public enum VBPlayStampValueType { SameValue, DifferenceValue };
    [Serializable]
    public class VirtualButtonDataModel
    {
        [SerializeField] public VirtualButtonType vbType;

        [FormerlySerializedAs("_name")]
        public string name;

        public float defaultValue; //for all
        public VBPlayStampValueType vbPlayStampValueType;

        //OnStartAnimationData
        public List<VBStartAnimationData> vbStartAnimations = new List<VBStartAnimationData>();
        public List<ClickTargetData> clickTargetDatas = new List<ClickTargetData>();

        //VB Click
        public bool isBack;
        public bool isSequentialClick;
        public List<FloatListData> sequentialClickDatas = new List<FloatListData>();

        //VB Press
        public bool isElastic; //for drag too
        public float elasticSpeed; //for drag too
        public ElasticType elasticType;
        public bool isReturnToStart;

        //VB Drag
        public DragDirection direction;
        public float deltaPositionX;
        public float deltaPositionY;
        public bool isSequentialDrag;
        public List<FloatListData> sequentialDragDatas = new List<FloatListData>();

        //Transition
        public int indexTransition;
        public TransitionMethod transitionMethod;
        public float followThroughDelay;

        //Output
        public SequencePlayMethod sequencePlayMethod;
        public List<VirtualButtonOutputData> vbOutputInteraction = new List<VirtualButtonOutputData>();
        public List<VirtualButtonOutputData> vbOutputFeedback = new List<VirtualButtonOutputData>();

        //effect
        public List<VirtualButtonEffectData> vbEffectDatas = new List<VirtualButtonEffectData>();
        public List<VirtualbuttonModifierData> vbModifierData;


        public VirtualButtonDataModel()
        {
            elasticSpeed = 1;
            vbModifierData = new List<VirtualbuttonModifierData>();
            name = "new virtual button";
            sequentialClickDatas = new List<FloatListData>();
            vbEffectDatas = new List<VirtualButtonEffectData>();
            defaultValue = 0;

            vbStartAnimations = new List<VBStartAnimationData>();
            clickTargetDatas = new List<ClickTargetData>();
            isBack = true;
            sequentialDragDatas = new List<FloatListData>();
            vbOutputInteraction = new List<VirtualButtonOutputData>();
            vbOutputFeedback = new List<VirtualButtonOutputData>();
            followThroughDelay = 0.1f;
            deltaPositionX = 0;
            deltaPositionY = 0;
        }

        public VirtualButtonDataModel(VirtualButtonDataModel newData)
        {
            this.vbType = newData.vbType;
            this.vbModifierData = newData.vbModifierData;
            this.defaultValue = newData.defaultValue;
            this.vbPlayStampValueType = newData.vbPlayStampValueType;

            //VIRTUAL BUTTON DRAG VARIABLE
            this.direction = newData.direction;
            this.deltaPositionX = newData.deltaPositionX;
            this.deltaPositionY = newData.deltaPositionY;
            this.sequentialDragDatas = newData.sequentialDragDatas;
            this.isSequentialDrag = newData.isSequentialDrag;

            //Virtual Button Press
            this.isElastic = newData.isElastic; //for drag too
            this.elasticSpeed = newData.elasticSpeed; //for drag too
            this.elasticType = newData.elasticType;
            this.isReturnToStart = newData.isReturnToStart;

            //VIRTUAL BUTTON Click VARIABLE
            this.isBack = newData.isBack;
            this.isSequentialClick = newData.isSequentialClick;
            this.sequentialClickDatas = newData.sequentialClickDatas;

            //On Start
            this.vbStartAnimations = newData.vbStartAnimations;
            this.clickTargetDatas = newData.clickTargetDatas;

            //Transition
            this.transitionMethod = newData.transitionMethod;
            this.indexTransition = newData.indexTransition;
            this.followThroughDelay = newData.followThroughDelay;

            //output
            this.sequencePlayMethod = newData.sequencePlayMethod;
            this.vbOutputInteraction = newData.vbOutputInteraction;
            this.vbOutputFeedback = newData.vbOutputFeedback;

            //effect
            this.vbEffectDatas = newData.vbEffectDatas;
        }
    }

    [Serializable]
    public class VBStartAnimationData
    {
        //manual animation
        public bool isAnimationScript;
        public string animationState;
        public AnimatorControllerType animatorController;
        //scripted animation
        public ScriptedAnimationElementType scriptedAnimationElement;
        public GameObjectType ObjectAnimation;
        public bool isReverseAnimation;
        public bool isDisabledFinished;

        public VBStartAnimationData()
        {
            animatorController = new AnimatorControllerType();
            scriptedAnimationElement = new ScriptedAnimationElementType();
            ObjectAnimation = new GameObjectType();
        }
    }

    [Serializable]
    public class ClickTargetData
    {
        public GameObjectType gameObject;
        public bool isInvert;
        public ClickTargetData()
        {
            gameObject = new GameObjectType();
        }
    }

    [Serializable]
    public class VirtualButtonOutputData
    {
        public bool isAnimationScript;
        public float speed;
        //manual animation
        public FeedbackManualType feedBackManualType;
        public AnimatorControllerType animatorController;
        public string animationState;
        public string animationParameter;
        public string animationParameterY;
        public float maximalValue;
        public float minimalValue;
        public List<ParameterData> parameterDatas = new List<ParameterData>();
        public List<AnimationStateData> animationDatas = new List<AnimationStateData>();
        //scripted animation
        public ScriptedAnimationElementType scriptedAnimationElement;
        public GameObjectType ObjectAnimation;
        public bool isReverseAnimation;
        public bool isDisabledFinished;
        public VirtualButtonOutputData()
        {
            speed = 1;
            animatorController = new AnimatorControllerType();
            scriptedAnimationElement = new ScriptedAnimationElementType();
            parameterDatas = new List<ParameterData>();
            animationDatas = new List<AnimationStateData>();
            ObjectAnimation = new GameObjectType();
            minimalValue = 0;
            maximalValue = 1;
            feedBackManualType = FeedbackManualType.State;
        }
    }

    [Serializable]
    public class VirtualbuttonModifierData
    {
        [SerializeField] public VirtualbuttonModifierType vbModifierType;
        public SetActiveInteraction setActiveInteraction;

        public List<InteractionObjectData> interactionObjectDatas = new List<InteractionObjectData>();
        public DefaultCondition defaultCondition;

        //Blink Variable
        [SerializeField] public float minIntensity;
        [SerializeField] public float maxIntensity;
        [SerializeField] public float delayTimeBlink;
        [SerializeField] public float delayTimeColor;
        [SerializeField] public Color color1;
        [SerializeField] public Color color2;
        [SerializeField] public bool isBlink;
        [SerializeField] public bool isChangeColor;

        //Skybox
        public VBSKyboxType skyBoxType;
        [SerializeField] public float dayExposure;
        [SerializeField] public float dayIntensityMultiplier;
        [SerializeField] public float speed;
        [SerializeField] public LightControlType light;
        [SerializeField] public float lightTransformX;
        [SerializeField] public float lightTransformY;
        [SerializeField] public float lightTransformZ;

        //SetActive
        public List<ModifierConditionValue> modifierConditionValues = new List<ModifierConditionValue>();

        public VirtualbuttonModifierData()
        {
            interactionObjectDatas = new List<InteractionObjectData>();
            modifierConditionValues = new List<ModifierConditionValue>();
            delayTimeBlink = 0.2f;
            delayTimeColor = 0.2f;
            maxIntensity = 1;
            skyBoxType = VBSKyboxType.Value;
            dayExposure = 1;
            dayIntensityMultiplier = 1;
            color1 = new Color(1, 1, 1, 1);
            color2 = new Color(0, 0, 0, 1);
            light = new LightControlType();
        }
    }

    [Serializable]
    public class VirtualButtonEffectData
    {
        public EffectType effectType;

        //AnimatorParameterLerp Variable
        public AnimatorControllerType animator;
        public string parameter;
        public float minValue;
        public float maxValue;
        public float speedAcc;

        public VirtualButtonEffectData()
        {
            animator = new AnimatorControllerType();
            minValue = 0;
            maxValue = 1;
            speedAcc = 0.2f;
        }
    }

    [Serializable]
    public class AnimationScriptDatas
    {
        public GameObjectType objectInteraction;
        public ScriptedAnimationElementType scriptableAnimation;
        public bool isReverseAnimation;

        public AnimationScriptDatas()
        {
            objectInteraction = new GameObjectType();
            scriptableAnimation = new ScriptedAnimationElementType();
        }
    }

    [Serializable]
    public class InteractionObjectData
    {
        public GameObjectType gameObject;
        public string animationStateName;

        public InteractionObjectData()
        {
            gameObject = new GameObjectType();
        }
    }

    [Serializable]
    public class FloatListData
    {
        public float durations;
        public float durationSecond;

        public FloatListData()
        {
            durations = 0;
        }
    }

    [Serializable]
    public class ParameterData
    {
        public string parameter;
        public float maximalValue;
        public float minimalValue;
        public ParameterType paramType;
        public ParameterData()
        {
            paramType = new ParameterType();
            minimalValue = 0;
            maximalValue = 1;
        }
    }

    [Serializable]
    public class AnimationStateData
    {
        public string animationState;
    }

    [Serializable]
    public class ModifierConditionValue
    {
        public float minValue;
        public float maxValue;
        public float durations;

        public ModifierConditionValue()
        {

        }
    }
}
