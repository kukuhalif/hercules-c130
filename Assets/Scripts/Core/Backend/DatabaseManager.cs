﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;

namespace VirtualTraining.Core
{
    public class CustomTreeElementReference<T>
        where T : TreeElement
    {
        public string completeName;
        public T data;

        public CustomTreeElementReference(string completeName, T data)
        {
            this.completeName = completeName;
            this.data = data;
        }
    }

    public static class DatabaseManager
    {
        static VirtualTrainingMateriConfig MATERI_CONFIG;
        static VirtualTrainingConfig CONFIG;

        static DatabaseManager()
        {
            MATERI_CONFIG = Resources.Load<VirtualTrainingMateriConfig>("materi config");
            CONFIG = Resources.Load<VirtualTrainingConfig>("config");
        }

        public static List<MateriContainer> GetMateriConfigDatas()
        {
            return MATERI_CONFIG.GetData().materiContainers;
        }

        public static bool IsVRMode()
        {
            return CONFIG.GetData().isVRMode;
        }

        public static void SwitchToVRMode()
        {
            CONFIG.GetData().isVRMode = true;
        }

        public static void SwitchToPCMode()
        {
            CONFIG.GetData().isVRMode = false;
        }

        public static List<string> GetPCSceneNames()
        {
            return CONFIG.GetData().PC_Scenes;
        }

        public static string GetPcUiSceneName()
        {
            var scenes = CONFIG.GetData().PC_Scenes;

            if (scenes.Count < 3)
                return "";

            return scenes[2];
        }

        public static string GetModelSceneName()
        {
            var scenes = CONFIG.GetData().isVRMode ? CONFIG.GetData().VR_Scenes : CONFIG.GetData().PC_Scenes;

            if (scenes.Count < 2)
                return "";

            return scenes[1];
        }

        public static List<string> GetVRSceneNames()
        {
            return CONFIG.GetData().VR_Scenes;
        }

        public static List<Material> GetXrayMaterials()
        {
            return CONFIG.GetData().xRayMaterials;
        }

        public static Color GetDefaultHighlightColor()
        {
            return CONFIG.GetData().highlightColor;
        }

        public static Color GetDefaultSelectedColor()
        {
            return CONFIG.GetData().selectedColor;
        }

        public static Color GetDefaultBlinkColor()
        {
            return CONFIG.GetData().blinkColor;
        }

        public static Color GetDefaultFresnelColor()
        {
            return CONFIG.GetData().fresnelColor;
        }

        public static VideoClip GetIntroVideo()
        {
            return MATERI_CONFIG.GetData().videoClip;
        }

        public static List<SFXData> GetSFXDatas()
        {
            return CONFIG.GetData().sfxs;
        }

        public static string GetQuizSceneName()
        {
            return CONFIG.GetData().quizScene;
        }

        public static float GetCameraArriveDifference()
        {
            return CONFIG.GetData().cameraArriveDifference;
        }

        public static float GetMaxRaycastDistance()
        {
            return CONFIG.GetData().maxRaycastDistance;
        }

        public static float GetResetObjectPositionDuration()
        {
            return CONFIG.GetData().resetObjectPositionDuration;
        }

        public static ObjectListTreeElement GetObjectListTree()
        {
            var elements = MATERI_CONFIG.GetData().objectListDatabase.ObjectListData.treeElements;
            var tree = TreeElementUtility.ListToTree(elements);
            return tree;
        }

        public static List<CullingTreeElement> GetCullingDatas()
        {
            return MATERI_CONFIG.GetData().cullingDatabase.GetData().treeElements;
        }

        public static List<MateriTreeAsset> GetMateriAssets()
        {
            List<MateriTreeAsset> materiTreeAssets = new List<MateriTreeAsset>();
            var mats = MATERI_CONFIG.GetData().materiContainers;
            foreach (var mat in mats)
            {
                materiTreeAssets.Add(mat.materi);
            }
            return materiTreeAssets;
        }

        private static T GetTree<T>(T root, List<List<T>> sources, bool firstChildIsFolder) where T : TreeElement
        {
            List<T> childs = new List<T>();
            for (int i = 0; i < sources.Count; i++)
            {
                var tree = TreeElementUtility.ListToTree(sources[i]);

                tree.parent = root;
                childs.Add(tree);
            }

            root.children = new List<TreeElement>();
            foreach (var child in childs)
            {
                child.parent = root;

                if (firstChildIsFolder)
                    child.isFolder = true;

                root.children.Add(child);
            }

            return root;
        }

        public static MateriTreeElement GetMateriTree()
        {
            var sources = MATERI_CONFIG.GetData().materiContainers;

            List<List<MateriTreeElement>> materies = new List<List<MateriTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<MateriTreeElement> elements = new List<MateriTreeElement>(sources[i].materi.MateriData.treeElements);

                for (int p = 0; p < elements.Count; p++)
                {
                    for (int d = 0; d < elements[p].MateriData.pdfs.Count; d++)
                    {
                        if (sources[i].pdfs.Count > elements[p].MateriData.pdfs[d].index)
                            elements[p].MateriData.pdfs[d].pdfAsset = sources[i].pdfs[elements[p].MateriData.pdfs[d].index];
                    }
                }

                materies.Add(elements);
            }

            MateriTreeElement root = new MateriTreeElement("root", -1, 0, MateriElementType.Materi);

            return GetTree(root, materies, true);
        }

        public static MaintenanceTreeElement GetMaintenanceTree()
        {
            List<List<MaintenanceTreeElement>> maintenances = new List<List<MaintenanceTreeElement>>();
            var mats = GetMateriTree();

            GetMaintenance(mats, maintenances);

            MaintenanceTreeElement root = new MaintenanceTreeElement("", -1, 0);
            List<MaintenanceTreeElement> materies = new List<MaintenanceTreeElement>();

            for (int i = 0; i < maintenances.Count; i++)
            {
                var materiAsset = GetMaintenanceTree(maintenances[i]);
                materies.Add((MaintenanceTreeElement)materiAsset);
            }

            root.children = new List<TreeElement>();

            for (int i = 0; i < materies.Count; i++)
            {
                root.children.Add(materies[i]);
                materies[i].parent = root;
            }

            return root;
        }

        public static List<CustomTreeElementReference<MaintenanceTreeElement>> GetEditorMaintenanceList()
        {
            List<CustomTreeElementReference<MaintenanceTreeElement>> maintenances = new List<CustomTreeElementReference<MaintenanceTreeElement>>();
            var mats = GetMateriTree();

            GetEditorMaintenance(mats, maintenances);

            return maintenances;
        }

        public static List<TroubleshootNode> GetTroubleshootNodes()
        {
            List<string> troubleshootNames = new List<string>();
            List<TroubleshootNode> troubleshoots = new List<TroubleshootNode>();
            var mats = GetMateriTree();

            GetTroubleshoot(mats, troubleshoots, troubleshootNames);

            return troubleshoots;
        }

        private static void GetEditorMaintenance(MateriTreeElement materiTreeElement, List<CustomTreeElementReference<MaintenanceTreeElement>> maintenances)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Maintenance &&
                materiTreeElement.MateriData.maintenanceData.treeElements.Count > 0)
            {
                // to calculate maintenance tree complete name
                TreeElementUtility.ListToTree(materiTreeElement.MateriData.maintenanceData.treeElements);

                for (int i = 0; i < materiTreeElement.MateriData.maintenanceData.treeElements.Count; i++)
                {
                    MaintenanceTreeElement maintenanceTreeElement = materiTreeElement.MateriData.maintenanceData.treeElements[i];
                    if (maintenanceTreeElement.depth > -1)
                    {
                        string completeName = "(materi) -> " + materiTreeElement.CompleteName + "/(maintenance) -> " + maintenanceTreeElement.CompleteName;
                        CustomTreeElementReference<MaintenanceTreeElement> newData = new CustomTreeElementReference<MaintenanceTreeElement>(completeName, maintenanceTreeElement);
                        maintenances.Add(newData);
                    }
                }
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetEditorMaintenance((MateriTreeElement)materiTreeElement.children[i], maintenances);
                }
            }
        }

        private static void GetMaintenance(MateriTreeElement materiTreeElement, List<List<MaintenanceTreeElement>> maintenances)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Maintenance &&
                materiTreeElement.MateriData.maintenanceData.treeElements.Count > 0)
            {
                maintenances.Add(materiTreeElement.MateriData.maintenanceData.treeElements);
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetMaintenance((MateriTreeElement)materiTreeElement.children[i], maintenances);
                }
            }
        }

        private static void GetTreeElementListFromTreeAndCompleteNames<T>(T treeElement, List<T> results, List<string> completeNameResults)
            where T : TreeElement
        {
            string completeName = treeElement.CompleteName;
            completeNameResults.Add(completeName);
            results.Add(treeElement);

            if (treeElement.hasChildren)
            {
                for (int i = 0; i < treeElement.children.Count; i++)
                {
                    GetTreeElementListFromTreeAndCompleteNames<T>((T)treeElement.children[i], results, completeNameResults);
                }
            }
        }

        private static void GetTroubleshoot(MateriTreeElement materiTreeElement, List<TroubleshootNode> troubleshootNodes, List<string> troubleshootNames)
        {
            if (materiTreeElement.MateriData != null &&
                materiTreeElement.MateriData.elementType == MateriElementType.Troubleshoot &&
                materiTreeElement.MateriData.troubleshootData != null &&
                materiTreeElement.MateriData.troubleshootData.troubleshootGraph != null &&
                materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes.Count > 0)
            {
                for (int i = 0; i < materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes.Count; i++)
                {
                    if (materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes[i] != null)
                    {
                        TroubleshootNode tn = materiTreeElement.MateriData.troubleshootData.troubleshootGraph.nodes[i] as TroubleshootNode;
                        string completeName = "(materi) -> " + materiTreeElement.CompleteName + "/(troubleshoot) -> " + tn.name;
                        tn.CompleteName = completeName;
                        troubleshootNodes.Add(tn);
                        troubleshootNames.Add(materiTreeElement.MateriData.name);
                    }
                }
            }

            if (materiTreeElement.hasChildren)
            {
                for (int i = 0; i < materiTreeElement.children.Count; i++)
                {
                    GetTroubleshoot((MateriTreeElement)materiTreeElement.children[i], troubleshootNodes, troubleshootNames);
                }
            }
        }

        public static MaintenanceTreeElement GetMaintenanceTree(List<MaintenanceTreeElement> elements, string rootName = "root")
        {
            List<List<MaintenanceTreeElement>> materies = new List<List<MaintenanceTreeElement>>();
            materies.Add(elements);

            MaintenanceTreeElement root = new MaintenanceTreeElement(rootName, -1, 0);

            return GetTree(root, materies, false);
        }

        public static ScriptedAnimationTreeAsset GetScriptedAnimationDatabase(int elementId)
        {
            var databases = MATERI_CONFIG.GetData().scriptedAnimationDatabases;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].ScriptedAnimationData.treeElements.Count; j++)
                {
                    if (databases[i].ScriptedAnimationData.treeElements[j].id == elementId)
                        return databases[i];
                }
            }

            return null;
        }

        public static ScriptedAnimationTreeElement GetScriptedAnimationTree()
        {
            var sources = MATERI_CONFIG.GetData().scriptedAnimationDatabases;

            List<List<ScriptedAnimationTreeElement>> materies = new List<List<ScriptedAnimationTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<ScriptedAnimationTreeElement> elements = new List<ScriptedAnimationTreeElement>(sources[i].ScriptedAnimationData.treeElements);
                materies.Add(elements);
            }

            ScriptedAnimationTreeElement root = new ScriptedAnimationTreeElement("root", -1, 0);

            return GetTree(root, materies, true);
        }

        public static VirtualButtonTreeAsset GetVirtualButtonDatabase(int elementId)
        {
            var databases = MATERI_CONFIG.GetData().virtualButtonDatabases;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].VirtualButtonData.treeElements.Count; j++)
                {
                    if (databases[i].VirtualButtonData.treeElements[j].id == elementId)
                        return databases[i];
                }
            }

            return null;
        }

        public static MateriTreeElement GetMateriElement(int elementId)
        {
            var databases = MATERI_CONFIG.GetData().materiContainers;
            for (int i = 0; i < databases.Count; i++)
            {
                for (int j = 0; j < databases[i].materi.MateriData.treeElements.Count; j++)
                {
                    if (databases[i].materi.MateriData.treeElements[j].id == elementId)
                        return databases[i].materi.MateriData.treeElements[j];
                }
            }

            return null;
        }

        public static VirtualButtonTreeElement GetVirtualButtonTree()
        {
            var sources = MATERI_CONFIG.GetData().virtualButtonDatabases;

            List<List<VirtualButtonTreeElement>> materies = new List<List<VirtualButtonTreeElement>>();
            for (int i = 0; i < sources.Count; i++)
            {
                List<VirtualButtonTreeElement> elements = new List<VirtualButtonTreeElement>(sources[i].VirtualButtonData.treeElements);
                materies.Add(elements);
            }

            VirtualButtonTreeElement root = new VirtualButtonTreeElement("root", -1, 0);

            return GetTree(root, materies, true);
        }

        public static SettingData GetDefaultSetting()
        {
            return CONFIG.GetData().defaultSetting;
        }

        public static MinMaxSettingData GetMinMaxSetting()
        {
            return CONFIG.GetData().minMaxSetting;
        }

        public static List<EditorIcon> GetEditorIcons()
        {
            return CONFIG.GetData().editorIcons;
        }

        public static List<Paroxe.PdfRenderer.PDFAsset> GetPdfUi()
        {
            return MATERI_CONFIG.GetData().pdfUis;
        }

        public static List<Paroxe.PdfRenderer.PDFAsset> GetPdfLink()
        {
            return MATERI_CONFIG.GetData().pdfLinks;
        }

        public static void SetUiTheme(int index)
        {
            CONFIG.GetData().selectedTheme = index;
        }

        public static int GetUiThemeIndex()
        {
            return CONFIG.GetData().selectedTheme;
        }

        public static UITheme GetUiTheme()
        {
            return CONFIG.GetData().uiThemes[CONFIG.GetData().selectedTheme];
        }

        public static ProjectDetails GetProjectDetails()
        {
            return MATERI_CONFIG.GetData().projectDetails;
        }

        public static CursorTexture GetCursorTexture()
        {
            return CONFIG.GetData().cursorTexture;
        }

        public static UITexture GetUITexture()
        {
            return CONFIG.GetData().uiTexture;
        }

        public static List<VideoClip> GetAppsLoadingVideos()
        {
            return CONFIG.GetData().appsLoadingVideos;
        }

        public static List<VideoClip> GetQuizLoadingVideos()
        {
            return CONFIG.GetData().quizLoadingVideos;
        }

        public static QuizData getQuizData()
        {
            return MATERI_CONFIG.GetData().quizData;
        }
    }
}
