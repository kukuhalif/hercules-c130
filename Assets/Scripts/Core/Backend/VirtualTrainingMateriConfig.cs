﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paroxe.PdfRenderer;
using UnityEngine.Video;
using UnityEngine.Serialization;

namespace VirtualTraining.Core
{
    [Serializable]
    public class MateriContainer
    {
        [SerializeField] public MateriTreeAsset materi;
        [SerializeField] public List<PDFAsset> pdfs;

        public MateriContainer(object obj) : this()
        {
            MateriTreeAsset asset = obj as MateriTreeAsset;
            materi = asset;
        }

        public MateriContainer()
        {
            pdfs = new List<PDFAsset>();
        }
    }
    [Serializable]
    public class QuizData
    {
        [SerializeField] public int quizAllAmount;
        [SerializeField] public int quizSectionAmount;

        public QuizData()
        {
            quizAllAmount = 50;
            quizSectionAmount = 10;
        }
    }

    [Serializable]
    public class MateriConfigData
    {
        [SerializeField] public VideoClip videoClip;
        [SerializeField] public List<MateriContainer> materiContainers;
        [SerializeField] public List<PDFAsset> pdfUis;
        [SerializeField] public List<PDFAsset> pdfLinks;
        [SerializeField] public ObjectListTreeAsset objectListDatabase;
        [SerializeField] public CullingTreeAsset cullingDatabase;
        [SerializeField] public List<VirtualButtonTreeAsset> virtualButtonDatabases;
        [SerializeField] public List<ScriptedAnimationTreeAsset> scriptedAnimationDatabases;
        [SerializeField] public ProjectDetails projectDetails;
        [SerializeField] public QuizData quizData;

        public MateriConfigData()
        {
            materiContainers = new List<MateriContainer>();
            pdfUis = new List<PDFAsset>();
            pdfLinks = new List<PDFAsset>();
            virtualButtonDatabases = new List<VirtualButtonTreeAsset>();
            scriptedAnimationDatabases = new List<ScriptedAnimationTreeAsset>();
            projectDetails = new ProjectDetails();
            quizData = new QuizData();
        }
    }

    [CreateAssetMenu(fileName = "Materi Config", menuName = "Virtual Training/Materi Config File")]
    public class VirtualTrainingMateriConfig : ScriptableObjectBase<MateriConfigData>
    {
        [FormerlySerializedAs("_materiConfigData")]
        [SerializeField] MateriConfigData materiConfigData;

        private void Awake()
        {
            Initialization();
        }

        public override MateriConfigData GetData()
        {
            return materiConfigData;
        }

        public override void Initialization()
        {
            if (materiConfigData == null)
                materiConfigData = new MateriConfigData();
        }

        public override void SetData(MateriConfigData data)
        {
            materiConfigData = data;
        }
    }
}
