﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethod
{
    public static void Swap<T>(this List<T> datas, int firstIndex, int secondIndex)
    {
        T firstItem = JsonUtility.FromJson<T>(JsonUtility.ToJson(datas[firstIndex]));
        T secondItem = JsonUtility.FromJson<T>(JsonUtility.ToJson(datas[secondIndex]));

        datas[firstIndex] = secondItem;
        datas[secondIndex] = firstItem;
    }

    public static void Reset(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public static void ResetScaleAndRotation(this Transform transform)
    {
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public static void ResetForScrollParent(this Transform transform)
    {
        transform.localPosition = new Vector3(0f, transform.localPosition.y, 0f);
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public static T[] GetComponentsInFirstChilds<T>(this Transform transform) where T : Component
    {
        List<T> components = new List<T>();
        for (int i = 0; i < transform.childCount; i++)
        {
            T cmp = transform.GetChild(i).GetComponent<T>();
            if (cmp != null)
                components.Add(cmp);
        }
        return components.ToArray();
    }

    public static T GetComponentInFirstChild<T>(this Transform transform) where T : Component
    {
        if (transform.childCount == 0)
            return null;

        for (int i = 0; i < transform.childCount; i++)
        {
            T comp = transform.GetChild(i).GetComponent<T>();
            if (comp != null)
                return comp;
        }

        return null;
    }

    public static T GetComponentInChildsExcludeThis<T>(this Transform transform) where T : Component
    {
        T[] results = transform.GetAllComponentsInChildsExcludeThis<T>();

        if (results.Length > 0)
            return results[0];

        return null;
    }

    public static T[] GetAllComponentsInChildsExcludeThis<T>(this Transform transform)
    {
        List<T> components = new List<T>();

        for (int i = 0; i < transform.childCount; i++)
        {
            GetAllComponent<T>(components, transform.GetChild(i));
        }

        return components.ToArray();
    }

    public static GameObject[] GetAllChilds(this GameObject obj)
    {
        List<GameObject> childs = new List<GameObject>();

        GetAllChild(obj, childs);

        return childs.ToArray();
    }

    static void GetAllChild(GameObject obj, List<GameObject> childs)
    {
        for (int i = 0; i < obj.transform.childCount; i++)
        {
            childs.Add(obj.transform.GetChild(i).gameObject);
            GetAllChild(obj.transform.GetChild(i).gameObject, childs);
        }
    }

    public static Transform[] GetAllChilds(this Transform transform)
    {
        List<Transform> childs = new List<Transform>();

        GetAllChild(transform, childs);

        return childs.ToArray();
    }

    static void GetAllChild(Transform transform, List<Transform> childs)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            childs.Add(transform.GetChild(i));
            GetAllChild(transform.GetChild(i), childs);
        }
    }

    public static T[] GetAllComponentsInChilds<T>(this Transform transform)
    {
        List<T> components = new List<T>();

        GetAllComponent<T>(components, transform);

        return components.ToArray();
    }

    static void GetAllComponent<T>(List<T> components, Transform obj)
    {
        T cmp = obj.GetComponent<T>();
        if (cmp != null)
        {
            components.Add(cmp);
        }

        for (int i = 0; i < obj.childCount; i++)
        {
            GetAllComponent<T>(components, obj.GetChild(i));
        }
    }

    public static List<T> GetComponentsInAllParent<T>(this Transform transform) where T : Component
    {
        Transform current = transform;
        List<T> results = new List<T>();
        while (current.parent != null)
        {
            T result = current.GetComponent<T>();

            if (result != null)
                results.Add(result);

            current = current.parent;
        }
        return results;
    }

    public static T GetComponentsInParentRecursive<T>(this Transform transform) where T : Component
    {
        Transform current = transform;
        while (current.parent != null)
        {
            current = current.parent;
            T result = current.GetComponent<T>();

            if (result != null)
                return result;
        }
        return null;
    }

    public static bool ContainsParam(this Animator anim, string paramName)
    {
        foreach (AnimatorControllerParameter param in anim.parameters)
        {
            if (param.name == paramName) return true;
        }
        return false;
    }

    public static List<Transform> GetAllParents(this Transform transform)
    {
        Transform current = transform;
        List<Transform> results = new List<Transform>();
        while (current.parent != null)
        {
            current = current.parent;

            if (current != null)
                results.Add(current);
        }
        return results;
    }

    public static List<GameObject> GetAllParents(this GameObject gameobject)
    {
        if (gameobject == null)
            return new List<GameObject>();

        GameObject current = gameobject;
        List<GameObject> results = new List<GameObject>();
        while (current.transform.parent != null)
        {
            current = current.transform.parent.gameObject;

            if (current != null)
                results.Add(current);
        }
        return results;
    }

    public static List<Transform> GetAllParents(this Transform transform, Transform root)
    {
        if (transform == root)
            return new List<Transform>();

        Transform current = transform;
        List<Transform> results = new List<Transform>();
        while (current.transform.parent != null)
        {
            current = current.parent;

            if (current != null)
            {
                if (current != root)
                    results.Add(current);
                else
                {
                    results.Add(root);
                    return results;
                }
            }
        }
        return results;
    }

    private static void AllGameobjectCount(Transform transform, ref int counter)
    {
        counter++;

        for (int i = 0; i < transform.childCount; i++)
        {
            AllGameobjectCount(transform.GetChild(i), ref counter);
        }
    }
}