﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace VirtualTraining.Core
{
    public class AsyncTask
    {
        public Thread Thread { get; protected set; }

        protected readonly object LockObject = new object();
        private readonly Queue<Action> callBackQueue;

        private Queue<Action> CallBackQueue
        {
            get
            {
                lock (LockObject)
                {
                    return callBackQueue;
                }
            }
        }

        protected AsyncTask() { }

        public AsyncTask(Action action)
        {
            Thread thread = new Thread(() =>
            {
                action();
            });
            Thread = thread;
            Thread.Start();
        }

        public AsyncTask(Action action, int runFrequencyMs)
        {
            callBackQueue = new Queue<Action>();
            Thread thread = new Thread(() =>
            {
                while (true)
                {
                    action();
                    CallBackQueue.Enqueue(action);
                    Thread.Sleep(runFrequencyMs);
                }
            });
            Thread = thread;
            Thread.Start();
        }

        public bool IsFinished
        {
            get { return Thread.IsAlive == false; }
        }

        protected virtual bool IsBackgroundTask
        {
            get { return CallBackQueue != null; }
        }

        public void ContinueInMainThread(Action action)
        {
            onTaskFinished = action;
        }

        private Action onTaskFinished;
        public virtual void OnTaskFinished()
        {
            if (onTaskFinished != null)
            {
                if (IsBackgroundTask)
                {
                    while (CallBackQueue.Count > 0)
                    {
                        CallBackQueue.Dequeue();
                        onTaskFinished();
                    }
                }
                else
                {
                    onTaskFinished();
                }
            }
        }
    }

    public class AsyncTask<T> : AsyncTask
    {
        public T Result { get; private set; }

        private readonly Queue<T> backgroundResults;

        private Queue<T> BackgroundResults
        {
            get
            {
                lock (LockObject)
                {
                    return backgroundResults;
                }
            }
        }

        private Action<T> onTaskFinishedResult;

        protected override bool IsBackgroundTask
        {
            get { return BackgroundResults != null; }
        }

        public AsyncTask(Func<T> func)
        {
            Thread thread = new Thread(() =>
            {
                Result = func();
            });
            Thread = thread;
            Thread.Start();
        }

        public AsyncTask(Func<T> func, int runFrequencyMs)
        {
            backgroundResults = new Queue<T>();
            Thread thread = new Thread(() =>
            {
                while (true)
                {
                    BackgroundResults.Enqueue(func());
                    Thread.Sleep(runFrequencyMs);
                }
            });
            Thread = thread;
            Thread.Start();
        }

        public void ContinueInMainThread(Action<T> action)
        {
            onTaskFinishedResult = action;
        }

        public override void OnTaskFinished()
        {
            if (onTaskFinishedResult != null)
            {
                if (IsBackgroundTask)
                {
                    while (BackgroundResults.Count > 0)
                    {
                        T result = BackgroundResults.Dequeue();
                        onTaskFinishedResult(result);
                    }
                }
                else
                {
                    onTaskFinishedResult(Result);
                }
            }
            else base.OnTaskFinished();
        }
    }
}