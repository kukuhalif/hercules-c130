﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VirtualTraining.Core
{
    public static class VirtualTrainingSceneManager
    {
        static GameObject objectRoot;

        public const string OBJECT_ROOT_NAME = "Object Root";

        static VirtualTrainingSceneManager()
        {
            objectRoot = GameObject.Find(OBJECT_ROOT_NAME);
        }

        private static void ResetStateBeforeReloadingScenes()
        {
            GameObjectReference.ClearGameobjectReferenceLookup();
        }

        public static GameObject GameObjectRoot
        {
            get
            {
                if (objectRoot == null)
                    objectRoot = GameObject.Find(OBJECT_ROOT_NAME);
                return objectRoot;
            }
        }

        public static string ProjectName
        {
            get => Application.productName;
        }

        public static void SwitchToPCMode()
        {
            ResetStateBeforeReloadingScenes();
            DatabaseManager.SwitchToPCMode();
            var scenes = DatabaseManager.GetPCSceneNames();
            // load loading screen
            SceneManager.LoadScene(scenes[0]);
        }

        public static void SwitchToVRMode()
        {
            ResetStateBeforeReloadingScenes();
            DatabaseManager.SwitchToVRMode();
            var scenes = DatabaseManager.GetVRSceneNames();
            // load loading screen
            SceneManager.LoadScene(scenes[0]);
        }

        public static void OpenQuiz()
        {
            // unload PC UI scene
            SceneManager.UnloadSceneAsync(DatabaseManager.GetPcUiSceneName(), UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            SceneManager.LoadScene(DatabaseManager.GetQuizSceneName(), LoadSceneMode.Additive);
        }

        public static void CloseQuiz()
        {
            // load PC UI scene
            SceneManager.LoadScene(DatabaseManager.GetPcUiSceneName(), LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync(DatabaseManager.GetQuizSceneName(), UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

            EventManager.TriggerEvent(new FreePlayEvent());

            EventManager.AddListener<InitializationUIDoneEvent>(UIInitDoneListener);
        }

        private static void UIInitDoneListener(InitializationUIDoneEvent e)
        {
            EventManager.TriggerEvent(new ShowStartingUIPanelEvent());
            EventManager.RemoveListener<InitializationUIDoneEvent>(UIInitDoneListener);
        }

        public static void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
