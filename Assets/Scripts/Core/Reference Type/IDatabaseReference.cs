using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public interface IDatabaseReference
    {
        int GetId();
        void SetId(int id);
    }
}
