using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public abstract class NodeReferenceType<T, U> : IDatabaseReference
        where T : NodeReferenceBase
    {
        [SerializeField] private int nodeId = -1;
        [NonSerialized] private T data = null;

        protected NodeReferenceBase nodeReference
        {
            get
            {
                if (data == null)
                {
                    if (nodeId == -1)
                        return null;

                    List<T> elements = GetList();
                    FindElement(elements);
                }
                return data;
            }
        }

        protected abstract List<T> GetList();
        public abstract U GetData();
        public abstract U GetData(NodeReferenceBase nodeReference);

        private void FindElement(List<T> elements)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (nodeId != -1 && nodeId == elements[i].id)
                {
                    data = elements[i];
                    break;
                }
            }
        }

        public int GetId()
        {
            return nodeId;
        }

        public void SetId(int id)
        {
            nodeId = id;
            data = null;
        }
    }
}
