using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    [Serializable]
    public class TroubleshootNodeType : NodeReferenceType<TroubleshootNode, TroubleshootDataModel>
    {
        public override TroubleshootDataModel GetData()
        {
            return GetData(nodeReference);
        }

        public override TroubleshootDataModel GetData(NodeReferenceBase nodeReference)
        {
            TroubleshootNode tn = nodeReference as TroubleshootNode;
            if (tn == null)
                return null;
            return tn.content.troubleshootdata;
        }

        protected override List<TroubleshootNode> GetList()
        {
            return DatabaseManager.GetTroubleshootNodes();
        }
    }
}

