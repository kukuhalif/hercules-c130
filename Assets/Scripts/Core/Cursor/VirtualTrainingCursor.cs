using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public static class VirtualTrainingCursor
    {
        public static void DefaultCursor()
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        public static void ConfineCursor(bool confine)
        {
            Cursor.lockState = confine ? CursorLockMode.Confined : CursorLockMode.None;
        }

        public static void Link()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().link, new Vector2(8, 0), CursorMode.Auto);
        }

        public static void HorizontalResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().horizontalResize, new Vector2(16, 16), CursorMode.Auto);
        }

        public static void VerticalResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().VerticalResize, new Vector2(16, 16), CursorMode.Auto);
        }

        public static void TopLeftResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().backSlashResize, new Vector2(16, 16), CursorMode.Auto);
        }

        public static void TopRightResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().slashResize, new Vector2(16, 16), CursorMode.Auto);
        }

        public static void BottomLeftResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().slashResize, new Vector2(16, 16), CursorMode.Auto);
        }

        public static void BottomRightResize()
        {
            Cursor.SetCursor(DatabaseManager.GetCursorTexture().backSlashResize, new Vector2(16, 16), CursorMode.Auto);
        }
    }
}
