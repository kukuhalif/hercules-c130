﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;

namespace VirtualTraining.Core
{
    public static class VirtualTrainingCamera
    {
        static CameraController pcCameraController;
        static Camera currentCamera;
        static Camera currentMonitorCamera;
        static bool isPulling;
        static bool isMovementEnabled = true;
        static int mainCameraLayer;
        static int monitorCameraLayer;
        static bool isVR;
        static Transform cameraPointer;

        public static Camera CurrentCamera { get => currentCamera; }
        public static Camera MonitorCamera { get => currentMonitorCamera; }
        public static Transform CameraTransform { get => currentCamera.transform; }
        public static Vector3 CameraPosition { get => currentCamera.transform.position; }
        public static Transform MonitorCameraTransform { get => currentMonitorCamera.transform; }
        public static bool IsVR { get => isVR; }
        public static bool IsPulling { set => isPulling = value; }


        static VirtualTrainingCamera()
        {
            mainCameraLayer = LayerMask.NameToLayer("Main Camera");
            monitorCameraLayer = LayerMask.NameToLayer("Monitor Camera");
        }

        public static int MainCameraLayer
        {
            get => mainCameraLayer;
        }

        public static int MonitorCameraLayer
        {
            get => monitorCameraLayer;
        }

        public static bool IsMovementEnabled
        {
            set
            {
                isMovementEnabled = value;
            }
            get
            {
                return isMovementEnabled && !isPulling;
            }
        }

        public static void SetCurrentCameraController(CameraController cameraController, bool isVR)
        {
            VirtualTrainingCamera.isVR = isVR;
            currentCamera = cameraController.GetCamera();
            pcCameraController = cameraController;
        }

        public static void SetCurrentMonitorCamera(Camera camera)
        {
            currentMonitorCamera = camera;
            GameObject cp = new GameObject();
            cp.name = "raycast pointer";
            cameraPointer = cp.transform;
            cameraPointer.SetParent(currentMonitorCamera.transform);
            cameraPointer.localPosition = new Vector3();
        }

        public static bool Raycast(ref RaycastHit hitInfo, LayerMask layer)
        {
            Vector2 pointerPosition = VirtualTrainingInputSystem.PointerPosition;
            Ray ray;

            if (isVR == true)
                ray = new Ray(SteamVR_LaserPointer.hand.transform.position, SteamVR_LaserPointer.hand.transform.forward);
            else
                ray = currentCamera.ScreenPointToRay(pointerPosition);

            if (Physics.Raycast(ray, out hitInfo, DatabaseManager.GetMaxRaycastDistance(), layer))
            {
                return true;
            }

            return false;
        }

        public static bool Raycast(ref RaycastHit hitInfo)
        {
            Vector2 pointerPosition = VirtualTrainingInputSystem.PointerPosition;
            Ray ray;

            if (isVR == true)
                ray = new Ray(SteamVR_LaserPointer.hand.transform.position, SteamVR_LaserPointer.hand.transform.forward);
            else
                ray = currentCamera.ScreenPointToRay(pointerPosition);

            if (Physics.Raycast(ray, out hitInfo, DatabaseManager.GetMaxRaycastDistance()))
            {
                return true;
            }

            return false;
        }

        public static bool RaycastFromMonitorCamera(ref RaycastHit hitInfo, Vector2 cursorPosition, RectTransform panelAreaRect)
        {
            // get pointer position relative to second monitor area ui
            Vector2 result;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelAreaRect, cursorPosition, null, out result);

            // position result x and y from -0.5 to 0.5
            Vector2 normalizedValue = new Vector2(result.x / panelAreaRect.rect.width, result.y / panelAreaRect.rect.height);
            // normalize position value (-1 to 1)
            normalizedValue *= 2f;

            // convert normalized position to Quaternion multiplied by camera field of view
            // invert y as horizontal rotation
            // vertical rotation multiply with half of vertical fov
            // horizontal rotation multiply with half of horizontal fov (50f vertical fov = 79.31688f)
            Vector3 euler = new Vector3(-normalizedValue.y * currentMonitorCamera.fieldOfView * 0.5f, normalizedValue.x * currentMonitorCamera.fieldOfView * 0.7931688f);
            Quaternion rotation = Quaternion.Euler(euler);
            cameraPointer.localRotation = rotation; // set camera pointer transform local rotation which is child of camera transform

            // raycast from camera pointer
            return Physics.Raycast(cameraPointer.position, cameraPointer.TransformDirection(Vector3.forward), out hitInfo);
        }

        public static void MoveCamera(CameraDestination cameraDestination, Action arriveAction)
        {
            if (pcCameraController == null)
                arriveAction.Invoke();
            else
                pcCameraController.MoveCamera(cameraDestination, arriveAction);
        }

        public static void ResetCameraPosition(Action arriveAction)
        {
            if (pcCameraController == null)
                arriveAction.Invoke();
            else
                pcCameraController.ResetCamera(arriveAction);
        }
    }
}