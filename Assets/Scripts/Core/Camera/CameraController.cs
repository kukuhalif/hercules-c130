﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Core
{
    public abstract class CameraController : MonoBehaviour
    {
        public abstract Camera GetCamera();

        public abstract void ResetCamera(Action arriveAction);

        public abstract void MoveCamera(CameraDestination cameraDestination, Action arriveAction);

        public abstract void MoveCamera(Vector3 position, Quaternion rotation, Transform cameraTarget, bool isOrthographic, float fov, float orthographicSize, Action arriveAction);
    }
}
