﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using XNodeEditor;
using VirtualTraining.Core;
using System;
using VirtualTraining.Feature;
using UnityEngine.Video;

namespace VirtualTraining.Tools
{
    //[CustomNodeGraphEditor(typeof(TroubleshootGraph))]
    public class TroubleshootGraphData : PropertyField
    {
        public GraphStartPosition startPosition;
        TroubleshootDataModel playedTroubleshoot;
        PartObjectEditor partObjectEditor;
        public List<string> troubleshootNodes = new List<string>();
        TroubleshootDataModel currentTroubleshootData;

        public TroubleshootGraphData(Action beforeModifiedCallback, string windowId, List<Material> xrayMats) : base(beforeModifiedCallback, windowId)
        {
            partObjectEditor = new PartObjectEditor(beforeModifiedCallback, windowId, xrayMats);

            CreateTab("categories", OnCategoriesTabCallback, "FIGURE", "SCHEMATIC", "ANIMATION", "VIRTUAL BUTTON");
            CreateTab("figure category", OnFigureListTabCallback, "CAMERA", "PART OBJECT", "CALLOUT", "HELPER", "CUTAWAY");
            CreateListView("monitor camera", null, null, OnMonitorCameraElementDrawCallback, null);
            CreateListView("schematic", null, null, OnSchematicElementDrawCallback, null, false, typeof(VideoClip), typeof(Texture2D));
            CreateListView("callout prefab", null, null, OnCalloutElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("helper prefab", null, null, OnHelperElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("virtual button element", FoldoutTextVirtualButtonListCallback, null, null, OnVirtualButtonElementDrawCallback);
            CreateListView("virtual button override", null, null, OnVirtualButtonOverrideDrawCallback, null);
            CreateListView("animation element", null, null, OnAnimationElementDrawCallback, null);
        }

        public void SetDropdownGraphEditor(ref TroubleshootGraph index)
        {
            //ShowSimpleField("Simplified Mode", "Simplified Mode", ref index.isSimplifiedMode);
            //ShowSimpleField("Size X Scale", "Size X Scalee", ref index.sizeMultiplierX);
            //ShowSimpleField("Size Y Scale", "Size Y Scalee", ref index.sizeMultiplierY);
            //ShowSimpleField("start postion", "Graph Start Position", ref index.startPositionGraph);
        }

        private void OnCategoriesTabCallback(int selected, object data, string fieldId)
        {
            TroubleshootDataModel materiData = data as TroubleshootDataModel;
            int idx = 0;

            switch (selected)
            {
                // figure 
                case 0:

                    ShowSimpleField("figure name", "figure name" + fieldId + idx++, ref materiData.figure.name);
                    ShowTab("figure category", materiData.figure, fieldId + idx++);

                    break;

                // schematic
                case 1:

                    ShowListField("schematic", materiData.schematics, fieldId + idx++);

                    break;

                // animation
                case 2:
                    ShowSimpleField("Sequence Animation", fieldId + idx++, ref materiData.figure.isSequenceAnimation);
                    ShowListField("animation element", materiData.figure.animationFigureDatas, fieldId + idx++);
                    for (int i = 0; i < materiData.figure.animationFigureDatas.Count; i++)
                    {
                        if (materiData.figure.animationFigureDatas[i].isInit == false)
                        {
                            materiData.figure.animationFigureDatas[i].sequentialQueue = i;
                            materiData.figure.animationFigureDatas[i].isInit = true;
                        }
                    }
                    break;

                // virtual button
                case 3:
                    ShowListField("virtual button element", materiData.figure.virtualButtonElement, fieldId + idx++);
                    break;
            }
        }
        private object OnMonitorCameraElementDrawCallback(string listId, object element, int index)
        {
            MonitorCamera monitorCamera = element as MonitorCamera;

            ShowSimpleField("name", listId + "monitorName" + index, ref monitorCamera.displayName, 40f, 200f);
            GUILayout.Space(5f);
            ShowCameraPositionField("monitor camera " + (index + 1), listId + "monitorDestination" + index, ref monitorCamera.destination);

            return monitorCamera;
        }

        private void OnFigureListTabCallback(int selected, object data, string fieldId)
        {
            Figure figure = data as Figure;
            int idx = 0;

            switch (selected)
            {
                // camera
                case 0:
                    ShowReferenceField<TroubleshootNodeType, TroubleshootNode>("Override Camera", fieldId + idx++, ref figure.cameraOverrideTroubleshoot);

                    EditorGUI.BeginDisabledGroup(figure.cameraOverrideTroubleshoot.GetId() != -1);
                    ShowCameraPositionField("camera destination", fieldId + idx++, ref figure.cameraDestination);
                    ShowListField("monitor camera", figure.monitorCameras, fieldId + idx++);
                    EditorGUI.EndDisabledGroup();

                    break;

                // part object
                case 1:

                    ShowReferenceField<TroubleshootNodeType, TroubleshootNode>("Override Part Object", fieldId + idx++, ref figure.partObjectOverrideTroubleshoot);

                    EditorGUI.BeginDisabledGroup(figure.partObjectOverrideTroubleshoot.GetId() != -1);

                    partObjectEditor.Show(figure.partObjects, fieldId, idx++);

                    EditorGUI.EndDisabledGroup();

                    break;

                // callout
                case 2:

                    ShowListField("callout prefab", figure.callouts, fieldId + idx++);

                    break;

                // helper
                case 3:

                    ShowListField("helper prefab", figure.helpers, fieldId + idx++);

                    break;

                // cutaway
                case 4:

                    CutawayEditor.Show(figure.cutaway, this, fieldId + idx++);

                    break;
            }
        }

        private object OnSchematicElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            Schematic schematic = element as Schematic;

            if (schematic.image == null && schematic.video == null)
            {
                EditorGUILayout.LabelField("drop image / video here ...");
                Rect elementRect = GUILayoutUtility.GetLastRect();
                Event e = Event.current;

                if (elementRect.Contains(e.mousePosition) && e.type == EventType.DragExited)
                {
                    var selections = DragAndDrop.objectReferences;

                    if (selections.Length > 0)
                    {
                        Texture2D image = (object)selections[0] as Texture2D;
                        VideoClip video = (object)selections[0] as VideoClip;

                        if (image != null)
                            schematic.image = image;
                        else if (video != null)
                            schematic.video = video;
                    }
                }
            }
            else
            {
                if (schematic.image != null)
                    ShowAssetField("image", listId + index + 1, ref schematic.image);

                else if (schematic.video != null)
                {
                    ShowAssetField("video", listId + index + 2, ref schematic.video);
                    ShowSimpleField("loop", listId + index + 3, ref schematic.isLooping);
                }

                ShowSimpleField("show immediately", listId + index + 4, ref schematic.showImmediately);
                ShowSimpleField("description id", listId + index + 5, ref schematic.descriptionId);
            }

            GUILayout.EndVertical();

            return schematic;
        }

        #region callout

        private object OnCalloutElementDrawCallback(string listId, object element, int index)
        {
            GameObject go = element as GameObject;
            Callout callout = null;
            if (go != null)
                callout = go.GetComponent<Callout>();

            ShowAssetField("callout prefab", listId + index, ref callout);

            if (callout == null)
                return go;

            return callout.gameObject;
        }

        #endregion

        #region helper

        private object OnHelperElementDrawCallback(string listId, object element, int index)
        {
            GameObject helper = element as GameObject;

            ShowSimpleField("helper prefab", listId + index, ref helper);

            return helper;
        }

        #endregion

        public void ContentView(ref ContentTroubleshootNodeGraph troubleshoot, Rect panelRect, TroubleshootNode troubeshootNode)
        {
            currentTroubleshootData = troubeshootNode.content.troubleshootdata;
            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("Play Troubleshoot"))
                {
                    playedTroubleshoot = troubleshoot.troubleshootdata;
                    EventManager.TriggerEvent(new TroubleshootPlayEvent(troubleshoot.troubleshootdata));
                }

                if (playedTroubleshoot == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("Reset Troubleshoot"))
                {
                    playedTroubleshoot = null;
                    EventManager.TriggerEvent(new TroubleshootPlayEvent());
                }

                GUILayout.BeginHorizontal();


                GUILayout.EndHorizontal();

                if (playedTroubleshoot == null)
                    EditorGUI.EndDisabledGroup();
            }
            troubleshootNodes.Clear();
            ShowSimpleField("Node Title", ref troubleshoot.troubleshootdata.title);
            ShowTextArea("description", panelRect.width - 15f, ref troubleshoot.troubleshootdata.description);
            ShowSimpleField("Node Add On Text", ref troubleshoot.troubleshootdata.text);
            ShowAssetField("Custom Button Prefab", ref troubleshoot.troubleshootdata.prefabCustomButton);
            ShowSimpleField("Show Popup Description", ref troubleshoot.troubleshootdata.isPopUpDescription);
            if (troubleshoot.troubleshootdata.isPopUpDescription)
                ShowTextArea("pop up description", panelRect.width - 15f, ref troubleshoot.troubleshootdata.popUpDescription);
            ShowTab("categories", troubleshoot.troubleshootdata);
            GUILayout.FlexibleSpace();
        }

        #region Animation

        private object OnAnimationElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            AnimationFigureData animationFigureData = element as AnimationFigureData;
            if (currentTroubleshootData.figure.isSequenceAnimation)
                ShowSimpleField("sequence queue", listId + index + idx++, ref animationFigureData.sequentialQueue);

            ShowSimpleField("animation script", listId + index + idx++, ref animationFigureData.isAnimatedScript);

            EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript);
            ShowSimpleField("animator name", listId + index + idx++, ref animationFigureData.animationName);
            ShowSimpleField("animator controller", listId + index + idx++, ref animationFigureData.animatorController);
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript == false);
            ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Animation", listId + index + idx++, ref animationFigureData.scriptedAnimationElement);
            ShowSimpleField("Object Animation", listId + index + idx++, ref animationFigureData.gameObjectInteraction);
            ShowSimpleField("Reverse Animation", listId + index + idx++, ref animationFigureData.isReverseAnimation);
            ShowSimpleField("Disabled After Finish", listId + index + idx++, ref animationFigureData.isDisabledFinished);
            EditorGUI.EndDisabledGroup();

            GUILayout.EndVertical();
            return animationFigureData;
        }

        #endregion

        private string FoldoutTextVirtualButtonListCallback(object element)
        {
            VirtualButtonFigureData materiView = element as VirtualButtonFigureData;
            if (materiView.virtualButtonElementType.GetData() != null)
                return materiView.virtualButtonElementType.GetData().name;
            else
                return "none virtual button";
        }
        private object OnVirtualButtonElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonFigureData virtualbuttonElement = element as VirtualButtonFigureData;
            ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("Virtual Button", listId + index + idx++, ref virtualbuttonElement.virtualButtonElementType);
            if (virtualbuttonElement.virtualButtonElementType.GetData() != null)
            {
                ShowSimpleField("Override Value", listId + index + idx++, ref virtualbuttonElement.isOverrideVirtualButton);
                if (virtualbuttonElement.isOverrideVirtualButton)
                {
                    ShowListField("virtual button override", virtualbuttonElement.overrideVirtualButton, listId + index + idx);
                }
            }

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        private object OnVirtualButtonOverrideDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            OverrideVariableVirtualButton virtualbuttonElement = element as OverrideVariableVirtualButton;

            ShowSimpleField("Override Variable", listId + index + idx++, ref virtualbuttonElement.OverrideVirtualButton);
            if (virtualbuttonElement.OverrideVirtualButton == OverrideVirtualButton.DefaultValue)
                ShowSimpleField("Default Value", listId + index + idx++, ref virtualbuttonElement.defaultValue);

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }
    }
}
#endif
