﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using XNodeEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomNodeGraphEditor(typeof(TroubleshootGraph))]
    public class TroubleshootGraphEditor : NodeGraphEditor
    {
        public static Materi currentMateri;
        public static TroubleshootGraph graphTemp;
        bool loadPressed, savePressed;
        public static EditorGUISplitView splitter;
        TroubleshootGraphData troubleshootGraphData;
        TroubleshootGraph currentGraph;
        string currentControlName;
        Texture2D texture;
        private Vector2 contentViewScrollPosition = new Vector2();
        TroubleshootNode content;

        //[MenuItem("Virtual Training/Troubleshoot Database &t")]
        public static void GetWindow()
        {
            OpenEditorWindow();
        }

        public TroubleshootGraphEditor()
        {
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }
        ~TroubleshootGraphEditor()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
        }

        private void PlayModeStateChanged(PlayModeStateChange obj)
        {
            InitializeOnPlay();
        }

        void InitializeOnPlay()
        {
            if (EditorPrefs.HasKey("OpenTroubleshoot"))
            {
                string path = EditorPrefs.GetString("OpenTroubleshoot");
                TroubleshootGraph troubleshoot = (TroubleshootGraph)AssetDatabase.LoadAssetAtPath(path, typeof(TroubleshootGraph));
                graphTemp = troubleshoot;
                var xrayMats = DatabaseManager.GetXrayMaterials();
                troubleshootGraphData = new TroubleshootGraphData(null, graphTemp.name, xrayMats);
                if (graphTemp != null)
                {
                    NodeEditorWindow.assetPath = AssetDatabase.GetAssetPath(graphTemp.GetInstanceID());
                }
            }
        }

        public override void OnOpen()
        {
            base.OnOpen();
            NodeEditorWindow.OnCloseEditorAction += TroubleshootGraphEditor_OnCloseEditorAction;
            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Horizontal, "xNode");

            if (graphTemp != null)
            {
                var xrayMats = DatabaseManager.GetXrayMaterials();
                troubleshootGraphData = new TroubleshootGraphData(null, currentMateri.troubleshootData.troubleshootGraph.name, xrayMats);
            }

            if (File.Exists(Application.dataPath + "/Temp/AssetTemp.asset"))
            {
                XNode.NodeGraph currentGraphTemp = (XNode.NodeGraph)AssetDatabase.LoadAssetAtPath("Assets/Temp/AssetTemp.asset", typeof(XNode.NodeGraph));
                currentGraph = currentGraphTemp as TroubleshootGraph;
            }


            texture = new Texture2D(1, 1);
            texture.SetPixel(0, 0, new Color(0.22f, 0.22f, 0.22f));
            texture.Apply();
        }

        private void TroubleshootGraphEditor_OnCloseEditorAction()
        {
            troubleshootGraphData.SaveCameraDestination();

            //Debug.Log("close window");
            NodeEditorWindow.OnCloseEditorAction -= TroubleshootGraphEditor_OnCloseEditorAction;
            //NodeEditorWindow.SaveTempFile();
            if (checkMD5(NodeEditorWindow.assetPath) != checkMD5(Application.dataPath + "/Temp/AssetTemp.asset"))
            {
                if (EditorUtility.DisplayDialog("Save", "Save?", "yes", "no"))
                {
                    NodeEditorWindow.SaveTempFile();
                }
            }
            //NodeEditorWindow.DeleteTempFile();
        }
        public string checkMD5(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        return Encoding.Default.GetString(md5.ComputeHash(stream));
                    }
                }
            }
            return null;
        }

        // Use this for initialization
        public override string GetNodeMenuName(System.Type type)
        {
            //Debug.Log(type.Namespace);
            if (type.Namespace == "VirtualTraining.Core")
            {
                return base.GetNodeMenuName(type).Replace("Virtual Training/Core/", "");
            }
            else return null;
        }

        void ShortcutInput()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.KeyDown:

                    if (e.alt)
                    {
                        if (e.keyCode == KeyCode.L)
                        {
                            loadPressed = true;
                            e.Use();
                        }
                        else if (e.keyCode == KeyCode.S)
                        {
                            savePressed = true;
                            e.Use();
                        }
                    }
                    break;
            }
        }

        Rect toolbarRect
        {
            get { return new Rect(20f, 00f, splitter.GetSplitPosition() - 10f, 0); }
        }

        Rect graphTroubleshootRect
        {
            get { return new Rect(0, 0, splitter.GetSplitPosition() - 3f, window.position.height - 30); }
        }
        protected Rect detailPanelRect
        {
            get { return new Rect(0f, 0f, window.position.width - splitter.GetSplitPosition() - 5f, window.position.height - 40); }
        }

        Rect bottomToolbar
        {
            get { return new Rect(0, window.position.height - 30f, window.position.width, 30f); }
        }

        public override void OnGUI()
        {
            base.OnGUI();
            ShortcutInput();
            splitter.BeginSplitView(toolbarRect.y, toolbarRect.height + graphTroubleshootRect.height);

            NodeEditorWindow.newGraphRect = graphTroubleshootRect;


            splitter.Split();
            GUILayout.BeginArea(detailPanelRect);

            GUI.skin.box.normal.background = texture;
            GUI.Box(detailPanelRect, GUIContent.none);

            contentViewScrollPosition = EditorGUILayout.BeginScrollView(contentViewScrollPosition);
            EditorGUILayout.BeginVertical();

            if (currentGraph != null)
            {
                if (content != null)
                {
                    TroubleshootNode contentTemp = Selection.activeObject as TroubleshootNode;
                    if (contentTemp != null)
                    {
                        if (contentTemp != content)
                            content = contentTemp;
                    }
                }
                else
                {
                    if (currentGraph.nodes.Count > 0)
                        content = currentGraph.nodes[0] as TroubleshootNode;
                }
                if (troubleshootGraphData != null && content != null)
                    troubleshootGraphData.ContentView(ref content.content, detailPanelRect, content);

                //troubleshootGraphData.ContentView(ref content.content);

                if (!string.IsNullOrEmpty(GUI.GetNameOfFocusedControl()))
                {
                    if (GUI.GetNameOfFocusedControl() != currentControlName)
                    {
                        EditorUtility.SetDirty(currentGraph);
                        currentControlName = GUI.GetNameOfFocusedControl();
                    }
                }
                EditorUtility.SetDirty(currentGraph);

            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();
            splitter.EndSplitView();

            GUILayout.BeginHorizontal();
            GUI.skin.box.normal.background = texture;
            GUI.Box(bottomToolbar, GUIContent.none);

            GUILayout.FlexibleSpace();
            if (currentGraph != null && troubleshootGraphData != null)
            {
                troubleshootGraphData.SetDropdownGraphEditor(ref currentGraph);
            }
            if (loadPressed || GUILayout.Button("load (alt + L)", GUILayout.Width(100)))
            {
                if (EditorUtility.DisplayDialog("Load", "Load ?", "yes", "no"))
                {
                    EditorUtility.SetDirty(currentGraph);
                    NodeEditorWindow.LoadTempFile();
                    OnOpen();
                }
                loadPressed = false;
            }

            if (savePressed || GUILayout.Button("save (alt + S)", GUILayout.Width(100)))
            {
                if (EditorUtility.DisplayDialog("Save", "Save ?", "yes", "no"))
                {
                    if (currentGraph != null)
                        EditorUtility.SetDirty(currentGraph);
                    NodeEditorWindow.SaveTempFile();
                }
                savePressed = false;
            }


            GUILayout.EndHorizontal();
            window.Repaint();
        }


        public static NodeEditorWindow OpenEditorWindow()
        {
            string key = "lastTroubleshootEditor";
            string lastFile = "";
            if (EditorPrefs.HasKey(key))
                lastFile = EditorPrefs.GetString(key);

            string[] guids = AssetDatabase.FindAssets("t:" + typeof(TroubleshootGraph).ToString());
            foreach (string guid in guids)
            {
                Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(TroubleshootGraph));
                if (string.IsNullOrEmpty(lastFile) || obj.name == lastFile)
                {
                    return OpenEditorWindow(obj as TroubleshootGraph);
                }
            }
            return null;
        }

        public static NodeEditorWindow OpenEditorWindow(TroubleshootGraph scriptableObjectAsset)
        {

            string key = "lastTroubleshootEditor";
            EditorPrefs.SetString(key, scriptableObjectAsset.name);

            NodeEditorWindow window = EditorWindow.GetWindow<NodeEditorWindow>();
            window.Focus();
            window.Repaint();
            //window.titleContent = new GUIContent(scriptableObjectAsset.name);
            window.titleContent = new GUIContent("Troubleshoot");
            NodeEditorWindow.CopyNodeGraph(scriptableObjectAsset.GetInstanceID()); //for asset

            //NodeEditorWindow.Open(scriptableObjectAsset); // for data
            return window;
        }

        public static void SetMateri(ref Materi materi)
        {
            currentMateri = materi;
            graphTemp = materi.troubleshootData.troubleshootGraph;
            string key = "OpenTroubleshoot";
            string pathInstance = AssetDatabase.GetAssetPath(materi.troubleshootData.troubleshootGraph.GetInstanceID());
            EditorPrefs.SetString(key, pathInstance);
            OpenEditorWindow(graphTemp);
        }
    }
}

#endif