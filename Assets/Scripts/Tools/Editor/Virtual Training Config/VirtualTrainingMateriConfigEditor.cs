﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Callbacks;
using UnityEditor;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;

namespace VirtualTraining.Tools
{
    public class VirtualTrainingMateriConfigEditor : EditorWindowDatabaseBase<VirtualTrainingMateriConfig, VirtualTrainingMateriConfigEditor, MateriConfigData>
    {
        [MenuItem("Virtual Training/Materi Config")]
        static void OpenWindow()
        {
            VirtualTrainingMateriConfigEditor window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualTrainingMateriConfigEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                return true;
            }
            return false;
        }

        public static void ShowMateriConfig()
        {
            VirtualTrainingMateriConfigEditor window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.propertyField.SetSelectedTab("materi config", 0);
            }
        }

        PropertyField propertyField;
        Vector2 scrollView = new Vector2();

        private void OnEnable()
        {
            propertyField = new PropertyField(RecordUndo, "config editor");
            propertyField.CreateTab("materi config", DrawTabCallback, "MATERI DATABASES", "OTHER DATABASES", "PDF", "PROJECT DETAILS", "STARTUP VIDEO", "QUIZ");
            propertyField.CreateListView("materi asset", null, null, MateriElementCallback, null, false, typeof(MateriTreeAsset));
            propertyField.CreateListView("pdf materi", null, null, PdfElementCallback, null, true, typeof(PDFAsset));
            propertyField.CreateListView("virtual button asset", null, null, VirtualButtonElementCallback, null, true, typeof(VirtualButtonTreeAsset));
            propertyField.CreateListView("scripted animation asset", null, null, ScriptedElementCallback, null, true, typeof(ScriptedAnimationTreeAsset));
            propertyField.CreateListView("pdf ui", null, null, PdfElementCallback, null, true, typeof(PDFAsset));
            propertyField.CreateListView("pdf link", null, null, PdfElementCallback, null, true, typeof(PDFAsset));
        }

        private object MateriElementCallback(string listId, object element, int index)
        {
            MateriContainer materi = element as MateriContainer;
            GUILayout.BeginVertical();

            propertyField.ShowAssetField("materi asset", listId + index, ref materi.materi);
            propertyField.ShowListField("pdf materi", materi.pdfs, listId + index + 1);

            GUILayout.EndVertical();

            return materi;
        }

        private object VirtualButtonElementCallback(string listId, object element, int index)
        {
            VirtualButtonTreeAsset vb = element as VirtualButtonTreeAsset;
            propertyField.ShowAssetField("virtual button asset", listId + index, ref vb);
            return vb;
        }

        private object ScriptedElementCallback(string listId, object element, int index)
        {
            ScriptedAnimationTreeAsset sa = element as ScriptedAnimationTreeAsset;
            propertyField.ShowAssetField("scripted animation asset", listId + index, ref sa);
            return sa;
        }

        private object PdfElementCallback(string listId, object element, int index)
        {
            PDFAsset pdf = element as PDFAsset;
            propertyField.ShowAssetField("pdf asset", listId + index, ref pdf);
            return pdf;
        }

        private void DrawTabCallback(int selected, object data, string fieldId)
        {
            MateriConfigData config = data as MateriConfigData;

            scrollView = GUILayout.BeginScrollView(scrollView);

            // materi databases
            if (selected == 0)
            {
                propertyField.ShowListField("materi asset", config.materiContainers);
            }
            else if (selected == 1) // other databases
            {
                propertyField.ShowAssetField("object list asset", ref config.objectListDatabase);

                GUILayout.Space(10f);
                propertyField.ShowAssetField("culling data asset", ref config.cullingDatabase);

                GUILayout.Space(10f);
                propertyField.ShowListField("virtual button asset", config.virtualButtonDatabases);

                GUILayout.Space(10f);
                propertyField.ShowListField("scripted animation asset", config.scriptedAnimationDatabases);
            }
            else if (selected == 2) // pdf
            {
                propertyField.ShowListField("pdf ui", config.pdfUis);

                GUILayout.Space(10f);
                propertyField.ShowListField("pdf link", config.pdfLinks);
            }
            else if (selected == 3) // Project details
            {
                propertyField.ShowSimpleField("project name", ref config.projectDetails.name);
                propertyField.ShowAssetField("project icon", ref config.projectDetails.icon);
                propertyField.ShowTextArea("project sub name", 500, ref config.projectDetails.subName);

                GUILayout.Space(10f);

                propertyField.ShowAssetField("help pdf", ref config.projectDetails.helpPDF);

                GUILayout.Space(10f);

                propertyField.ShowTextArea("about", 500, ref config.projectDetails.about);

            }
            else if (selected == 4) // Startup video
            {
                propertyField.ShowAssetField("startup video clip", "video clip", ref config.videoClip);
            }
            else if (selected == 5) // Quiz
            {
                propertyField.ShowSimpleField("quiz all amount", ref config.quizData.quizAllAmount);
                propertyField.ShowSimpleField("quiz persection amount", ref config.quizData.quizSectionAmount);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
        }

        private void OnGUI()
        {
            propertyField.ShowTab("materi config", ScriptableObjectTemp.GetData());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }

            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif
