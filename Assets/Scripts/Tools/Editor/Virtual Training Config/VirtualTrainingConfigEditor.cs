﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using UnityEngine.Video;

namespace VirtualTraining.Tools
{
    public class VirtualTrainingConfigEditor : EditorWindowDatabaseBase<VirtualTrainingConfig, VirtualTrainingConfigEditor, ConfigData>
    {
        [MenuItem("Virtual Training/Config")]
        static void OpenWindow()
        {
            VirtualTrainingConfigEditor window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualTrainingConfigEditor window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                return true;
            }
            return false;
        }

        PropertyField propertyField;
        Vector2 scrollView = new Vector2();

        private void OnEnable()
        {
            propertyField = new PropertyField(RecordUndo, "config editor");
            propertyField.CreateTab("config", DrawTabCallback, "SCENES", "SETTING", "AUDIO", "MATERIAL", "EDITOR ICON", "UI", "CURSOR TEXTURE");
            propertyField.CreateListView("scene asset", null, null, SceneElementCallback, null, true, typeof(SceneAsset));
            propertyField.CreateListView("xray material", null, null, MaterialElementCallback, null, true, typeof(Material));
            propertyField.CreateListView("sfx", null, null, SFXDataElementCallback, null, false, typeof(AudioClip));
            propertyField.CreateListView("icon", null, null, IconElementCallback, null, false, typeof(Texture2D));
            propertyField.CreateListView("ui theme", null, null, UIThemeCallback, null, false);
            propertyField.CreateListView("video clip", null, null, VideoClipElementCallback, null, true, typeof(VideoClip));
        }

        private void DrawTabCallback(int selected, object data, string fieldId)
        {
            ConfigData config = data as ConfigData;

            scrollView = GUILayout.BeginScrollView(scrollView);

            if (selected == 0) // scenes
            {
                propertyField.ShowSimpleField("VR mode", ref config.isVRMode);

                GUILayout.Space(10f);

                GUILayout.Label("PC scenes configuration");
                EditorGUILayout.LabelField("first scene = start scene", EditorStyles.helpBox);
                propertyField.ShowListField("scene asset", config.PC_SceneAssets, "pc");

                GUILayout.Space(10f);

                GUILayout.Label("VR scenes configuration");
                EditorGUILayout.LabelField("first scene = start scene", EditorStyles.helpBox);
                propertyField.ShowListField("scene asset", config.VR_SceneAssets, "vr");

                GUILayout.Space(10f);

                propertyField.ShowAssetField("quiz scene", ref config.quizSceneAsset);

            }
            else if (selected == 1) // setting
            {
                GUILayout.Label("system configuration");
                propertyField.ShowSimpleField("camera arrive difference", ref config.cameraArriveDifference, 200, 350);
                propertyField.ShowSimpleField("max raycast distance", ref config.maxRaycastDistance, 200, 350);
                propertyField.ShowSimpleField("reset object position duration", ref config.resetObjectPositionDuration, 200, 350);

                GUILayout.Space(10f);

                GUILayout.Label("default setting");
                propertyField.ShowSimpleField("rotate speed", ref config.defaultSetting.rotateSpeed, 200, 350);
                propertyField.ShowSimpleField("zoom speed", ref config.defaultSetting.zoomSpeed, 200, 350);
                propertyField.ShowSimpleField("drag speed", ref config.defaultSetting.dragSpeed, 200, 350);
                propertyField.ShowSimpleField("move camera speed", ref config.defaultSetting.cameraTransitionSpeed, 200, 350);
                propertyField.ShowSimpleField("voice volume", ref config.defaultSetting.voiceVolume, 200, 350);
                propertyField.ShowSimpleField("sfx volume", ref config.defaultSetting.sfxVolume, 200, 350);
                propertyField.ShowSimpleField("vr canvas distance", ref config.defaultSetting.vrCanvasDistance, 200, 350);
                propertyField.ShowSimpleField("vr menu height", ref config.defaultSetting.vrMenuHeight, 200, 350);
                propertyField.ShowSimpleField("show background object", ref config.defaultSetting.showEnvirontmentObject, 200, 350);

                GUILayout.Space(10f);
                GUILayout.Label("min and max setting");
                propertyField.ShowSimpleField("min rotate speed", ref config.minMaxSetting.rotateSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max rotate speed", ref config.minMaxSetting.rotateSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min zoom speed", ref config.minMaxSetting.zoomSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max zoom speed", ref config.minMaxSetting.zoomSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min drag speed", ref config.minMaxSetting.dragSpeed.x, 200, 350);
                propertyField.ShowSimpleField("max drag speed", ref config.minMaxSetting.dragSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min move camera speed", ref config.minMaxSetting.moveCameraSpeed.x, 200, 350); ;
                propertyField.ShowSimpleField("max move camera speed", ref config.minMaxSetting.moveCameraSpeed.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min bgm volume speed", ref config.minMaxSetting.bgmVolume.x, 200, 350);
                propertyField.ShowSimpleField("max bgm volume speed", ref config.minMaxSetting.bgmVolume.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min sfx volume speed", ref config.minMaxSetting.sfxVolume.x, 200, 350);
                propertyField.ShowSimpleField("max sfx volume speed", ref config.minMaxSetting.sfxVolume.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min vr canvas distance speed", ref config.minMaxSetting.vrCanvasDistance.x, 200, 350);
                propertyField.ShowSimpleField("max vr canvas distance speed", ref config.minMaxSetting.vrCanvasDistance.y, 200, 350);

                GUILayout.Space(5f);

                propertyField.ShowSimpleField("min vr menu height speed", ref config.minMaxSetting.vrMenuHeight.x, 200, 350);
                propertyField.ShowSimpleField("max vr menu height speed", ref config.minMaxSetting.vrMenuHeight.y, 200, 350);
            }
            else if (selected == 2) // audio
            {
                propertyField.ShowListField("sfx", config.sfxs);
            }
            else if (selected == 3) // part object
            {
                propertyField.ShowSimpleField("default highlight color", ref config.highlightColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default selected color", ref config.selectedColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default blink color", ref config.blinkColor);

                GUILayout.Space(10f);
                propertyField.ShowSimpleField("default fresnel color", ref config.fresnelColor);

                GUILayout.Space(10f);
                propertyField.ShowListField("xray material", config.xRayMaterials);
            }
            else if (selected == 4) // editor icon
            {
                if (GUILayout.Button("add all category"))
                {
                    for (int i = 0; i < System.Enum.GetValues(typeof(IconEnum)).Length; i++)
                    {
                        var icon = new EditorIcon((IconEnum)i, null);
                        config.editorIcons.Add(icon);
                    }
                }

                if (GUILayout.Button("sort category"))
                {
                    for (int i = 0; i < config.editorIcons.Count; i++)
                    {
                        config.editorIcons[i].category = (IconEnum)i;
                    }
                }

                propertyField.ShowListField("icon", config.editorIcons);
            }
            else if (selected == 5) // UI
            {
                GUILayout.Label("ui texture");

                propertyField.ShowAssetField("play", ref config.uiTexture.play);
                propertyField.ShowAssetField("pause", ref config.uiTexture.pause);
                propertyField.ShowAssetField("minimize panel", ref config.uiTexture.minimizePanel);
                propertyField.ShowAssetField("windowed panel", ref config.uiTexture.windowedPanel);
                propertyField.ShowAssetField("maximize panel", ref config.uiTexture.maximizePanel);
                propertyField.ShowAssetField("close panel", ref config.uiTexture.closePanel);
                propertyField.ShowAssetField("dropdown arrow", ref config.uiTexture.dropdownArrow);
                propertyField.ShowAssetField("checkmark", ref config.uiTexture.checkmark);

                GUILayout.Space(10f);
                GUILayout.Label("apps loading videos");
                propertyField.ShowListField("video clip", config.appsLoadingVideos, "apps");

                GUILayout.Space(10f);
                GUILayout.Label("quiz loading videos");
                propertyField.ShowListField("video clip", config.quizLoadingVideos, "quiz");

                GUILayout.Space(10f);

                propertyField.ShowListField("ui theme", config.uiThemes);
            }
            else if (selected == 6) // Cursor Texture
            {
                propertyField.ShowAssetField("link", ref config.cursorTexture.link);
                propertyField.ShowAssetField("horizontal resize", ref config.cursorTexture.horizontalResize);
                propertyField.ShowAssetField("vertical resize", ref config.cursorTexture.VerticalResize);
                propertyField.ShowAssetField("slash resize", ref config.cursorTexture.slashResize);
                propertyField.ShowAssetField("back slash resize", ref config.cursorTexture.backSlashResize);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
        }

        protected override void Save()
        {
            SetSceneNames();
            BuildSettingSetup();
            SetEditorIconLookup();
            base.Save();
        }

        private void SetEditorIconLookup()
        {
            EditorIconResources.LoadIcons();
        }

        private object SceneElementCallback(string listId, object element, int index)
        {
            SceneAsset scene = element as SceneAsset;

            propertyField.ShowAssetField("scene asset", listId + index, ref scene);

            if (index == 0)
                GUILayout.Label("<- starting scene");
            if (index == 1)
                GUILayout.Label("<- model scene");
            if (index == 2)
                GUILayout.Label("<- UI scene");

            return scene;
        }

        private object MaterialElementCallback(string listId, object element, int index)
        {
            Material mat = element as Material;
            propertyField.ShowAssetField("material", listId + index, ref mat);
            return mat;
        }

        private object SFXDataElementCallback(string listId, object element, int index)
        {
            SFXData sfxData = element as SFXData;
            GUILayout.BeginVertical();

            propertyField.ShowSimpleField("SFX", listId + index, ref sfxData.sfx);
            propertyField.ShowAssetField("audio clip", listId + index + 1, ref sfxData.clip);

            GUILayout.EndVertical();

            return sfxData;
        }

        private object IconElementCallback(string listId, object element, int index)
        {
            EditorIcon editorIcon = element as EditorIcon;
            GUILayout.BeginHorizontal();

            propertyField.ShowSimpleField("category", listId + index, ref editorIcon.category);
            propertyField.ShowAssetField("icon", listId + index + 1, ref editorIcon.icon);

            GUILayout.EndHorizontal();

            return editorIcon;
        }

        private object VideoClipElementCallback(string listId, object element, int index)
        {
            VideoClip video = element as VideoClip;
            GUILayout.BeginHorizontal();

            propertyField.ShowAssetField("video", listId + index, ref video);

            GUILayout.EndHorizontal();

            return video;
        }

        private object UIThemeCallback(string listId, object element, int index)
        {
            UITheme uiTheme = element as UITheme;

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            propertyField.ShowSimpleField("theme name", ref uiTheme.name);
            EditorGUI.BeginDisabledGroup(!Application.isPlaying);
            if (GUILayout.Button("switch theme", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH)))
            {
                EventManager.TriggerEvent(new SwitchUIThemeEvent(index));
            }
            EditorGUI.EndDisabledGroup();
            GUILayout.EndHorizontal();

            GUILayout.Space(10f);
            GUILayout.Label("Generic Text");
            propertyField.ShowSimpleField("generic text", ref uiTheme.genericTextColor);

            GUILayout.Space(10f);
            GUILayout.Label("Background");
            propertyField.ShowSimpleField("background", ref uiTheme.backgroundColor);

            GUILayout.Space(10f);
            GUILayout.Label("Taskbar");
            propertyField.ShowSimpleField("background", ref uiTheme.taskbarBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Button");
            propertyField.ShowSimpleField("default background", ref uiTheme.buttonBgDefaultColor);
            propertyField.ShowSimpleField("highlight background", ref uiTheme.buttonBgHighlightColor);
            propertyField.ShowSimpleField("default icon", ref uiTheme.buttonIconDefaultColor);
            propertyField.ShowSimpleField("pressed icon", ref uiTheme.buttonIconPressedColor);
            propertyField.ShowSimpleField("disabled background", ref uiTheme.buttonBgDisabledColor);
            propertyField.ShowSimpleField("disabled icon", ref uiTheme.buttonIconDisabledColor);

            GUILayout.Space(10f);

            GUILayout.Label("Toggle");
            propertyField.ShowSimpleField("default background", ref uiTheme.toggleBgDefaultColor);
            propertyField.ShowSimpleField("selected background", ref uiTheme.toggleBgSelectedColor);
            propertyField.ShowSimpleField("highlighted on background", ref uiTheme.toggleBgHighlightedOnColor);
            propertyField.ShowSimpleField("highlighted off background", ref uiTheme.toggleBgHighlightedOffColor);
            propertyField.ShowSimpleField("default icon", ref uiTheme.toggleIconDefaultColor);
            propertyField.ShowSimpleField("selected icon", ref uiTheme.toggleIconSelectedColor);

            GUILayout.Space(10f);

            GUILayout.Label("Panel");
            propertyField.ShowSimpleField("title", ref uiTheme.panelTitleColor);
            propertyField.ShowSimpleField("title dragged", ref uiTheme.panelTitleDraggedColor);
            propertyField.ShowSimpleField("content", ref uiTheme.panelContentColor);
            propertyField.ShowSimpleField("title text", ref uiTheme.panelTitleTextColor);
            propertyField.ShowSimpleField("dragged title text", ref uiTheme.panelTitleTextDraggedColor);

            GUILayout.Space(10f);
            GUILayout.Label("Slider");
            propertyField.ShowSimpleField("handle default", ref uiTheme.sliderHandleDefaultColor);
            propertyField.ShowSimpleField("handle highlighted", ref uiTheme.sliderHandleHighlightedColor);
            propertyField.ShowSimpleField("handle pressed", ref uiTheme.sliderHandlePressedColor);
            propertyField.ShowSimpleField("handle selected", ref uiTheme.sliderHandleSelectedColor);
            propertyField.ShowSimpleField("handle disabled", ref uiTheme.sliderHandleDisabledColor);
            propertyField.ShowSimpleField("slider fill", ref uiTheme.sliderFillColor);
            propertyField.ShowSimpleField("slider background", ref uiTheme.sliderBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Scrollbar");
            propertyField.ShowSimpleField("handle default", ref uiTheme.scrollbarHandleDefaultColor);
            propertyField.ShowSimpleField("handle highlighted", ref uiTheme.scrollbarHandleHighlightedColor);
            propertyField.ShowSimpleField("handle pressed", ref uiTheme.scrollbarHandlePressedColor);
            propertyField.ShowSimpleField("handle selected", ref uiTheme.scrollbarHandleSelectedColor);
            propertyField.ShowSimpleField("handle disabled", ref uiTheme.scrollbarHandleDisabledColor);
            propertyField.ShowSimpleField("scrollbar background", ref uiTheme.scrollbarBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Input Field");
            propertyField.ShowSimpleField("input field text normal", ref uiTheme.inputFieldTextNormalColor);
            propertyField.ShowSimpleField("input field text highlighted", ref uiTheme.inputFieldTextHighlightColor);
            propertyField.ShowSimpleField("input field text pressed", ref uiTheme.inputFieldTextPressedColor);
            propertyField.ShowSimpleField("input field text selected", ref uiTheme.inputFieldTextSelectedColor);
            propertyField.ShowSimpleField("input field text disabled", ref uiTheme.inputFieldTextDisabledColor);
            propertyField.ShowSimpleField("input field background", ref uiTheme.inputFieldBgColor);

            GUILayout.Space(10f);
            GUILayout.Label("Dropdown");
            propertyField.ShowSimpleField("dropdown normal", ref uiTheme.dropdownTextNormalColor);
            propertyField.ShowSimpleField("dropdown highlighted", ref uiTheme.dropdownTextHighlightColor);
            propertyField.ShowSimpleField("dropdown pressed", ref uiTheme.dropdownTextPressedColor);
            propertyField.ShowSimpleField("dropdown selected", ref uiTheme.dropdownTextSelectedColor);
            propertyField.ShowSimpleField("dropdown disabled", ref uiTheme.dropdownTextDisabledColor);

            GUILayout.Space(10f);
            GUILayout.Label("Text Link");
            propertyField.ShowSimpleField("text link normal", ref uiTheme.TextLinkNormalColor);
            propertyField.ShowSimpleField("text link pressed", ref uiTheme.TextLinkPressedColor);
            propertyField.ShowSimpleField("text link hover", ref uiTheme.TextLinkHoverColor);
            propertyField.ShowSimpleField("text link after", ref uiTheme.TextLinkAfterColor);

            GUILayout.EndVertical();

            return uiTheme;
        }

        private bool IsBuildSceneContain(List<EditorBuildSettingsScene> editorSettings, string newScenePath)
        {
            for (int i = 0; i < editorSettings.Count; i++)
            {
                if (editorSettings[i].path == newScenePath)
                    return true;
            }
            return false;
        }

        private void BuildSettingSetup()
        {
            List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();

            var scenes = ScriptableObjectTemp.GetData().PC_SceneAssets;
            for (int i = 0; i < scenes.Count; i++)
            {
                string scenePath = AssetDatabase.GetAssetPath(scenes[i]);
                if (!string.IsNullOrEmpty(scenePath) && !IsBuildSceneContain(editorBuildSettingsScenes, scenePath))
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
            }

            scenes = ScriptableObjectTemp.GetData().VR_SceneAssets;
            for (int i = 0; i < scenes.Count; i++)
            {
                string scenePath = AssetDatabase.GetAssetPath(scenes[i]);
                if (!string.IsNullOrEmpty(scenePath) && !IsBuildSceneContain(editorBuildSettingsScenes, scenePath))
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
            }

            string quizScenePath = AssetDatabase.GetAssetPath(ScriptableObjectTemp.GetData().quizSceneAsset);
            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(quizScenePath, true));

            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
        }

        private void SetSceneNames()
        {
            ScriptableObjectTemp.GetData().PC_Scenes.Clear();
            ScriptableObjectTemp.GetData().VR_Scenes.Clear();

            var scenes = ScriptableObjectTemp.GetData().PC_SceneAssets;
            foreach (var scene in scenes)
            {
                ScriptableObjectTemp.GetData().PC_Scenes.Add(scene.name);
            }

            scenes = ScriptableObjectTemp.GetData().VR_SceneAssets;
            foreach (var scene in scenes)
            {
                ScriptableObjectTemp.GetData().VR_Scenes.Add(scene.name);
            }

            string quizSceneName = ScriptableObjectTemp.GetData().quizSceneAsset.name;
            ScriptableObjectTemp.GetData().quizScene = quizSceneName;
        }

        private void OnGUI()
        {
            propertyField.ShowTab("config", ScriptableObjectTemp.GetData());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (GUILayout.Button("set scenes to build"))
            {
                if (EditorUtility.DisplayDialog("config", "set scenes to build ?", "ok", "cancel"))
                {
                    SetSceneNames();
                    BuildSettingSetup();
                }
            }

            if (GUILayout.Button("load"))
            {
                if (EditorUtility.DisplayDialog("config", "load ?", "ok", "cancel"))
                    Load();
            }

            if (GUILayout.Button("save"))
            {
                if (EditorUtility.DisplayDialog("config", "save ?", "ok", "cancel"))
                {
                    Save();
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}

#endif
