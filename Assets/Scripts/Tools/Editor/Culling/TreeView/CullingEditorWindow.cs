#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class CullingEditorWindow : TreeViewEditorBase<CullingTreeAsset, CullingEditorWindow, CullingData, CullingTreeElement>
    {
        CullingContentEditor cullingContentEditor;

        [MenuItem("Virtual Training/Culling Editor")]
        public static void GetWindow()
        {
            CullingEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            CullingEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                window.Initialize();
                return true;
            }
            return false;
        }

        private void OnEnable()
        {
            cullingContentEditor = new CullingContentEditor(RecordUndo, WindowId);
        }

        protected override TreeViewWithTreeModel<CullingTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<CullingTreeElement>(ScriptableObjectTemp.GetData().treeElements);
            return new TreeViewWithTreeModel<CullingTreeElement>(TreeViewState, treeModel);
        }

        protected override CullingTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new CullingTreeElement("new object", depth, id);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnGUI()
        {
            base.OnGUI();
        }

        protected override void ContentView(List<CullingTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            for (int i = 0; i < selectedElements.Count; i++)
            {
                if (!selectedElements[i].isFolder)
                    cullingContentEditor.Inspector(selectedElements[i].data, i);
            }

            EditorGUILayout.EndVertical();
        }
    }
}

#endif