#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(CullingTreeAsset))]
    public class CullingTreeAssetEditor : TreeAssetEditorBase<CullingTreeAsset, CullingTreeElement>
    {
        // Start is called before the first frame update
        protected override CullingTreeAsset GetAsset()
        {
            return (CullingTreeAsset)target;
        }

        protected override List<CullingTreeElement> GetTreeElements()
        {
            return GetAsset().GetData().treeElements;
        }
    }
}

#endif
