﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(CalloutText))]
    [CanEditMultipleObjects]
    public class CalloutTextArea : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GUILayout.BeginVertical();

            CalloutText calloutText = (CalloutText)target;
            GUILayout.Label("Callout text");
            calloutText.content = EditorGUILayout.TextArea(calloutText.content);

            // set dirty biar textnya diganti tiap frame, tenang ini cuma jalan kalo objectnya diselect aja
#if UNITY_EDITOR
            EditorUtility.SetDirty(target);
#endif

            GUILayout.EndVertical();
        }
    }
}

#endif
