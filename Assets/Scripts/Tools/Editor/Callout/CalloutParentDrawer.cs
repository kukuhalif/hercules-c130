﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(Callout))]
    [CanEditMultipleObjects]
    public class CalloutParentDrawer : Editor
    {
        // reparent to target (editor only)
        CalloutPoint[] points;
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (!Application.isPlaying)
            {
                GUILayout.Label("\nPlay at related materi to create or edit callout for easier preview !!!");
                EditorGUI.BeginDisabledGroup(true);
            }

            Callout calloutParent = target as Callout;

            if (calloutParent.pivotPoint == null)
                calloutParent.CreatePivot();

            if (!Application.isPlaying)
                calloutParent.created = false;

            GUILayout.BeginVertical();

            if (GUILayout.Button("create callout element"))
            {
                GameObject newCallout = new GameObject("Callout");
                newCallout.transform.SetParent(calloutParent.transform);
                CalloutElement ct = newCallout.AddComponent<CalloutElement>();
                ct.CreateCalloutPoint();
                ct.Setup(calloutParent);
            }

            int index = 0;

            for (int i = 0; i < calloutParent.transform.childCount; i++)
            {
                CalloutElement callout = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();

                if (callout == null)
                    continue;

                GUILayout.BeginHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("index : " + index + " : " + calloutParent.transform.GetChild(i).name))
                {
                    EditorGUIUtility.PingObject(calloutParent.transform.GetChild(i).gameObject);
                    Selection.activeGameObject = calloutParent.transform.GetChild(i).gameObject;
                }

                index++;

                GUILayout.EndVertical();

                if (GUILayout.Button("delete"))
                {
                    DestroyImmediate(calloutParent.transform.GetChild(i).gameObject);
                    break;
                }

                if (GUILayout.Button("copy"))
                {
                    CalloutElement source = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();
                    calloutParent.copySource = source;
                }

                if (calloutParent.copySource != null && GUILayout.Button("paste"))
                {
                    CalloutElement destination = calloutParent.transform.GetChild(i).GetComponent<CalloutElement>();

                    destination.inversePivot = calloutParent.copySource.inversePivot;
                    destination.ignoreX = calloutParent.copySource.ignoreX;
                    destination.ignoreY = calloutParent.copySource.ignoreY;
                    destination.ignoreZ = calloutParent.copySource.ignoreZ;
                    destination.lineLengthAdd = calloutParent.copySource.lineLengthAdd;
                    destination.cameraDistance = calloutParent.copySource.cameraDistance;
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.Space(10);

            if (GUILayout.Button("reparent point to target"))
            {
                points = calloutParent.transform.GetAllComponentsInChilds<CalloutPoint>();
                for (int i = 0; i < points.Length; i++)
                {
                    points[i].reparentCalloutPoint = true;
                    points[i].ReparentToTarget();
                    points[i].reparentCalloutPoint = false;
                }
            }

            if(points != null && GUILayout.Button("reparent point to original parent"))
            {
                for (int i = 0; i < points.Length; i++)
                {
                    points[i].ReparentToOriginalParent();
                }
            }

            GUILayout.EndVertical();

            if (!Application.isPlaying)
            {
                EditorGUI.EndDisabledGroup();
            }
        }
    }
}

#endif