﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(CalloutPoint))]
    [CanEditMultipleObjects]
    public class CalloutPointEditor : Editor
    {
        CalloutPoint _callout;

        private void OnEnable()
        {
            _callout = (CalloutPoint)target;
        }

        void OnSceneGUI()
        {
            //right click
            if (Event.current.type == EventType.MouseDown && Event.current.button == 1 && Event.current.shift)
            {
                Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(worldRay, out hitInfo, float.PositiveInfinity))
                {
                    _callout.transform.position = hitInfo.point;
                }
                Event.current.Use();
            }
        }
    }
}
#endif