﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public class CalloutValueEditor : EditorWindow
    {
        [MenuItem("Virtual Training/Callout Value Editor")]
        static void GetWindow()
        {
            EditorWindow window = GetWindow<CalloutValueEditor>();
            window.titleContent = new GUIContent("Callout Value Editor");
        }

        [MenuItem("Virtual Training/Callout Value Editor", true)]
        static bool ShowValidation()
        {
            return !Application.isPlaying;
        }


        string filter = "";
        string[] pathOptions;
        string path = "";
        Vector2 scrollPos = new Vector2();
        List<Callout> callouts = new List<Callout>();

        // callout values
        bool changeLineLengthScale = false;
        float lineLengthScale = 10f;
        bool changeBoxScaleConstant = false;
        float boxScaleConstant = 0.035f;
        bool changeLineWidthConstant = false;
        float lineWidthConstant = 2.5f;
        bool changeCalloutLineMaterial = false;
        Material calloutLineMaterial;
        bool changeTextMargin = false;
        float topMargin = 5, bottomMargin = 5, leftMargin = 10, rightMargin = 10;

        private void OnEnable()
        {
            pathOptions = Directory.GetDirectories(Application.dataPath, "*.*", SearchOption.AllDirectories);
            for (int i = 0; i < pathOptions.Length; i++)
            {
                pathOptions[i] = pathOptions[i].Replace(@"\", @"/");
            }
            path = EditorPrefs.GetString("callout value editor");
        }

        private void OnDisable()
        {
            EditorPrefs.SetString("callout value editor", path);
        }

        private void OnGUI()
        {
            filter = EditorGUILayout.TextField("path filter", filter);

            GUILayout.BeginVertical();
            GUILayout.Label("callout folder select");

            int selectedIndex = 0;
            for (int i = 0; i < pathOptions.Length; i++)
            {
                if (path == pathOptions[i])
                {
                    selectedIndex = i;
                    break;
                }
            }

            selectedIndex = EditorGUILayout.Popup(selectedIndex, pathOptions);
            path = pathOptions[selectedIndex];

            GUILayout.EndVertical();

            if (string.IsNullOrEmpty(path))
                EditorGUI.BeginDisabledGroup(true);
            if (GUILayout.Button("load callout from : " + path))
            {
                GetAllCallouts();
            }

            if (string.IsNullOrEmpty(path))
                EditorGUI.EndDisabledGroup();

            if (callouts.Count == 0)
                EditorGUI.BeginDisabledGroup(true);

            changeLineLengthScale = EditorGUILayout.Toggle("change line length scale ?", changeLineLengthScale);
            lineLengthScale = EditorGUILayout.FloatField("line length scale", lineLengthScale);

            changeBoxScaleConstant = EditorGUILayout.Toggle("change box scale constant ?", changeBoxScaleConstant);
            boxScaleConstant = EditorGUILayout.FloatField("box scale constant", boxScaleConstant);

            changeLineWidthConstant = EditorGUILayout.Toggle("change line width constant ?", changeLineWidthConstant);
            lineWidthConstant = EditorGUILayout.FloatField("line width constant", lineWidthConstant);

            changeCalloutLineMaterial = EditorGUILayout.Toggle("change callout line material ?", changeCalloutLineMaterial);
            calloutLineMaterial = EditorGUILayout.ObjectField("line material", calloutLineMaterial, typeof(Material), true) as Material;

            changeTextMargin = EditorGUILayout.Toggle("change text margin ?", changeTextMargin);
            topMargin = EditorGUILayout.FloatField("top margin", topMargin);
            bottomMargin = EditorGUILayout.FloatField("bottom margin", bottomMargin);
            leftMargin = EditorGUILayout.FloatField("left margin", leftMargin);
            rightMargin = EditorGUILayout.FloatField("right margin", rightMargin);

            if (GUILayout.Button("modify value"))
            {
                ModifyValue();
                Debug.Log("changing the callout value is complete");
            }

            if (callouts.Count == 0)
                EditorGUI.EndDisabledGroup();

            if (callouts.Count > 0)
            {
                scrollPos = GUILayout.BeginScrollView(scrollPos);
                for (int i = 0; i < callouts.Count; i++)
                {
                    GUILayout.Label((i + 1).ToString() + " " + callouts[i].gameObject.name);
                }
                GUILayout.EndScrollView();
            }
        }

        void GetAllCallouts()
        {
            callouts.Clear();
            string[] calloutPaths = Directory.GetFiles(path, "*.prefab", SearchOption.AllDirectories);
            for (int i = 0; i < calloutPaths.Length; i++)
            {
                calloutPaths[i] = calloutPaths[i].Replace(Application.dataPath, "");
                calloutPaths[i] = calloutPaths[i].Replace(@"\", @"/");
                calloutPaths[i] = "Assets" + calloutPaths[i];

                GameObject prefab = (GameObject)AssetDatabase.LoadAssetAtPath(calloutPaths[i], typeof(GameObject));
                if (prefab != null)
                {
                    Callout cp = prefab.GetComponent<Callout>();
                    if (cp != null)
                    {
                        callouts.Add(cp);
                    }
                }
            }
        }

        void ModifyValue()
        {
            for (int i = 0; i < callouts.Count; i++)
            {
                Callout instantiatedCallout = Instantiate(callouts[i]);

                if (changeLineLengthScale)
                    instantiatedCallout.lineLengthScale = lineLengthScale;
                if (changeBoxScaleConstant)
                    instantiatedCallout.boxScaleConstant = boxScaleConstant;
                if (changeLineWidthConstant)
                    instantiatedCallout.lineWidthConstant = lineWidthConstant;

                if (changeCalloutLineMaterial)
                {
                    LineRenderer[] lines = instantiatedCallout.transform.GetAllComponentsInChilds<LineRenderer>();
                    for (int l = 0; l < lines.Length; l++)
                    {
                        if (lines[l] != null)
                            lines[l].sharedMaterial = calloutLineMaterial;
                    }
                }

                if (changeTextMargin)
                    instantiatedCallout.SetMargin(topMargin, bottomMargin, leftMargin, rightMargin);

                // GetPrefabAssetPathOfNearestInstanceRoot cuma bisa dipake pas ngga di play / gameobject prefab di scene warnanya biru alias masih berupa prefab, kalo udah berupa instantiated object ga bakal bisa ini
                string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(callouts[i].gameObject);
                PrefabUtility.SaveAsPrefabAssetAndConnect(instantiatedCallout.gameObject, prefabPath, InteractionMode.AutomatedAction);

                DestroyImmediate(instantiatedCallout.gameObject);
            }
        }
    }
}

#endif