﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(CalloutElement))]
    [CanEditMultipleObjects]
    public class CalloutDrawer : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (!Application.isPlaying)
            {
                GUILayout.Label("\nPlay at related materi to create or edit callout for easier preview !!!");
                EditorGUI.BeginDisabledGroup(true);
            }

            CalloutElement callout = target as CalloutElement;

            GUILayout.BeginVertical();

            if (GUILayout.Button("add line"))
            {
                callout.CreateCalloutPoint();
            }

            for (int i = 0; i < callout.PointCount(); i++)
            {
                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical();

                if (GUILayout.Button("index : " + i))
                {
                    EditorGUIUtility.PingObject(callout.GetPoint(i));
                    Selection.activeGameObject = callout.GetPoint(i);
                }

                GameObject go = EditorGUILayout.ObjectField(callout.GetPoint(i), typeof(GameObject), true) as GameObject;
                callout.SetPoint(go, i);

                GUILayout.EndVertical();

                if (GUILayout.Button("delete"))
                {
                    callout.DeletePointAndLine(i);
#if UNITY_EDITOR
                    EditorUtility.SetDirty(target);
#endif
                    break;
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();

            if (!Application.isPlaying)
            {
                EditorGUI.EndDisabledGroup();
            }
        }
    }
}

#endif