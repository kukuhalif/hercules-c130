﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using System.IO;

namespace VirtualTraining.Tools
{
    class MaintenanceEditorWindow : TreeViewEditorBase<MaintenanceTreeAsset, MaintenanceEditorWindow, MaintenanceData, MaintenanceTreeElement>
    {
        MaintenanceContentEditor maintenanceContentEditor;
        [SerializeField] Materi materi;
        [SerializeField] int materiInstanceID;
        [SerializeField] int materiTreeID;

        public static void GetWindow()
        {
            MaintenanceEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.Initialize();
            }
        }
        public static MaintenanceEditorWindow OnOpenAsset(int instanceID)
        {
            MaintenanceEditorWindow window = OpenEditorWindow(instanceID, true);
            if (window != null)
            {
                window.Load();
                window.Initialize();
                return window;
            }

            return null;
        }

        public static void OpenMaintenance(Materi materi, int materiId, int InstanceID)
        {
            MaintenanceTreeAsset maintenance;
            //buat temp asset
            if (!Directory.Exists(Application.dataPath + "/Temp"))
                Directory.CreateDirectory(Application.dataPath + "/Temp");

            if (!File.Exists(Application.dataPath + "/Temp/" + materiId + ".asset"))
            {
                maintenance = ScriptableObject.CreateInstance<MaintenanceTreeAsset>();
                AssetDatabase.CreateAsset(maintenance, "Assets/Temp/" + materiId + ".asset");
            }
            else
            {
                maintenance = (MaintenanceTreeAsset)AssetDatabase.LoadAssetAtPath("Assets/Temp/" + materiId + ".asset", typeof(MaintenanceTreeAsset));
            }
            //set data temp maintenance
            maintenance.SetData(VirtualTrainingEditorUtility.CloneObject(materi.maintenanceData));
            //opem editor
            var window = OnOpenAsset(maintenance.GetInstanceID());
            // set materi
            window.materiTreeID = materiId;
            window.materiInstanceID = InstanceID;
            // set window title
            window.titleContent = new GUIContent(materi.name);

        }

        protected override TreeViewWithTreeModel<MaintenanceTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<MaintenanceTreeElement>(ScriptableObjectTemp.MaintenanceData.treeElements);
            return new TreeViewWithTreeModel<MaintenanceTreeElement>(TreeViewState, treeModel);
        }

        protected override MaintenanceTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new MaintenanceTreeElement("New Maintenance Data", depth, id);
        }

        protected override void Initialize()
        {
            // if scriptable object removed
            if (ScriptableObjectOriginalFile == null)
                return;

            EditorApplication.playModeStateChanged += LogPlayModeState;
            base.Initialize();
            var xrayMats = DatabaseManager.GetXrayMaterials();

            maintenanceContentEditor = new MaintenanceContentEditor(RecordUndo, WindowId, xrayMats);
        }

        protected override void Save()
        {
            base.Save();

            if (materiInstanceID != 0)
            {
                MateriEditorWindow materiEditor = MateriEditorWindow.GetWindow(materiInstanceID, false, false);
                materiEditor.SetScriptableObjectMaintenance(VirtualTrainingEditorUtility.CloneObject(ScriptableObjectTemp.MaintenanceData), materiTreeID);
            }
        }

        protected override void OnDisable()
        {
            EditorApplication.playModeStateChanged -= LogPlayModeState;

            base.OnDisable();

            if (maintenanceContentEditor != null)
                maintenanceContentEditor.SaveCameraDestination();

            if (File.Exists(Application.dataPath + "/Temp/" + materiTreeID + ".asset"))
            {
                AssetDatabase.DeleteAsset("Assets/Temp/" + materiTreeID + ".asset");
            }
        }

        void LogPlayModeState(PlayModeStateChange state)
        {
            MateriEditorWindow materiEditor = MateriEditorWindow.GetWindow(materiInstanceID, false, false);
            materiEditor.SetScriptableObjectMaintenance(VirtualTrainingEditorUtility.CloneObject(ScriptableObjectTemp.MaintenanceData), materiTreeID);

            if (ScriptableObjectOriginalFile != null)
                Save();

            Close();
        }

        protected override void ContentView(List<MaintenanceTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();

            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                    maintenanceContentEditor.DetailInspector(selectedElements[0].data, detailPanelRect, selectedElements[0]);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        maintenanceContentEditor.PreviewInspector(selectedElements[i].data, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "maintenance name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].name = selectedElements[i].data.title;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].data.title)
                    {
                        selectedElements[i].data.title = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }
    }
}
#endif
