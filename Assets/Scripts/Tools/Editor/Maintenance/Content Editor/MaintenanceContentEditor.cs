﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public class MaintenanceContentEditor : PropertyField
    {
        EditorGUISplitView splitter;
        MaintenanceDataModel playedMaintenance;
        PartObjectEditor partObjectEditor;
        MaintenanceDataModel currentMaintenanceData;

        public MaintenanceContentEditor(Action beforeModifiedCallback, string windowId, List<Material> xrayMats) : base(beforeModifiedCallback, windowId)
        {
            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Vertical, WindowId + "description");

            partObjectEditor = new PartObjectEditor(beforeModifiedCallback, windowId, xrayMats);

            CreateTab("categories", OnCategoriesTabCallback, "FIGURE", "SCHEMATIC", "ANIMATION", "VIRTUAL BUTTON");
            CreateTab("figure category", OnFigureListTabCallback, "CAMERA", "PART OBJECT", "CALLOUT", "HELPER", "CUTAWAY");

            CreateListView("monitor camera", null, null, OnMonitorCameraElementDrawCallback, null);
            CreateListView("callout prefab", null, null, OnCalloutElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("helper prefab", null, null, OnHelperElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("schematic", null, null, OnSchematicElementDrawCallback, null, false, typeof(VideoClip), typeof(Texture2D));
            CreateListView("virtual button element", FoldoutTextVirtualButtonListCallback, null, null, OnVirtualButtonElementDrawCallback);
            CreateListView("virtual button override", null, null, OnVirtualButtonOverrideDrawCallback, null);
            CreateListView("animation element", null, null, OnAnimationElementDrawCallback, null);
        }

        #region schematic

        private object OnSchematicElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            Schematic schematic = element as Schematic;

            if (schematic.image == null && schematic.video == null)
            {
                EditorGUILayout.LabelField("drop image / video here ...");
                Rect elementRect = GUILayoutUtility.GetLastRect();
                Event e = Event.current;

                if (elementRect.Contains(e.mousePosition) && e.type == EventType.DragExited)
                {
                    var selections = DragAndDrop.objectReferences;

                    if (selections.Length > 0)
                    {
                        Texture2D image = (object)selections[0] as Texture2D;
                        VideoClip video = (object)selections[0] as VideoClip;

                        if (image != null)
                            schematic.image = image;
                        else if (video != null)
                            schematic.video = video;
                    }
                }
            }
            else
            {
                if (schematic.image != null)
                    ShowAssetField("image", listId + index + 1, ref schematic.image);

                else if (schematic.video != null)
                {
                    ShowAssetField("video", listId + index + 2, ref schematic.video);
                    ShowSimpleField("loop", listId + index + 3, ref schematic.isLooping);
                }

                ShowSimpleField("show immediately", listId + index + 4, ref schematic.showImmediately);
                ShowSimpleField("description id", listId + index + 5, ref schematic.descriptionId);
            }

            GUILayout.EndVertical();

            return schematic;
        }

        #endregion

        #region callout

        private object OnCalloutElementDrawCallback(string listId, object element, int index)
        {
            GameObject go = element as GameObject;
            Callout callout = null;
            if (go != null)
                callout = go.GetComponent<Callout>();

            ShowAssetField("callout prefab", listId + index, ref callout);

            if (callout == null)
                return go;

            return callout.gameObject;
        }

        #endregion

        #region helper

        private object OnHelperElementDrawCallback(string listId, object element, int index)
        {
            GameObject helper = element as GameObject;

            ShowSimpleField("helper prefab", listId + index, ref helper);

            return helper;
        }

        #endregion

        #region monitor camera

        private object OnMonitorCameraElementDrawCallback(string listId, object element, int index)
        {
            MonitorCamera monitorCamera = element as MonitorCamera;

            ShowSimpleField("name", listId + "monitorName" + index, ref monitorCamera.displayName, 40f, 200f);
            GUILayout.Space(5f);
            ShowCameraPositionField("monitor camera " + (index + 1), listId + "monitorDestination" + index, ref monitorCamera.destination);

            return monitorCamera;
        }

        #endregion

        #region figure

        private string FoldoutTextFigureCallback(object element)
        {
            Figure figure = element as Figure;
            return figure.name;
        }

        private object OnFigureDetailDrawCallback(string listId, object element, int index)
        {
            Figure figure = element as Figure;

            ShowSimpleField("figure name", "figure name" + listId + index + 1, ref figure.name);
            ShowTab("figure category", figure, listId + index + 2);

            return figure;
        }

        private void OnFigureListTabCallback(int selected, object data, string fieldId)
        {
            Figure figure = data as Figure;
            int idx = 0;

            switch (selected)
            {
                // camera
                case 0:
                    ShowReferenceField<MaintenanceElementType, MaintenanceTreeElement>("overide camera", ref figure.cameraOverrideMaintenance);
                    EditorGUI.BeginDisabledGroup(figure.cameraOverrideMaintenance.GetId() != -1);
                    ShowCameraPositionField("camera destination", fieldId + idx++, ref figure.cameraDestination);
                    ShowListField("monitor camera", figure.monitorCameras, fieldId + idx++);
                    EditorGUI.EndDisabledGroup();

                    break;

                // part object
                case 1:

                    ShowReferenceField<MaintenanceElementType, MaintenanceTreeElement>("overide part object", ref figure.partObjectOverrideMaintenance);
                    EditorGUI.BeginDisabledGroup(figure.partObjectOverrideMaintenance.GetId() != -1);

                    partObjectEditor.Show(figure.partObjects, fieldId, idx++);

                    EditorGUI.EndDisabledGroup();

                    break;

                // callout
                case 2:

                    ShowListField("callout prefab", figure.callouts, fieldId + idx++);

                    break;

                // helper
                case 3:

                    ShowListField("helper prefab", figure.helpers, fieldId + idx++);

                    break;

                // cutaway
                case 4:

                    CutawayEditor.Show(figure.cutaway, this, fieldId + idx++);

                    break;
            }
        }

        #endregion

        #region Animation
        private object OnAnimationElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            AnimationFigureData animationFigureData = element as AnimationFigureData;


            if (currentMaintenanceData.figure.isSequenceAnimation)
                ShowSimpleField("sequence queue", listId + index + idx++, ref animationFigureData.sequentialQueue);

            ShowSimpleField("animation script", listId + index + idx++, ref animationFigureData.isAnimatedScript);

            EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript);
            ShowSimpleField("animator name", listId + index + idx++, ref animationFigureData.animationName);
            ShowSimpleField("animator controller", listId + index + idx++, ref animationFigureData.animatorController);
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript == false);
            ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Animation", listId + index + idx++, ref animationFigureData.scriptedAnimationElement);
            ShowSimpleField("Object Animation", listId + index + idx++, ref animationFigureData.gameObjectInteraction);
            ShowSimpleField("Reverse Animation", listId + index + idx++, ref animationFigureData.isReverseAnimation);
            ShowSimpleField("Disabled After Finish", listId + index + idx++, ref animationFigureData.isDisabledFinished);
            EditorGUI.EndDisabledGroup();

            GUILayout.EndVertical();
            return animationFigureData;
        }

        #endregion

        private string FoldoutTextVirtualButtonListCallback(object element)
        {
            VirtualButtonFigureData materiView = element as VirtualButtonFigureData;
            if (materiView.virtualButtonElementType.GetData() != null)
                return materiView.virtualButtonElementType.GetData().name;
            else
                return "none virtual button";
        }
        private object OnVirtualButtonElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonFigureData virtualbuttonElement = element as VirtualButtonFigureData;
            ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("Virtual Button", listId + index + idx++, ref virtualbuttonElement.virtualButtonElementType);
            if (virtualbuttonElement.virtualButtonElementType.GetData() != null)
            {
                ShowSimpleField("Override Value", listId + index + idx++, ref virtualbuttonElement.isOverrideVirtualButton);
                if (virtualbuttonElement.isOverrideVirtualButton)
                {
                    ShowListField("virtual button override", virtualbuttonElement.overrideVirtualButton, listId + index + idx);
                }
            }

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        private object OnVirtualButtonOverrideDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            OverrideVariableVirtualButton virtualbuttonElement = element as OverrideVariableVirtualButton;

            ShowSimpleField("Override Variable", listId + index + idx++, ref virtualbuttonElement.OverrideVirtualButton);
            if (virtualbuttonElement.OverrideVirtualButton == OverrideVirtualButton.DefaultValue)
                ShowSimpleField("Default Value", listId + index + idx++, ref virtualbuttonElement.defaultValue);

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        private void OnCategoriesTabCallback(int selected, object data, string fieldId)
        {
            MaintenanceDataModel maintenaceData = data as MaintenanceDataModel;
            int idx = 0;

            switch (selected)
            {
                // figure list
                case 0:

                    ShowTab("figure category", maintenaceData.figure);

                    break;

                // schematic
                case 1:

                    ShowListField("schematic", maintenaceData.schematics, fieldId + idx++);

                    break;

                // animation
                case 2:
                    ShowSimpleField("Sequence Animation", fieldId + idx++, ref maintenaceData.figure.isSequenceAnimation);
                    ShowListField("animation element", maintenaceData.figure.animationFigureDatas, fieldId + idx++);
                    for (int i = 0; i < maintenaceData.figure.animationFigureDatas.Count; i++)
                    {
                        if(maintenaceData.figure.animationFigureDatas[i].isInit == false)
                        {
                            maintenaceData.figure.animationFigureDatas[i].sequentialQueue = i;
                            maintenaceData.figure.animationFigureDatas[i].isInit = true;
                        }
                    }
                    break;

                // virtual button
                case 3:
                    ShowListField("virtual button element", maintenaceData.figure.virtualButtonElement, fieldId + idx++);
                    break;
            }
        }

        public void PreviewInspector(MaintenanceDataModel maintenanceData, int index)
        {
            ShowSimpleField("maintenance title", index.ToString(), ref maintenanceData.title);
        }

        public void DetailInspector(MaintenanceDataModel maintenanceData, Rect panelRect, MaintenanceTreeElement selectedElements)
        {
            splitter.BeginSplitView(0f, panelRect.width);
            currentMaintenanceData = maintenanceData;
            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("Play Maintenance"))
                {
                    playedMaintenance = maintenanceData;
                    EventManager.TriggerEvent(new MaintenancePlayEvent(maintenanceData));
                }

                if (playedMaintenance == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset maintenance"))
                {
                    playedMaintenance = null;
                    EventManager.TriggerEvent(new MaintenancePlayEvent());
                }

                GUILayout.BeginHorizontal();


                GUILayout.EndHorizontal();

                if (playedMaintenance == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);

            ShowSimpleField("maintenance name", ref maintenanceData.title);
            ShowSimpleField("hide description panel", ref maintenanceData.hideDescriptionPanel);
            ShowSimpleField("show description popup", ref maintenanceData.showDescriptionPopup);
            if (maintenanceData.showDescriptionPopup)
                ShowTextArea("description pop up", panelRect.width - 15f, ref maintenanceData.descriptionPopUp);
            ShowSimpleField("disable background", ref maintenanceData.disableBackground);
            ShowTextArea("description", panelRect.width - 15f, ref maintenanceData.description);
            splitter.Split();
            GUILayout.Space(5f);
            ShowTab("categories", maintenanceData);

            splitter.EndSplitView();
        }
    }
}
#endif
