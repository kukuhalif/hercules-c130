﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public static class EditorIconResources
    {
        static Dictionary<IconEnum, Texture2D> iconLookup = new Dictionary<IconEnum, Texture2D>();

        static EditorIconResources()
        {
            LoadIcons();
        }

        public static void LoadIcons()
        {
            iconLookup.Clear();

            var icons = DatabaseManager.GetEditorIcons();

            foreach (var icon in icons)
            {
                iconLookup.Add(icon.category, icon.icon);
            }
        }

        public static Texture2D Get(IconEnum category)
        {
            if (iconLookup.ContainsKey(category))
                return iconLookup[category];

            return new Texture2D(30, 18);
        }
    }
}

#endif