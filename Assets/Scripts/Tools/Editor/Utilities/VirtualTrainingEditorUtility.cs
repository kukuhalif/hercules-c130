﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor.SceneManagement;

namespace VirtualTraining.Tools
{
    public static class VirtualTrainingEditorUtility
    {
        public static void SetModelSceneDirty()
        {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetSceneByName(DatabaseManager.GetModelSceneName()));
        }

        public static T CloneObject<T>(T source)
        {
            string json = JsonUtility.ToJson(source);
            return JsonUtility.FromJson<T>(json);
        }
    }
}

#endif
