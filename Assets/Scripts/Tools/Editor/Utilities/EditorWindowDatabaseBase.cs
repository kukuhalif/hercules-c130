﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using Object = UnityEngine.Object;

namespace VirtualTraining.Tools
{
    public abstract class EditorWindowDatabaseBase<S, W, T> : EditorWindow
        where S : ScriptableObjectBase<T>
        where W : EditorWindow
    {
        string windowNameId;
        int databaseInstanceId;

        [SerializeField] S scriptableObjectOriginal;
        [SerializeField] S scriptableObjectTemp;
        [SerializeField] Stack<T> undoActions = new Stack<T>();
        [SerializeField] Stack<T> redoActions = new Stack<T>();

        protected string WindowId
        {
            get => windowNameId;
        }

        protected int InstanceId
        {
            get => databaseInstanceId;
        }

        protected S ScriptableObjectOriginalFile
        {
            get => scriptableObjectOriginal;
        }

        protected S ScriptableObjectTemp
        {
            get
            {
                if (scriptableObjectTemp == null)
                {
                    scriptableObjectTemp = CreateInstance<S>();
                    scriptableObjectTemp.name = scriptableObjectOriginal.name;

                    T data = scriptableObjectOriginal.GetData();
                    T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(data);

                    scriptableObjectTemp.SetData(convertedData);
                }

                return scriptableObjectTemp;
            }
            set
            {
                scriptableObjectTemp = value;
            }
        }

        protected static W OpenEditorWindow()
        {
            string key = "lastEditor" + typeof(S).ToString();
            string lastFile = "";
            if (EditorPrefs.HasKey(key))
                lastFile = EditorPrefs.GetString(key);

            string[] guids = AssetDatabase.FindAssets("t:" + typeof(S).ToString());
            bool windowOpened = false;

            foreach (string guid in guids)
            {
                Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(S));
                if (string.IsNullOrEmpty(lastFile) || obj.name == lastFile)
                {
                    windowOpened = true;
                    return OpenEditorWindow(obj as S, false);
                }
            }
            if (!windowOpened && guids.Length > 0)
            {
                Object obj = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guids[0]), typeof(S));
                return OpenEditorWindow(obj as S, false);
            }
            return null;
        }

        protected static W OpenEditorWindow(int instanceID, bool newWindow = true)
        {
            // get actual database asset from instance id
            S scriptableObjectAsset = EditorUtility.InstanceIDToObject(instanceID) as S;

            // check if window is already opened
            W oldWindow = null;
            W[] oldWindows = Resources.FindObjectsOfTypeAll<W>();

            foreach (var window in oldWindows)
            {
                EditorWindowDatabaseBase<S, W, T> thisWindowType = window as EditorWindowDatabaseBase<S, W, T>;
                if (thisWindowType != null)
                {
                    if (thisWindowType.ScriptableObjectOriginalFile == scriptableObjectAsset)
                    {
                        oldWindow = window;
                        break;
                    }
                }
            }

            // is window is already opened, dont open new window
            if (oldWindow != null)
                newWindow = false;

            if (scriptableObjectAsset != null)
            {
                return OpenEditorWindow(scriptableObjectAsset, newWindow, oldWindow);
            }
            return null; // we did not handle the open
        }

        static W OpenEditorWindow(S scriptableObjectAsset, bool newWindow, W oldWindow = null)
        {
            string key = "lastEditor" + typeof(S).ToString();
            EditorPrefs.SetString(key, scriptableObjectAsset.name);

            W window = null;

            if (oldWindow != null)
            {
                window = oldWindow;
            }
            else
            {
                if (newWindow)
                {
                    window = CreateInstance<W>();
                    window.Show();
                }
                else
                {
                    window = GetWindow<W>();
                }
            }

            window.Focus();
            window.Repaint();
            window.titleContent = new GUIContent(scriptableObjectAsset.name);
            EditorWindowDatabaseBase<S, W, T> editorWindowDatabase = window as EditorWindowDatabaseBase<S, W, T>;
            editorWindowDatabase.scriptableObjectOriginal = scriptableObjectAsset;
            editorWindowDatabase.databaseInstanceId = scriptableObjectAsset.GetInstanceID();
            editorWindowDatabase.windowNameId = scriptableObjectAsset.name;

            return window;
        }

        protected virtual void Save()
        {
            T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(ScriptableObjectTemp.GetData());
            scriptableObjectOriginal.SetData(convertedData);
            EditorUtility.SetDirty(scriptableObjectOriginal);
            AssetDatabase.SaveAssets(); //save file immediately

            ClearUndoRedoAction();
        }

        protected void Load()
        {
            T data = scriptableObjectOriginal.GetData();
            T clonedData = VirtualTrainingEditorUtility.CloneObject(data);
            ScriptableObjectTemp.SetData(clonedData);
        }

        protected virtual void OnDisable()
        {
            // if scriptable object is removed
            if (scriptableObjectOriginal == null)
                return;

            // check window instance
            bool exist = false;
            W[] oldWindows = Resources.FindObjectsOfTypeAll<W>();

            foreach (var window in oldWindows)
            {
                EditorWindowDatabaseBase<S, W, T> thisWindowType = window as EditorWindowDatabaseBase<S, W, T>;
                if (thisWindowType != null)
                {
                    if (thisWindowType.ScriptableObjectOriginalFile == scriptableObjectOriginal)
                    {
                        exist = true;
                        break;
                    }
                }
            }

            if (!exist)
                return;

            string newData = JsonUtility.ToJson(ScriptableObjectTemp.GetData());
            string oldData = JsonUtility.ToJson(scriptableObjectOriginal.GetData());

            if (string.IsNullOrEmpty(newData))
                return;

            if (!string.Equals(newData, oldData))
            {
                if (EditorUtility.DisplayDialog(titleContent.text, "save data ?", "yes", "no"))
                {
                    Save();
                }
            }
        }

        T CopyData(T original)
        {
            T convertedData = VirtualTrainingEditorUtility.CloneObject<T>(original);
            return convertedData;
        }

        protected int UndoCount()
        {
            return undoActions.Count;
        }

        protected int RedoCount()
        {
            return redoActions.Count;
        }

        protected void RecordUndo()
        {
            undoActions.Push(CopyData(ScriptableObjectTemp.GetData()));
        }

        protected void UndoAction()
        {
            redoActions.Push(CopyData(ScriptableObjectTemp.GetData()));

            if (undoActions.Count > 0)
            {
                T undoAction = CopyData(undoActions.Pop());
                ScriptableObjectTemp.SetData(undoAction);
            }

            GUI.FocusControl(null);
        }

        protected void RedoAction()
        {
            undoActions.Push(CopyData(ScriptableObjectTemp.GetData()));

            if (redoActions.Count > 0)
            {
                T redoAction = CopyData(redoActions.Pop());
                ScriptableObjectTemp.SetData(redoAction);
            }

            GUI.FocusControl(null);
        }

        protected void ClearUndoRedoAction()
        {
            undoActions.Clear();
            redoActions.Clear();
        }
    }
}

#endif