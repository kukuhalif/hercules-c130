﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        [MenuItem("Virtual Training Shortcut/Save Camera Position &-", false)]
        static void SaveCameraPosition()
        {
            isSaveCameraPressed = true;
        }

        [MenuItem("Virtual Training Shortcut/Save Camera Position &-", true)]
        static bool SaveCameraPositionVerify()
        {
            return isEditingCamera;
        }

        static bool isEditingCamera;
        static bool isSaveCameraPressed;
        private CameraDestination cameraDestination;
        private string currentEditCameraControlName = "";
        private Camera cameraPointer = null;

        private void CameraDestinationInitialization()
        {
            isEditingCamera = false;
            isSaveCameraPressed = false;
        }

        public void ShowCameraPositionField(string cameraName, ref CameraDestination cameraDestination, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            ShowCameraPositionField(cameraName, cameraName, ref cameraDestination, labelWidth, fieldWidth);
        }

        public void ShowCameraPositionField(string cameraName, string controlName, ref CameraDestination cameraDestination, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)
        {
            GUILayout.BeginHorizontal();

            float buttonWidth = fieldWidth - labelWidth;

            if (!isEditingCamera)
            {
                if (DrawButton(IconEnum.EditPosition, "edit this camera position", 30f))
                {
                    this.cameraDestination = cameraDestination;
                    isEditingCamera = true;
                    currentEditCameraControlName = controlName;

                    GameObject cameraPointer = new GameObject("camera pointer");
                    cameraPointer.transform.SetAsLastSibling();
                    cameraPointer.transform.position = cameraDestination.position;
                    cameraPointer.transform.rotation = cameraDestination.rotation;
                    this.cameraPointer = cameraPointer.AddComponent<Camera>();
                    this.cameraPointer.fieldOfView = cameraDestination.fov;
                    this.cameraPointer.orthographic = cameraDestination.isOrthographic;
                    this.cameraPointer.orthographicSize = cameraDestination.orthographicSize;
                    this.cameraPointer.nearClipPlane = 0.01f;
                    EditorGUIUtility.PingObject(cameraPointer);
                    Selection.activeGameObject = cameraPointer;
                }

                GUILayout.Space(5f);
                ShowSimpleField("target", controlName + "cameraTarget", ref cameraDestination.target, 40f, 200f);

                GUILayout.Space(5f);
                var favoriteIcon = EditorIconResources.Get(IconEnum.EditFavourite);
                if (GUILayout.Button(new GUIContent(favoriteIcon, "add to favourite"), GUILayout.Width(favoriteIcon.width)))
                {
                    var favorite = EditorWindow.GetWindow<FavoriteEditor>();
                    favorite.AddCameraDestination(cameraDestination);
                }

                if (FavoriteEditor.tempCameraDestination != null && GUILayout.Button("paste from favorite", GUILayout.Width(buttonWidth)))
                {
                    cameraDestination = new CameraDestination(FavoriteEditor.tempCameraDestination);
                }

                GUILayout.Space(5f);
                GUILayout.Label(cameraDestination.ToString(), GUILayout.ExpandWidth(true));
            }
            else if (currentEditCameraControlName == controlName && GUILayout.Button("save " + cameraName, GUILayout.Width(buttonWidth)))
            {
                isEditingCamera = false;
                isSaveCameraPressed = true;
            }

            GUILayout.EndHorizontal();

            if (currentEditCameraControlName == controlName && isSaveCameraPressed)
            {
                SaveCameraDestination();
            }
        }

        public void SaveCameraDestination()
        {
            if (cameraDestination == null)
                return;

            beforeModifiedCallback?.Invoke();

            isSaveCameraPressed = false;
            isEditingCamera = false;

            cameraDestination.position = cameraPointer.transform.position;
            cameraDestination.rotation = cameraPointer.transform.rotation;
            cameraDestination.isOrthographic = cameraPointer.orthographic;
            cameraDestination.orthographicSize = cameraPointer.orthographicSize;
            cameraDestination.fov = cameraPointer.fieldOfView;

            if (Application.isPlaying)
                Object.Destroy(cameraPointer.gameObject);
            else
                Object.DestroyImmediate(cameraPointer.gameObject);

            cameraPointer = null;
            currentEditCameraControlName = "";

            cameraDestination = null;
        }
    }
}

#endif