﻿#if UNITY_EDITOR

using System;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        string windowId;
        Action beforeModifiedCallback;

        protected string WindowId
        {
            get => windowId;
        }

        public PropertyField(Action beforeModifiedCallback, string windowId)
        {
            this.beforeModifiedCallback = beforeModifiedCallback;
            this.windowId = windowId;
            ReferenceFieldInitialization();
            CameraDestinationInitialization();
        }

        public void TriggerBeforeModifiedCallback()
        {
            beforeModifiedCallback?.Invoke();
        }
    }
}

#endif