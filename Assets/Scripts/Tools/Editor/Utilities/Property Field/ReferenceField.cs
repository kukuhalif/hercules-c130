﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        class ReferenceTypeDropdownData
        {
            public int id;
            public string name;
            Action<int> openEditorAction;

            public ReferenceTypeDropdownData(int id, string name, Action<int> openEditorAction)
            {
                this.id = id;
                this.name = name;
                this.openEditorAction = openEditorAction;
            }

            public bool canOpenEditor()
            {
                return openEditorAction != null;
            }

            public void OpenEditor()
            {
                if (openEditorAction != null)
                    openEditorAction.Invoke(id);
            }
        }

        string referenceFilter = "";
        Dictionary<Type, List<ReferenceTypeDropdownData>> databaseReferenceLookup = new Dictionary<Type, List<ReferenceTypeDropdownData>>();

        private void ReferenceFieldInitialization()
        {
            var scriptedAnimation = DatabaseManager.GetScriptedAnimationTree();
            CacheTreeElement(scriptedAnimation, (id) =>
            {
                var asset = DatabaseManager.GetScriptedAnimationDatabase(id);
                ScriptedAnimationEditorWindow.GetWindow(asset.GetInstanceID());
                ScriptedAnimationEditorWindow.SelectItem(id);

            });

            var virtualButton = DatabaseManager.GetVirtualButtonTree();
            CacheTreeElement(virtualButton, (id) =>
             {
                 var asset = DatabaseManager.GetVirtualButtonDatabase(id);
                 VirtualButtonEditorWindow.GetWindow(asset.GetInstanceID());
                 VirtualButtonEditorWindow.SelectItem(id);
             });

            var maintenance = DatabaseManager.GetEditorMaintenanceList();
            CacheCustomList(maintenance);

            var troubleshoot = DatabaseManager.GetTroubleshootNodes();
            CacheNodeReference(troubleshoot);
        }

        private void CacheTreeElement<T>(T element, Action<int> openEditorAction = null)
            where T : TreeElement
        {
            if (!element.hasChildren)
                return;

            for (int i = 0; i < element.children.Count; i++)
            {
                if (!databaseReferenceLookup.ContainsKey(typeof(T)))
                    databaseReferenceLookup.Add(typeof(T), new List<ReferenceTypeDropdownData>());

                if (element.children[i].depth > -1)
                    databaseReferenceLookup[typeof(T)].Add(new ReferenceTypeDropdownData(element.children[i].id, element.children[i].CompleteName, openEditorAction));

                CacheTreeElement<T>(element.children[i] as T, openEditorAction);
            }
        }

        private void CacheCustomList<T>(List<CustomTreeElementReference<T>> datas, Action<int> openEditorAction = null)
            where T : TreeElement
        {
            if (!databaseReferenceLookup.ContainsKey(typeof(T)))
                databaseReferenceLookup.Add(typeof(T), new List<ReferenceTypeDropdownData>());

            for (int i = 0; i < datas.Count; i++)
            {
                databaseReferenceLookup[typeof(T)].Add(new ReferenceTypeDropdownData(datas[i].data.id, datas[i].completeName, openEditorAction));
            }
        }

        private void CacheNodeReference<T>(List<T> datas, Action<int> openEditorAction = null)
            where T : NodeReferenceBase
        {
            if (!databaseReferenceLookup.ContainsKey(typeof(T)))
                databaseReferenceLookup.Add(typeof(T), new List<ReferenceTypeDropdownData>());

            for (int i = 0; i < datas.Count; i++)
            {
                databaseReferenceLookup[typeof(T)].Add(new ReferenceTypeDropdownData(datas[i].id, datas[i].CompleteName, openEditorAction));
            }
        }

        /// <summary>
        /// show tree view element reference field data
        /// </summary>
        /// <typeparam name="T">reference type class, inherit from IDatabaseReference</typeparam>
        /// <typeparam name="U">type key</typeparam>
        /// <param name="title">field title as unique control name</param>
        /// <param name="data">reference data</param>
        public void ShowReferenceField<T, U>(string title, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_DROPDOWN_FIELD_WIDTH)
            where T : IDatabaseReference
        {
            ShowReferenceField<T, U>(title, title, ref data, labelWidth, fieldWidth);
        }

        /// <summary>
        /// show tree view element reference field data
        /// </summary>
        /// <typeparam name="T">reference type class, inherit from IDatabaseReference</typeparam>
        /// <typeparam name="U">type key</typeparam>
        /// <param name="title">field title</param>
        /// <param name="controlName">unique control name</param>
        /// <param name="data">reference data</param>
        public void ShowReferenceField<T, U>(string title, string controlName, ref T data, float labelWidth = VirtualTrainingEditorConfig.DEFAULT_LABEL_FIELD_WIDTH, float fieldWidth = VirtualTrainingEditorConfig.DEFAULT_DROPDOWN_FIELD_WIDTH)
            where T : IDatabaseReference
        {
            // rect for drop area
            Rect dropArea = new Rect();

            GUILayout.BeginHorizontal();

            EditorGUIUtility.labelWidth = labelWidth;

            // set selected index for dropdown
            // set dropdown options
            int selectedIndex = 0;
            List<string> dropdownOption = new List<string>();
            dropdownOption.Add("none selected ...");
            var refDatas = databaseReferenceLookup[typeof(U)];
            for (int i = 0; i < refDatas.Count; i++)
            {
                if (data.GetId() == refDatas[i].id)
                    selectedIndex = i + 1;

                // is filtered
                if (GUI.GetNameOfFocusedControl() == controlName + "filter")
                {
                    if (string.IsNullOrEmpty(referenceFilter) || refDatas[i].name.ToLower().Replace(" ", "").Contains(referenceFilter.ToLower().Replace(" ", "")))
                        dropdownOption.Add(refDatas[i].name);
                    else
                        dropdownOption.Add("");
                }
                else
                {
                    dropdownOption.Add(refDatas[i].name);
                }
            }

            GUI.SetNextControlName(controlName);
            if (GUI.GetNameOfFocusedControl() == controlName)
            {
                GUI.color = Color.cyan;
                int newSelectedIndex = EditorGUILayout.Popup(title, selectedIndex, dropdownOption.ToArray(), GUILayout.Width(fieldWidth));
                if (newSelectedIndex != selectedIndex)
                {
                    beforeModifiedCallback?.Invoke();
                }

                if (newSelectedIndex > 0)
                    data.SetId(refDatas[newSelectedIndex - 1].id);
                else
                    data.SetId(-1);

                GUI.color = Color.white;
                dropArea = GUILayoutUtility.GetLastRect();
            }
            else
            {
                EditorGUILayout.Popup(title, selectedIndex, dropdownOption.ToArray(), GUILayout.Width(fieldWidth));
                dropArea = GUILayoutUtility.GetLastRect();
            }

            // dropdown filter
            EditorGUIUtility.labelWidth = 30f;
            GUI.SetNextControlName(controlName + "filter");
            if (GUI.GetNameOfFocusedControl() == controlName + "filter")
            {
                VirtualTrainingEditorState.ClearDraggedId();
                referenceFilter = EditorGUILayout.TextField("filter", referenceFilter, GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH - labelWidth));
            }
            else
            {
                EditorGUILayout.TextField("filter", "", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH - labelWidth));
            }

            if (selectedIndex > 0 && databaseReferenceLookup[typeof(U)][selectedIndex - 1].canOpenEditor())
            {
                if (GUILayout.Button("open editor"))
                {
                    databaseReferenceLookup[typeof(U)][selectedIndex - 1].OpenEditor();
                }
            }

            GUILayout.FlexibleSpace();

            GUILayout.EndHorizontal();

            // drop area handler
            Event currentEvent = Event.current;
            EventType currentEventType = currentEvent.type;

            if (dropArea.Contains(currentEvent.mousePosition))
            {
                int draggedId = VirtualTrainingEditorState.GetDraggedId();
                if (draggedId == -1)
                    return;

                GUI.FocusControl(controlName);

                if (currentEventType == EventType.DragExited)
                {
                    for (int i = 0; i < refDatas.Count; i++)
                    {
                        if (draggedId == refDatas[i].id)
                        {
                            if (data.GetId() != refDatas[i].id)
                                beforeModifiedCallback?.Invoke();

                            data.SetId(refDatas[i].id);
                            VirtualTrainingEditorState.ClearDraggedId();
                            break;
                        }
                    }
                }
            }
        }
    }
}

#endif
