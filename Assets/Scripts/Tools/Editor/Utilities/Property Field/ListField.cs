﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        public class ListView
        {
            private bool isAsset;
            Type[] dropTypes;

            private string windowId;
            private Action beforeModifiedCallback;

            private bool expandAllCommand;
            private bool collapseAllCommand;

            private Texture2D foldoutOpen;
            private Texture2D foldoutClose;

            private Texture2D grabIcon;
            private List<object> grabbedElements = new List<object>();
            private Rect grabbedRect;
            private Rect handleDestinationRect;
            private float grabOffset;
            private int dropDestination;
            private List<Rect> elementRects = new List<Rect>();

            private bool removeSelectedAction;

            private bool duplicateSelectedAction;

            private bool copySelectedAction;

            private bool cutSelectedAction;

            private bool pasteAction;

            private bool clearAction;

            private int firstSelected;
            private List<int> selectedIndexes = new List<int>();

            public delegate string FoldoutTextDelegate(object element);
            public FoldoutTextDelegate FoldoutTextCallback;

            public delegate void DrawHeaderDelegate();
            public DrawHeaderDelegate OnDrawHeaderCallback;

            public delegate object DrawElementDelegate(string listId, object element, int index);
            public DrawElementDelegate OnDrawElementCallback;

            public delegate object DrawDetailElementDelegate(string listId, object element, int index);
            public DrawDetailElementDelegate OnDrawDetailElementCallback;

            public ListView()
            {
                grabIcon = EditorIconResources.Get(IconEnum.ListEditorGrabListElement);
                foldoutOpen = EditorIconResources.Get(IconEnum.ListFoldoutOpen);
                foldoutClose = EditorIconResources.Get(IconEnum.ListFoldoutClose);
            }

            public Action GetRecordUndo()
            {
                return beforeModifiedCallback;
            }

            public bool IsAsset
            {
                get => isAsset;
            }

            public Type[] DropTypes
            {
                get => dropTypes;
            }

            private void SetElementRect(int index, Rect rect)
            {
                while (index + 1 > elementRects.Count)
                    elementRects.Add(new Rect());

                elementRects[index] = rect;
            }

            private bool IsMultipleSelect
            {
                get => Event.current.control || Event.current.shift;
            }

            public void Setup(string windowId, Action beforeModifiedCallback, FoldoutTextDelegate foldoutTextCallback, DrawHeaderDelegate drawHeaderCallback, DrawElementDelegate drawElementCallback, DrawDetailElementDelegate drawDetailElementCallback, bool isAsset, params Type[] dropTypes)
            {
                this.windowId = windowId;
                FoldoutTextCallback = foldoutTextCallback;
                OnDrawHeaderCallback = drawHeaderCallback;
                OnDrawElementCallback = drawElementCallback;
                OnDrawDetailElementCallback = drawDetailElementCallback;
                this.beforeModifiedCallback = beforeModifiedCallback;
                this.isAsset = isAsset;
                this.dropTypes = dropTypes;
            }

            public bool UseFoldout
            {
                get => OnDrawDetailElementCallback != null;
            }

            public bool IsHaveSelection
            {
                get => selectedIndexes.Count > 0;
            }

            public void ClearSelection()
            {
                selectedIndexes.Clear();
            }

            public void AddSelection(int index)
            {
                if (!selectedIndexes.Contains(index))
                    selectedIndexes.Add(index);
            }

            public void RemoveSelectedItems()
            {
                removeSelectedAction = true;
            }

            public void DuplicateSelectedItems()
            {
                duplicateSelectedAction = true;
            }

            public void CopySelection()
            {
                copySelectedAction = true;
            }

            public void CutSelection()
            {
                cutSelectedAction = true;
            }

            public void Paste()
            {
                pasteAction = true;
            }

            public void Clear()
            {
                clearAction = true;
            }

            public void ExpandAll()
            {
                expandAllCommand = true;
            }

            public void CollapseAll()
            {
                collapseAllCommand = true;
            }

            public void OnGUI<T>(string listId, List<T> datas, string additionalId)
            {
                string combinedId = listId + additionalId;

                // clear
                if (clearAction)
                {
                    beforeModifiedCallback?.Invoke();
                    datas.Clear();
                    selectedIndexes.Clear();

                    clearAction = false;
                }

                // expand all command
                if (expandAllCommand)
                {
                    for (int i = 0; i < datas.Count; i++)
                    {
                        VirtualTrainingEditorState.SetFoldoutState(windowId, combinedId + i, true);
                    }

                    expandAllCommand = false;
                }

                // collapse all command
                if (collapseAllCommand)
                {
                    for (int i = 0; i < datas.Count; i++)
                    {
                        VirtualTrainingEditorState.SetFoldoutState(windowId, combinedId + i, false);
                    }

                    collapseAllCommand = false;
                }

                if (datas.Count == 0)
                    EditorGUILayout.LabelField("list is empty ...");

                // draw title
                if (OnDrawHeaderCallback != null)
                {
                    GUILayout.BeginHorizontal();

                    if (UseFoldout)
                        GUILayout.Space((grabIcon.width * 2) + (foldoutOpen.width));
                    else
                        GUILayout.Space(grabIcon.width * 2);

                    if (datas.Count > 0)
                        OnDrawHeaderCallback();

                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }

                // copy / cut selected
                if (copySelectedAction || cutSelectedAction)
                {
                    List<object> copies = new List<object>();

                    for (int i = 0; i < selectedIndexes.Count; i++)
                    {
                        copies.Add(datas[selectedIndexes[i]]);
                    }

                    VirtualTrainingEditorState.SetListFieldElementClipboard(copies);
                    copySelectedAction = false;

                    if (cutSelectedAction)
                    {
                        beforeModifiedCallback?.Invoke();
                        foreach (var item in copies)
                        {
                            datas.Remove((T)item);
                        }

                        selectedIndexes.Clear();

                        cutSelectedAction = false;
                    }
                }

                // paste element
                if (pasteAction)
                {
                    beforeModifiedCallback?.Invoke();

                    int insertPosition = int.MaxValue;

                    for (int i = 0; i < selectedIndexes.Count; i++)
                    {
                        if (insertPosition > selectedIndexes[i])
                        {
                            insertPosition = selectedIndexes[i];
                        }
                    }

                    selectedIndexes.Clear();

                    var pasted = VirtualTrainingEditorState.GetListFieldElementClipboard<T>();
                    foreach (var item in pasted)
                    {
                        if (datas.Count - 1 > insertPosition)
                        {
                            datas.Insert(insertPosition, item);
                            selectedIndexes.Add(insertPosition);
                            insertPosition++;
                        }
                        else
                        {
                            datas.Add(item);
                            selectedIndexes.Add(datas.Count - 1);
                        }
                    }

                    pasteAction = false;
                }

                // remove selected items
                if (removeSelectedAction)
                {
                    beforeModifiedCallback?.Invoke();

                    List<T> removedItems = new List<T>();

                    for (int i = 0; i < selectedIndexes.Count; i++)
                    {
                        removedItems.Add(datas[selectedIndexes[i]]);
                    }

                    foreach (var item in removedItems)
                    {
                        datas.Remove(item);
                    }

                    selectedIndexes.Clear();
                    grabbedElements.Clear();
                    removeSelectedAction = false;
                }

                // duplicate selected items
                if (duplicateSelectedAction)
                {
                    beforeModifiedCallback?.Invoke();

                    List<T> selectedItems = new List<T>();
                    List<T> newItems = new List<T>();

                    if (isAsset)
                    {
                        for (int i = 0; i < selectedIndexes.Count; i++)
                        {
                            selectedItems.Add(datas[selectedIndexes[i]]);
                            T newItem = datas[selectedIndexes[i]];
                            newItems.Add(newItem);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < selectedIndexes.Count; i++)
                        {
                            selectedItems.Add(datas[selectedIndexes[i]]);
                            T newItem = VirtualTrainingEditorUtility.CloneObject<T>(datas[selectedIndexes[i]]);
                            newItems.Add(newItem);
                        }
                    }

                    selectedIndexes.Sort();
                    List<int> newSelecteds = new List<int>();
                    int counter = 1;
                    for (int i = 0; i < selectedIndexes.Count; i++)
                    {
                        datas.Insert(selectedIndexes[i] + counter, newItems[counter - 1]);

                        newSelecteds.Add(selectedIndexes[i] + counter);
                        VirtualTrainingEditorState.SetFoldoutState(windowId, combinedId + selectedIndexes[i] + counter + 1, true);

                        counter++;
                    }

                    selectedIndexes.Clear();
                    for (int i = 0; i < newSelecteds.Count; i++)
                    {
                        selectedIndexes.Add(newSelecteds[i]);
                    }

                    grabbedElements.Clear();
                    duplicateSelectedAction = false;
                }

                for (int i = 0; i < datas.Count; i++)
                {
                    Rect elementRect = new Rect();

                    if (UseFoldout)
                        elementRect = EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    else
                        elementRect = EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

                    SetElementRect(i, elementRect);

                    GUILayout.BeginHorizontal();

                    GUILayout.Label((i + 1).ToString());

                    // grab control
                    Rect grabRect = EditorGUILayout.GetControlRect(GUILayout.MaxWidth(grabIcon.width));
                    Rect draggingRect = grabRect;
                    grabRect.width = grabIcon.width;
                    grabRect.height = grabIcon.height;
                    GUI.DrawTexture(grabRect, grabIcon);

                    // draw selected indicator
                    if (selectedIndexes.Contains(i))
                    {
                        EditorGUI.DrawRect(draggingRect, Color.cyan);
                    }

                    // set handle dragging destination rect
                    if (dropDestination == i)
                        handleDestinationRect = draggingRect;

                    bool foldoutState = true;

                    // draw foldout
                    if (UseFoldout)
                    {
                        // get foldout state
                        foldoutState = VirtualTrainingEditorState.GetFoldoutState(windowId, combinedId + i);

                        // set foldout state

                        string foldText = "";
                        if (FoldoutTextCallback != null)
                            foldText = FoldoutTextCallback(datas[i]);

                        Texture2D foldoutTexture = foldoutState ? foldoutOpen : foldoutClose;

                        if (GUILayout.Button(foldoutTexture, EditorStyles.miniButton))
                            foldoutState = !foldoutState;

                        if (foldText != "")
                            EditorGUILayout.LabelField(foldText);

                        VirtualTrainingEditorState.SetFoldoutState(windowId, combinedId + i, foldoutState);
                    }

                    // draw element
                    if (OnDrawElementCallback != null)
                        datas[i] = (T)OnDrawElementCallback.Invoke(combinedId, datas[i], i);

                    GUILayout.FlexibleSpace();

                    // remove button
                    if (UseFoldout && DrawButton(IconEnum.EditRemove, "remove element"))
                    {
                        beforeModifiedCallback?.Invoke();

                        datas.RemoveAt(i);
                        elementRects.RemoveAt(i);
                        selectedIndexes.Clear();
                        break;
                    }

                    GUILayout.EndHorizontal();

                    if (OnDrawDetailElementCallback != null)
                    {
                        GUILayout.BeginVertical();

                        if (foldoutState || !UseFoldout)
                        {
                            datas[i] = (T)OnDrawDetailElementCallback.Invoke(combinedId, datas[i], i);
                        }

                        GUILayout.EndVertical();
                    }

                    if (!UseFoldout)
                    {
                        GUILayout.FlexibleSpace();

                        // set foldout state
                        VirtualTrainingEditorState.SetFoldoutState(windowId, combinedId + i, foldoutState);
                    }

                    // check if mouse cursor inside dragging rect
                    if (draggingRect.Contains(Event.current.mousePosition))
                    {
                        if (Event.current.type == EventType.MouseDown)
                        {
                            // clear selection if multiple selection is false
                            if (!IsMultipleSelect)
                            {
                                selectedIndexes.Clear();
                            }

                            // add selection
                            if (!selectedIndexes.Contains(i))
                            {
                                if (selectedIndexes.Count == 0)
                                    firstSelected = i;
                                if (!selectedIndexes.Contains(i))
                                    selectedIndexes.Add(i);
                            }

                            // if multiple selection true, handle shift selection
                            if (IsMultipleSelect)
                            {
                                // shift selection, in between
                                if (Event.current.shift)
                                {
                                    if (firstSelected < i)
                                    {
                                        for (int s = firstSelected; s <= i; s++)
                                        {
                                            if (!selectedIndexes.Contains(s))
                                                selectedIndexes.Add(s);
                                        }
                                    }
                                    else
                                    {
                                        for (int s = i; s <= firstSelected; s++)
                                        {
                                            if (!selectedIndexes.Contains(s))
                                                selectedIndexes.Add(s);
                                        }
                                    }
                                }
                            }

                            // clear grabbed items
                            grabbedElements.Clear();

                            // sort selection
                            selectedIndexes.Sort();
                            for (int s = 0; s < selectedIndexes.Count; s++)
                            {
                                T grabbed = datas[selectedIndexes[s]];
                                if (!grabbedElements.Contains(grabbed))
                                    grabbedElements.Add(grabbed);
                            }
                            grabbedRect = elementRects[i];
                            grabOffset = grabbedRect.y - Event.current.mousePosition.y;

                            // unfocus element
                            GUI.FocusControl(null);
                        }
                    }

                    if (Event.current.type == EventType.MouseUp)
                    {
                        // set is grabbed false
                        grabbedElements.Clear();
                    }

                    // remove button
                    if (!UseFoldout && DrawButton(IconEnum.EditRemove, "remove element"))
                    {
                        beforeModifiedCallback?.Invoke();

                        datas.RemoveAt(i);
                        elementRects.RemoveAt(i);
                        selectedIndexes.Clear();
                        break;
                    }

                    if (UseFoldout)
                        EditorGUILayout.EndVertical();
                    else
                        EditorGUILayout.EndHorizontal();
                }

                // grabbed element
                if (grabbedElements.Count > 0)
                {
                    // check grabbed item position inside drop destination
                    grabbedRect.y = Event.current.mousePosition.y + grabOffset;
                    for (int i = 0; i < elementRects.Count; i++)
                    {
                        if (elementRects[i].Contains(Event.current.mousePosition))
                        {
                            dropDestination = i;
                            break;
                        }
                    }

                    // drag dragged handle
                    EditorGUI.DrawRect(handleDestinationRect, Color.yellow);

                    if (!selectedIndexes.Contains(dropDestination))
                    {
                        // swap dragged item
                        if (!Equals(grabbedElements, datas[dropDestination]))
                        {
                            // record undo
                            beforeModifiedCallback?.Invoke();

                            // remove grabbed items
                            foreach (var element in grabbedElements)
                            {
                                datas.Remove((T)element);
                            }

                            // clear selection
                            selectedIndexes.Clear();

                            // insert grabbed items at new index
                            if (dropDestination < datas.Count)
                            {
                                for (int i = grabbedElements.Count - 1; i >= 0; i--)
                                {
                                    // insert dragged item
                                    datas.Insert(dropDestination, (T)grabbedElements[i]);

                                    // add new inserted item as selected items
                                    selectedIndexes.Add(dropDestination + i);
                                }
                            }
                            else // if index is out of range then add at last position
                            {
                                foreach (var element in grabbedElements)
                                {
                                    datas.Add((T)element);

                                    // add new added item as selected items
                                    selectedIndexes.Add(datas.Count - 1);
                                }
                            }
                        }
                    }
                }
            }
        }

        Dictionary<string, Dictionary<string, ListView>> listView = new Dictionary<string, Dictionary<string, ListView>>();

        ListView GetListView(string windowId, string id, string insideId)
        {
            if (listView.ContainsKey(id))
            {
                var insideLookup = listView[id];
                string combinedId = id + insideId;
                if (insideLookup.ContainsKey(combinedId))
                {
                    return insideLookup[combinedId];
                }
                else
                {
                    ListView defaultListView = listView[id][id];
                    ListView newListView = new ListView();
                    newListView.Setup(windowId, defaultListView.GetRecordUndo(), defaultListView.FoldoutTextCallback, defaultListView.OnDrawHeaderCallback, defaultListView.OnDrawElementCallback, defaultListView.OnDrawDetailElementCallback, defaultListView.IsAsset, defaultListView.DropTypes);
                    insideLookup.Add(combinedId, newListView);
                    return newListView;
                }
            }

            return null;
        }

        /// <summary>
        /// create generic list editor
        /// </summary>
        /// <param name="id">list id/param>
        /// <param name="foldoutTextCallback">determines what text to draw beside foldout, enter null if you don't want to display text next to the foldout</param>
        /// <param name="onDrawHeaderCallback">determines how header will be drawn, enter null if you don't want to display header</param>
        /// <param name="onDrawElementCallback">determines how element will be drawn, enter null if you dont want to draw element</param>
        /// <param name="onDrawDetailElementCallback">determines how detail element will be draw, detail element only drawn if foldout value is true, enter null if you don't want to draw detail element</param>
        /// <param name="isAsset">set true if list item is asset</param>
        /// <param name="dropTypes">supported dropped item into list</param>
        public void CreateListView(string id, ListView.FoldoutTextDelegate foldoutTextCallback, ListView.DrawHeaderDelegate onDrawHeaderCallback, ListView.DrawElementDelegate onDrawElementCallback, ListView.DrawDetailElementDelegate onDrawDetailElementCallback, bool isAsset = false, params Type[] dropTypes)
        {
            var newListView = new ListView();
            newListView.Setup(WindowId, beforeModifiedCallback, foldoutTextCallback, onDrawHeaderCallback, onDrawElementCallback, onDrawDetailElementCallback, isAsset, dropTypes);
            Dictionary<string, ListView> insideListView = new Dictionary<string, ListView>();
            insideListView.Add(id, newListView);
            listView.Add(id, insideListView);
        }

        /// <summary>
        /// show reorderable list field
        /// </summary>
        /// <typeparam name="T">type of element</typeparam>
        /// <param name="listId">id of created list</param>
        /// <param name="datas">list to show</param>
        /// <param name="additionalId">additional id for list for multiple list</param>
        public void ShowListField<T>(string listId, List<T> datas, string additionalId = "")
        {
            ListView listView = GetListView(WindowId, listId, additionalId);

            Rect labelRect = EditorGUILayout.BeginVertical(EditorStyles.label);
            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.BeginHorizontal();

            GUILayout.Label(listId);

            // drop objects to add new element
            if (listView.DropTypes.Length > 0)
            {
                Rect dropRect = GUILayoutUtility.GetLastRect();
                dropRect.width = labelRect.width;

                Event e = Event.current;

                bool added = false;

                if (dropRect.Contains(e.mousePosition))
                {
                    var objs = DragAndDrop.objectReferences;
                    foreach (var obj in objs)
                    {
                        foreach (var type in listView.DropTypes)
                        {
                            if (type == obj.GetType())
                            {
                                if (e.type == EventType.DragUpdated)
                                {
                                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                                }
                                else if (e.type == EventType.DragExited)
                                {
                                    DragAndDrop.visualMode = DragAndDropVisualMode.None;

                                    var tipe = typeof(T);
                                    if (listView.IsAsset)
                                    {
                                        object newItem = obj;
                                        datas.Add((T)newItem);
                                    }
                                    else
                                    {
                                        ConstructorInfo constructorInfo = tipe.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, new[] { typeof(object) }, null);
                                        if (constructorInfo == null)
                                        {
                                            Debug.LogError(typeof(T).ToString() + " tidak punya konstruktor (object obj)");
                                            return;
                                        }
                                        else
                                        {
                                            var newItem = constructorInfo.Invoke(new[] { obj });
                                            datas.Add((T)newItem);
                                            added = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (added)
                        return;
                }
            }

            if (DrawButton(IconEnum.EditMultipleSelect, "select all"))
            {
                listView.ClearSelection();
                for (int i = 0; i < datas.Count; i++)
                {
                    listView.AddSelection(i);
                }
            }

            if (DrawButton(IconEnum.EditAdd, "add"))
            {
                beforeModifiedCallback?.Invoke();

                if (listView.IsAsset)
                    datas.Add(default);
                else
                {
                    if (typeof(T).GetConstructor(Type.EmptyTypes) != null)
                        datas.Add((T)Activator.CreateInstance(typeof(T)));
                    else
                        datas.Add(default);
                }

                listView.ClearSelection();
            }

            if (DrawButton(IconEnum.EditRemove, "remove selected"))
            {
                listView.RemoveSelectedItems();
            }

            if (DrawButton(IconEnum.EditDuplicate, "duplicate selected"))
            {
                listView.DuplicateSelectedItems();
            }

            if (DrawButton(IconEnum.EditCopy, "copy selected"))
            {
                listView.CopySelection();
            }

            if (DrawButton(IconEnum.EditCut, "cut selected"))
            {
                listView.CutSelection();
            }

            if (DrawButton(IconEnum.EditPaste, "paste"))
            {
                listView.Paste();
            }

            if (listView.UseFoldout)
            {
                if (GUILayout.Button("expand all", EditorStyles.miniButton))
                {
                    listView.ExpandAll();
                }

                if (GUILayout.Button("collapse all", EditorStyles.miniButton))
                {
                    listView.CollapseAll();
                }
            }

            if (listView.DropTypes.Length > 0)
            {
                string types = "";
                for (int i = 0; i < listView.DropTypes.Length; i++)
                {
                    types += listView.DropTypes[i].Name;
                    if (i < listView.DropTypes.Length - 2)
                    {
                        types += " ,";
                    }
                    else if (i < listView.DropTypes.Length - 1)
                    {
                        types += " or ";
                    }
                }
                GUILayout.Label("drop [ " + types + " ] here ...");
            }

            GUILayout.FlexibleSpace();

            if (DrawButton(IconEnum.EditClear, "clear list"))
            {
                listView.Clear();
            }

            GUILayout.EndHorizontal();

            listView.OnGUI(listId, datas, additionalId);

            GUILayout.EndVertical();
            GUILayout.EndVertical();
        }
    }
}
#endif