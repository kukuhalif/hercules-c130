﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public partial class PropertyField
    {
        public delegate void DrawTabContentDelegate(int selected, object data, string fieldId);

        class Tab
        {
            public string[] TabNames;
            public DrawTabContentDelegate DrawTabContentCallback;

            public void SetSelected(string windowId, string id, int selected)
            {
                VirtualTrainingEditorState.SetTabState(windowId, id, selected);
            }

            public int GetSelected(string windowId, string id)
            {
                return VirtualTrainingEditorState.GetTabState(windowId, id);
            }

            public Tab(string[] tabNames, DrawTabContentDelegate drawTabContentCallback)
            {
                TabNames = tabNames;
                DrawTabContentCallback = drawTabContentCallback;
            }
        }

        Dictionary<string, Tab> _tabs = new Dictionary<string, Tab>();

        /// <summary>
        /// create tab view
        /// </summary>
        /// <param name="id">tab id</param>
        /// <param name="onDrawTabContentCallback">tab content callback</param>
        /// <param name="tabNames">tab button names</param>
        public void CreateTab(string id, DrawTabContentDelegate onDrawTabContentCallback, params string[] tabNames)
        {
            _tabs.Add(id, new Tab(tabNames, onDrawTabContentCallback));
        }

        /// <summary>
        /// show tab
        /// </summary>
        /// <param name="windowId">window id</param>
        /// <param name="id">tab id</param>
        /// <param name="additionalId">for multiple tab</param>
        public void ShowTab(string id, object data, string additionalId = "")
        {
            Tab tab = _tabs[id];

            GUILayout.BeginHorizontal(EditorStyles.helpBox);

            string currentSelectedId = additionalId == "" ? id : additionalId;

            for (int i = 0; i < tab.TabNames.Length; i++)
            {
                if (tab.GetSelected(WindowId, currentSelectedId) == i)
                    GUI.color = Color.cyan;

                if (GUILayout.Button(tab.TabNames[i]))
                    tab.SetSelected(WindowId, currentSelectedId, i);

                GUI.color = Color.white;
            }

            GUILayout.EndHorizontal();

            tab.DrawTabContentCallback(tab.GetSelected(WindowId, currentSelectedId), data, currentSelectedId);
        }

        /// <summary>
        /// set selected tab
        /// </summary>
        /// <param name="id">tab id</param>
        /// <param name="newSelected">new selected index</param>
        /// <param name="additionalId">for multiple tab</param>
        public void SetSelectedTab(string id, int newSelected, string additionalId = "")
        {
            Tab tab = _tabs[id];
            string currentSelectedId = additionalId == "" ? id : additionalId;

            tab.SetSelected(WindowId, currentSelectedId, newSelected);
        }
    }
}

#endif
