﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public static class VirtualTrainingEditorState
    {
        static VirtualTrainingEditorState()
        {
            treeViewStateLookup = new Dictionary<string, TreeViewState>();
            tabStateLookup = new Dictionary<string, int>();
            foldoutStateLookup = new Dictionary<string, bool>();
            treeElements = new List<object>();
            listElements = new List<object>();
        }

        public static void SaveStates(string windowId)
        {
            SaveTreeViewState(windowId);
            SaveTabState();
            SaveFoldoutState();
        }

        #region treeview

        static Dictionary<string, TreeViewState> treeViewStateLookup;

        public static TreeViewState GetTreeViewState(string id)
        {
            if (treeViewStateLookup.ContainsKey(id))
            {
                TreeViewState state = treeViewStateLookup[id];
                if (state == null)
                    state = new TreeViewState();

                return state;
            }

            TreeViewState newState = new TreeViewState();

            if (EditorPrefs.HasKey("treeState" + id))
            {
                string json = EditorPrefs.GetString("treeState" + id);
                newState = JsonUtility.FromJson<TreeViewState>(json);
            }

            treeViewStateLookup.Add(id, newState);
            return newState;
        }

        static void SaveTreeViewState(string id)
        {
            var state = GetTreeViewState(id);
            string json = JsonUtility.ToJson(state);
            EditorPrefs.SetString("treeState" + id, json);
        }

        #endregion

        #region tab

        static Dictionary<string, int> tabStateLookup;

        public static int GetTabState(string windowId, string fieldId)
        {
            string id = windowId + fieldId;

            if (tabStateLookup.ContainsKey(id))
            {
                return tabStateLookup[id];
            }

            int state = 0;

            if (EditorPrefs.HasKey(id))
            {
                state = EditorPrefs.GetInt(id);
            }

            tabStateLookup.Add(id, state);

            return state;
        }

        public static void SetTabState(string windowId, string fieldId, int state)
        {
            tabStateLookup[windowId + fieldId] = state;
        }

        static void SaveTabState()
        {
            foreach (var key in tabStateLookup.Keys)
            {
                EditorPrefs.SetInt(key, tabStateLookup[key]);
            }
        }

        #endregion

        #region foldout

        static Dictionary<string, bool> foldoutStateLookup;

        public static bool GetFoldoutState(string windowId, string fieldId)
        {
            string id = windowId + fieldId;

            if (foldoutStateLookup.ContainsKey(id))
            {
                return foldoutStateLookup[id];
            }

            bool state = true;

            if (EditorPrefs.HasKey(id))
            {
                state = EditorPrefs.GetBool(id);
            }

            foldoutStateLookup.Add(id, state);

            return state;
        }

        public static void SetFoldoutState(string windowId, string fieldId, bool state)
        {
            foldoutStateLookup[windowId + fieldId] = state;
        }

        static void SaveFoldoutState()
        {
            foreach (var key in foldoutStateLookup.Keys)
            {
                EditorPrefs.SetBool(key, foldoutStateLookup[key]);
            }
        }

        #endregion

        #region drag n drop

        static int draggedId = -1;

        public static void SetDraggedId(int id)
        {
            draggedId = id;
        }

        public static void ClearDraggedId()
        {
            draggedId = -1;
        }

        public static int GetDraggedId()
        {
            return draggedId;
        }

        #endregion

        #region tree element clipboard

        static List<object> treeElements;

        public static void SetTreeElementClipboard(List<object> treeElements)
        {
            VirtualTrainingEditorState.treeElements = treeElements;
        }

        public static List<T> GetTreeElementClipboard<T>() where T : TreeElement
        {
            List<T> newDatas = new List<T>();

            for (int i = 0; i < treeElements.Count; i++)
            {
                try
                {
                    newDatas.Add(VirtualTrainingEditorUtility.CloneObject<T>((T)treeElements[i]));
                }
                catch (System.Exception e)
                {
                    return new List<T>();
                }
            }

            return newDatas;
        }

        #endregion

        #region list field clipboard

        static List<object> listElements;

        public static void SetListFieldElementClipboard(List<object> elements)
        {
            listElements = elements;
        }

        public static List<T> GetListFieldElementClipboard<T>()
        {
            List<T> newDatas = new List<T>();

            for (int i = 0; i < listElements.Count; i++)
            {
                try
                {
                    newDatas.Add(VirtualTrainingEditorUtility.CloneObject<T>((T)listElements[i]));
                }
                catch (System.Exception e)
                {
                    return new List<T>();
                }
            }

            return newDatas;
        }

        #endregion
    }
}

#endif