﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VirtualTraining.Tools
{
    public static class VirtualTrainingEditorConfig
    {
        public const float DEFAULT_LABEL_FIELD_WIDTH = 150f;
        public const float DEFAULT_SIMPLE_FIELD_WIDTH = 300f;
        public const float DEFAULT_DROPDOWN_FIELD_WIDTH = 500f;

    }
}