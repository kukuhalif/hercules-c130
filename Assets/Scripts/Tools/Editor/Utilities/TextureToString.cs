﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

namespace VirtualTraining.Tools
{
    // enable read/write = true
    // compresion = none
    public class TextureToString
    {
        static string extension = ".png";

        [MenuItem("Assets/Texture/Encode to string", true)]
        static bool ValidateConvertPDFToAsset()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            return HasExtension(path);
        }

        [MenuItem("Assets/Texture/Encode to string")]
        public static void ConvertPDFToAsset()
        {
            for (int i = 0; i < Selection.assetGUIDs.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[i]);
                string newPath = ConvertToInternalPath(path);

                Texture2D image = AssetDatabase.LoadAssetAtPath(newPath, typeof(Texture2D)) as Texture2D;

                if (image != null)
                {
                    byte[] bytes;
                    bytes = image.EncodeToPNG();
                    string enc = Convert.ToBase64String(bytes);

                    string logPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/encoded " + image.name + ".txt";
                    Debug.Log(logPath);

                    StreamWriter writer = new StreamWriter(logPath, true);
                    writer.WriteLine(enc);
                    writer.Close();
                }
            }
        }

        public static bool HasExtension(string asset)
        {
            return asset.EndsWith(extension, StringComparison.OrdinalIgnoreCase);
        }

        public static string ConvertToInternalPath(string asset)
        {
            string left = asset.Substring(0, asset.Length - extension.Length);
            return left + extension;
        }
    }
}

#endif