﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class EditorGUISplitView
    {
        public enum Direction
        {
            Horizontal,
            Vertical
        }

        bool isInitialized;
        string splitterId = "splitterId";
        Direction splitDirection;
        float splitNormalizedPosition = 0.5f;
        float loadedSplitNormalizedPosition = 0.5f;
        bool resize;
        Vector2 scrollPosition;
        Vector2 secondaryScrollPosition;
        Rect availableRect;

        public EditorGUISplitView(Direction splitDirection, string splitterId)
        {
            isInitialized = false;
            this.splitDirection = splitDirection;
            this.splitterId += splitterId;
        }

        public void BeginSplitView(float position, float lineLength)
        {
            if (!isInitialized)
            {
                if (EditorPrefs.HasKey(splitterId))
                    splitNormalizedPosition = loadedSplitNormalizedPosition = EditorPrefs.GetFloat(splitterId);

                isInitialized = true;
            }

            Rect tempRect;

            if (splitDirection == Direction.Horizontal)
                tempRect = EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            else
                tempRect = EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true));

            if (tempRect.width > 0.0f)
            {
                availableRect = tempRect;

                if (splitDirection == Direction.Horizontal)
                {
                    availableRect.height = lineLength;
                    availableRect.y = position;
                }
                else
                {
                    availableRect.width = lineLength;
                    availableRect.x = position;
                }
            }
            if (splitDirection == Direction.Horizontal)
                scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(availableRect.width * splitNormalizedPosition));
            else
                scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Height(availableRect.height * splitNormalizedPosition));
        }

        public void Split()
        {
            GUILayout.EndScrollView();
            ResizeSplitFirstView();
            GUILayout.Space(5f);
            secondaryScrollPosition = GUILayout.BeginScrollView(secondaryScrollPosition);
        }

        public void EndSplitView()
        {
            if (splitDirection == Direction.Horizontal)
                EditorGUILayout.EndHorizontal();
            else
                EditorGUILayout.EndVertical();

            GUILayout.EndScrollView();
        }

        public float GetSplitPosition()
        {
            if (splitDirection == Direction.Horizontal)
                return availableRect.width * splitNormalizedPosition;

            return availableRect.height * splitNormalizedPosition;
        }

        private void ResizeSplitFirstView()
        {
            Rect resizeHandleRect;

            if (splitDirection == Direction.Horizontal)
                resizeHandleRect = new Rect(availableRect.width * splitNormalizedPosition, availableRect.y, 5f, availableRect.height);
            else
                resizeHandleRect = new Rect(availableRect.x, availableRect.height * splitNormalizedPosition, availableRect.width, 5f);

            GUI.DrawTexture(resizeHandleRect, EditorGUIUtility.whiteTexture);

            if (splitDirection == Direction.Horizontal)
                EditorGUIUtility.AddCursorRect(resizeHandleRect, MouseCursor.ResizeHorizontal);
            else
                EditorGUIUtility.AddCursorRect(resizeHandleRect, MouseCursor.ResizeVertical);

            if (Event.current.type == EventType.MouseDown && resizeHandleRect.Contains(Event.current.mousePosition))
            {
                resize = true;
            }
            if (resize)
            {
                if (splitDirection == Direction.Horizontal)
                    splitNormalizedPosition = Event.current.mousePosition.x / availableRect.width;
                else
                    splitNormalizedPosition = Event.current.mousePosition.y / availableRect.height;

                if (splitNormalizedPosition < 0)
                    splitNormalizedPosition = 0f;
                else if (splitNormalizedPosition > 1f)
                    splitNormalizedPosition = 1f;
            }
            if (resize && Event.current.type == EventType.MouseUp)
            {
                resize = false;
                if (loadedSplitNormalizedPosition != splitNormalizedPosition)
                {
                    loadedSplitNormalizedPosition = splitNormalizedPosition;
                    EditorPrefs.SetFloat(splitterId, splitNormalizedPosition);
                }
            }
        }
    }
}

#endif