﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class ModelSetup : EditorWindow
    {
        [MenuItem("Virtual Training/Model Setup")]
        static void OpenWindow()
        {
            var window = GetWindow<ModelSetup>();
            window.titleContent = new GUIContent("Model Setup");
        }

        [MenuItem("Virtual Training/Model Setup", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        static Action DONE_ACTION;

        GameObject root;
        PropertyField propertyField;

        class NewModel
        {
            public GameObjectType parent;
            public GameObject model;

            public NewModel(object obj) : this()
            {
                GameObject oo = obj as GameObject;
                if (oo != null)
                    model = oo;
            }

            public NewModel()
            {
                parent = new GameObjectType();
            }
        }

        List<NewModel> newModels = new List<NewModel>();

        private void OnEnable()
        {
            root = VirtualTrainingSceneManager.GameObjectRoot;
            propertyField = new PropertyField(null, "model setup");
            propertyField.CreateListView("model", null, null, OnModelElementDrawCallback, null, false, typeof(GameObject));
        }

        private object OnModelElementDrawCallback(string listId, object element, int index)
        {
            NewModel newModel = element as NewModel;

            propertyField.ShowAssetField("model", listId + index + "a", ref newModel.model);
            propertyField.ShowSimpleField("new model parent", listId + index + "b", ref newModel.parent);

            return newModel;
        }

        private void OnGUI()
        {
            GUILayout.Label("untuk parent object dibawah ini masukkan object yang ingin di setup saja / object yg baru dimasukkan, namun jika ingin nge setup semua object masukkan object root");
            root = EditorGUILayout.ObjectField("Parent object", root, typeof(GameObject), true) as GameObject;

            GUILayout.Space(10);

            GUILayout.Label("masukkan model (fbx / prefab) yang akan dimasukkan ke dalam object root");
            propertyField.ShowListField("model", newModels);

            GUILayout.Space(10);

            if (GUILayout.Button("Create new object root"))
            {
                root = new GameObject(VirtualTrainingSceneManager.OBJECT_ROOT_NAME);
            }

            if (GUILayout.Button("Clear object"))
            {
                if (EditorUtility.DisplayDialog("model setup", "clear all object root childs ?", "yes", "no"))
                {
                    List<GameObject> objTemp = new List<GameObject>();

                    for (int i = 0; i < root.transform.childCount; i++)
                    {
                        objTemp.Add(root.transform.GetChild(i).gameObject);
                    }

                    foreach (var obj in objTemp)
                    {
                        DestroyImmediate(obj);
                    }
                }
            }

            if (root == null)
                EditorGUI.BeginDisabledGroup(true);

            if (GUILayout.Button("Setup"))
            {
                AddModelToScene();
                StartSetup(root, this, null);
            }

            GUILayout.Label("yang dilakukan saat model setup adalah :");
            GUILayout.Label("1. add gameobject reference component pada setiap gameobject model, sehingga gameobject tersebut dapat di reference oleh database");
            GUILayout.Label("2. add collider component as trigger");
            GUILayout.Label("3. disable animator");
            GUILayout.Label("4. check duplicated gameobject id");

            if (root == null)
                EditorGUI.EndDisabledGroup();

            Repaint();
        }

        private void AddModelToScene()
        {
            Debug.Log("add model prefab to scene");

            for (int i = 0; i < newModels.Count; i++)
            {
                GameObject newInstance = Instantiate(newModels[i].model);
                newInstance.name = newModels[i].model.name;
                newInstance.transform.position = newModels[i].model.transform.position;
                newInstance.transform.localScale = newModels[i].model.transform.localScale;
                newInstance.transform.SetParent(newModels[i].parent.gameObject == null ? root.transform : newModels[i].parent.gameObject.transform, true);
            }
        }

        public static void StartSetup(GameObject root, object owner, Action doneAction)
        {
            Debug.Log("start setup for " + root.name);
            DONE_ACTION = doneAction;
            EditorUtility.DisplayProgressBar("model setup", "setup is working", 0f);
            EditorCoroutineUtility.StartCoroutine(Starting(root, owner), owner);
        }

        private static IEnumerator Starting(GameObject root, object owner)
        {
            yield return EditorCoroutineUtility.StartCoroutine(Setup(root), owner);
            yield return EditorCoroutineUtility.StartCoroutine(CheckDuplicatedId(), owner);
            DoneAction();
        }

        private static void DoneAction()
        {
            Debug.Log("set model scene dirty");
            VirtualTrainingEditorUtility.SetModelSceneDirty();
            EditorUtility.ClearProgressBar();
            if (DONE_ACTION != null)
            {
                Debug.Log("invoke done callback action");
                DONE_ACTION.Invoke();
            }
            Debug.Log("model setup done");
            EditorUtility.DisplayDialog("model setup", "setup is complete", "ok");
        }

        static IEnumerator Setup(GameObject root)
        {
            Debug.Log("get all childs");
            GameObject[] childs = root.GetAllChilds();

            int childCount = childs.Length;
            Debug.Log("total childs is " + childCount);

            GameObject obj = null;
            int counter = 0;

            for (int i = 0; i < childCount; i++)
            {
                obj = childs[i];

                float progress = (float)i / (float)childCount;

                EditorUtility.DisplayProgressBar("model setup", "add gameobject reference " + obj.name, progress);

                // 1. Gameobject reference
                GameObjectReference gameObjectReference = obj.GetComponent<GameObjectReference>();
                if (gameObjectReference == null)
                    gameObjectReference = obj.AddComponent<GameObjectReference>();

                // if id is empty then set with transform instance id
                if (gameObjectReference.Id == 0)
                    gameObjectReference.SetId(obj.transform.GetInstanceID());

                EditorUtility.DisplayProgressBar("model setup", "set object layer " + obj.name, progress);

                EditorUtility.DisplayProgressBar("model setup", "set collider " + obj.name, progress);

                // 2. Collider
                MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
                if (meshRenderer != null)
                {
                    Collider collider = obj.GetComponent<Collider>();
                    if (collider == null)
                    {
                        obj.AddComponent<MeshCollider>();
                    }
                }

                EditorUtility.DisplayProgressBar("model setup", "disable animator " + obj.name, progress);

                // 3. Disable Animator
                Animator animator = obj.GetComponent<Animator>();
                if (animator != null)
                {
                    animator.enabled = false;
                }

                EditorUtility.DisplayProgressBar("model setup", "initialize gameobject reference " + obj.name, progress);

                if (counter > 500)
                {
                    counter = 0;
                    yield return null;
                }
                counter++;
            }
        }

        static IEnumerator CheckDuplicatedId()
        {
            Debug.Log("check duplicated Id");
            Debug.Log("get all references");
            GameObjectReference[] references = VirtualTrainingSceneManager.GameObjectRoot.transform.GetAllComponentsInChilds<GameObjectReference>();

            int referenceCount = references.Length;
            Debug.Log("total references is " + referenceCount);

            int counter = 0;
            int invalidIdCounter = 0;

            Dictionary<int, GameObjectReference> objectReferenceLookup = new Dictionary<int, GameObjectReference>();

            for (int i = 0; i < referenceCount; i++)
            {
                float progress = (float)i / (float)referenceCount;

                EditorUtility.DisplayProgressBar("model setup", "check duplicated id " + references[i].gameObject.name, progress);

                int id = references[i].Id;

                // if id valid
                if (!objectReferenceLookup.ContainsKey(id))
                {
                    objectReferenceLookup.Add(id, references[i]);
                }
                else // if id is invalid
                {
                    // create new id
                    Debug.Log("create new id for " + references[i].gameObject.name);

                    int newId = 0;
                    string parentName = "";

                    do
                    {
                        var parents = references[i].transform.GetAllParents();
                        parentName = "";

                        for (int p = parents.Count - 1; p >= 0; p--)
                        {
                            parentName += parents[p].name + " / ";
                        }

                        newId = Animator.StringToHash(parentName + invalidIdCounter);

                        invalidIdCounter++;
                    } while (objectReferenceLookup.ContainsKey(newId));

                    Debug.Log("new id generated for " + parentName);

                    references[i].SetId(newId);
                }

                if (counter > 500)
                {
                    counter = 0;
                    yield return null;
                }
                counter++;
            }
        }
    }
}
#endif