﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class ShadowCastingEditor : EditorWindow
    {
        bool castShadow;
        bool receiveShadow;

        [MenuItem("Virtual Training/Shadow Casting")]
        private static void ShowWindow()
        {
            var window = GetWindow<ShadowCastingEditor>();
            window.titleContent = new GUIContent("Shadow");
        }

        private void OnGUI()
        {
            castShadow = EditorGUILayout.Toggle("cast shadow", castShadow);
            receiveShadow = EditorGUILayout.Toggle("receive shadow", receiveShadow);

            string names = "";

            if (Selection.gameObjects.Length == 0)
            {
                GUILayout.Label("please select target objects !");
                EditorGUI.BeginDisabledGroup(true);
            }
            else
                names = "on : ";

            var selection = Selection.gameObjects;

            for (int i = 0; i < selection.Length; i++)
            {
                if (i < selection.Length - 2)
                    names += selection[i].name + " , ";
                else if (i < selection.Length - 1)
                    names += selection[i].name + " and ";
                else
                    names += selection[i].name;
            }

            if (GUILayout.Button("setup shadow " + names))
                SetShadow();

            if (Selection.gameObjects.Length == 0)
            {
                EditorGUI.EndDisabledGroup();
            }

            Repaint();
        }

        private void SetShadow()
        {
            for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
                if (EditorUtility.DisplayDialog("Shadow", "Set Shadow casting on " + Selection.gameObjects[i].name + " ?", "yes", "no"))
                {
                    SetShadow(Selection.gameObjects[i]);
                }
            }
        }

        private void SetShadow(GameObject obj)
        {
            MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

            if (renderer != null)
            {
                Undo.RegisterCompleteObjectUndo(renderer, "disable cast and receive shadow " + obj.name);

                renderer.shadowCastingMode = castShadow ? UnityEngine.Rendering.ShadowCastingMode.On : UnityEngine.Rendering.ShadowCastingMode.Off;
                renderer.receiveShadows = receiveShadow;
            }

            for (int i = 0; i < obj.transform.childCount; i++)
            {
                SetShadow(obj.transform.GetChild(i).gameObject);
            }
        }
    }
}


#endif
