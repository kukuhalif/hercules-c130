﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using UnityEditor.IMGUI.Controls;

namespace VirtualTraining.Tools
{
    [Serializable]
    public class ModelTreeElement : TreeElement
    {
        public override string GetDuplicateJsonData()
        {
            return "";
        }

        public override void SetDuplicatedJsonData(string data)
        {

        }
    }

    public class ModelUpdaterTreeView : TreeViewWithTreeModel<ModelTreeElement>
    {
        public class CellRect
        {
            public Rect column1;
            public Rect column2;

            public CellRect(Rect column1, Rect column2)
            {
                this.column1 = column1;
                this.column2 = column2;
            }

            public Rect CombinedRect
            {
                get
                {
                    Rect combi = column1;
                    combi.width += column2.width;
                    return combi;
                }
            }
        }

        bool isOldObj;
        GameObject rootObj;
        bool isFirstBuildRow;
        float verticalPosition;
        Dictionary<int, Rect> linePositionLookup;
        Dictionary<int, CellRect> cellRectLookup;
        Func<int, string> pairDebugText;
        Action<bool, int> dragCallback;
        Action<bool, int> contextClickedCallback;
        SearchField searchField;
        bool onlyShowNotFound;
        Func<int, bool, bool> isHasPair;

        public ModelUpdaterTreeView(bool oldObj, bool onlyShowNotFound, TreeViewState state, TreeModel<ModelTreeElement> model, MultiColumnHeader multiColumnHeader, GameObject rootObj, Func<int, string> pairDebugText, Action<bool, int> dragCallback, Action<bool, int> contextClickedCallback, Func<int, bool, bool> isHasPair) : base(state, multiColumnHeader, model)
        {
            isOldObj = oldObj;
            this.rootObj = rootObj;
            isFirstBuildRow = true;
            showAlternatingRowBackgrounds = true;
            showBorder = true;
            linePositionLookup = new Dictionary<int, Rect>();
            cellRectLookup = new Dictionary<int, CellRect>();
            this.pairDebugText = pairDebugText;
            this.dragCallback = dragCallback;
            this.contextClickedCallback = contextClickedCallback;
            searchField = new SearchField();
            this.onlyShowNotFound = onlyShowNotFound;
            this.isHasPair = isHasPair;
        }

        public void OnlyShowNotFound(bool showNotFound)
        {
            onlyShowNotFound = showNotFound;
        }

        public void Search(Rect rect)
        {
            searchString = searchField.OnGUI(rect, searchString);
        }

        public void SetFirstBuildRow(bool firstBuild)
        {
            isFirstBuildRow = firstBuild;
        }

        public Rect GetLineRect(int id)
        {
            if (linePositionLookup.ContainsKey(id))
            {
                Rect rect = linePositionLookup[id];
                rect.y -= verticalPosition;
                return rect;
            }

            return new Rect();
        }

        public Rect GetCellRect(int id)
        {
            if (cellRectLookup.ContainsKey(id))
            {
                Rect rect = cellRectLookup[id].CombinedRect;
                rect.y -= verticalPosition;
                return rect;
            }

            return new Rect();
        }

        public float VerticalPosition
        {
            get => verticalPosition;
            set => verticalPosition = value;
        }

        protected override void ContextClicked()
        {

        }

        protected override void ContextClickedItem(int id)
        {
            contextClickedCallback.Invoke(isOldObj, id);
        }

        protected override void SetupDragAndDrop(SetupDragAndDropArgs args)
        {
            var ids = args.draggedItemIDs;
            if (ids.Count != 1)
                return;

            dragCallback.Invoke(isOldObj, ids[0]);
        }

        protected override DragAndDropVisualMode HandleDragAndDrop(DragAndDropArgs args)
        {
            return DragAndDropVisualMode.Link;
        }

        protected override bool CanMultiSelect(TreeViewItem item)
        {
            return false;
        }

        protected override void SelectionChanged(IList<int> selectedIds)
        {

        }

        protected override bool CanStartDrag(CanStartDragArgs args)
        {
            return true;
        }

        protected override bool CanBeParent(TreeViewItem item)
        {
            return false;
        }

        protected override bool CanRename(TreeViewItem item)
        {
            return false;
        }

        protected override IList<TreeViewItem> BuildRows(TreeViewItem root)
        {
            var rows = GetRows() ?? new List<TreeViewItem>(0);

            rows.Clear();
            if (rootObj != null)
            {
                var item = CreateTreeViewItemForGameObject(rootObj);
                root.AddChild(item);

                if (hasSearch && item.displayName.ToLower().Contains(searchString.ToLower()))
                    rows.Add(item);
                else if (!hasSearch)
                    rows.Add(item);

                if (rootObj != null && rootObj.transform.childCount > 0)
                {
                    if (IsExpanded(item.id))
                    {
                        AddChildrenRecursive(rootObj, item, rows);
                    }
                    else
                    {
                        item.children = CreateChildListForCollapsedParent();
                    }
                }
            }

            SetupDepthsFromParentsAndChildren(root);

            if (onlyShowNotFound)
            {
                List<TreeViewItem> removeds = new List<TreeViewItem>();
                foreach (var row in rows)
                {
                    if (isHasPair(row.id, isOldObj))
                    {
                        removeds.Add(row);
                    }
                }
                foreach (var rem in removeds)
                {
                    rows.Remove(rem);
                }
            }

            return rows;
        }

        private TreeViewItem CreateTreeViewItemForGameObject(GameObject gameObject)
        {
            // We can use the GameObject instanceID for TreeViewItem id, as it ensured to be unique among other items in the tree.
            // To optimize reload time we could delay fetching the transform.name until it used for rendering (prevents allocating strings 
            // for items not rendered in large trees)
            // We just set depth to -1 here and then call SetupDepthsFromParentsAndChildren at the end of BuildRootAndRows to set the depths.

            int newId = 0;
            string name = "";

            if (gameObject != null)
            {
                newId = gameObject.GetInstanceID();
                name = gameObject.name;
            }

            // expand all hack
            if (isFirstBuildRow)
            {
                var oldExpanded = GetExpanded();
                var expanded = new List<int>(oldExpanded);
                expanded.Add(newId);
                SetExpanded(expanded);
            }

            return new TreeViewItem(newId, -1, name);
        }

        private void AddChildrenRecursive(GameObject go, TreeViewItem item, IList<TreeViewItem> rows)
        {
            int childCount = go.transform.childCount;

            item.children = new List<TreeViewItem>(childCount);
            for (int i = 0; i < childCount; ++i)
            {
                var childTransform = go.transform.GetChild(i);
                var childItem = CreateTreeViewItemForGameObject(childTransform.gameObject);
                item.AddChild(childItem);

                if (hasSearch && childItem.displayName.ToLower().Contains(searchString.ToLower()))
                    rows.Add(childItem);
                else if (!hasSearch)
                    rows.Add(childItem);

                if (childTransform.childCount > 0)
                {
                    if (IsExpanded(childItem.id))
                    {
                        AddChildrenRecursive(childTransform.gameObject, childItem, rows);
                    }
                    else
                    {
                        childItem.children = CreateChildListForCollapsedParent();
                    }
                }
            }
        }

        protected override void RowGUI(RowGUIArgs args)
        {
            var item = args.item;

            for (int i = 0; i < args.GetNumVisibleColumns(); ++i)
            {
                CellGUI(args.GetCellRect(i), item, args.GetColumn(i), ref args);
            }
        }

        void CellGUI(Rect cellRect, TreeViewItem item, int column, ref RowGUIArgs args)
        {
            // Center cell rect vertically (makes it easier to place controls, icons etc in the cells)
            CenterRectUsingSingleLineHeight(ref cellRect);

            if (column == 0)
            {
                if (cellRectLookup.ContainsKey(item.id))
                    cellRectLookup[item.id].column1 = cellRect;
                else
                    cellRectLookup.Add(item.id, new CellRect(cellRect, new Rect()));
            }
            else
            {
                if (cellRectLookup.ContainsKey(item.id))
                    cellRectLookup[item.id].column2 = cellRect;
                else
                    cellRectLookup.Add(item.id, new CellRect(new Rect(), cellRect));
            }

            cellRect.x += GetContentIndent(item);

            switch (column)
            {
                case 0:
                    {
                        EditorGUI.LabelField(cellRect, item.displayName);

                        if (!isOldObj)
                        {
                            // set line rect
                            cellRect.y += EditorGUIUtility.singleLineHeight / 3f;
                            cellRect.x -= foldoutWidth * 0.5f;

                            // set line position
                            if (linePositionLookup.ContainsKey(item.id))
                                linePositionLookup[item.id] = cellRect;
                            else
                                linePositionLookup.Add(item.id, cellRect);
                        }
                    }
                    break;
                case 1:
                    {
                        // debug text rect
                        cellRect.width = 100f;

                        // debug text
                        EditorGUI.LabelField(cellRect, pairDebugText(item.id));

                        // line position
                        cellRect.x += 100f;

                        if (isOldObj)
                        {
                            // set line rect
                            cellRect.y += EditorGUIUtility.singleLineHeight / 3f;

                            // set line position
                            if (linePositionLookup.ContainsKey(item.id))
                                linePositionLookup[item.id] = cellRect;
                            else
                                linePositionLookup.Add(item.id, cellRect);
                        }
                    }
                    break;
            }
        }
    }
}

#endif