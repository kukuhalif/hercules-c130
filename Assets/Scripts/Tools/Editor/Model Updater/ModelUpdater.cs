﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VirtualTraining.Core;
using UnityEditor.IMGUI.Controls;
using Unity.EditorCoroutines.Editor;

namespace VirtualTraining.Tools
{
    public class ModelUpdater : EditorWindow
    {
        [MenuItem("Virtual Training/Model Updater &U")]
        static void OpenWindow()
        {
            var window = GetWindow<ModelUpdater>();
            window.titleContent = new GUIContent("Model Updater");
        }

        [MenuItem("Virtual Training/Model Updater &U", true)]
        static bool OpenWindowValidate()
        {
            return !Application.isPlaying;
        }

        class ObjectPair
        {
            public GameObject oldObj;
            public GameObject newObj;
            public int oldId;
            public int newId;
            public bool defaultConnection;
            public Color lineColor
            {
                get
                {
                    if (defaultConnection)
                        return Color.green;
                    else return Color.red;
                }
            }

            public ObjectPair(GameObject oldObj, GameObject newObj, bool defaultConnection)
            {
                this.oldObj = oldObj;
                this.newObj = newObj;
                oldId = oldObj.GetInstanceID();
                newId = newObj.GetInstanceID();
                this.defaultConnection = defaultConnection;
            }
        }

        MultiColumnHeaderState _oldHeaderState, _newHeaderState;
        TreeViewState _oldTreeViewState, _newTreeViewState;
        ModelUpdaterTreeView _oldTreeView, _newTreeView;
        GameObject _oldObj, _newObj, _instantiatedNewObject;
        List<ObjectPair> _objectPairs = new List<ObjectPair>();
        List<int> _availableOldTree = new List<int>();
        List<int> _availableNewTree = new List<int>();
        bool _showTree = true;
        bool _showLine = true;
        bool _onlyShowNotFound = false;
        bool _isSearching;
        bool _isSearchOld;
        int _searchId;
        Rect _searchRectDebug;
        float _treeViewVerticalPos = 140f;
        float _lineVerticalOffset = 30f;

        bool _isWorking;

        private void OnEnable()
        {
            _isWorking = false;
            ReloadTree();
        }

        private MultiColumnHeaderState NewMultiHeaderState()
        {
            MultiColumnHeaderState.Column column1 = new MultiColumnHeaderState.Column();

            column1.allowToggleVisibility = false;
            column1.autoResize = true;
            column1.canSort = false;
            column1.headerContent = new GUIContent("object");
            column1.width = position.width / 4f;

            MultiColumnHeaderState.Column column2 = new MultiColumnHeaderState.Column();

            column2.allowToggleVisibility = false;
            column2.autoResize = true;
            column2.canSort = false;
            column2.headerContent = new GUIContent("action");
            column2.width = position.width / 4f;

            MultiColumnHeaderState newState = new MultiColumnHeaderState(new MultiColumnHeaderState.Column[] { column1, column2 });
            return newState;
        }

        private void ReloadTree()
        {
            _oldTreeViewState = new TreeViewState();
            var oldTreeModel = new TreeModel<ModelTreeElement>(CreateNewTreeModel());
            _oldHeaderState = NewMultiHeaderState();
            var oldMultiHeader = new MultiColumnHeader(_oldHeaderState);
            _oldTreeView = new ModelUpdaterTreeView(true, _onlyShowNotFound, _oldTreeViewState, oldTreeModel, oldMultiHeader, _oldObj, OldTreeDebugPair, DragCallback, ContextClickedCallback, IsHasPair);
            _oldTreeView.Reload();
            _oldTreeView.SetFirstBuildRow(false);

            _newTreeViewState = new TreeViewState();
            var newTreeModel = new TreeModel<ModelTreeElement>(CreateNewTreeModel());
            _newHeaderState = NewMultiHeaderState();
            var newMultiHeader = new MultiColumnHeader(_newHeaderState);
            _newTreeView = new ModelUpdaterTreeView(false, _onlyShowNotFound, _newTreeViewState, newTreeModel, newMultiHeader, _newObj, NewTreeDebugPair, DragCallback, ContextClickedCallback, IsHasPair);
            _newTreeView.Reload();
            _newTreeView.SetFirstBuildRow(false);

            _availableOldTree.Clear();
            _availableNewTree.Clear();

            if (_oldObj != null && _newObj != null)
            {
                SearchObjectPair();
                SearchAvailableObject(true, _oldObj);
                SearchAvailableObject(false, _newObj);
            }
        }

        private bool IsHasPair(int id, bool old)
        {
            foreach (var item in _objectPairs)
            {
                if (old && id == item.oldId)
                    return true;

                if (!old && id == item.newId)
                    return true;
            }
            return false;
        }

        private bool IsPaired(bool old, int id)
        {
            bool found = false;
            foreach (var pair in _objectPairs)
            {
                if (old && pair.oldId == id)
                {
                    found = true;
                    break;
                }
                if (!old && pair.newId == id)
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        private void ContextClickedCallback(bool old, int id)
        {
            if (_oldObj != null && _newObj != null)
            {
                if (!IsPaired(old, id))
                    return;

                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("detach connection"), false, () =>
                 {
                     int removeIndex = -1;
                     for (int i = 0; i < _objectPairs.Count; i++)
                     {
                         if (old && _objectPairs[i].oldId == id)
                         {
                             removeIndex = i;
                             break;
                         }
                         if (!old && _objectPairs[i].newId == id)
                         {
                             removeIndex = i;
                             break;
                         }
                     }

                     if (removeIndex != -1)
                     {
                         var pair = _objectPairs[removeIndex];
                         _availableOldTree.Add(pair.oldId);
                         _availableNewTree.Add(pair.newId);
                         _objectPairs.RemoveAt(removeIndex);
                     }
                 });

                bool searchFromParent = false;
                GameObject oldObj = null, newObj = null;

                foreach (var pair in _objectPairs)
                {
                    if (old && pair.oldId == id)
                    {
                        searchFromParent = true;
                        oldObj = pair.oldObj;
                        newObj = pair.newObj;
                        break;
                    }

                    if (!old && pair.newId == id)
                    {
                        searchFromParent = true;
                        oldObj = pair.oldObj;
                        newObj = pair.newObj;
                        break;
                    }
                }

                if (searchFromParent)
                {
                    menu.AddItem(new GUIContent("search child pair from this parent"), false, () =>
                    {
                        SearchPairFromParent(oldObj, newObj);
                    });
                }

                menu.ShowAsContext();
            }
        }

        private void DragCallback(bool old, int id)
        {
            if (_oldObj != null && _newObj != null)
            {
                if (IsPaired(old, id))
                    return;

                if (old)
                    _searchRectDebug = _oldTreeView.GetCellRect(id);
                else
                    _searchRectDebug = _newTreeView.GetCellRect(id);

                if (old)
                    _searchRectDebug.position += new Vector2(10, _treeViewVerticalPos + _lineVerticalOffset);
                else
                    _searchRectDebug.position += new Vector2(position.width / 2f, _treeViewVerticalPos + _lineVerticalOffset);

                _searchRectDebug.width = 10f;

                StartSearchPair(old, id);
                DragAndDrop.StartDrag("search pair");
            }
        }

        private List<Rect> AvailableRects()
        {
            List<int> ids = _isSearchOld ? _availableNewTree : _availableOldTree;
            List<Rect> rects = new List<Rect>();

            for (int i = 0; i < ids.Count; i++)
            {
                Rect cellRect = new Rect();
                if (_isSearchOld)
                    cellRect = _newTreeView.GetCellRect(ids[i]);
                else
                    cellRect = _oldTreeView.GetCellRect(ids[i]);

                if (_isSearchOld)
                    cellRect.position += new Vector2(position.width / 2f, _treeViewVerticalPos + _lineVerticalOffset);
                else
                    cellRect.position += new Vector2(10, _treeViewVerticalPos + _lineVerticalOffset);

                cellRect.width -= 5f;

                rects.Add(cellRect);
            }

            return rects;
        }

        private void StartSearchPair(bool old, int id)
        {
            _isSearching = true;
            _isSearchOld = old;
            _searchId = id;
        }

        private string OldTreeDebugPair(int id)
        {
            for (int i = 0; i < _objectPairs.Count; i++)
            {
                if (id == _objectPairs[i].oldId)
                {
                    if (_objectPairs[i].oldObj != null)
                        return "will be replaced";

                    return "";
                }
            }
            return "will be deleted";
        }

        private string NewTreeDebugPair(int id)
        {
            for (int i = 0; i < _objectPairs.Count; i++)
            {
                if (id == _objectPairs[i].newId)
                {
                    if (_objectPairs[i].newObj != null)
                        return "will replace";

                    return "";
                }
            }
            return "will be added";
        }

        private ModelTreeElement CreateHiddenRoot()
        {
            var root = new ModelTreeElement();
            root.depth = -1;
            root.name = "Root";
            root.id = 0;
            return root;
        }

        private IList<ModelTreeElement> CreateNewTreeModel()
        {
            var data = new List<ModelTreeElement>();
            data.Add(CreateHiddenRoot());
            return data;
        }

        private void SearchAvailableObject(bool old, GameObject obj)
        {
            bool found = false;
            for (int i = 0; i < _objectPairs.Count; i++)
            {
                if (old && _objectPairs[i].oldObj == obj)
                {
                    found = true;
                    break;
                }
                if (!old && _objectPairs[i].newObj == obj)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                if (old)
                    _availableOldTree.Add(obj.GetInstanceID());
                else
                    _availableNewTree.Add(obj.GetInstanceID());
            }

            for (int i = 0; i < obj.transform.childCount; i++)
            {
                SearchAvailableObject(old, obj.transform.GetChild(i).gameObject);
            }
        }

        private void SearchObjectPair()
        {
            Debug.Log("start search object pair");
            _objectPairs.Clear();
            SearchObjectPair(_newObj);
            Debug.Log("total pair is " + _objectPairs.Count);
        }

        private void SearchObjectPair(GameObject newObj)
        {
            GameObject oldObj = FindPair(newObj);
            if (oldObj != null)
            {
                bool oldFound = false;
                foreach (var pair in _objectPairs)
                {
                    if (pair.oldObj == oldObj)
                    {
                        oldFound = true;
                        break;
                    }
                }
                bool newFound = false;
                foreach (var pair in _objectPairs)
                {
                    if (pair.newObj == newObj)
                    {
                        newFound = true;
                        break;
                    }
                }
                if (!oldFound && !newFound)
                    _objectPairs.Add(new ObjectPair(oldObj, newObj, true));
            }

            for (int i = 0; i < newObj.transform.childCount; i++)
            {
                SearchObjectPair(newObj.transform.GetChild(i).gameObject);
            }
        }

        private void SearchGameobject(GameObject obj, int id, ref GameObject result)
        {
            if (obj.GetInstanceID() == id)
                result = obj;

            if (result == null)
                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    SearchGameobject(obj.transform.GetChild(i).gameObject, id, ref result);
                }
        }

        private void SearchGameobject(GameObject obj, GameObject reference, ref GameObject result, Transform objRoot, Transform referenceRoot)
        {
            if (IsPairing(obj, reference, objRoot, referenceRoot, false))
                result = obj;

            if (result == null)
                for (int i = 0; i < obj.transform.childCount; i++)
                {
                    SearchGameobject(obj.transform.GetChild(i).gameObject, reference, ref result, objRoot, referenceRoot);
                }
        }

        private GameObject FindPair(GameObject newObj)
        {
            GameObject pair = null;
            FindPair(newObj, _oldObj, ref pair);
            return pair;
        }

        private void FindPair(GameObject newObj, GameObject oldObj, ref GameObject pair)
        {
            if (IsPairing(newObj, oldObj, _newObj.transform, _oldObj.transform, false))
            {
                pair = oldObj;
            }

            if (pair == null)
            {
                for (int i = 0; i < oldObj.transform.childCount; i++)
                {
                    FindPair(newObj, oldObj.transform.GetChild(i).gameObject, ref pair);
                }
            }
        }

        private bool IsPairing(GameObject a, GameObject b, Transform aRoot, Transform bRoot, bool removeRootFromComparison)
        {
            if (a.name != b.name)
                return false;

            var aAncestors = a.transform.GetAllParents(aRoot);
            var bAncestors = b.transform.GetAllParents(bRoot);

            if (removeRootFromComparison)
            {
                aAncestors.Remove(aRoot);
                bAncestors.Remove(bRoot);
            }

            if (aAncestors.Count != bAncestors.Count)
                return false;

            for (int i = 0; i < aAncestors.Count; i++)
            {
                if (aAncestors[i].name != bAncestors[i].name)
                    return false;
            }

            return true;
        }

        private IEnumerator ReplaceObject()
        {
            Debug.Log("start replace object");
            Debug.Log("instantiate new object");
            EditorUtility.DisplayProgressBar("model updater", "instantiate new object", 0f);

            string newObjName = _newObj.name;
            Transform parent = _oldObj.transform.parent;
            _instantiatedNewObject = Instantiate(_newObj);
            _instantiatedNewObject.transform.SetParent(parent, true);
            _instantiatedNewObject.name = newObjName;

            Debug.Log("start looping for " + _objectPairs.Count + " pairs");

            int counter = 0;

            for (int i = 0; i < _objectPairs.Count; i++)
            {
                GameObject instantiatedNewObj = null;
                SearchGameobject(_instantiatedNewObject, _objectPairs[i].newObj, ref instantiatedNewObj, _instantiatedNewObject.transform, _newObj.transform);

                foreach (var component in _objectPairs[i].oldObj.GetComponents<Component>())
                {
                    if (component != null)
                    {
                        float progress = (float)i / (float)_objectPairs.Count;

                        if (instantiatedNewObj != null)
                        {
                            EditorUtility.DisplayProgressBar("model updater", "copy " + component.name, progress);

                            UnityEditorInternal.ComponentUtility.CopyComponent(component);
                            var temps = instantiatedNewObj.GetComponents<Component>();

                            bool alreadyExist = false;

                            foreach (var temp in temps)
                            {
                                Type a = temp.GetType();
                                Type b = component.GetType();
                                if (a == b)
                                {
                                    UnityEditorInternal.ComponentUtility.PasteComponentValues(component);
                                    alreadyExist = true;
                                }
                            }

                            if (!alreadyExist)
                                UnityEditorInternal.ComponentUtility.PasteComponentAsNew(instantiatedNewObj);
                        }
                    }
                    else
                        Debug.LogError(_objectPairs[i].newObj.name + " is null");

                    if (counter > 500)
                    {
                        counter = 0;
                        yield return null;
                    }
                    counter++;
                }
            }

            Debug.Log("destroy old object");
            EditorUtility.DisplayProgressBar("model updater", "destroy old object", 1f);

            DestroyImmediate(_oldObj);

            Debug.Log("set scene dirty");
            VirtualTrainingEditorUtility.SetModelSceneDirty();
            EditorUtility.ClearProgressBar();

            Debug.Log("start model setup for " + _instantiatedNewObject.name);
            ModelSetup.StartSetup(_instantiatedNewObject, this, DoneUpdateAction);
        }

        private void DoneUpdateAction()
        {
            _isWorking = false;

            _newObj = _oldObj = null;
            _objectPairs.Clear();
            _availableNewTree.Clear();
            _availableOldTree.Clear();

            ReloadTree();
        }

        private Rect OldTreeViewRect(float treeViewWidth)
        {
            return new Rect(20, _treeViewVerticalPos, treeViewWidth, position.height - _treeViewVerticalPos - 10f);
        }

        private Rect NewTreeViewRect(float treeViewWidth)
        {
            return new Rect(position.width / 2f, _treeViewVerticalPos, treeViewWidth, position.height - _treeViewVerticalPos - 10f);
        }

        private void SearchPairFromParent(GameObject oldParent, GameObject newParent)
        {
            Transform[] oldChilds = oldParent.transform.GetAllChilds();
            Transform[] newChilds = newParent.transform.GetAllChilds();

            foreach (var oldChild in oldChilds)
            {
                foreach (var newChild in newChilds)
                {
                    if (oldChild.name == newChild.name)
                    {
                        bool isPairing = IsPairing(oldChild.gameObject, newChild.gameObject, oldParent.transform, newParent.transform, true);

                        if (isPairing)
                        {
                            bool oldFound = false;
                            foreach (var pair in _objectPairs)
                            {
                                if (pair.oldObj == oldChild.gameObject)
                                {
                                    oldFound = true;
                                    break;
                                }
                            }
                            bool newFound = false;
                            foreach (var pair in _objectPairs)
                            {
                                if (pair.newObj == newChild.gameObject)
                                {
                                    newFound = true;
                                    break;
                                }
                            }

                            if (!oldFound && !newFound)
                            {
                                _objectPairs.Add(new ObjectPair(oldChild.gameObject, newChild.gameObject, false));
                                _availableOldTree.Remove(oldChild.gameObject.GetInstanceID());
                                _availableNewTree.Remove(newChild.gameObject.GetInstanceID());
                            }
                        }
                    }
                }
            }
        }

        private void SearchNotFoundElementByName()
        {
            // get childs and itself
            List<Transform> oldElements = new List<Transform>(_oldObj.transform.GetAllChilds());
            oldElements.Insert(0, _oldObj.transform);
            List<Transform> newElements = new List<Transform>(_newObj.transform.GetAllChilds());
            newElements.Insert(0, _newObj.transform);

            // get used element
            List<GameObject> usedOldObjs = new List<GameObject>();
            List<GameObject> usedNewObjs = new List<GameObject>();
            foreach (var pair in _objectPairs)
            {
                usedOldObjs.Add(pair.oldObj);
                usedNewObjs.Add(pair.newObj);
            }

            // search not found old element
            foreach (var oldElement in oldElements)
            {
                bool found = false;
                foreach (var pair in _objectPairs)
                {
                    if (oldElement.gameObject.GetInstanceID() == pair.oldId)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    foreach (var newElement in newElements)
                    {
                        if (oldElement.gameObject.name == newElement.gameObject.name && !usedNewObjs.Contains(newElement.gameObject))
                        {
                            ObjectPair pair = new ObjectPair(oldElement.gameObject, newElement.gameObject, false);
                            _objectPairs.Add(pair);
                            _availableOldTree.Remove(pair.oldId);
                            _availableNewTree.Remove(pair.newId);
                            usedNewObjs.Add(newElement.gameObject);
                            break;
                        }
                    }
                }
            }

            // search not found new element
            foreach (var newElement in newElements)
            {
                bool found = false;
                foreach (var pair in _objectPairs)
                {
                    if (newElement.gameObject.GetInstanceID() == pair.newId)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    foreach (var oldElement in oldElements)
                    {
                        if (oldElement.gameObject.name == newElement.gameObject.name && !usedOldObjs.Contains(oldElement.gameObject))
                        {
                            ObjectPair pair = new ObjectPair(oldElement.gameObject, newElement.gameObject, false);
                            _objectPairs.Add(pair);
                            _availableOldTree.Remove(pair.oldId);
                            _availableNewTree.Remove(pair.newId);
                            usedOldObjs.Add(oldElement.gameObject);
                            break;
                        }
                    }
                }
            }
        }

        private void LabelAndSearch(Rect oldTreeRect, Rect newTreeRect)
        {
            Rect oldLabelRect = new Rect(10, _treeViewVerticalPos - 20, 100, 20);
            GUI.Label(oldLabelRect, "Old Object");
            Rect oldSearchRect = oldLabelRect;
            oldSearchRect.x += 100;
            oldSearchRect.width = oldTreeRect.width - 100;
            _oldTreeView.Search(oldSearchRect);

            Rect newLabelRect = new Rect(position.width / 2f, _treeViewVerticalPos - 20, 100, 20);
            GUI.Label(newLabelRect, "New Object");
            Rect newSearchRect = newLabelRect;
            newSearchRect.x += 100f;
            newSearchRect.width = newTreeRect.width - 100;
            _newTreeView.Search(newSearchRect);
        }

        private void OnGUI()
        {
            if (_isWorking)
            {
                GUILayout.Label("now loading ...");
                return;
            }

            GameObject newOld = (GameObject)EditorGUILayout.ObjectField("old object", _oldObj, typeof(GameObject), true);
            GameObject newNew = (GameObject)EditorGUILayout.ObjectField("new object", _newObj, typeof(GameObject), true);

            if (newOld != _oldObj || newNew != _newObj)
            {
                _oldObj = newOld;
                _newObj = newNew;
                ReloadTree();
            }

            if (_oldObj == null || _newObj == null)
                EditorGUI.BeginDisabledGroup(true);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("reset connection"))
            {
                if (EditorUtility.DisplayDialog("replace object", "reset connection ?", "yes", "no"))
                {
                    ReloadTree();
                }
            }

            if (GUILayout.Button("search not found element by name"))
            {
                if (EditorUtility.DisplayDialog("replace object", "search not found element by name ?", "yes", "no"))
                {
                    SearchNotFoundElementByName();
                }
            }

            if (GUILayout.Button("search child connection from custom connection (red)"))
            {
                if (EditorUtility.DisplayDialog("replace object", "search child connection ?", "yes", "no"))
                {
                    List<ObjectPair> nonDefaultPairs = new List<ObjectPair>();
                    foreach (var pair in _objectPairs)
                    {
                        if (!pair.defaultConnection)
                            nonDefaultPairs.Add(pair);
                    }
                    foreach (var pair in nonDefaultPairs)
                    {
                        SearchPairFromParent(pair.oldObj, pair.newObj);
                    }
                }
            }

            if (GUILayout.Button("replace object"))
            {
                if (EditorUtility.DisplayDialog("replace object", "replace old object with new object ?", "yes", "no"))
                {
                    _isWorking = true;
                    EditorCoroutineUtility.StartCoroutine(ReplaceObject(), this);
                }
            }

            GUILayout.EndHorizontal();

            _showTree = GUILayout.Toggle(_showTree, "show tree");
            _showLine = GUILayout.Toggle(_showLine, "show pair line");
            bool only = GUILayout.Toggle(_onlyShowNotFound, "only show not found");
            if (only != _onlyShowNotFound)
            {
                _onlyShowNotFound = only;
                _oldTreeView.OnlyShowNotFound(_onlyShowNotFound);
                _newTreeView.OnlyShowNotFound(_onlyShowNotFound);
                _oldTreeView.Reload();
                _newTreeView.Reload();
            }

            float treeViewWidth = position.width / 2f - 20f;

            if (_oldObj == null || _newObj == null)
                EditorGUI.EndDisabledGroup();

            Rect oldTreeRect = OldTreeViewRect(treeViewWidth);
            Rect newTreeRect = NewTreeViewRect(treeViewWidth);

            LabelAndSearch(oldTreeRect, newTreeRect);

            if (_showTree)
            {
                if (_oldTreeView != null)
                {
                    Rect sliderRect = oldTreeRect;
                    sliderRect.x = 5;
                    sliderRect.width = 10;

                    int bottom = _oldTreeView.GetRows().Count * (int)EditorGUIUtility.singleLineHeight - (int)oldTreeRect.height;
                    _oldTreeView.VerticalPosition = GUI.VerticalSlider(sliderRect, _oldTreeView.VerticalPosition, 0, bottom);

                    GUILayout.BeginArea(oldTreeRect);

                    oldTreeRect.height = _oldTreeView.totalHeight + EditorGUIUtility.singleLineHeight;
                    oldTreeRect.x = 0;
                    oldTreeRect.y = -_oldTreeView.VerticalPosition;

                    _oldTreeView.OnGUI(oldTreeRect);

                    GUILayout.EndArea();
                }

                if (_newTreeView != null)
                {
                    Rect sliderRect = newTreeRect;
                    sliderRect.x = position.width - 15f;
                    sliderRect.width = 10;

                    int bottom = _newTreeView.GetRows().Count * (int)EditorGUIUtility.singleLineHeight - (int)newTreeRect.height;
                    _newTreeView.VerticalPosition = GUI.VerticalSlider(sliderRect, _newTreeView.VerticalPosition, 0, bottom);

                    GUILayout.BeginArea(newTreeRect);

                    newTreeRect.height = _newTreeView.totalHeight + EditorGUIUtility.singleLineHeight;
                    newTreeRect.y = -_newTreeView.VerticalPosition;
                    newTreeRect.x = 0;

                    _newTreeView.OnGUI(newTreeRect);

                    GUILayout.EndArea();
                }
            }

            if (_isSearching)
            {
                if (Event.current.type == EventType.DragExited)
                    _isSearching = false;

                EditorGUI.DrawRect(_searchRectDebug, Color.red);

                var mousePos = Event.current.mousePosition;
                var availableRects = AvailableRects();
                for (int i = 0; i < availableRects.Count; i++)
                {
                    if (availableRects[i].Contains(mousePos))
                    {
                        Rect cell = availableRects[i];
                        cell.width = 10f;
                        EditorGUI.DrawRect(cell, Color.green);

                        if (Event.current.type == EventType.DragExited)
                        {
                            GameObject oldObj = null;
                            GameObject newObj = null;

                            if (_isSearchOld)
                            {
                                SearchGameobject(_oldObj, _searchId, ref oldObj);
                                SearchGameobject(_newObj, _availableNewTree[i], ref newObj);
                            }
                            else
                            {
                                SearchGameobject(_oldObj, _availableOldTree[i], ref oldObj);
                                SearchGameobject(_newObj, _searchId, ref newObj);
                            }

                            _objectPairs.Add(new ObjectPair(oldObj, newObj, false));
                            _availableOldTree.Remove(oldObj.GetInstanceID());
                            _availableNewTree.Remove(newObj.GetInstanceID());

                            _isSearching = false;

                            break;
                        }
                    }
                    else
                    {
                        Rect cell = availableRects[i];
                        cell.width = 10f;
                        EditorGUI.DrawRect(cell, Color.cyan);
                    }
                }
            }

            if (_showLine)
            {
                Handles.BeginGUI();

                Rect oldTreeLineRect = OldTreeViewRect(treeViewWidth);
                Rect newTreeLineRect = NewTreeViewRect(treeViewWidth);

                foreach (var pair in _objectPairs)
                {
                    var oldElement = _oldTreeView.GetLineRect(pair.oldId);
                    var newElement = _newTreeView.GetLineRect(pair.newId);

                    oldElement.position += new Vector2(15, _treeViewVerticalPos + _lineVerticalOffset);
                    newElement.position += new Vector2((position.width / 2f) + 5, _treeViewVerticalPos + _lineVerticalOffset);

                    var oldRows = _oldTreeView.GetRows();
                    var newRows = _newTreeView.GetRows();

                    bool oldFound = false;
                    bool newFound = false;

                    foreach (var oldItem in oldRows)
                    {
                        if (oldItem.id == pair.oldId)
                        {
                            oldFound = true;
                            break;
                        }
                    }

                    foreach (var newItem in newRows)
                    {
                        if (newItem.id == pair.newId)
                        {
                            newFound = true;
                            break;
                        }
                    }

                    if (oldFound && newFound)
                    {
                        Handles.color = pair.lineColor;

                        Vector2 oldPos = oldElement.position;
                        Vector2 newPos = newElement.position;

                        if (oldTreeLineRect.Contains(oldPos) && newTreeLineRect.Contains(newPos))
                        {
                            Handles.DrawLine(oldPos, newPos);
                        }
                        else
                        {
                            if (oldTreeLineRect.Contains(oldElement.position))
                            {
                                if (newPos.y < _treeViewVerticalPos)
                                {
                                    Handles.DrawLine(oldPos, new Vector2(position.width / 2f, _treeViewVerticalPos));
                                }
                                else if (newPos.y > newTreeLineRect.height + _treeViewVerticalPos)
                                {
                                    Handles.DrawLine(oldPos, new Vector2(position.width / 2f, newTreeLineRect.height + _treeViewVerticalPos));
                                }
                            }
                            else if (newTreeLineRect.Contains(newElement.position))
                            {
                                if (oldPos.y < _treeViewVerticalPos)
                                {
                                    Handles.DrawLine(new Vector2(position.width / 2f, _treeViewVerticalPos), newPos);
                                }
                                else if (oldPos.y > newTreeLineRect.height + _treeViewVerticalPos)
                                {
                                    Handles.DrawLine(new Vector2(position.width / 2f, newTreeLineRect.height + _treeViewVerticalPos), newPos);
                                }
                            }
                        }
                    }
                }

                if (_isSearching)
                {
                    Rect searchRect = new Rect();
                    if (_isSearchOld)
                    {
                        searchRect = _oldTreeView.GetLineRect(_searchId);
                        searchRect.position += new Vector2(10, _treeViewVerticalPos + _lineVerticalOffset);
                    }
                    else
                    {
                        searchRect = _newTreeView.GetLineRect(_searchId);
                        searchRect.position += new Vector2(position.width / 2f, _treeViewVerticalPos + _lineVerticalOffset);
                    }

                    Handles.color = Color.red;
                    Handles.DrawLine(searchRect.position, Event.current.mousePosition);
                }

                Handles.EndGUI();
            }

            Repaint();
        }
    }
}

#endif