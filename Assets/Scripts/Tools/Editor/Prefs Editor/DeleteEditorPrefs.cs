#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class DeleteAllPrefs : ScriptableObject
    {
        [MenuItem("Tools/Prefs/Delete all editor prefs")]
        static void DeleteAllEditorPrefs()
        {
            if (EditorUtility.DisplayDialog("Delete all editor preferences.",
                "Are you sure you want to delete all the editor preferences? " +
                "This action cannot be undone.", "Yes", "No"))
            {
                EditorPrefs.DeleteAll();
            }
        }

        [MenuItem("Tools/Prefs/Delete all player prefs")]
        static void DeleteAllPlayerPrefs()
        {
            if (EditorUtility.DisplayDialog("Delete all player preferences.",
                "Are you sure you want to delete all the player preferences? " +
                "This action cannot be undone.", "Yes", "No"))
            {
                PlayerPrefs.DeleteAll();
            }
        }
    }
}

#endif