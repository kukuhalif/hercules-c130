﻿#if UNITY_EDITOR

using System.Collections.Generic;

namespace VirtualTraining.Tools
{

    public interface IPreferenceProvider
    {
        void SetKeyValue(string valueName, object value);
        void FetchKeyValues(IDictionary<string, object> prefsLookup);
        object ValueField(string valueName, object value);
    }
}

#endif