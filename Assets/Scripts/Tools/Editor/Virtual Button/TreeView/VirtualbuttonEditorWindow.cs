﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    class VirtualButtonEditorWindow : TreeViewEditorBase<VirtualButtonTreeAsset, VirtualButtonEditorWindow, VirtualButtonData, VirtualButtonTreeElement>
    {
        VirtualButtonContentEditor virtualbuttonContentEditor;
        [MenuItem("Virtual Training/Virtual Button Editor &v")]
        public static void GetWindow()
        {
            VirtualButtonEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                window.Initialize();
                return true;
            }
            return false;
        }

        public static void GetWindow(int instanceID)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Save();
                window.Load();
                window.Initialize();
            }
        }

        public static void SelectItem(int id)
        {
            VirtualButtonEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.TreeView.SetSelection(new List<int> { id });
                window.TreeView.SetFocusAndEnsureSelectedItem();
            }
        }

        private void OnEnable()
        {
            virtualbuttonContentEditor = new VirtualButtonContentEditor(RecordUndo, WindowId);
        }

        protected override TreeViewWithTreeModel<VirtualButtonTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<VirtualButtonTreeElement>(ScriptableObjectTemp.VirtualButtonData.treeElements);
            return new TreeViewWithTreeModel<VirtualButtonTreeElement>(TreeViewState, treeModel);
        }

        protected override VirtualButtonTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new VirtualButtonTreeElement("new virtual button", depth, id);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnGUI()
        {
            base.OnGUI();

        }
        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void ContentView(List<VirtualButtonTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            for (int i = 0; i < selectedElements.Count; i++)
            {
                if (!selectedElements[0].isFolder)
                {
                    virtualbuttonContentEditor.ShowContent(selectedElements[i].VirtualButtonData, i);
                    selectedElements[i].VirtualButtonData.name = selectedElements[i].CompleteName;
                }
            }
            EditorGUILayout.EndVertical();
        }
    }
}

#endif
