﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class VirtualButtonContentEditor : PropertyField
    {
        VirtualButtonDataModel vbDataCurrent;
        /// <summary>
        /// Show Content Virtual Button
        /// </summary>
        /// <param name="virtualbuttonData"><data in virtual button data model/param>
        /// <param name="index"><index data model in list parent/param>
        public void ShowContent(VirtualButtonDataModel virtualbuttonData, int index)
        {
            vbDataCurrent = virtualbuttonData;
            if (Application.isPlaying)
                isAppPlaying = true;
            else
                isAppPlaying = false;

            EditorGUI.BeginDisabledGroup(isAppPlaying == false);
            if (GUILayout.Button(isPreview ? "Stop Preview" : "Virtual Button Preview"))
            {
                isPreview = !isPreview;
                if (isPreview)
                {
                    EventManager.TriggerEvent(new VirtualButtonEvent(new List<VirtualButtonDataModel>() { virtualbuttonData }));
                    EventManager.TriggerEvent(new VirtualButtonEventInstantiate());
                }
                else
                {
                    EventManager.TriggerEvent(new VirtualButtonEventDestroy());
                }

            }
            EditorGUI.EndDisabledGroup();
            ShowTab("categories", virtualbuttonData, index.ToString() + 1);

        }

        void ShowVirtualButton(VirtualButtonDataModel virtualButtonData, int index)
        {
            int idx = 0;
            GUILayout.BeginVertical();
            ShowSimpleField("Virtual Button Type:", index.ToString() + idx++, ref virtualButtonData.vbType);

            switch ((int)virtualButtonData.vbType)
            {
                case 0:
                    ShowVBClick(virtualButtonData, index.ToString() + idx++);
                    break;
                case 1:
                    ShowVBDrag(virtualButtonData, index.ToString() + idx++);
                    break;
                case 2:
                    ShowVBPress(virtualButtonData, index.ToString() + idx++);
                    break;
                default:
                    ShowVBClick(virtualButtonData, index.ToString() + idx++);
                    break;
            }

            GUILayout.EndVertical();
        }

        void ShowVBDrag(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            if (!virtualbuttonDataChild.isSequentialDrag)
            {
                ShowSimpleField("Elastic", index + idx++, ref virtualbuttonDataChild.isElastic);
                EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.isElastic == false);
                ShowSimpleField("Elastic Speed", index + idx++, ref virtualbuttonDataChild.elasticSpeed);
                EditorGUI.EndDisabledGroup();
            }

            ShowSimpleField("Drag Direction", index + idx++, ref virtualbuttonDataChild.direction);
            EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.direction == DragDirection.Vertical);
            EditorGUILayout.BeginHorizontal();
            ShowSimpleField("Delta Position X", index + idx++, ref virtualbuttonDataChild.deltaPositionX);
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.direction == DragDirection.Horizontal);
            ShowSimpleField("Delta Position Y", index + idx++, ref virtualbuttonDataChild.deltaPositionY);
            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();

            if (virtualbuttonDataChild.direction != DragDirection.Both)
            {
                ShowSimpleField("Sequential", index + idx++, ref virtualbuttonDataChild.isSequentialDrag);
                if (virtualbuttonDataChild.isSequentialDrag == true)
                {
                    ShowSimpleField("Sequence Method", index + idx++, ref virtualbuttonDataChild.sequencePlayMethod);
                    if (virtualbuttonDataChild.isSequentialDrag)
                    {
                        if (virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
               virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
               virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                        {
                            ShowSimpleField("Time Stamp Value Type", index.ToString() + idx++, ref virtualbuttonDataChild.vbPlayStampValueType);
                            ShowListField("Time Stamp Values", virtualbuttonDataChild.sequentialDragDatas, index.ToString() + idx++);
                        }
                    }
                }
            }
        }

        void ShowVBClick(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            ShowSimpleField("Sequential", index + idx++, ref virtualbuttonDataChild.isSequentialClick);

            if (virtualbuttonDataChild.isSequentialClick)
            {
                ShowSimpleField("Sequence Method", index + idx++, ref virtualbuttonDataChild.sequencePlayMethod);
                if (virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
    virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
    virtualbuttonDataChild.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    ShowSimpleField("Time Stamp Value Type", index.ToString() + idx++, ref virtualbuttonDataChild.vbPlayStampValueType);
                    ShowListField("Time Stamp Values", virtualbuttonDataChild.sequentialClickDatas, index.ToString() + idx++);
                }
            }
        }

        void ShowVBPress(VirtualButtonDataModel virtualbuttonDataChild, string index)
        {
            int idx = 0;
            ShowSlider("Default Value", index + idx++, ref virtualbuttonDataChild.defaultValue, 0, 1);
            ShowSimpleField("Return to start", index + idx++, ref virtualbuttonDataChild.isReturnToStart);

            ShowSimpleField("Elastic", index + idx++, ref virtualbuttonDataChild.isElastic);
            EditorGUI.BeginDisabledGroup(virtualbuttonDataChild.isElastic == false);
            ShowSimpleField("Elastic Speed", index + idx++, ref virtualbuttonDataChild.elasticSpeed);
            ShowSimpleField("Elastic Type", index + idx++, ref virtualbuttonDataChild.elasticType);
            EditorGUI.EndDisabledGroup();
            if (virtualbuttonDataChild.isElastic && virtualbuttonDataChild.elasticType == ElasticType.Both)
                virtualbuttonDataChild.isReturnToStart = false;
        }

        void ShowVbModifierSetActive(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            ShowListField("List Object Interaction Modifier", modifierData.interactionObjectDatas, listid + idx++);
        }
        void ShowVbModifierBlink(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            GUILayout.Label("variable blink");
            ShowSimpleField("Blink", listid + idx++, ref modifierData.isBlink);

            if (modifierData.isBlink)
            {
                ShowSimpleField("Delay Blinking Time", listid + idx++, ref modifierData.delayTimeBlink);
                ShowSimpleField("Minimal Intensity", listid + idx++, ref modifierData.minIntensity);
                ShowSimpleField("Maximal Intensity", listid + idx++, ref modifierData.maxIntensity);
            }

            ShowSimpleField("Change Color", listid + idx++, ref modifierData.isChangeColor);

            if(modifierData.isChangeColor)
            {
                ShowSimpleField("Delay Color Time", listid + idx++, ref modifierData.delayTimeColor);
                ShowSimpleField("Color 1", listid + idx++, ref modifierData.color1);
                ShowSimpleField("Color 2", listid + idx++, ref modifierData.color2);
            }

            ShowListField("List Object Interaction Modifier", modifierData.interactionObjectDatas, listid + idx++);
        }

        void ShowVbModifierSkybox(VirtualbuttonModifierData modifierData, string listid)
        {
            int idx = 0;
            GUILayout.Label("variable skybox");
            ShowSimpleField("Click Skybox Type", listid + idx++, ref modifierData.skyBoxType);
            EditorGUI.BeginDisabledGroup(modifierData.skyBoxType == VBSKyboxType.Value);
            ShowSimpleField("Speed", listid + idx++, ref modifierData.speed);
            EditorGUI.EndDisabledGroup();

            ShowSimpleField("Destination Exposure", listid + idx++, ref modifierData.dayExposure);
            ShowSimpleField("Destination Intensity Multiplier", listid + idx++, ref modifierData.dayIntensityMultiplier);
            ShowSimpleField("Light Object", listid + idx++, ref modifierData.light);
            if (modifierData.light.gameObject != null)
            {
                EditorGUILayout.BeginHorizontal();
                ShowSimpleField("Transform X", listid + idx++, ref modifierData.lightTransformX);
                ShowSimpleField("Transform Y", listid + idx++, ref modifierData.lightTransformY);
                ShowSimpleField("Transform Z", listid + idx++, ref modifierData.lightTransformZ);
                EditorGUILayout.EndHorizontal();
            }
        }

        void ShowEffectLerp(VirtualButtonEffectData vbEffect, string listid)
        {
            int idx = 0;
            ShowSimpleField("Animator", listid + idx++, ref vbEffect.animator);
            ShowSimpleField("Min Value", listid + idx++, ref vbEffect.minValue);
            ShowSimpleField("Max Value", listid + idx++, ref vbEffect.maxValue);
            ShowSimpleField("Parameter", listid + idx++, ref vbEffect.parameter);
            ShowSlider("Speed Acceleration", listid + idx++, ref vbEffect.speedAcc, 0, 1);
        }
    }
}
#endif
