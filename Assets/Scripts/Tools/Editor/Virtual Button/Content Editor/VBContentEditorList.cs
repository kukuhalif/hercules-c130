﻿
#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public partial class VirtualButtonContentEditor : PropertyField
    {
        public Vector2 scrollPos = new Vector2();
        bool isPreview;
        bool isAppPlaying;
        bool isSequentialDrag;
        Action _recordUndo;
        public VirtualButtonContentEditor(Action beforeModifiedCallback, string windowId) : base(beforeModifiedCallback, windowId)
        {
            Initialize();
        }

        void Initialize()
        {
            CreateListView("List Modifier", OnDrawHeaderModifier, null, null, OnDrawElementModifier);
            CreateListView("List Sequential Durations", OnDrawHeaderSequentialDuration, null, OnDrawElementsSequentialDuration, null);
            CreateListView("List Object Interaction Modifier", OnDrawHeaderObjectModifier, null, null, OnDrawElementsObjectModiifier);
            CreateListView("Trigger Modifier Values", OnDrawHeaderSequentialDuration, null, OnDrawElementsFloatRounded, null);
            CreateListView("List Effect Virtual Button", OnDrawHeaderEffect, null, null, OnDrawElementsEffect);

            CreateTab("categories", OnCategoriesTabCallback, "SETUP", "OUTPUT", "MODIFIER");
            CreateListView("List On Start Animation", OnDrawHeaderOnStartAnimation, null, null, OnDrawElementOnStartAnimation);
            CreateListView("List Click Target", OnDrawHeaderClickTarget, null, null, OnDrawElementClickTarget);
            CreateListView("Time Stamp Values", OnDrawHeaderSequentialDuration, null, OnDrawElementsSequentialDuration, null);
            CreateListView("Interaction Output", OnDrawHeaderOutput, null, OnDrawElementsInteractionOutput, null);
            CreateListView("Feedback Output", OnDrawHeaderOutput, null, OnDrawElementsFeedbackOutput, null);
            CreateListView("Parameter Datas", OnDrawHeaderParameterList, null, OnDrawElementsParameterList, null);
            CreateListView("Animation Datas", OnDrawHeaderAnimationStates, null, OnDrawElementsAnimationState, null);
        }

        string OnDrawHeaderModifier(object element)
        {
            VirtualbuttonModifierData figure = element as VirtualbuttonModifierData;
            return figure.vbModifierType.ToString();
        }

        object OnDrawElementModifier(string listId, object element, int index)
        {
            VirtualbuttonModifierData modifierData = element as VirtualbuttonModifierData;
            int idx = 0;
            string id = listId + "Modifier" + index;
            GUILayout.BeginVertical();
            ShowSimpleField("Modifier Type:", id + idx++, ref modifierData.vbModifierType);
            ShowSimpleField("Default Condition :", id + idx++, ref modifierData.defaultCondition);
            ShowListField("Trigger Modifier Values", modifierData.modifierConditionValues, id + idx++);
            switch ((int)modifierData.vbModifierType)
            {
                case 0:
                    ShowVbModifierSetActive(modifierData, id + idx++);
                    break;
                case 1:
                    ShowVbModifierBlink(modifierData, id + idx++);
                    break;
                case 2:
                    ShowVbModifierSkybox(modifierData, id + idx++);
                    break;
                default:
                    ShowVbModifierSetActive(modifierData, id + idx++);
                    break;
            }
            GUILayout.EndVertical();
            return modifierData;
        }

        string OnDrawHeaderOnStartAnimation(object element)
        {
            string name = "empty";
            VBStartAnimationData vbStartAnimationData = element as VBStartAnimationData;
            if (vbStartAnimationData.isAnimationScript == false)
            {
                if (vbStartAnimationData.animatorController.gameObject != null)
                    name = vbStartAnimationData.animatorController.gameObject.name;
            }
            else
            {
                if (vbStartAnimationData.ObjectAnimation.gameObject != null)
                    name = vbStartAnimationData.ObjectAnimation.gameObject.name;
            }
            return name;
        }

        object OnDrawElementOnStartAnimation(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VBStartAnimationData interactionData = element as VBStartAnimationData;
            ShowSimpleField("Animation Script:", listId + index + idx++, ref interactionData.isAnimationScript);
            EditorGUI.BeginDisabledGroup(interactionData.isAnimationScript == true);
            ShowSimpleField("Animation State:", listId + index + idx++, ref interactionData.animationState);
            ShowSimpleField("Animator Controller", listId + index + idx++, ref interactionData.animatorController);
            EditorGUI.EndDisabledGroup();
            EditorGUI.BeginDisabledGroup(interactionData.isAnimationScript == false);
            ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Scripted Animations", listId + index + idx++, ref interactionData.scriptedAnimationElement);
            ShowSimpleField("Object Animation", listId + index + idx++, ref interactionData.ObjectAnimation);
            ShowSimpleField("Reverse Animation", listId + index + idx++, ref interactionData.isReverseAnimation);
            ShowSimpleField("Disabled Animation", listId + index + idx++, ref interactionData.isDisabledFinished);
            EditorGUI.EndDisabledGroup();
            GUILayout.EndVertical();
            return interactionData;
        }

        string OnDrawHeaderClickTarget(object element)
        {
            string name = "empty";
            ClickTargetData clickTargetData = element as ClickTargetData;
            if (clickTargetData.gameObject.gameObject != null)
            {
                name = clickTargetData.gameObject.gameObject.name;
            }
            return name;
        }

        object OnDrawElementClickTarget(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            ClickTargetData clickTargetData = element as ClickTargetData;
            ShowSimpleField("Object Target", listId + index + idx++, ref clickTargetData.gameObject);
            ShowSimpleField("Invert", listId + index + idx++, ref clickTargetData.isInvert);
            GUILayout.EndVertical();
            return clickTargetData;
        }

        string OnDrawHeaderSequentialDuration(object element)
        {
            FloatListData scriptedContainer = element as FloatListData;
            return scriptedContainer.durations.ToString();
        }

        object OnDrawElementsSequentialDuration(string listId, object element, int index)
        {
            int idx = 0;
            FloatListData scriptedContainer = element as FloatListData;
            ShowSimpleField("Time Stamp:", listId + index + idx++, ref scriptedContainer.durations);
            if ((vbDataCurrent.vbType == VirtualButtonType.Click && vbDataCurrent.isSequentialClick) || (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.isSequentialDrag))
            {
                if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampParameter ||
                    vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation ||
                    vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampState)
                {
                    if (vbDataCurrent.vbPlayStampValueType == VBPlayStampValueType.DifferenceValue)
                        ShowSimpleField("Time Stamp Feedback:", listId + index + idx++, ref scriptedContainer.durationSecond);
                }
            }
            return scriptedContainer;
        }

        private void OnCategoriesTabCallback(int selected, object data, string fieldId)
        {
            VirtualButtonDataModel virtualbuttonData = data as VirtualButtonDataModel;
            int idx = 0;

            switch (selected)
            {
                // Setup
                case 0:

                    ShowListField("List On Start Animation", virtualbuttonData.vbStartAnimations, fieldId + idx++);
                    ShowListField("List Click Target", virtualbuttonData.clickTargetDatas, fieldId + idx++);
                    ShowVirtualButton(virtualbuttonData, selected + idx++);
                    break;

                // Output
                case 1:
                    if ((virtualbuttonData.vbType == VirtualButtonType.Click || (virtualbuttonData.vbType == VirtualButtonType.Drag) && virtualbuttonData.isSequentialDrag))
                        ShowSimpleField("Transition Method", fieldId.ToString() + idx++, ref virtualbuttonData.transitionMethod);
                    else
                    {
                        List<string> transitionCustom = new List<string>();
                        transitionCustom.Add("Direct");
                        transitionCustom.Add("FollowThrough");
                        virtualbuttonData.indexTransition = ShowDropdownField("Transition Method", fieldId + idx++, transitionCustom[virtualbuttonData.indexTransition], transitionCustom);
                        virtualbuttonData.transitionMethod = (TransitionMethod)virtualbuttonData.indexTransition;
                    }

                    if (virtualbuttonData.transitionMethod == TransitionMethod.Followthrough)
                        ShowSimpleField("Delay", fieldId + idx++, ref virtualbuttonData.followThroughDelay);

                    ShowListField("Interaction Output", virtualbuttonData.vbOutputInteraction, fieldId + idx++);
                    ShowListField("Feedback Output", virtualbuttonData.vbOutputFeedback, fieldId + idx++);
                    break;

                // Modifier
                case 2:

                    ShowListField("List Modifier", virtualbuttonData.vbModifierData, fieldId + idx++);
                    ShowListField("List Effect Virtual Button", virtualbuttonData.vbEffectDatas, fieldId + idx++);
                    break;
            }
        }

        string OnDrawHeaderOutput(object element)
        {
            string name = "empty";
            VBStartAnimationData vbStartAnimationData = element as VBStartAnimationData;
            if (vbStartAnimationData.isAnimationScript == false)
            {
                if (vbStartAnimationData.animatorController.gameObject != null)
                    name = vbStartAnimationData.animatorController.gameObject.name;
            }
            else
            {
                if (vbStartAnimationData.ObjectAnimation.gameObject != null)
                    name = vbStartAnimationData.ObjectAnimation.gameObject.name;
            }
            return name;
        }

        object OnDrawElementsInteractionOutput(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonOutputData vbOutputData = element as VirtualButtonOutputData;
            if (vbDataCurrent != null)
            {
                if (vbDataCurrent.vbType == VirtualButtonType.Click)
                {
                    if (vbDataCurrent.isSequentialClick)
                    {
                        if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation || vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation)
                            vbOutputData.isAnimationScript = true;
                        else
                            vbOutputData.isAnimationScript = false;
                    }
                }
                else if (vbDataCurrent.vbType == VirtualButtonType.Drag)
                {
                    if (vbDataCurrent.isSequentialDrag)
                    {
                        isSequentialDrag = true;
                        if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.PlayScriptedAnimation || vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation)
                            vbOutputData.isAnimationScript = true;
                        else
                            vbOutputData.isAnimationScript = false;
                    }
                    else
                        isSequentialDrag = false;
                }
            }

            ShowSimpleField("animation script", listId + index + idx++, ref vbOutputData.isAnimationScript);
            ShowSimpleField("speed", listId + index + idx++, ref vbOutputData.speed);
            if (vbOutputData.isAnimationScript == false)
            {
                ShowSimpleField("animator controller", listId + index + idx++, ref vbOutputData.animatorController);
                if ((vbDataCurrent.vbType == VirtualButtonType.Click && vbDataCurrent.isSequentialClick) || (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.isSequentialDrag))
                {
                    if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.PlayParameter || vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampParameter)
                    {
                        ShowListField("Parameter Datas", vbOutputData.parameterDatas, listId + index + idx++);
                        vbOutputData.feedBackManualType = FeedbackManualType.Parameter;
                    }
                    else if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.PlayState)
                    {
                        vbOutputData.feedBackManualType = FeedbackManualType.State;
                        ShowListField("Animation Datas", vbOutputData.animationDatas, listId + index + idx++);
                    }
                    else
                    {
                        vbOutputData.feedBackManualType = FeedbackManualType.State;
                        ShowSimpleField("animation state", listId + index + idx++, ref vbOutputData.animationState);
                    }

                }
                else
                {
                    ShowSimpleField("animation state", listId + index + idx++, ref vbOutputData.animationState);
                    if (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.direction == DragDirection.Both)
                    {
                        ShowSimpleField("animation parameter X", listId + index + idx++, ref vbOutputData.animationParameter);
                        ShowSimpleField("animation parameter Y", listId + index + idx++, ref vbOutputData.animationParameterY);
                    }
                    else
                        ShowSimpleField("animation parameter", listId + index + idx++, ref vbOutputData.animationParameter);

                    ShowSimpleField("min value", listId + index + idx++, ref vbOutputData.minimalValue);
                    ShowSimpleField("max value", listId + index + idx++, ref vbOutputData.maximalValue);
                }
            }
            else
            {
                ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Scripted Animations", listId + index + idx++, ref vbOutputData.scriptedAnimationElement);
                ShowSimpleField("object animation", listId + index + idx++, ref vbOutputData.ObjectAnimation);
                ShowSimpleField("reverse animation", listId + index + idx++, ref vbOutputData.isReverseAnimation);
                ShowSimpleField("disabled after finished", listId + index + idx++, ref vbOutputData.isDisabledFinished);
            }
            GUILayout.EndVertical();
            return vbOutputData;
        }

        object OnDrawElementsFeedbackOutput(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonOutputData vbOutputData = element as VirtualButtonOutputData;
            ShowSimpleField("animation script", listId + index + idx++, ref vbOutputData.isAnimationScript);
            ShowSimpleField("speed", listId + index + idx++, ref vbOutputData.speed);
            if (vbOutputData.isAnimationScript == false)
            {
                ShowSimpleField("animator controller", listId + index + idx++, ref vbOutputData.animatorController);

                if ((vbDataCurrent.vbType == VirtualButtonType.Click && vbDataCurrent.isSequentialClick) || (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.isSequentialDrag))
                {
                    ShowSimpleField("feedback type", listId + index + idx++, ref vbOutputData.feedBackManualType);
                    if (vbOutputData.feedBackManualType == FeedbackManualType.Parameter)
                    {
                        ShowListField("Parameter Datas", vbOutputData.parameterDatas, listId + index + idx++);
                    }
                    else
                    {
                        if (vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampState || vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampParameter
                            || vbDataCurrent.sequencePlayMethod == SequencePlayMethod.TimeStampScriptedAnimation)
                        {
                            ShowSimpleField("animation state", listId + index + idx++, ref vbOutputData.animationState);
                        }
                        else
                            ShowListField("Animation Datas", vbOutputData.animationDatas, listId + index + idx++);
                    }
                }
                else
                {
                    ShowSimpleField("animation state", listId + index + idx++, ref vbOutputData.animationState);
                    if (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.direction == DragDirection.Both)
                    {
                        ShowSimpleField("animation parameter X", listId + index + idx++, ref vbOutputData.animationParameter);
                        ShowSimpleField("animation parameter Y", listId + index + idx++, ref vbOutputData.animationParameterY);
                    }
                    else
                        ShowSimpleField("animation parameter", listId + index + idx++, ref vbOutputData.animationParameter);

                    ShowSimpleField("min value", listId + index + idx++, ref vbOutputData.minimalValue);
                    ShowSimpleField("max value", listId + index + idx++, ref vbOutputData.maximalValue);
                }
            }
            else
            {
                ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Scripted Animations", listId + index + idx++, ref vbOutputData.scriptedAnimationElement);
                ShowSimpleField("object animation", listId + index + idx++, ref vbOutputData.ObjectAnimation);
                ShowSimpleField("reverse animation", listId + index + idx++, ref vbOutputData.isReverseAnimation);
                ShowSimpleField("disabled after finished", listId + index + idx++, ref vbOutputData.isDisabledFinished);
            }
            GUILayout.EndVertical();
            return vbOutputData;
        }

        object OnDrawElementsFloatRounded(string listId, object element, int index)
        {
            float idx = 0;
            ModifierConditionValue scriptedContainer = element as ModifierConditionValue;
            scriptedContainer.durations = Mathf.Round(scriptedContainer.durations * 10f) / 10f;
            ShowSimpleField("Min Value:", listId + index + idx++, ref scriptedContainer.minValue);
            ShowSimpleField("Max Value:", listId + index + idx++, ref scriptedContainer.maxValue);
            return scriptedContainer;
        }

        string OnDrawHeaderObjectModifier(object element)
        {
            string name = "empty";
            InteractionObjectData vbModifierData = element as InteractionObjectData;
            if (vbModifierData != null)
            {
                if (vbModifierData.gameObject.gameObject != null)
                    name = vbModifierData.gameObject.gameObject.name;
            }
            return name;
        }

        object OnDrawElementsObjectModiifier(string listId, object element, int index)
        {
            InteractionObjectData vbModifierData = element as InteractionObjectData;
            ShowSimpleField("Object Interaction", listId + index + 0, ref vbModifierData.gameObject);
            return vbModifierData;
        }
        string OnDrawHeaderEffect(object element)
        {
            VirtualButtonEffectData vbEffectData = element as VirtualButtonEffectData;
            return vbEffectData.parameter;
        }

        object OnDrawElementsEffect(string listId, object element, int index)
        {
            int idx = 0;
            GUILayout.BeginVertical();
            VirtualButtonEffectData vbEffectDatas = element as VirtualButtonEffectData;
            ShowSimpleField("Effect Type", listId + index + idx++, ref vbEffectDatas.effectType);
            string id = listId + "effect" + index;
            switch ((int)vbEffectDatas.effectType)
            {
                case 0:
                    ShowEffectLerp(vbEffectDatas, id + idx++);
                    break;
            }
            GUILayout.EndVertical();
            return vbEffectDatas;
        }
        string OnDrawHeaderParameterList(object element)
        {
            ParameterData scriptedContainer = element as ParameterData;
            return scriptedContainer.parameter.ToString();
        }

        object OnDrawElementsParameterList(string listId, object element, int index)
        {
            int idx = 0;
            GUILayout.BeginHorizontal();
            ParameterData parameterData = element as ParameterData;
            ShowSimpleField("Parameter:", listId + index + idx++, ref parameterData.parameter);
            ShowSimpleField("min value", listId + index + idx++, ref parameterData.minimalValue);
            ShowSimpleField("max value", listId + index + idx++, ref parameterData.maximalValue);
            if (vbDataCurrent.vbType == VirtualButtonType.Drag && vbDataCurrent.direction == DragDirection.Both)
            {
                ShowSimpleField("Type:", listId + index + idx++, ref parameterData.paramType);
            }
            GUILayout.EndHorizontal();
            return parameterData;
        }

        string OnDrawHeaderAnimationStates(object element)
        {
            AnimationStateData animationStateData = element as AnimationStateData;
            return animationStateData.animationState;
        }

        object OnDrawElementsAnimationState(string listId, object element, int index)
        {
            int idx = 0;
            AnimationStateData animationStateData = element as AnimationStateData;
            ShowSimpleField("Animation State", listId + index + idx++, ref animationStateData.animationState);
            return animationStateData;
        }
    }
}
#endif
