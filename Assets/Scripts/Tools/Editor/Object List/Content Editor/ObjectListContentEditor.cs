﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class ObjectListContentEditor : PropertyField
    {
        public ObjectListContentEditor(Action beforeModifiedCallback, string windowId) : base(beforeModifiedCallback, windowId)
        {
            CreateListView("objects", null, null, OnObjectListElementCallback, null, false, typeof(GameObject));
        }

        private object OnObjectListElementCallback(string listId, object element, int index)
        {
            GameObjectType got = element as GameObjectType;
            ShowSimpleField("object", listId + index, ref got);
            return got;
        }

        public void PreviewInspector(ObjectListDataModel objectListData, int index)
        {
            ShowSimpleField("name", index.ToString(), ref objectListData.name);
        }

        public void DetailInspector(ObjectListDataModel objectListData)
        {
            ShowSimpleField("name", ref objectListData.name);

            GUILayout.Space(5f);

            ShowListField("objects", objectListData.objects);
        }
    }
}

#endif