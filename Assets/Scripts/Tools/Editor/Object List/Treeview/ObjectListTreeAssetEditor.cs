﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(ObjectListTreeAsset))]
    public class ObjectListTreeAssetEditor : TreeAssetEditorBase<ObjectListTreeAsset, ObjectListTreeElement>
    {
        // Start is called before the first frame update
        protected override ObjectListTreeAsset GetAsset()
        {
            return (ObjectListTreeAsset)target;
        }

        protected override List<ObjectListTreeElement> GetTreeElements()
        {
            return GetAsset().ObjectListData.treeElements;
        }
    }
}

#endif
