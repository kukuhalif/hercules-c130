﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class FavoriteEditor : EditorWindow
    {
        Vector2 ScrollPos = new Vector2();

        class GameobjectList
        {
            public string Name;
            public List<GameObjectType> Gameobjects = new List<GameObjectType>();

            public GameobjectList(string name)
            {
                Name = name;
            }
        }

        class CameraData
        {
            public string name;
            public CameraDestination camDestination;

            public CameraData()
            {
                name = "";
                camDestination = new CameraDestination();
            }

            public CameraData(string name, CameraDestination camDestination)
            {
                this.name = name;
                this.camDestination = camDestination;
            }
        }

        public static CameraDestination tempCameraDestination;

        List<Color> FavoriteColors = new List<Color>();
        List<GameobjectList> FavoriteObjectList = new List<GameobjectList>();
        List<CameraData> FavoriteCameras = new List<CameraData>();

        PropertyField _propertyField;

        [MenuItem("Virtual Training/Favorite")]
        public static void ShowWindow()
        {
            var window = GetWindow(typeof(FavoriteEditor)) as FavoriteEditor;
            window.titleContent = new GUIContent("favorite editor");
        }

        public void AddCameraDestination(CameraDestination newCamDes)
        {
            FavoriteCameras.Add(new CameraData("new camera", newCamDes));
        }

        private object OnGameobjectElementDrawCallback(string listId, object element, int index)
        {
            GameObjectType got = element as GameObjectType;
            _propertyField.ShowSimpleField("gameobject", listId + index, ref got);
            return got;
        }

        private void OnEnable()
        {
            _propertyField = new PropertyField(null, "favorite");
            _propertyField.CreateListView("gameobjects", null, null, OnGameobjectElementDrawCallback, null, false, typeof(GameObject));

            FavoriteColors.Clear();
            GetData("/" + VirtualTrainingSceneManager.ProjectName + "/FavoriteColor.txt", (string data) =>
            {
                string[] colorDatas = data.Split(',');
                try
                {
                    FavoriteColors.Add(new Color(float.Parse(colorDatas[0]), float.Parse(colorDatas[1]), float.Parse(colorDatas[2]), float.Parse(colorDatas[3])));
                }
                catch (System.Exception e)
                {

                }
            });


            FavoriteCameras.Clear();
            GetData("/" + VirtualTrainingSceneManager.ProjectName + "/FavoriteCamera.txt", (string data) =>
            {
                string[] cameraDatas = data.Split('#');
                for (int i = 0; i < cameraDatas.Length; i++)
                {
                    cameraDatas[i] = cameraDatas[i].Trim();
                    if (!string.IsNullOrEmpty(cameraDatas[i]))
                    {
                        CameraData newCamData = JsonUtility.FromJson<CameraData>(cameraDatas[i]);
                        FavoriteCameras.Add(newCamData);
                    }
                }
            });

            FavoriteObjectList.Clear();
            GetData("/" + VirtualTrainingSceneManager.ProjectName + "/FavoriteGameobjectList.txt", (string data) =>
            {
                string[] lists = data.Split('#');
                foreach (var item in lists)
                {
                    string[] contents = item.Split('%');
                    if (!string.IsNullOrEmpty(contents[0].Trim()))
                    {
                        GameobjectList newItem = new GameobjectList(contents[0]);

                        for (int c = 0; c < contents.Length; c++)
                        {
                            string[] objects = contents[c].Split('^');
                            for (int o = 1; o < objects.Length; o++)
                            {
                                string id = objects[o].Trim();
                                if (!string.IsNullOrEmpty(id))
                                {
                                    id = id.Replace("~", "");
                                    int intId = int.Parse(id);
                                    newItem.Gameobjects.Add(new GameObjectType(intId));
                                }
                            }
                        }

                        FavoriteObjectList.Add(newItem);
                    }

                }
            });
        }

        void GetData(string filePath, Action<string> addAction)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + filePath;
            if (File.Exists(path))
            {
                StreamReader reader = new StreamReader(path);
                string data = reader.ReadToEnd();
                reader.Close();

                string[] datas = data.Split('#');
                foreach (string item in datas)
                {
                    if (item != "")
                    {
                        addAction(item);
                    }
                }
            }
        }

        private void OnDisable()
        {
            SaveData("/" + VirtualTrainingSceneManager.ProjectName, "/FavoriteColor.txt", () =>
            {
                string clr = "";
                foreach (var item in FavoriteColors)
                {
                    clr += item.r + "," + item.g + "," + item.b + "," + item.a + "#";
                }

                return clr;
            });

            SaveData("/" + VirtualTrainingSceneManager.ProjectName, "/FavoriteGameobjectList.txt", () =>
            {
                string obj = "";
                foreach (var item in FavoriteObjectList)
                {
                    obj += "#" + item.Name + "%";
                    foreach (var go in item.Gameobjects)
                    {
                        obj += "^" + go.Id + "~";
                    }
                }

                return obj;
            });

            SaveData("/" + VirtualTrainingSceneManager.ProjectName, "/FavoriteCamera.txt", () =>
            {
                string data = "";
                foreach (var item in FavoriteCameras)
                {
                    data += (JsonUtility.ToJson(item) + "#");
                }

                return data;
            });
        }

        void SaveData(string directory, string fileName, Func<string> parseDataAction)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + directory;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path += fileName;
            string data = parseDataAction();
            StreamWriter writer = new StreamWriter(path);
            writer.WriteLine(data);
            writer.Close();
        }

        private void OnGUI()
        {
            ScrollPos = GUILayout.BeginScrollView(ScrollPos);

            GUILayout.BeginVertical();

            GUILayout.BeginVertical("Box");

            if (GUILayout.Button("Add Color"))
            {
                FavoriteColors.Add(new Color());
            }

            for (int i = 0; i < FavoriteColors.Count; i++)
            {
                GUILayout.BeginHorizontal();
                FavoriteColors[i] = EditorGUILayout.ColorField("color " + i, FavoriteColors[i]);

                if (GUILayout.Button("Delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        FavoriteColors.RemoveAt(i);
                    }
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();

            GUILayout.Space(30);

            GUILayout.BeginVertical("Box");

            if (GUILayout.Button("Add Gameobject List"))
            {
                FavoriteObjectList.Add(new GameobjectList("new list"));
            }

            for (int i = 0; i < FavoriteObjectList.Count; i++)
            {
                GUILayout.BeginVertical("Box");

                GUILayout.BeginHorizontal();

                FavoriteObjectList[i].Name = EditorGUILayout.TextField((i + 1).ToString() + ". list name", FavoriteObjectList[i].Name);

                if (GUILayout.Button("Delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        FavoriteObjectList.RemoveAt(i);
                        break;
                    }
                }

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("add gameobject"))
                {
                    FavoriteObjectList[i].Gameobjects.Add(null);
                }

                if (GUILayout.Button("copy gameobjects"))
                {

                }

                GUILayout.EndHorizontal();

                _propertyField.ShowListField("gameobjects", FavoriteObjectList[i].Gameobjects, i.ToString());

                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.Space(30);

            GUILayout.Label("Favorite Camera Position Count : " + FavoriteCameras.Count.ToString());

            for (int i = 0; i < FavoriteCameras.Count; i++)
            {
                GUILayout.BeginVertical("Box");

                GUILayout.BeginHorizontal();
                GUILayout.Label((i + 1) + ". camera name ");
                FavoriteCameras[i].name = EditorGUILayout.TextField(FavoriteCameras[i].name, GUILayout.Width(400));

                if (GUILayout.Button("copy camera position"))
                {
                    tempCameraDestination = new CameraDestination(FavoriteCameras[i].camDestination);
                }
                GUILayout.FlexibleSpace();

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("pos " + FavoriteCameras[i].camDestination.position);
                GUILayout.Label(", rot " + FavoriteCameras[i].camDestination.rotation);
                if (FavoriteCameras[i].camDestination.target.gameObject != null)
                    GUILayout.Label(", target " + FavoriteCameras[i].camDestination.target.gameObject.name);
                GUILayout.FlexibleSpace();

                if (GUILayout.Button("delete"))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Delete ?", "yes", "no"))
                    {
                        FavoriteCameras.Remove(FavoriteCameras[i]);
                    }
                }

                GUILayout.EndHorizontal();

                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.EndScrollView();
        }
    }
}

#endif