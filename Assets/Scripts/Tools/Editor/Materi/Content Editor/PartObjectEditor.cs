using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using UnityEditor;

namespace VirtualTraining.Tools
{
    public class PartObjectEditor : PropertyField
    {
        bool enableFrenel = false;
        Action beforeModifiedCallback;
        bool clickedPartObjectHeader;
        PartObjectAction clickedPartObjectAction;
        List<string> xrayMats = new List<string>();

        public PartObjectEditor(Action beforeModifiedCallback, string windowId, List<Material> xrayMats) : base(beforeModifiedCallback, windowId)
        {
            this.beforeModifiedCallback = beforeModifiedCallback;
            for (int i = 0; i < xrayMats.Count; i++)
            {
                this.xrayMats.Add(xrayMats[i].name);
            }
            CreateListView("part object", null, OnPartObjectHeaderDrawCallback, OnPartObjectElementDrawCallback, OnPartObjectDetailDrawCallback, false, typeof(GameObject));
        }

        public void Show(List<PartObject> partObjects, string fieldId, int index)
        {
            ShowListField("part object", partObjects, fieldId + index);

            if (clickedPartObjectHeader)
            {
                beforeModifiedCallback?.Invoke();

                foreach (var part in partObjects)
                {
                    part.action = clickedPartObjectAction;
                }

                clickedPartObjectHeader = false;
            }
        }

        private void OnPartObjectHeaderDrawCallback()
        {
            EditorGUILayout.LabelField("target object", GUILayout.Width(135f));

            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectSolo), "solo")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Solo; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectHide), "hide")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Disable; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectHighlight), "highlight")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Highlight; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectXRay), "x ray")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Xray; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectShow), "show")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Enable; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectSelect), "select")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Select; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectBlink), "blink")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Blink; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectSetLayer), "set layer")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.SetLayer; }
            if (DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectIgnoreCutaway), "ignore cutaway")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.IgnoreCutaway; }
            if (enableFrenel && DrawButtonIcon(EditorIconResources.Get(IconEnum.PartObjectIgnoreCutaway), "fresnel")) { clickedPartObjectHeader = true; clickedPartObjectAction = PartObjectAction.Fresnel; }
        }

        private object OnPartObjectElementDrawCallback(string listId, object element, int index)
        {
            PartObject partObject = element as PartObject;

            string id = listId + "partObj" + index;

            ShowSimpleField("", id, ref partObject.target, 0f, 120f);
            GUILayout.Space(19f);
            float toggleWidth = 37f;

            var actions = Enum.GetValues(typeof(PartObjectAction));
            bool[] states = new bool[actions.Length];

            int elementCount = enableFrenel ? states.Length : states.Length - 1;

            for (int i = 0; i < elementCount; i++)
            {
                states[i] = partObject.action == (PartObjectAction)actions.GetValue(i);
                ShowSimpleField("", id + i, ref states[i], 0f, toggleWidth);
                if (states[i])
                    partObject.action = (PartObjectAction)actions.GetValue(i);
            }

            return partObject;
        }

        private object OnPartObjectDetailDrawCallback(string listId, object element, int index)
        {
            PartObject partObject = element as PartObject;
            string id = listId + "partObjDetail" + index;
            int idx = 0;

            switch (partObject.action)
            {
                case PartObjectAction.Solo:

                    // no data

                    break;
                case PartObjectAction.Disable:

                    // no data

                    break;
                case PartObjectAction.Highlight:

                    ShowSimpleField("no recursive", id + idx++, ref partObject.highlightNoRecursive);
                    ShowSimpleField("set highlight color", id + idx++, ref partObject.setHighlightColor);
                    EditorGUI.BeginDisabledGroup(!partObject.setHighlightColor);
                    ShowSimpleField("default highlight color", id + idx++, ref partObject.useDefaultHighlightColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultHighlightColor);
                    ShowSimpleField("custom highlight color", id + idx++, ref partObject.highlightColor);
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.EndDisabledGroup();
                    XrayMaterialDropdown("x ray variant", id + idx++, ref partObject.xrayIndex);

                    break;
                case PartObjectAction.Xray:

                    XrayMaterialDropdown("x ray variant", id + idx++, ref partObject.xrayIndex);

                    break;
                case PartObjectAction.Enable:

                    // no data

                    break;
                case PartObjectAction.Select:

                    ShowSimpleField("no recursive", id + idx++, ref partObject.highlightNoRecursive);
                    ShowSimpleField("set selected color", id + idx++, ref partObject.setHighlightColor);
                    EditorGUI.BeginDisabledGroup(!partObject.setHighlightColor);
                    ShowSimpleField("default selected color", id + idx++, ref partObject.useDefaultHighlightColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultHighlightColor);
                    ShowSimpleField("custom selected color", id + idx++, ref partObject.highlightColor);
                    EditorGUI.EndDisabledGroup();
                    EditorGUI.EndDisabledGroup();

                    break;
                case PartObjectAction.Blink:

                    ShowSimpleField("default blink color", id + idx++, ref partObject.useDefaultBlinkColor);
                    EditorGUI.BeginDisabledGroup(partObject.useDefaultBlinkColor);
                    ShowSimpleField("custom blink color", id + idx++, ref partObject.blinkColor);
                    EditorGUI.EndDisabledGroup();
                    ShowSimpleField("no recursive", id + idx++, ref partObject.blinkNoRecursive);
                    ShowSimpleField("speed", id + idx++, ref partObject.blinkSpeed);

                    break;
                case PartObjectAction.SetLayer:

                    ShowSimpleField("layer", id + idx++, ref partObject.layerOption);

                    break;
                case PartObjectAction.IgnoreCutaway:
                    break;
                case PartObjectAction.Fresnel:

                    ShowSimpleField("no recursive", id + idx++, ref partObject.fresnelNoRecursive);
                    ShowSimpleField("fresnel default color", id + idx++, ref partObject.useFresnelDefaultColor);
                    EditorGUI.BeginDisabledGroup(partObject.useFresnelDefaultColor);
                    ShowSimpleField("fresnel color", id + idx++, ref partObject.fresnelColor);
                    EditorGUI.EndDisabledGroup();
                    ShowSimpleField("fresnel with blink", id + idx++, ref partObject.fresnelWithBlink);
                    ShowSimpleField("blink speed", id + idx++, ref partObject.blinkSpeed);

                    break;
            }

            return partObject;
        }

        private void XrayMaterialDropdown(string title, string controlName, ref int index)
        {
            index = ShowDropdownField(title, controlName, xrayMats[index], xrayMats);
        }
    }
}

