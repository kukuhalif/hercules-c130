﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using VirtualTraining.Core;
using VirtualTraining.Feature;
using Paroxe.PdfRenderer;
using System.IO;


namespace VirtualTraining.Tools
{
    public class MateriContentEditor : PropertyField
    {
        class MateriViewData
        {
            public Figure figure;
            public List<AnimationFigureData> animations = new List<AnimationFigureData>();
            public List<VirtualButtonFigureData> virtualButtonElements = new List<VirtualButtonFigureData>();
            public List<Figure> figureOption = new List<Figure>();
            public bool isSequenceAnimation;
            public MateriViewData(Figure figure, List<Figure> figureOption, List<AnimationFigureData> animations = null, List<VirtualButtonFigureData> virtualButtonElements = null)
            {
                this.figure = figure;
                this.figureOption = figureOption;
                if (animations != null)
                {
                    for (int i = 0; i < animations.Count; i++)
                    {
                        this.animations.Add(animations[i]);
                    }
                }

                if (virtualButtonElements != null)
                {
                    for (int i = 0; i < virtualButtonElements.Count; i++)
                    {
                        this.virtualButtonElements.Add(virtualButtonElements[i]);
                    }
                }

            }
            public MateriViewData()
            {
                figure = null;
                animations = new List<AnimationFigureData>();
                figureOption = new List<Figure>();
                virtualButtonElements = new List<VirtualButtonFigureData>();
            }
        }

        EditorGUISplitView splitter;
        List<string> pdfAssetNames = new List<string>();
        List<PDFAsset> pdfAssets = new List<PDFAsset>();
        Materi playedMateri = null;
        int currentFigureIndex;
        int index;
        int currentTreeElementId;
        bool isAnimationInit;
        bool isVirtualButtonInit;
        List<MateriViewData> animDatas;
        List<MateriViewData> virtualButtonDatas;
        private int instanceId;
        PartObjectEditor partObjectEditor;

        public MateriContentEditor(Action beforeModifiedCallback, string windowId, List<PDFAsset> pdfAssets, List<Material> xrayMats, int instanceId) : base(beforeModifiedCallback, windowId)
        {
            splitter = new EditorGUISplitView(EditorGUISplitView.Direction.Vertical, WindowId + "description");
            animDatas = new List<MateriViewData>();
            virtualButtonDatas = new List<MateriViewData>();
            this.pdfAssets = pdfAssets;
            this.instanceId = instanceId;

            for (int i = 0; i < pdfAssets.Count; i++)
            {
                pdfAssetNames.Add(pdfAssets[i].name);
            }

            partObjectEditor = new PartObjectEditor(beforeModifiedCallback, windowId, xrayMats);

            CreateTab("categories", OnCategoriesTabCallback, "FIGURE LIST", "SCHEMATIC", "ANIMATION", "VIRTUAL BUTTON");
            CreateTab("figure category", OnFigureListTabCallback, "CAMERA", "PART OBJECT", "CALLOUT", "HELPER", "CUTAWAY");

            CreateListView("figure", FoldoutTextFigureCallback, null, null, OnFigureDetailDrawCallback);
            CreateListView("monitor camera", null, null, OnMonitorCameraElementDrawCallback, null);
            CreateListView("callout prefab", null, null, OnCalloutElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("helper prefab", null, null, OnHelperElementDrawCallback, null, true, typeof(GameObject));
            CreateListView("schematic", null, null, OnSchematicElementDrawCallback, null, false, typeof(VideoClip), typeof(Texture2D));
            CreateListView("pdf", null, null, OnPdfElementDrawCallback, null);
            CreateListView("virtual button", FoldoutTextVirtualButtonCallback, null, null, OnVirtualButtonDrawCallback);
            CreateListView("virtual button element", FoldoutTextVirtualButtonListCallback, null, null, OnVirtualButtonElementDrawCallback);
            CreateListView("virtual button override", null, null, OnVirtualButtonOverrideDrawCallback, null);

            CreateListView("animation", FoldoutTextAnimationCallback, null, null, OnAnimationDrawCallback);
            CreateListView("animation element", null, null, OnAnimationElementDrawCallback, null);
        }

        #region pdf

        private object OnPdfElementDrawCallback(string listId, object element, int index)
        {
            PdfData pdfMateri = element as PdfData;

            GUILayout.BeginVertical();

            if (pdfAssetNames.Count > 0)
            {
                pdfMateri.index = ShowDropdownField("pdf", listId + index, pdfAssetNames[pdfMateri.index], pdfAssetNames);
                ShowSimpleField("page", listId + index + 1, ref pdfMateri.page);
            }
            else
            {
                GUILayout.Label("setup materi pdf at materi config please");
            }

            GUILayout.EndVertical();

            return pdfMateri;
        }

        #endregion

        #region schematic

        private object OnSchematicElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            Schematic schematic = element as Schematic;

            if (schematic.image == null && schematic.video == null)
            {
                EditorGUILayout.LabelField("drop image / video here ...");
                Rect elementRect = GUILayoutUtility.GetLastRect();
                Event e = Event.current;

                if (elementRect.Contains(e.mousePosition) && e.type == EventType.DragExited)
                {
                    var selections = DragAndDrop.objectReferences;

                    if (selections.Length > 0)
                    {
                        Texture2D image = (object)selections[0] as Texture2D;
                        VideoClip video = (object)selections[0] as VideoClip;

                        if (image != null)
                            schematic.image = image;
                        else if (video != null)
                            schematic.video = video;
                    }
                }
            }
            else
            {
                if (schematic.image != null)
                    ShowAssetField("image", listId + index + 1, ref schematic.image);

                else if (schematic.video != null)
                {
                    ShowAssetField("video", listId + index + 2, ref schematic.video);
                    ShowSimpleField("loop", listId + index + 3, ref schematic.isLooping);
                }

                ShowSimpleField("show immediately", listId + index + 4, ref schematic.showImmediately);
                ShowSimpleField("description id", listId + index + 5, ref schematic.descriptionId);
            }

            GUILayout.EndVertical();

            return schematic;
        }

        #endregion

        #region callout

        private object OnCalloutElementDrawCallback(string listId, object element, int index)
        {
            GameObject go = element as GameObject;
            Callout callout = null;
            if (go != null)
                callout = go.GetComponent<Callout>();

            ShowAssetField("callout prefab", listId + index, ref callout);

            if (callout == null)
                return go;

            return callout.gameObject;
        }

        #endregion

        #region helper

        private object OnHelperElementDrawCallback(string listId, object element, int index)
        {
            GameObject helper = element as GameObject;

            ShowSimpleField("helper prefab", listId + index, ref helper);

            return helper;
        }

        #endregion

        #region monitor camera

        private object OnMonitorCameraElementDrawCallback(string listId, object element, int index)
        {
            MonitorCamera monitorCamera = element as MonitorCamera;

            ShowSimpleField("name", listId + "monitorName" + index, ref monitorCamera.displayName, 40f, 200f);
            GUILayout.Space(5f);
            ShowCameraPositionField("monitor camera " + (index + 1), listId + "monitorDestination" + index, ref monitorCamera.destination);

            return monitorCamera;
        }

        #endregion

        #region figure

        private string FoldoutTextFigureCallback(object element)
        {
            Figure figure = element as Figure;
            return figure.name;
        }

        private object OnFigureDetailDrawCallback(string listId, object element, int index)
        {
            Figure figure = element as Figure;

            ShowSimpleField("figure name", "figure name" + listId + index + 1, ref figure.name);
            ShowTab("figure category", figure, listId + index + 2);

            return figure;
        }

        private void OnFigureListTabCallback(int selected, object data, string fieldId)
        {
            Figure figure = data as Figure;
            int idx = 0;

            switch (selected)
            {
                // camera
                case 0:

                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Main Camera");
                    ShowCameraPositionField("camera destination", fieldId + idx++, ref figure.cameraDestination);

                    GUILayout.FlexibleSpace();

                    GUILayout.EndHorizontal();

                    ShowListField("monitor camera", figure.monitorCameras, fieldId + idx++);

                    break;

                // part object
                case 1:

                    partObjectEditor.Show(figure.partObjects, fieldId, idx++);

                    break;

                // callout
                case 2:

                    ShowListField("callout prefab", figure.callouts, fieldId + idx++);

                    break;

                // helper
                case 3:

                    ShowListField("helper prefab", figure.helpers, fieldId + idx++);

                    break;

                // cutaway
                case 4:

                    CutawayEditor.Show(figure.cutaway, this, fieldId);

                    break;
            }
        }

        #endregion

        #region virtual button

        private string FoldoutTextVirtualButtonCallback(object element)
        {
            MateriViewData materiView = element as MateriViewData;
            if (materiView.figure != null)
                return materiView.figure.name;
            else
                return "none figure";
        }

        private object OnVirtualButtonDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();

            MateriViewData virtualbuttonFigures = element as MateriViewData;

            int selectedIndex = 0;
            List<string> figuresName = new List<string>();
            for (int i = 0; i < virtualbuttonFigures.figureOption.Count; i++)
            {
                string name = i + "_" + virtualbuttonFigures.figureOption[i].name;
                figuresName.Add(name);
                if (object.Equals(virtualbuttonFigures.figure, virtualbuttonFigures.figureOption[i]))
                {
                    selectedIndex = i;
                }
            }

            if (figuresName.Count > 0)
            {
                selectedIndex = ShowDropdownField("figure", listId + index + 2, figuresName[selectedIndex], figuresName);
                virtualbuttonFigures.figure = virtualbuttonFigures.figureOption[selectedIndex];
                ShowListField("virtual button element", virtualbuttonFigures.virtualButtonElements, listId + index + 3);
            }

            GUILayout.EndVertical();
            return virtualbuttonFigures;
        }
        private string FoldoutTextVirtualButtonListCallback(object element)
        {
            VirtualButtonFigureData materiView = element as VirtualButtonFigureData;
            if (materiView.virtualButtonElementType.GetData() != null)
                return materiView.virtualButtonElementType.GetData().name;
            else
                return "none virtual button";
        }
        private object OnVirtualButtonElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            VirtualButtonFigureData virtualbuttonElement = element as VirtualButtonFigureData;
            ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("Virtual Button", listId + index + idx++, ref virtualbuttonElement.virtualButtonElementType);
            if (virtualbuttonElement.virtualButtonElementType.GetData() != null)
            {
                ShowSimpleField("Override Value", listId + index + idx++, ref virtualbuttonElement.isOverrideVirtualButton);
                if (virtualbuttonElement.isOverrideVirtualButton)
                {
                    ShowListField("virtual button override", virtualbuttonElement.overrideVirtualButton, listId + index + idx);
                }
            }

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        private object OnVirtualButtonOverrideDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            OverrideVariableVirtualButton virtualbuttonElement = element as OverrideVariableVirtualButton;

            ShowSimpleField("Override Variable", listId + index + idx++, ref virtualbuttonElement.OverrideVirtualButton);
            if (virtualbuttonElement.OverrideVirtualButton == OverrideVirtualButton.DefaultValue)
                ShowSimpleField("Default Value", listId + index + idx++, ref virtualbuttonElement.defaultValue);

            GUILayout.EndVertical();
            return virtualbuttonElement;
        }

        #endregion

        #region Animation

        private string FoldoutTextAnimationCallback(object element)
        {
            MateriViewData materiView = element as MateriViewData;
            if (materiView.figure != null)
                return materiView.figure.name;
            else
                return "none figure";
        }

        private object OnAnimationDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            MateriViewData animationOnFigure = element as MateriViewData;

            int selectedIndex = 0;
            List<string> figuresName = new List<string>();
            for (int i = 0; i < animationOnFigure.figureOption.Count; i++)
            {
                string name = i + "_" + animationOnFigure.figureOption[i].name;
                figuresName.Add(name);
                if (object.Equals(animationOnFigure.figure, animationOnFigure.figureOption[i]))
                {
                    selectedIndex = i;
                }
            }

            if (figuresName.Count > 0)
            {
                selectedIndex = ShowDropdownField("figure", listId + index + 2, figuresName[selectedIndex], figuresName);
                ShowSimpleField("Sequence Animation", listId + index + 3, ref animationOnFigure.isSequenceAnimation);
                animationOnFigure.figure = animationOnFigure.figureOption[selectedIndex];
                ShowListField("animation element", animationOnFigure.animations, listId + index + 3);
            }

            GUILayout.EndVertical();
            return animationOnFigure;
        }

        private object OnAnimationElementDrawCallback(string listId, object element, int index)
        {
            GUILayout.BeginVertical();
            int idx = 0;
            AnimationFigureData animationFigureData = element as AnimationFigureData;

            if (animationFigureData.isSequential)
                ShowSimpleField("sequence queue", listId + index + idx++, ref animationFigureData.sequentialQueue);

            ShowSimpleField("virtual button type", listId + index + idx++, ref animationFigureData.isVirtualButton);

            if (animationFigureData.isVirtualButton)
            {
                ShowReferenceField<VirtualButtonElementType, VirtualButtonTreeElement>("virtual button", listId + index + idx++, ref animationFigureData.virtualButton.virtualButtonElementType);
                ShowSimpleField("start value", listId + index + idx++, ref animationFigureData.virtualButton.startValue);
                ShowSimpleField("end value", listId + index + idx++, ref animationFigureData.virtualButton.endValue);
                ShowSimpleField("speed", listId + index + idx++, ref animationFigureData.virtualButton.speedVirtualButton);

                if(animationFigureData.virtualButton.virtualButtonElementType.GetData() != null)
                {
                    if(animationFigureData.virtualButton.virtualButtonElementType.GetData().vbType == VirtualButtonType.Drag && 
                        animationFigureData.virtualButton.virtualButtonElementType.GetData().direction == DragDirection.Both)
                    {
                        ShowSimpleField("start value Y", listId + index + idx++, ref animationFigureData.virtualButton.startValue);
                        ShowSimpleField("end value Y", listId + index + idx++, ref animationFigureData.virtualButton.endValue);
                    }
                }
            }
            else
            {
                ShowSimpleField("animation script", listId + index + idx++, ref animationFigureData.isAnimatedScript);

                EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript);
                ShowSimpleField("animator name", listId + index + idx++, ref animationFigureData.animationName);
                ShowSimpleField("animator controller", listId + index + idx++, ref animationFigureData.animatorController);
                EditorGUI.EndDisabledGroup();

                EditorGUI.BeginDisabledGroup(animationFigureData.isAnimatedScript == false);
                ShowReferenceField<ScriptedAnimationElementType, ScriptedAnimationTreeElement>("Animation", listId + index + idx++, ref animationFigureData.scriptedAnimationElement);
                ShowSimpleField("Object Animation", listId + index + idx++, ref animationFigureData.gameObjectInteraction);
                ShowSimpleField("Reverse Animation", listId + index + idx++, ref animationFigureData.isReverseAnimation);
                ShowSimpleField("Disabled After Finish", listId + index + idx++, ref animationFigureData.isDisabledFinished);
                EditorGUI.EndDisabledGroup();
            }

            GUILayout.EndVertical();
            return animationFigureData;
        }

        #endregion

        private void OnCategoriesTabCallback(int selected, object data, string fieldId)
        {
            Materi materiData = data as Materi;
            int idx = 0;

            switch (selected)
            {
                // figure list
                case 0:

                    ShowListField("figure", materiData.figures, fieldId + idx++);
                    isAnimationInit = false;
                    isVirtualButtonInit = false;
                    break;

                // schematic
                case 1:

                    ShowListField("schematic", materiData.schematics, fieldId + idx++);
                    isAnimationInit = false;
                    isVirtualButtonInit = false;
                    break;

                // animation
                case 2:

                    OpenAnimationTab(materiData, fieldId, idx);
                    isVirtualButtonInit = false;
                    break;

                // virtual button
                case 3:

                    OpenVirtualButtonTab(materiData, fieldId, idx);
                    isAnimationInit = false;
                    break;
            }

        }

        public void PreviewInspector(Materi materiData, int index)
        {
            ShowSimpleField("materi name", index.ToString(), ref materiData.name);
        }

        public void DetailInspector(MateriTreeElement materiTreeElement, Rect panelRect)
        {
            Materi materiData = materiTreeElement.MateriData;

            splitter.BeginSplitView(0f, panelRect.width);
            if (materiTreeElement.id != currentTreeElementId)
            {
                isAnimationInit = false;
                isVirtualButtonInit = false;
            }
            currentTreeElementId = materiTreeElement.id;
            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("enter play mode to enable play test materi (don't forget to select save if the dialogue box appears when enter/exit play mode)", EditorStyles.helpBox);
            }
            else
            {
                if (GUILayout.Button("play materi"))
                {
                    playedMateri = materiData;
                    currentFigureIndex = 0;

                    for (int i = 0; i < materiData.pdfs.Count; i++)
                    {
                        if (pdfAssets.Count > i)
                            materiData.pdfs[i].pdfAsset = pdfAssets[materiData.pdfs[i].index];
                        else
                            Debug.LogError("setup materi pdf at materi config please");
                    }

                    EventManager.TriggerEvent(new MateriEvent(materiTreeElement));
                }

                if (playedMateri == null)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("reset materi / free play"))
                {
                    currentFigureIndex = 0;
                    playedMateri = null;
                    EventManager.TriggerEvent(new FreePlayEvent());
                }

                GUILayout.BeginHorizontal();

                if (GUILayout.Button("play previous figure"))
                {
                    currentFigureIndex--;
                    if (currentFigureIndex < 0)
                        currentFigureIndex = playedMateri.figures.Count - 1;
                    EventManager.TriggerEvent(new PrevFigureEvent());
                }

                if (GUILayout.Button("play next figure"))
                {
                    currentFigureIndex++;
                    if (playedMateri.figures.Count <= currentFigureIndex)
                        currentFigureIndex = 0;
                    EventManager.TriggerEvent(new NextFigureEvent());
                }

                GUILayout.Label("current figure : " + currentFigureIndex);

                GUILayout.EndHorizontal();

                if (playedMateri == null)
                    EditorGUI.EndDisabledGroup();
            }

            GUILayout.Space(5f);

            if (materiData.elementType == MateriElementType.Materi)
            {
                ShowSimpleField("materi name", ref materiData.name);
                ShowSimpleField("hide description panel", ref materiData.hideDescriptionPanel);
                ShowSimpleField("show description popup", ref materiData.showDescriptionPopup);
                ShowSimpleField("disable background", ref materiData.disableBackground);
                ShowListField("pdf", materiData.pdfs);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description);

                splitter.Split();

                GUILayout.Space(5f);

                ShowTab("categories", materiData);
            }
            else if (materiData.elementType == MateriElementType.Maintenance)
            {
                GUILayout.Label("maintenance");

                if (GUILayout.Button("Open Maintenance Window"))
                {
                    MaintenanceEditorWindow.OpenMaintenance(materiData, materiTreeElement.id, instanceId);
                }

                ShowSimpleField("maintenance name", ref materiData.name);
                ShowSimpleField("hide description panel", ref materiData.hideDescriptionPanel);
                ShowSimpleField("show description popup", ref materiData.showDescriptionPopup);
                ShowSimpleField("disable background", ref materiData.disableBackground);
                ShowTextArea("description", panelRect.width - 15f, ref materiData.description);

                splitter.Split();

                GUILayout.Space(5f);

                ShowTab("categories", materiData);
            }
            else if (materiData.elementType == MateriElementType.Troubleshoot)
            {
                GUILayout.Label("troubleshoot");
                if (GUILayout.Button("Open Troubleshoot Graph"))
                {
                    OpenTroubleshootGraph(materiData);
                }
                materiData.troubleshootData.CompleteNameMateri = materiTreeElement.CompleteName;
                ShowSimpleField("troubleshoot name", ref materiData.name);
                ShowSimpleField("hide description panel", ref materiData.hideDescriptionPanel);
                ShowSimpleField("show description popup", ref materiData.showDescriptionPopup);
                ShowSimpleField("disable background", ref materiData.disableBackground);
                ShowSimpleField("Simplified Mode", "Simplified Mode", ref materiData.troubleshootData.isSimplifiedMode);
                ShowSimpleField("Size X Scale", "Size X Scalee", ref materiData.troubleshootData.sizeMultiplierX);
                ShowSimpleField("Size Y Scale", "Size Y Scalee", ref materiData.troubleshootData.sizeMultiplierY);
                ShowSimpleField("start postion", "Graph Start Position", ref materiData.troubleshootData.startPositionGraph);
                ShowListField("pdf", materiData.pdfs);

                splitter.Split();

                GUILayout.Space(5f);

                ShowTab("categories", materiData);
            }
            splitter.EndSplitView();
        }

        public void OpenTroubleshootGraph(Materi materiData)
        {
            if (!Directory.Exists(Application.dataPath + "/Content Files/Troubleshoot"))
                Directory.CreateDirectory(Application.dataPath + "/Content Files/Troubleshoot");

            if (materiData.troubleshootData.troubleshootGraph == null)
            {
                if (!File.Exists(Application.dataPath + "/Content Files/Troubleshoot/" + materiData.name + ".asset"))
                {
                    TroubleshootGraph troubleshoot = new TroubleshootGraph();
                    AssetDatabase.CreateAsset(troubleshoot, "Assets/Content Files/Troubleshoot/" + materiData.name + ".asset");
                    troubleshoot.name = materiData.name;
                    materiData.troubleshootData.troubleshootGraph = troubleshoot;
                    TroubleshootGraphEditor.SetMateri(ref materiData);
                }
                else
                {
                    index = 0;
                    CheckNameTroubleshoot(materiData.name, materiData);
                }
            }
            else
            {
                TroubleshootGraphEditor.SetMateri(ref materiData);
            }
        }

        void CheckNameTroubleshoot(string name, Materi materiData)
        {
            index++;
            name = name + index;
            if (!File.Exists(Application.dataPath + "/Content Files/Troubleshoot/" + name + ".asset"))
            {
                TroubleshootGraph troubleshoot = new TroubleshootGraph();
                AssetDatabase.CreateAsset(troubleshoot, "Assets/Content Files/Troubleshoot/" + name + ".asset");
                troubleshoot.name = name;
                materiData.troubleshootData.troubleshootGraph = troubleshoot;
                TroubleshootGraphEditor.SetMateri(ref materiData);
            }
            else
            {
                CheckNameTroubleshoot(name, materiData);
            }
        }

        void OpenAnimationTab(Materi materiData, string fieldId, int idx)
        {
            if (isAnimationInit == false)
            {
                animDatas.Clear();
                for (int l = 0; l < materiData.figures.Count; l++)
                {
                    if (materiData.figures[l].animationFigureDatas.Count > 0)
                    {
                        MateriViewData view = new MateriViewData(materiData.figures[l], materiData.figures, materiData.figures[l].animationFigureDatas, null);
                        view.isSequenceAnimation = materiData.figures[l].isSequenceAnimation;
                        animDatas.Add(view);
                    }
                }

                isAnimationInit = true;
            }
            ShowListField("animation", animDatas, fieldId + idx++);

            if (materiData.figures.Count != 0)
            {
                for (int i = 0; i < animDatas.Count; i++)
                {
                    //check data baru
                    if (animDatas[i].figure == null)
                    {
                        animDatas[i] = new MateriViewData(materiData.figures[0], materiData.figures, new List<AnimationFigureData>(), null);
                    }
                }

                for (int i = 0; i < materiData.figures.Count; i++)
                {
                    materiData.figures[i].animationFigureDatas.Clear();

                    for (int j = 0; j < animDatas.Count; j++)
                    {
                        string figureMateri = JsonUtility.ToJson(materiData.figures[i]);
                        string animDatas = JsonUtility.ToJson(this.animDatas[j].figure);

                        if (string.Equals(figureMateri, animDatas))
                        {
                            materiData.figures[i].isSequenceAnimation = this.animDatas[j].isSequenceAnimation;
                            for (int m = 0; m < this.animDatas[j].animations.Count; m++)
                            {
                                if (this.animDatas[j].isSequenceAnimation)
                                {
                                    if (this.animDatas[j].animations[m].isInit == false)
                                    {
                                        this.animDatas[j].animations[m].sequentialQueue = m;
                                        this.animDatas[j].animations[m].isInit = true;
                                    }
                                }
                                this.animDatas[j].animations[m].isSequential = this.animDatas[j].isSequenceAnimation;

                                materiData.figures[i].animationFigureDatas.Add(this.animDatas[j].animations[m]);
                            }
                        }
                    }
                }
            }
            else
            {
                animDatas.Clear();
            }
        }

        void OpenVirtualButtonTab(Materi materiData, string fieldId, int idx)
        {
            if (isVirtualButtonInit == false)
            {
                virtualButtonDatas.Clear();
                for (int l = 0; l < materiData.figures.Count; l++)
                {
                    if (materiData.figures[l].virtualButtonElement.Count > 0)
                    {
                        MateriViewData view = new MateriViewData(materiData.figures[l], materiData.figures, null, materiData.figures[l].virtualButtonElement);
                        virtualButtonDatas.Add(view);
                    }
                }

                isVirtualButtonInit = true;
            }
            ShowListField("virtual button", virtualButtonDatas, fieldId + idx++);

            if (materiData.figures.Count != 0)
            {
                for (int i = 0; i < virtualButtonDatas.Count; i++)
                {
                    if (virtualButtonDatas[i].figure == null)
                    {
                        virtualButtonDatas[i] = new MateriViewData(materiData.figures[0], materiData.figures, null, new List<VirtualButtonFigureData>());
                    }
                }

                for (int i = 0; i < materiData.figures.Count; i++)
                {
                    materiData.figures[i].virtualButtonElement.Clear();

                    for (int j = 0; j < virtualButtonDatas.Count; j++)
                    {
                        string figureMateri = JsonUtility.ToJson(materiData.figures[i]);
                        string animDatas = JsonUtility.ToJson(virtualButtonDatas[j].figure);
                        if (string.Equals(figureMateri, animDatas))
                        {
                            for (int m = 0; m < virtualButtonDatas[j].virtualButtonElements.Count; m++)
                            {
                                materiData.figures[i].virtualButtonElement.Add(virtualButtonDatas[j].virtualButtonElements[m]);
                            }
                        }
                    }
                }
            }
            else
            {
                virtualButtonDatas.Clear();
            }
        }

    }
}

#endif