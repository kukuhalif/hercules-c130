﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using VirtualTraining.Feature;

namespace VirtualTraining.Tools
{
    public static class CutawayEditor
    {
        public static void Show(Cutaway cutaway, PropertyField propertyField, string fieldId)
        {
            fieldId += "cutaway";
            int idx = 0;

            if (cutaway.state != CutawayState.None)
                GUILayout.BeginHorizontal();

            propertyField.ShowSimpleField("cutaway", fieldId + idx++, ref cutaway.state);

            if (cutaway.state != CutawayState.None)
            {
                if (!Application.isPlaying)
                {
                    GUILayout.Label("enter play mode to enable preview cutaway");
                }
                else
                {
                    GUILayout.BeginVertical();

                    if (GUILayout.Button("preview cutaway in object root bounds", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                    {
                        propertyField.TriggerBeforeModifiedCallback();

                        CutawayManager cutawayManager = GameObject.FindObjectOfType<CutawayManager>();
                        cutawayManager.BoundSetup();

                        cutaway.position = cutawayManager.transform.position;
                        cutaway.rotation = cutawayManager.transform.rotation.eulerAngles;
                        cutaway.scale = cutawayManager.transform.localScale;

                        EventManager.TriggerEvent(new CutawayEvent(cutaway));
                    }

                    if (GUILayout.Button("preview cutaway based on data", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                    {
                        EventManager.TriggerEvent(new CutawayEvent(cutaway));
                    }

                    if (GUILayout.Button("disable cutaway", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                    {
                        Cutaway newCutaway = new Cutaway();
                        newCutaway.state = CutawayState.None;
                        EventManager.TriggerEvent(new CutawayEvent(newCutaway));
                    }

                    if (GUILayout.Button("get position, rotation and scale", GUILayout.Width(VirtualTrainingEditorConfig.DEFAULT_SIMPLE_FIELD_WIDTH)))
                    {
                        CutawayManager cutawayManager = GameObject.FindObjectOfType<CutawayManager>();

                        propertyField.TriggerBeforeModifiedCallback();

                        cutaway.position = cutawayManager.transform.position;
                        cutaway.rotation = cutawayManager.transform.rotation.eulerAngles;
                        cutaway.scale = cutawayManager.transform.localScale;
                    }

                    GUILayout.EndVertical();
                }

                GUILayout.EndHorizontal();

                GUILayout.Space(10f);

                propertyField.ShowSimpleField("x position", fieldId + idx++, ref cutaway.position.x);
                propertyField.ShowSimpleField("y position", fieldId + idx++, ref cutaway.position.y);
                propertyField.ShowSimpleField("z position", fieldId + idx++, ref cutaway.position.z);

                GUILayout.Space(10f);

                propertyField.ShowSimpleField("x rotation", fieldId + idx++, ref cutaway.rotation.x);
                propertyField.ShowSimpleField("y rotation", fieldId + idx++, ref cutaway.rotation.y);
                propertyField.ShowSimpleField("z rotation", fieldId + idx++, ref cutaway.rotation.z);

                GUILayout.Space(10f);

                propertyField.ShowSimpleField("x scale", fieldId + idx++, ref cutaway.scale.x);
                propertyField.ShowSimpleField("y scale", fieldId + idx++, ref cutaway.scale.y);
                propertyField.ShowSimpleField("z scale", fieldId + idx++, ref cutaway.scale.z);
            }
        }
    }
}
#endif
