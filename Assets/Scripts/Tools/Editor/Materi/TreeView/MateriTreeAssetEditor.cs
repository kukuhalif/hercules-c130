#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(MateriTreeAsset))]
    public class MateriTreeAssetEditor : TreeAssetEditorBase<MateriTreeAsset, MateriTreeElement>
    {
        protected override MateriTreeAsset GetAsset()
        {
            return (MateriTreeAsset)target;
        }

        protected override List<MateriTreeElement> GetTreeElements()
        {
            return GetAsset().MateriData.treeElements;
        }
    }
}

#endif