#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
using Paroxe.PdfRenderer;
using System.IO;

namespace VirtualTraining.Tools
{
    class MateriEditorWindow : TreeViewEditorBase<MateriTreeAsset, MateriEditorWindow, MateriData, MateriTreeElement>
    {
        MateriContentEditor materiContentEditor;

        [MenuItem("Virtual Training/Materi Editor &m")]
        public static void GetWindow()
        {
            MateriEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            MateriEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                window.Initialize();
                return true;
            }
            return false;
        }

        public static MateriEditorWindow GetWindow(int instanceId, bool newWindow = false, bool alwaysInitialize = true)
        {
            MateriEditorWindow window = OpenEditorWindow(instanceId, newWindow);
            if (window != null)
            {
                if (alwaysInitialize || window.TreeView == null)
                {
                    window.Load();
                    window.Initialize();
                }
                return window;
            }
            return null;
        }

        protected override TreeViewWithTreeModel<MateriTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<MateriTreeElement>(ScriptableObjectTemp.MateriData.treeElements);
            return new TreeViewWithTreeModel<MateriTreeElement>(TreeViewState, treeModel);
        }

        protected override MateriTreeElement NewTreeElement(int depth, int id, int command)
        {
            switch ((MateriElementType)command)
            {
                case MateriElementType.Materi:

                    return new MateriTreeElement("new materi", depth, id, MateriElementType.Materi);

                case MateriElementType.Maintenance:

                    return new MateriTreeElement("new maintenance", depth, id, MateriElementType.Maintenance);

                case MateriElementType.Troubleshoot:

                    return new MateriTreeElement("new troubleshoot", depth, id, MateriElementType.Troubleshoot);

                default:

                    return new MateriTreeElement("new element", depth, id, MateriElementType.Materi);
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            List<PDFAsset> pdfAssets = new List<PDFAsset>();
            var configDatas = DatabaseManager.GetMateriConfigDatas();
            foreach (var configData in configDatas)
            {
                if (configData.materi == ScriptableObjectOriginalFile)
                {
                    pdfAssets = configData.pdfs;
                    break;
                }
            }
            var xrayMats = DatabaseManager.GetXrayMaterials();

            materiContentEditor = new MateriContentEditor(RecordUndo, WindowId, pdfAssets, xrayMats, InstanceId);
            TreeView.SetCustomContextMenuInsideElement(InsideElementContextMenuItem());
            TreeView.SetCustomContextMenuOutsideElement(OutsideElementContextMenuItem());
        }

        protected override void OnDisable()
        {
            if (materiContentEditor != null)
                materiContentEditor.SaveCameraDestination();

            base.OnDisable();
        }

        protected override void OnGUI()
        {
            base.OnGUI();
        }

        protected override bool UseTopLeftButton()
        {
            return true;
        }

        private void OpenDatabase(int elementId)
        {
            var databases = DatabaseManager.GetMateriAssets();
            for (int d = 0; d < databases.Count; d++)
            {
                var elements = databases[d].MateriData.treeElements;
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elementId == elements[i].id)
                    {
                        if (InstanceId != databases[d].GetInstanceID())
                        {
                            Close();
                            var window = GetWindow(databases[d].GetInstanceID());
                            window.TreeView.SetSelection(new List<int> { elementId });
                        }
                        else
                        {
                            TreeView.SetSelection(new List<int> { elementId });
                        }
                        break;
                    }
                }
            }
        }

        protected override void TopLeftButtonAction()
        {
            var materiAssets = DatabaseManager.GetMateriAssets();
            var materiTree = DatabaseManager.GetMateriTree();

            List<MateriTreeElement> materiList = new List<MateriTreeElement>();
            TreeElementUtility.TreeToList(materiTree, materiList);

            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("edit materi config"), false, () =>
            {
                VirtualTrainingMateriConfigEditor.ShowMateriConfig();
            });

            for (int i = 0; i < materiAssets.Count; i++)
            {
                MateriTreeAsset asset = materiAssets[i];
                if (InstanceId != asset.GetInstanceID())
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " open -> " + asset.name), false, () =>
                    {
                        Close();
                        GetWindow(asset.GetInstanceID());
                    });
                }
                else
                {
                    menu.AddItem(new GUIContent("other databases/" + (i + 1) + " current materi -> " + asset.name), false, null);
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("all content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Materi)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("general content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Maintenance)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("maintenance content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            for (int i = 0; i < materiList.Count; i++)
            {
                string name = materiList[i].CompleteName;

                if (!string.IsNullOrEmpty(name) && materiList[i].depth > -1 && materiList[i].MateriData.elementType == MateriElementType.Troubleshoot)
                {
                    int elementId = materiList[i].id;
                    menu.AddItem(new GUIContent("troubleshoot content/" + name), false, () =>
                    {
                        OpenDatabase(elementId);
                    });
                }
            }

            menu.ShowAsContext();
        }

        private GenericMenu InsideElementContextMenuItem()
        {
            GenericMenu menu = CreateContextItems(false);
            menu.AddItem(new GUIContent("copy"), false, () =>
            {
                TreeView.CopyItems();
            });
            menu.AddItem(new GUIContent("paste"), false, () =>
            {
                TreeView.PasteItems();
            });
            menu.AddItem(new GUIContent("duplicate"), false, () =>
            {
                TreeView.DuplicateItems();
            });
            menu.AddItem(new GUIContent("remove"), false, () =>
            {
                TreeView.RemoveItem();
            });

            return menu;
        }

        private GenericMenu OutsideElementContextMenuItem()
        {
            return CreateContextItems(true);
        }

        private GenericMenu CreateContextItems(bool paste)
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("create new folder"), false, () =>
            {
                TreeView.CreateNewItem((int)MateriElementType.Materi, true);
            });
            menu.AddItem(new GUIContent("create new/general content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Materi);
             });
            menu.AddItem(new GUIContent("create new/maintenance content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Maintenance);
             });
            menu.AddItem(new GUIContent("create new/troubleshoot content"), false, () =>
             {
                 TreeView.CreateNewItem((int)MateriElementType.Troubleshoot);
             });
            if (paste)
            {
                menu.AddItem(new GUIContent("paste"), false, () =>
                {
                    TreeView.PasteItems();
                });
            }

            return menu;
        }

        protected override void Save()
        {
            base.Save();
            CheckTroubleshootGraph();
        }

        protected override void ContentView(List<MateriTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                    materiContentEditor.DetailInspector(selectedElements[0], detailPanelRect);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        materiContentEditor.PreviewInspector(selectedElements[i].MateriData, i);
                }
            }

            if (GUI.GetNameOfFocusedControl() == "materi name" || GUI.GetNameOfFocusedControl() == "troubleshoot name" || GUI.GetNameOfFocusedControl() == "maintenance name")
            {
                // rename element from materi name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].MateriData.name)
                    {
                        selectedElements[i].name = selectedElements[i].MateriData.name;
                        TreeView.Reload();
                    }
                }
            }
            else
            {
                // rename materi name from element name
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (selectedElements[i].name != selectedElements[i].MateriData.name)
                    {
                        selectedElements[i].MateriData.name = selectedElements[i].name;
                        TreeView.Reload();
                    }
                }
            }

            EditorGUILayout.EndVertical();
        }

        public void SetScriptableObjectMaintenance(MaintenanceData maintenanceData, int materiTreeId)
        {
            for (int i = 0; i < ScriptableObjectTemp.MateriData.treeElements.Count; i++)
            {
                if (ScriptableObjectTemp.MateriData.treeElements[i].id == materiTreeId)
                {
                    ScriptableObjectTemp.MateriData.treeElements[i].MateriData.maintenanceData = maintenanceData;
                    Save();
                    return;
                }
            }

            if (EditorUtility.DisplayDialog("warning", "materi for this maintenance is not found, create new maintenance materi for this ?", "yes", "no"))
            {
                TreeView.CreateNewItem((int)MateriElementType.Maintenance);
                var selecteds = TreeView.GetSelection();
                var newMaintenance = TreeView.FindElement(selecteds[0]);
                newMaintenance.name = "new missing maintenance";
                newMaintenance.MateriData.maintenanceData = maintenanceData;
                Save();
            }
        }

        void CheckTroubleshootGraph()
        {
            if (!Directory.Exists(Application.dataPath + "/Content Files/Troubleshoot"))
                Directory.CreateDirectory(Application.dataPath + "/Content Files/Troubleshoot");

            string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + "Content Files/Troubleshoot");

            foreach (string fileName in fileEntries)
            {
                int index = fileName.LastIndexOf("/");
                string localPath = "Assets/" + "Content Files";

                if (index > 0)
                    localPath += fileName.Substring(index);

                List<MateriContainer> config = DatabaseManager.GetMateriConfigDatas();
                TroubleshootGraph t = AssetDatabase.LoadAssetAtPath(localPath, typeof(TroubleshootGraph)) as TroubleshootGraph;
                if (t != null)
                {
                    int x = 0;
                    for (int i = 0; i < config.Count; i++)
                    {
                        for (int j = 0; j < config[i].materi.MateriData.treeElements.Count; j++)
                        {
                            if (t == config[i].materi.MateriData.treeElements[j].MateriData.troubleshootData.troubleshootGraph)
                            {
                                x = 1;
                                break;
                            }
                        }
                    }

                    if (x == 0)
                    {
                        AssetDatabase.DeleteAsset(localPath);
                    }
                    x = 0;
                }
            }
        }
    }
}

#endif