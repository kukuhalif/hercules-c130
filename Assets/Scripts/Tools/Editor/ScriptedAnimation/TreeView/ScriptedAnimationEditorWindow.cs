﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using VirtualTraining.Core;
namespace VirtualTraining.Tools
{
    public class ScriptedAnimationEditorWindow : TreeViewEditorBase<ScriptedAnimationTreeAsset, ScriptedAnimationEditorWindow, ScriptedAnimationData, ScriptedAnimationTreeElement>
    {
        ScriptedAnimationContentEditor scriptedAnimationContentEditor;

        [MenuItem("Virtual Training/Scripted Animation Editor &a")]
        public static void GetWindow()
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.Load();
                window.Initialize();
            }
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Load();
                window.Initialize();
                return true;
            }
            return false;
        }

        public static void GetWindow(int instanceID)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow(instanceID);
            if (window != null)
            {
                window.Save();
                window.Load();
                window.Initialize();
            }
        }

        public static void SelectItem(int id)
        {
            ScriptedAnimationEditorWindow window = OpenEditorWindow();
            if (window != null)
            {
                window.TreeView.SetSelection(new List<int> { id });
                window.TreeView.SetFocusAndEnsureSelectedItem();
            }
        }

        protected override TreeViewWithTreeModel<ScriptedAnimationTreeElement> CreateNewTreeView()
        {
            var treeModel = new TreeModel<ScriptedAnimationTreeElement>(ScriptableObjectTemp.ScriptedAnimationData.treeElements);
            return new TreeViewWithTreeModel<ScriptedAnimationTreeElement>(TreeViewState, treeModel);
        }

        protected override ScriptedAnimationTreeElement NewTreeElement(int depth, int id, int command)
        {
            return new ScriptedAnimationTreeElement("new scripted animation", depth, id);
        }


        protected override void Initialize()
        {
            if (scriptedAnimationContentEditor != null)
                scriptedAnimationContentEditor.ResetAllTransform();

            base.Initialize();

            scriptedAnimationContentEditor = new ScriptedAnimationContentEditor(RecordUndo, WindowId);
        }

        protected override void OnDisable()
        {
            if (scriptedAnimationContentEditor != null)
                scriptedAnimationContentEditor.ResetAllTransform();
            base.OnDisable();
        }

        protected override void OnGUI()
        {
            base.OnGUI();
        }

        protected override void ContentView(List<ScriptedAnimationTreeElement> selectedElements)
        {
            EditorGUILayout.BeginVertical();
            if (selectedElements.Count == 1)
            {
                if (!selectedElements[0].isFolder)
                    scriptedAnimationContentEditor.ShowContent(selectedElements[0].data, 0);
            }
            else if (selectedElements.Count > 1)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    if (!selectedElements[0].isFolder)
                        scriptedAnimationContentEditor.ShowContent(selectedElements[i].data, i);
                }
            }

            EditorGUILayout.EndVertical();
        }

    }
}

#endif
