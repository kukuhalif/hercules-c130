﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    [CustomEditor(typeof(ScriptedAnimationTreeAsset))]
    public class ScriptedAnimationTreeAssetEditor : TreeAssetEditorBase<ScriptedAnimationTreeAsset, ScriptedAnimationTreeElement>
    {
        protected override ScriptedAnimationTreeAsset GetAsset()
        {
            return (ScriptedAnimationTreeAsset)target;
        }

        protected override List<ScriptedAnimationTreeElement> GetTreeElements()
        {
            return GetAsset().ScriptedAnimationData.treeElements;
        }
    }
}
#endif
