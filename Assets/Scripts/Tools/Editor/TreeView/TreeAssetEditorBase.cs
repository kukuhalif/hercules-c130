﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public abstract class TreeAssetEditorBase<A, E> : Editor
        where A : ScriptableObject
        where E : TreeElement
    {
        MyTreeView treeView;
        SearchField searchField;
        const string kSessionStateKeyPrefix = "TVS";

        protected abstract A GetAsset();

        protected abstract List<E> GetTreeElements();

        void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedoPerformed;

            var treeViewState = new TreeViewState();
            var jsonState = SessionState.GetString(kSessionStateKeyPrefix + GetAsset().GetInstanceID(), "");
            if (!string.IsNullOrEmpty(jsonState))
                JsonUtility.FromJsonOverwrite(jsonState, treeViewState);
            var treeModel = new TreeModel<E>(GetTreeElements());
            treeView = new MyTreeView(treeViewState, treeModel);
            treeView.BeforeDroppingDraggedItemsCallback += OnBeforeDroppingDraggedItems;
            treeView.Reload();

            searchField = new SearchField();

            searchField.downOrUpArrowKeyPressed += treeView.SetFocusAndEnsureSelectedItem;
        }


        void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedoPerformed;
            if (GetAsset() != null && treeView != null)
                SessionState.SetString(kSessionStateKeyPrefix + GetAsset().GetInstanceID(), JsonUtility.ToJson(treeView.state));
        }

        void OnUndoRedoPerformed()
        {
            if (treeView != null)
            {
                treeView.TreeModel.SetData(GetTreeElements());
                treeView.Reload();
            }
        }

        void OnBeforeDroppingDraggedItems(IList<TreeViewItem> draggedRows)
        {
            Undo.RecordObject(GetAsset(), string.Format("Moving {0} Item{1}", draggedRows.Count, draggedRows.Count > 1 ? "s" : ""));
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Space(5f);
            ToolBar();
            GUILayout.Space(3f);

            const float topToolbarHeight = 20f;
            const float spacing = 2f;
            float totalHeight = treeView.totalHeight + topToolbarHeight + 2 * spacing;
            Rect rect = GUILayoutUtility.GetRect(0, 10000, 0, totalHeight);
            Rect toolbarRect = new Rect(rect.x, rect.y, rect.width, topToolbarHeight);
            Rect multiColumnTreeViewRect = new Rect(rect.x, rect.y + topToolbarHeight + spacing, rect.width, rect.height - topToolbarHeight - 2 * spacing);
            SearchBar(toolbarRect);
            DoTreeView(multiColumnTreeViewRect);
        }

        void SearchBar(Rect rect)
        {
            treeView.searchString = searchField.OnGUI(rect, treeView.searchString);
        }

        void DoTreeView(Rect rect)
        {
            treeView.OnGUI(rect);
        }

        void ToolBar()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                var style = "miniButton";
                if (GUILayout.Button("Expand All", style))
                {
                    treeView.ExpandAll();
                }

                if (GUILayout.Button("Collapse All", style))
                {
                    treeView.CollapseAll();
                }

                GUILayout.FlexibleSpace();
            }
        }


        class MyTreeView : TreeViewWithTreeModel<E>
        {
            public MyTreeView(TreeViewState state, TreeModel<E> model)
                : base(state, model)
            {
                showBorder = true;
                showAlternatingRowBackgrounds = true;
            }
        }
    }
}

#endif