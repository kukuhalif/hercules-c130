﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public abstract class TreeViewEditorBase<S, W, T, E> : EditorWindowDatabaseBase<S, W, T>
        where S : ScriptableObjectBase<T>
        where W : EditorWindow
        where E : TreeElement
    {
        EditorGUISplitView horizontalSplitView;
        [SerializeField] private SearchField searchField;
        [SerializeField] private TreeViewWithTreeModel<E> treeView;
        [SerializeField] private TreeViewState treeViewState;
        private bool savePressed, loadPressed, deletePressed, undoPressed, redoPressed;
        private Vector2 contentViewScrollPosition = new Vector2();

        protected TreeViewWithTreeModel<E> TreeView { get => treeView; }

        public TreeViewEditorBase()
        {
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }
        ~TreeViewEditorBase()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
        }

        protected abstract TreeViewWithTreeModel<E> CreateNewTreeView();
        protected abstract E NewTreeElement(int depth, int id, int command);
        protected TreeViewState TreeViewState
        {
            get
            {
                var state = VirtualTrainingEditorState.GetTreeViewState(WindowId);
                return state;
            }
        }

        protected virtual void Initialize()
        {
            // if scriptable object is removed
            if (ScriptableObjectOriginalFile == null)
                Close();

            horizontalSplitView = new EditorGUISplitView(EditorGUISplitView.Direction.Horizontal, typeof(S).ToString());

            treeView = CreateNewTreeView();

            TreeView.SetGenerateElementIdCallback(GenerateElementIdCallback);
            TreeView.NewElement = NewTreeElement;

            searchField = new SearchField();
            searchField.downOrUpArrowKeyPressed += TreeView.SetFocusAndEnsureSelectedItem;

            TreeView.Reload();

            TreeView.BeforeTreeChangedCallback += BeforeTreeChanged;
            TreeView.BeforeNameChangedCallback += TreeViewBeforeNameChanged;
            TreeView.SelectionChangedCallback += OnSelectionChanged;
            TreeView.StartDragCallback += OnStartDrag;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            VirtualTrainingEditorState.SaveStates(WindowId);
        }

        private void PlayModeStateChanged(PlayModeStateChange obj)
        {
            Initialize();
            OnDisable();
        }

        private void TreeViewBeforeNameChanged()
        {
            RecordUndo();
        }

        private void BeforeTreeChanged()
        {
            RecordUndo();
        }

        private int GenerateElementIdCallback()
        {
            int id = 0;
            do
            {
                int instanceId = GetInstanceID();
                int elementCount = TreeView.TreeModel.ElementCount;
                int random = UnityEngine.Random.Range(-10000, 10000);

                string st = instanceId.ToString() + elementCount.ToString() + random.ToString();
                id = Animator.StringToHash(st);
            }
            while (!treeView.IsIdValid(id));

            return id;
        }

        protected virtual void OnSelectionChanged()
        {
            VirtualTrainingEditorState.ClearDraggedId();
            var selections = TreeView.GetSelection();
            if (selections.Count > 0)
            {
                E element = TreeView.FindElement(selections[0]);
                VirtualTrainingEditorState.SetDraggedId(element.id);
            }
        }

        protected virtual void OnStartDrag(List<TreeViewItem> items)
        {
            VirtualTrainingEditorState.ClearDraggedId();
            if (items.Count >= 1)
                VirtualTrainingEditorState.SetDraggedId(items[0].id);
        }

        Rect toolbarRect
        {
            get { return new Rect(20f, 10f, horizontalSplitView.GetSplitPosition() - 10f, 20f); }
        }

        Rect searchRect
        {
            get { return new Rect(55f, 10f, horizontalSplitView.GetSplitPosition() - 45f, 20f); }
        }

        Rect fullSearchRect
        {
            get { return new Rect(20f, 10f, horizontalSplitView.GetSplitPosition() - 10f, 20f); }
        }

        Rect configRect
        {
            get { return new Rect(20f, 8f, 30f, 30f); }
        }

        Rect treeViewRect
        {
            get { return new Rect(20, 40, horizontalSplitView.GetSplitPosition() - 25f, position.height - 60); }
        }

        Rect bottomToolbarRect
        {
            get { return new Rect(20f, position.height - 18f, position.width - 40f, 16f); }
        }

        protected Rect detailPanelRect
        {
            get { return new Rect(0f, 0f, position.width - horizontalSplitView.GetSplitPosition(), position.height - 30f); }
        }

        void SearchBar(Rect rect)
        {
            if (TreeView != null && searchField != null)
                TreeView.searchString = searchField.OnGUI(rect, TreeView.searchString);
        }

        void DoTreeView(Rect rect)
        {
            if (TreeView != null)
                TreeView.OnGUI(rect);
        }

        void BottomToolBar(Rect rect)
        {
            GUILayout.BeginArea(rect);

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Expand All", EditorStyles.miniButton))
                {
                    TreeView.ExpandAll();
                }

                if (GUILayout.Button("Collapse All", EditorStyles.miniButton))
                {
                    TreeView.CollapseAll();
                }

                if (GUILayout.Button("Create new", EditorStyles.miniButton))
                {
                    TreeView.CreateNewItem();
                }

                if (GUILayout.Button("Copy", EditorStyles.miniButton))
                {
                    TreeView.CopyItems();
                }

                if (GUILayout.Button("Paste", EditorStyles.miniButton))
                {
                    TreeView.PasteItems();
                }

                if (GUILayout.Button("Duplicate", EditorStyles.miniButton))
                {
                    TreeView.DuplicateItems();
                }

                if (deletePressed || GUILayout.Button("Remove", EditorStyles.miniButton))
                {
                    deletePressed = false;
                    TreeView.RemoveItem();
                }

                GUILayout.FlexibleSpace();

                // undo

                if (UndoCount() == 0)
                {
                    undoPressed = false;
                    EditorGUI.BeginDisabledGroup(true);
                }

                if (undoPressed || GUILayout.Button("Undo (alt + Z) " + (UndoCount() > 0 ? UndoCount().ToString() : ""), EditorStyles.miniButton))
                {
                    undoPressed = false;
                    UndoAction();
                    Initialize();
                }

                if (UndoCount() == 0)
                    EditorGUI.EndDisabledGroup();

                // redo

                if (RedoCount() == 0)
                {
                    redoPressed = false;
                    EditorGUI.BeginDisabledGroup(true);
                }

                if (redoPressed || GUILayout.Button("Redo (alt + Y) " + (RedoCount() > 0 ? RedoCount().ToString() : ""), EditorStyles.miniButton))
                {
                    redoPressed = false;
                    RedoAction();
                    Initialize();
                }

                if (RedoCount() == 0)
                    EditorGUI.EndDisabledGroup();

                // clear undo / redo

                if (UndoCount() == 0 && RedoCount() == 0)
                    EditorGUI.BeginDisabledGroup(true);

                if (GUILayout.Button("Clear undo/redo", EditorStyles.miniButton))
                {
                    ClearUndoRedoAction();
                }

                if (UndoCount() == 0 && RedoCount() == 0)
                    EditorGUI.EndDisabledGroup();

                // load

                if (loadPressed || GUILayout.Button("Load (alt + L)", EditorStyles.miniButton))
                {
                    loadPressed = false;
                    if (EditorUtility.DisplayDialog("load", "load ?", "yes", "no"))
                    {
                        Load();
                        Initialize();
                    }
                }

                if (savePressed || GUILayout.Button("Save (alt + S)", EditorStyles.miniButton))
                {
                    savePressed = false;
                    if (EditorUtility.DisplayDialog("save", "save ?", "yes", "no"))
                        Save();
                }
            }

            GUILayout.EndArea();
        }

        void ShortcutInput()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.KeyDown:

                    if (e.alt)
                    {
                        if (e.keyCode == KeyCode.L)
                        {
                            loadPressed = true;
                            e.Use();
                        }
                        else if (e.keyCode == KeyCode.S)
                        {
                            savePressed = true;
                            e.Use();
                        }
                        else if (e.keyCode == KeyCode.Z)
                        {
                            undoPressed = true;
                            e.Use();
                        }
                        else if (e.keyCode == KeyCode.Y)
                        {
                            redoPressed = true;
                            e.Use();
                        }
                    }
                    else if (TreeView.HasFocus() && e.keyCode == KeyCode.Delete)
                    {
                        deletePressed = true;
                        e.Use();
                    }

                    break;
            }
        }

        List<E> GetSelectedElements()
        {
            if (TreeView == null)
                return new List<E>();

            List<E> elements = new List<E>();
            var selections = TreeView.GetSelection();
            for (int i = 0; i < selections.Count; i++)
            {
                E element = TreeView.TreeModel.Find(selections[i]);
                if (element != null)
                    elements.Add(element);
            }
            return elements;
        }

        void TopLeftButton()
        {
            if (PropertyField.DrawButton(configRect, IconEnum.PathMateri))
            {
                TopLeftButtonAction();
            }
            PropertyField.DrawTooltip(configRect, "show materi path", 150f, new Vector2(0, configRect.height / 2f));
        }

        protected virtual bool UseTopLeftButton()
        {
            return false;
        }

        protected virtual void TopLeftButtonAction()
        {

        }

        protected virtual void OnGUI()
        {
            if (horizontalSplitView == null)
                return;

            ShortcutInput();
            horizontalSplitView.BeginSplitView(toolbarRect.y, toolbarRect.height + treeViewRect.height);
            if (UseTopLeftButton())
            {
                TopLeftButton();
                SearchBar(searchRect);
            }
            else
            {
                SearchBar(fullSearchRect);
            }
            DoTreeView(treeViewRect);
            horizontalSplitView.Split();
            GUILayout.BeginArea(detailPanelRect);
            contentViewScrollPosition = EditorGUILayout.BeginScrollView(contentViewScrollPosition);
            ContentView(GetSelectedElements());
            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();
            horizontalSplitView.EndSplitView();
            BottomToolBar(bottomToolbarRect);
            Repaint();
        }

        protected virtual void ContentView(List<E> selectedElements)
        {

        }
    }
}

#endif