#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using VirtualTraining.Core;

namespace VirtualTraining.Tools
{
    public class TreeViewItem<T> : TreeViewItem where T : TreeElement
    {
        public T data { get; set; }

        public TreeViewItem(int id, int depth, string displayName, T data) : base(id, depth, displayName)
        {
            this.data = data;
        }
    }

    public class TreeViewWithTreeModel<T> : UnityEditor.IMGUI.Controls.TreeView where T : TreeElement
    {
        TreeModel<T> treeModel;
        readonly List<TreeViewItem> rows = new List<TreeViewItem>(100);
        private event Action beforeTreeChangedCallback;
        private event Action treeChangedCallback;
        private event Action beforeNameChangedCallback;
        private event Action selectionChangedCallback;
        private event Action<List<TreeViewItem>> startDragCallback;

        GenericMenu contextMenuInsideElement = null;
        GenericMenu contextMenuOutsideElement = null;

        public TreeModel<T> TreeModel { get { return treeModel; } }

        public Func<int, int, int, T> NewElement { set => newElement = value; }

        private event Action<IList<TreeViewItem>> beforeDroppingDraggedItemsCallback;

        Func<int, int, int, T> newElement;

        public Action BeforeTreeChangedCallback { get => beforeTreeChangedCallback; set => beforeTreeChangedCallback = value; }
        public Action TreeChangedCallback { get => treeChangedCallback; set => treeChangedCallback = value; }
        public Action BeforeNameChangedCallback { get => beforeNameChangedCallback; set => beforeNameChangedCallback = value; }
        public Action SelectionChangedCallback { get => selectionChangedCallback; set => selectionChangedCallback = value; }
        public Action<List<TreeViewItem>> StartDragCallback { get => startDragCallback; set => startDragCallback = value; }
        public Action<IList<TreeViewItem>> BeforeDroppingDraggedItemsCallback { get => beforeDroppingDraggedItemsCallback; set => beforeDroppingDraggedItemsCallback = value; }

        public TreeViewWithTreeModel(TreeViewState state, TreeModel<T> model) : base(state)
        {
            showBorder = true;
            showAlternatingRowBackgrounds = true;
            Init(model);
        }

        public TreeViewWithTreeModel(TreeViewState state, MultiColumnHeader multiColumnHeader, TreeModel<T> model)
            : base(state, multiColumnHeader)
        {
            Init(model);
        }

        public void CreateNewItem(int command = -1, bool isFolderElement = false)
        {
            int id = TreeModel.GenerateUniqueID();
            var selection = GetSelection();

            if (selection.Count == 1)
            {
                TreeElement parent = TreeModel.Find(selection[0]);
                int depth = parent != null ? parent.depth : 0;
                var element = newElement.Invoke(depth, id, command);
                element.isFolder = isFolderElement;
                if (isFolderElement)
                    element.name = "new folder";
                TreeModel.AddElement(element, parent == null ? null : parent.parent, int.MaxValue);
            }
            else
            {
                TreeElement parent = TreeModel.Root;
                int depth = parent != null ? parent.depth + 1 : 0;
                var element = newElement.Invoke(depth, id, command);
                element.isFolder = isFolderElement;
                if (isFolderElement)
                    element.name = "new folder";
                TreeModel.AddElement(element, parent, int.MaxValue);
            }

            // Select newly created element
            SetSelection(new[] { id }, TreeViewSelectionOptions.RevealAndFrame);
        }

        public void SetGenerateElementIdCallback(Func<int> generateElementIdCallback)
        {
            treeModel.SetGenerateElementIdCallback(generateElementIdCallback);
        }

        public bool IsIdValid(int id)
        {
            return treeModel.IsIdValid(id);
        }

        public T FindElement(int id)
        {
            return TreeModel.Find(id);
        }

        public void RemoveItem()
        {
            var selection = GetSelection();
            TreeModel.RemoveElements(selection);
        }

        public void CopyItems()
        {
            var selections = GetSelection();
            List<object> toDuplicates = new List<object>();
            foreach (var selection in selections)
            {
                var treeItem = TreeModel.Find(selection);
                toDuplicates.Add(treeItem);
            }
            VirtualTrainingEditorState.SetTreeElementClipboard(toDuplicates);
        }

        public void PasteItems()
        {
            var newDatas = VirtualTrainingEditorState.GetTreeElementClipboard<T>();

            var selection = GetSelection();

            List<T> itemToDuplicates = VirtualTrainingEditorState.GetTreeElementClipboard<T>();

            List<int> newItems = new List<int>();

            foreach (var item in itemToDuplicates)
            {
                T itemToDuplicate = item;

                int id = TreeModel.GenerateUniqueID();
                TreeElement parent = FindElement(rootItem.id);
                int depth = parent != null ? parent.depth + 1 : 0;
                var newElement = this.newElement.Invoke(depth, id, -1);
                newElement.name = itemToDuplicate.name + " (pasted)";
                string jsonData = itemToDuplicate.GetDuplicateJsonData();
                newElement.SetDuplicatedJsonData(jsonData);
                TreeModel.AddElement(newElement, parent, int.MaxValue);
                newItems.Add(id);
            }

            // Select newly created element
            SetSelection(newItems, TreeViewSelectionOptions.RevealAndFrame);
        }

        public void DuplicateItems()
        {
            var selection = GetSelection();
            List<int> itemToDuplicates = new List<int>();
            foreach (var selected in selection)
            {
                if (!itemToDuplicates.Contains(selected))
                    itemToDuplicates.Add(selected);

                GetAllChilds(selected, itemToDuplicates);
            }

            List<int> newItems = new List<int>();

            foreach (var item in itemToDuplicates)
            {
                T itemToDuplicate = TreeModel.Find(item);

                int id = TreeModel.GenerateUniqueID();
                TreeElement parent = itemToDuplicate.parent;
                int depth = parent != null ? parent.depth + 1 : 0;
                var newElement = this.newElement.Invoke(depth, id, -1);
                string oldName = itemToDuplicate.name;
                oldName = itemToDuplicate.name.Replace(" (duplicated)", "");
                newElement.name = oldName + " (duplicated)";
                string jsonData = itemToDuplicate.GetDuplicateJsonData();
                newElement.SetDuplicatedJsonData(jsonData);
                TreeModel.AddElement(newElement, parent, int.MaxValue);
                newItems.Add(id);
            }

            // Select newly created element
            SetSelection(newItems, TreeViewSelectionOptions.RevealAndFrame);
        }

        void GetAllChilds(int currentParent, List<int> childs)
        {
            T parent = TreeModel.Find(currentParent);

            if (parent != null && parent.children != null)
            {
                foreach (var child in parent.children)
                {
                    if (!childs.Contains(child.id))
                    {
                        childs.Add(child.id);
                    }

                    GetAllChilds(child.id, childs);
                }
            }
        }

        void Init(TreeModel<T> model)
        {
            treeModel = model;
            treeModel.beforeModelChanged += BeforeModelChanged;
            treeModel.modelChanged += ModelChanged;
        }

        void BeforeModelChanged()
        {
            if (beforeTreeChangedCallback != null)
                beforeTreeChangedCallback();
        }
        void ModelChanged()
        {
            if (treeChangedCallback != null)
                treeChangedCallback();

            Reload();
        }

        protected override TreeViewItem BuildRoot()
        {
            int depthForHiddenRoot = -1;
            return new TreeViewItem<T>(treeModel.Root.id, depthForHiddenRoot, treeModel.Root.name, treeModel.Root);
        }

        protected override IList<TreeViewItem> BuildRows(TreeViewItem root)
        {
            if (treeModel.Root == null)
            {
                Debug.LogError("tree model root is null. did you call SetData()?");
            }

            rows.Clear();
            if (!string.IsNullOrEmpty(searchString))
            {
                Search(treeModel.Root, searchString, rows);
            }
            else
            {
                if (treeModel.Root.hasChildren)
                    AddChildrenRecursive(treeModel.Root, 0, rows);
            }

            // We still need to setup the child parent information for the rows since this 
            // information is used by the TreeView internal logic (navigation, dragging etc)
            SetupParentsAndChildrenFromDepths(root, rows);

            return rows;
        }

        void AddChildrenRecursive(T parent, int depth, IList<TreeViewItem> newRows)
        {
            foreach (T child in parent.children)
            {
                var item = new TreeViewItem<T>(child.id, depth, child.name, child);
                newRows.Add(item);

                if (child.hasChildren)
                {
                    if (IsExpanded(child.id))
                    {
                        AddChildrenRecursive(child, depth + 1, newRows);
                    }
                    else
                    {
                        item.children = CreateChildListForCollapsedParent();
                    }
                }
            }
        }

        void Search(T searchFromThis, string search, List<TreeViewItem> result)
        {
            if (string.IsNullOrEmpty(search))
                throw new ArgumentException("Invalid search: cannot be null or empty", "search");

            const int kItemDepth = 0; // tree is flattened when searching

            Stack<T> stack = new Stack<T>();
            foreach (var element in searchFromThis.children)
                stack.Push((T)element);
            while (stack.Count > 0)
            {
                T current = stack.Pop();
                // Matches search?
                if (current.name.IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    result.Add(new TreeViewItem<T>(current.id, kItemDepth, current.name, current));
                }

                if (current.children != null && current.children.Count > 0)
                {
                    foreach (var element in current.children)
                    {
                        stack.Push((T)element);
                    }
                }
            }
            SortSearchResult(result);
        }

        protected virtual void SortSearchResult(List<TreeViewItem> rows)
        {
            rows.Sort((x, y) => EditorUtility.NaturalCompare(x.displayName, y.displayName)); // sort by displayName by default, can be overriden for multicolumn solutions
        }

        protected override IList<int> GetAncestors(int id)
        {
            return treeModel.GetAncestors(id);
        }

        protected override IList<int> GetDescendantsThatHaveChildren(int id)
        {
            return treeModel.GetDescendantsThatHaveChildren(id);
        }


        // Dragging
        //-----------

        const string k_GenericDragID = "GenericDragColumnDragging";

        protected override bool CanStartDrag(CanStartDragArgs args)
        {
            return true;
        }

        protected override void SetupDragAndDrop(SetupDragAndDropArgs args)
        {
            if (hasSearch)
                return;

            DragAndDrop.PrepareStartDrag();
            var draggedRows = GetRows().Where(item => args.draggedItemIDs.Contains(item.id)).ToList();
            DragAndDrop.SetGenericData(k_GenericDragID, draggedRows);
            DragAndDrop.objectReferences = new UnityEngine.Object[] { }; // this IS required for dragging to work
            string title = draggedRows.Count == 1 ? draggedRows[0].displayName : "< Multiple >";
            DragAndDrop.StartDrag(title);

            if(startDragCallback != null)
                startDragCallback.Invoke(draggedRows);
        }

        protected override DragAndDropVisualMode HandleDragAndDrop(DragAndDropArgs args)
        {
            // Check if we can handle the current drag data (could be dragged in from other areas/windows in the editor)
            var draggedRows = DragAndDrop.GetGenericData(k_GenericDragID) as List<TreeViewItem>;
            if (draggedRows == null)
                return DragAndDropVisualMode.None;

            // Parent item is null when dragging outside any tree view items.
            switch (args.dragAndDropPosition)
            {
                case DragAndDropPosition.UponItem:
                case DragAndDropPosition.BetweenItems:
                    {
                        bool validDrag = ValidDrag(args.parentItem, draggedRows);
                        if (args.performDrop && validDrag)
                        {
                            T parentData = ((TreeViewItem<T>)args.parentItem).data;
                            OnDropDraggedElementsAtIndex(draggedRows, parentData, args.insertAtIndex == -1 ? 0 : args.insertAtIndex);
                        }
                        return validDrag ? DragAndDropVisualMode.Move : DragAndDropVisualMode.None;
                    }

                case DragAndDropPosition.OutsideItems:
                    {
                        if (args.performDrop)
                            OnDropDraggedElementsAtIndex(draggedRows, treeModel.Root, treeModel.Root.children.Count);

                        return DragAndDropVisualMode.Move;
                    }
                default:
                    Debug.LogError("Unhandled enum " + args.dragAndDropPosition);
                    return DragAndDropVisualMode.None;
            }
        }

        public virtual void OnDropDraggedElementsAtIndex(List<TreeViewItem> draggedRows, T parent, int insertIndex)
        {
            if (beforeDroppingDraggedItemsCallback != null)
                beforeDroppingDraggedItemsCallback(draggedRows);

            var draggedElements = new List<TreeElement>();
            foreach (var x in draggedRows)
                draggedElements.Add(((TreeViewItem<T>)x).data);

            var selectedIDs = draggedElements.Select(x => x.id).ToArray();
            treeModel.MoveElements(parent, insertIndex, draggedElements);
            SetSelection(selectedIDs, TreeViewSelectionOptions.RevealAndFrame);
        }


        bool ValidDrag(TreeViewItem parent, List<TreeViewItem> draggedItems)
        {
            TreeViewItem currentParent = parent;
            while (currentParent != null)
            {
                if (draggedItems.Contains(currentParent))
                    return false;
                currentParent = currentParent.parent;
            }
            return true;
        }

        protected override bool CanRename(TreeViewItem item)
        {
            return true;
        }

        protected override void RenameEnded(RenameEndedArgs args)
        {
            if (beforeNameChangedCallback != null)
                beforeNameChangedCallback();

            if (args.acceptedRename)
            {
                var element = TreeModel.Find(args.itemID);
                element.name = args.newName;
                Reload();
            }
        }

        protected override void ContextClickedItem(int id)
        {
            base.ContextClickedItem(id);
            Event.current.Use();

            if (contextMenuInsideElement == null)
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("create folder"), false, () =>
                {
                    CreateNewItem(-1, true);
                });
                menu.AddItem(new GUIContent("create new"), false, () =>
                {
                    CreateNewItem();
                });
                menu.AddItem(new GUIContent("copy"), false, () =>
                {
                    CopyItems();
                });
                menu.AddItem(new GUIContent("paste"), false, () =>
                {
                    PasteItems();
                });
                menu.AddItem(new GUIContent("duplicate"), false, () =>
                {
                    DuplicateItems();
                });
                menu.AddItem(new GUIContent("remove"), false, () =>
                {
                    RemoveItem();
                });
                menu.ShowAsContext();
            }
            else
            {
                contextMenuInsideElement.ShowAsContext();
            }
        }

        protected override void ContextClicked()
        {
            base.ContextClicked();
            Event.current.Use();

            if (contextMenuOutsideElement == null)
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("create folder"), false, () =>
                {
                    CreateNewItem(-1, true);
                });
                menu.AddItem(new GUIContent("create new"), false, () =>
                {
                    CreateNewItem();
                });
                menu.AddItem(new GUIContent("paste"), false, () =>
                {
                    PasteItems();
                });
                menu.ShowAsContext();
            }
            else
            {
                contextMenuOutsideElement.ShowAsContext();
            }
        }

        protected override void SelectionChanged(IList<int> selectedIds)
        {
            if (selectionChangedCallback != null)
                selectionChangedCallback();
        }

        public void SetCustomContextMenuInsideElement(GenericMenu menu)
        {
            contextMenuInsideElement = menu;
        }

        public void SetCustomContextMenuOutsideElement(GenericMenu menu)
        {
            contextMenuOutsideElement = menu;
        }
    }

}

#endif