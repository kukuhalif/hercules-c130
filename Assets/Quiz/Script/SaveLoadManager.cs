﻿using CheckingKnowledge;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace CheckingKnowledge
{
    [System.Serializable]
    public class ResultQuizData
    {
        public string Username;
        public string StudentId;
        public string Date;
        public string Duration;
        public string Score;
        public List<string> Reports;
        public List<bool> AnswerStatus;
        public string MateriName;
        public string ReportPicture;

        public ResultQuizData(string username, string id, string date, string duration, string score, List<string> reports, List<bool> answerStatus, string materiName, string reportPic)
        {
            //array 0 - 4 history data result
            Username = username;
            StudentId = id;
            Date = date;
            Duration = duration;
            Score = score;
            //array 5 dst list quiz yang sudah diajukan
            Reports = reports;
            AnswerStatus = answerStatus;
            MateriName = materiName;
            ReportPicture = reportPic;
            SaveResult();
        }

        void SaveResult()
        {
            EventManager.TriggerEvent(new SaveDataEvent(this));
        }
    }

    public class SaveLoadManager : MonoBehaviour
    {

        private List<ResultQuizData> _ListOfDataResult;
        private ResultQuizData ResultQuiz;

        List<GameObject> _HistoryList;
        private void Awake()
        {
            EventManager.AddListener<SaveDataEvent>(SaveResult);
            EventManager.AddListener<LoadDataEvent>(LoadHistory);
        }

        private void Start()
        {
            Init();
        }

        public void Init()
        {
            DataSaveLoad.InitSaveLoadDatabase();
        }

        private void SaveResult(SaveDataEvent e)
        {
            DataSaveLoad.SaveResult(e.Result);
        }

        IEnumerator LoadHistorySequence(LoadDataEvent e)
        {
            FileInfo[] historyFiles = null;
            historyFiles = DataSaveLoad.GetHistoryCount();
            _HistoryList = new List<GameObject>();
            yield return null;

            for (int i = 0; i < historyFiles.Length; i++)
            {
                DataSaveLoad.SavedData datasaveload = DataSaveLoad.LoadResult(historyFiles[i]);

                GameObject g = Instantiate(e.InstantiateObject, transform.position, Quaternion.identity) as GameObject;
                LoadTextData t = g.GetComponent<LoadTextData>();

                _HistoryList.Add(g);
                g.SetActive(true);
                g.transform.SetParent(e.DataParent);
                g.transform.localScale = new Vector3(1, 1, 1);

                t.DataText[0].text = datasaveload.Datas[0];
                t.DataText[1].text = datasaveload.Datas[1];
                t.DataText[2].text = datasaveload.Datas[2];
                t.DataText[3].text = datasaveload.Datas[3];
                t.DataText[4].text = datasaveload.Datas[4];
                t.DataText[5].text = datasaveload.Datas[5];

                t.quizAmount = datasaveload.Reports.Count;
                t.resultSnapshotData = datasaveload.resultPictureData;

                t.rightAnswer = 0;
                for (int r = 0; r < datasaveload.AnswerStatus.Count; r++)
                {
                    if (datasaveload.AnswerStatus[r])
                        t.rightAnswer++;
                }

                LoadTextData load = g.GetComponent<LoadTextData>();
                load.AnswerStatus = datasaveload.AnswerStatus;
                load.Reports = datasaveload.Reports;

                yield return null;
            }
        }

        public void LoadHistory(LoadDataEvent e)
        {
            DestroyObjectLoad();

            StartCoroutine(LoadHistorySequence(e));
        }
        void DestroyObjectLoad()
        {
            if (_HistoryList == null)
                return;

            for (int i = 0; i < _HistoryList.Count; i++)
                Destroy(_HistoryList[i]);
        }
        private void OnDestroy()
        {
            EventManager.RemoveListener<SaveDataEvent>(SaveResult);
            EventManager.RemoveListener<LoadDataEvent>(LoadHistory);
        }
    }
}
