﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CheckingKnowledge;

namespace CheckingKnowledge
{
    public abstract class ButtonAnswer : MonoBehaviour
    {
        public abstract void SetDataAnswer(Answer answer);
    }
}
