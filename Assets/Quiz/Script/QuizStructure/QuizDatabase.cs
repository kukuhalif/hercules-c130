﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CheckingKnowledge
{
    public class QuizDatabase : MonoBehaviour
    {
        [SerializeField]
        bool LoadA, LoadB;

        public Dropdown SelectMateriDropdown;

        public List<string> _DefaultPath = new List<string>();
        //bikin satu jenis quiz satu list

        [Header("Database Quiz")]
        [SerializeField]
        List<QuizA> _QuizADatabase;
        [SerializeField]
        List<QuizB> _QuizBDatabase;

        [Header("Initialization Quiz")]
        [SerializeField]
        private int _QuizTypeAmount;

        bool IsNull;

        bool AStilLoad, BStilLoad;
        public bool LoadDatabaseFinished;

        [SerializeField]
        int AllMateriAmount;
        [SerializeField]
        int QuizA, QuizB;

        private void Start()
        {
            SetDropdown();
        }

        void SetDropdown()
        {
            string path = System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/");
            string[] files = Directory.GetFiles(path, "*.fyr");

            SelectMateriDropdown.options.Clear();
            SelectMateriDropdown.options.Add(new Dropdown.OptionData("All Section"));

            for (int i = 0; i < files.Length; i++)
            {
                SelectMateriDropdown.options.Add(new Dropdown.OptionData(Path.GetFileNameWithoutExtension(files[i])));
            }
        }

        public void Init()
        {
            SetPath();
            StartCoroutine(ImportDatabase());

            //check null answer
            //for (int i = 0; i < _QuizADatabase.Count; i++)
            //{
            //    bool answer = false;
            //    for (int ii = 0; ii < _QuizADatabase[i].AnswerOption.Count; ii++)
            //    {
            //        if (_QuizADatabase[i].AnswerOption[ii].IsTrue)
            //            answer = true;
            //    }
            //    if (!answer)
            //    {
            //        Debug.Log("A " + i + " " + _QuizADatabase[i].Question);
            //    }
            //}

            //for (int i = 0; i < _QuizBDatabase.Count; i++)
            //{
            //    for (int ii = 0; ii < _QuizBDatabase[i].Question.Count; ii++)
            //    {
            //        bool answer = false;
            //        for (int iii = 0; iii < _QuizBDatabase[i].Question[ii].OptionAnswer.Count; iii++)
            //        {
            //            if (_QuizBDatabase[i].Question[ii].OptionAnswer[iii].IsTrue)
            //                answer = true;
            //        }
            //        if (!answer)
            //        {
            //            Debug.Log("B " + i + " " + ii + " " + _QuizBDatabase[i].Question[ii]);
            //        }
            //    }
            //}
        }
        private void SetPath()
        {
            if (SelectMateriDropdown.value == 0)
            {
                for (int i = 1; i < SelectMateriDropdown.options.Count; i++)
                {
                    _DefaultPath.Add(System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/" + SelectMateriDropdown.options[i].text + ".fyr"));
                }
            }
            else
                _DefaultPath.Add(System.IO.Path.Combine(Application.streamingAssetsPath, "QuizData/QuizDatabase/" + SelectMateriDropdown.options[SelectMateriDropdown.value].text + ".fyr"));

            for (int i = 0; i < _DefaultPath.Count; i++)
            {
                _DefaultPath[i] = _DefaultPath[i].Replace(@"\", "/");
            }
        }


        List<int> GenerateRandom(int count, int min, int max)
        {
            if (max <= min || count < 0 ||
                    (count > max - min && max - min > 0))
            {
                return new List<int>();
            }

            System.Random random = new System.Random(System.DateTime.Now.Millisecond);

            HashSet<int> candidates = new HashSet<int>();

            for (int top = max - count; top < max; top++)
            {
                if (!candidates.Add(random.Next(min, top + 1)))
                {
                    candidates.Add(top);
                }
            }

            List<int> result = candidates.ToList();

            for (int i = result.Count - 1; i > 0; i--)
            {
                int k = random.Next(i + 1);
                int tmp = result[k];
                result[k] = result[i];
                result[i] = tmp;
            }
            return result;
        }

        [SerializeField] List<QuizA> qaz = new List<QuizA>();

        //randon quis dengan panjang yg bisa ditentukan
        public List<Quiz> GetQuizList(ref int quizAmount, bool randomQuiz)
        {
            // all materi
            if (SelectMateriDropdown.value == 0)
                quizAmount = AllMateriAmount;
            else // per sistem
            {
                if (LoadA && LoadB)
                    quizAmount = QuizA + QuizB;
                else if (LoadA)
                    quizAmount = QuizA;
                else
                    quizAmount = QuizB;
            }

            if (IsNull)
                return null;
            List<Quiz> randomResults = new List<Quiz>();

            if (randomQuiz)
            {
                List<int> randomIndex = GenerateRandom(quizAmount, 0, quizAmount);

                for (int i = 0; i < randomIndex.Count; i++)
                {
                    int quizType = 1;
                    if (_QuizBDatabase.Count > 0)
                    {
                        quizType = Random.Range(1, _QuizTypeAmount + 1);
                    }
                    int random = 0;
                    switch (quizType)
                    {
                        case 1: //quiz a
                            if (!LoadA)
                                break;
                            random = randomIndex[randomIndex[i]];
                            if (random >= _QuizADatabase.Count - 1)
                                random = _QuizADatabase.Count - 1;

                            //if (!_QuizADatabase[random].Selected)
                            //{
                                randomResults.Add(_QuizADatabase[random]);
                                _QuizADatabase[random].Selected = true;
                            //}

                            break;
                        case 2: //quiz b
                            if (!LoadB)
                                break;
                            random = randomIndex[randomIndex[i]];
                            if (random >= _QuizBDatabase.Count - 1)
                                random = _QuizBDatabase.Count - 1;

                            //if (!_QuizBDatabase[random].Selected)
                            //{
                                randomResults.Add(_QuizBDatabase[random]);
                                _QuizBDatabase[random].Selected = true;
                            //}

                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                //if (LoadA && LoadB)
                //    quizAmount = QuizA + QuizB;
                //else if (LoadA)
                //    quizAmount = QuizA;
                //else if (LoadB)
                //    quizAmount = QuizB;

                if (LoadA)
                    for (int i = 0; i < quizAmount; i++)
                    {
                        randomResults.Add(_QuizADatabase[i]);
                    }

                if (LoadB)
                    for (int i = 0; i < quizAmount; i++)
                    {
                        randomResults.Add(_QuizBDatabase[i]);
                    }
            }
            for (int i = 0; i < randomResults.Count; i++)
            {
                qaz.Add((QuizA)randomResults[i]);
            }
            return randomResults;
        }

        #region import database
        IEnumerator ImportDatabase()
        {
            LoadDatabaseFinished = false;

            for (int i = 0; i < _DefaultPath.Count; i++)
            {
                if (LoadA)
                {
                    if (File.Exists(_DefaultPath[i]))
                    {
                        AStilLoad = true;
                        StartCoroutine(ImportDatabase(_DefaultPath[i], _QuizADatabase, true));
                    }
                    else
                    {
                        EventManager.TriggerEvent(new PopUpQuizEvent(-1, "Database Quiz NOT FOUND !!"));
                        IsNull = true;
                    }
                }

                if (LoadB)
                {
                    if (File.Exists(_DefaultPath[i]))
                    {
                        BStilLoad = true;
                        StartCoroutine(ImportDatabase(_DefaultPath[i], _QuizBDatabase, false));
                    }
                    else
                    {
                        EventManager.TriggerEvent(new PopUpQuizEvent(-1, "Database Quiz NOT FOUND !!"));
                        IsNull = true;
                    }
                }
                yield return null;
            }
            while ((LoadA && AStilLoad) || (LoadB && BStilLoad))
                yield return null;

            LoadDatabaseFinished = true;
        }

        IEnumerator ImportDatabase<T>(string path, List<T> quizDatas, bool a) where T : Quiz
        {
            string[] datas = File.ReadAllLines(path);
            for (int i = 0; i < datas.Length; i++)
            {
                quizDatas.Add(JsonUtility.FromJson<T>(EncryptionManager.Decrypt(datas[i])));
                if (i % 2 == 0)
                    yield return null;
            }
            if (a)
                AStilLoad = false;
            else
                BStilLoad = false;
        }

        #endregion
    }
}