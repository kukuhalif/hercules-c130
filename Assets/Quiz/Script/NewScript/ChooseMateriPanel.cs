using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using CheckingKnowledge;
namespace VirtualTraining.UI
{
    public class ChooseMateriPanel : UIPanel
    {
        [SerializeField] QuizManager quizManager;
        [SerializeField] HistoryManager historyManager;
        [SerializeField] QuizPopUp quizPopUp;
        [SerializeField] InteractionButton startButton;
        [SerializeField] InteractionButton historyResult;
        [SerializeField] InteractionButton closeChooseMateri;
        [SerializeField] InteractionToggle randomToggle;
        private bool isRandom;
        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            startButton.OnClickEvent += StartQuizListener;
            historyResult.OnClickEvent += OpenHistoryResult;
            closeChooseMateri.OnClickEvent += CloseChooseMateriQuiz;
            if (randomToggle != null)
            {
                randomToggle.OnChangeStateEvent += LockPanelToggleListener;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            startButton.OnClickEvent -= StartQuizListener;
            historyResult.OnClickEvent -= OpenHistoryResult;
            closeChooseMateri.OnClickEvent -= CloseChooseMateriQuiz;
            if (randomToggle != null)
            {
                randomToggle.OnChangeStateEvent -= LockPanelToggleListener;
            }
        }

        void StartQuizListener()
        {
            quizManager.InitQuizData();
        }

        void OpenHistoryResult()
        {
            historyManager.OpenHistory();
        }

        void CloseChooseMateriQuiz()
        {
            quizPopUp.OnQuitButton();
        }

        private void LockPanelToggleListener(bool on)
        {
            isRandom = on;
            quizManager.PlaySFX("confirm");
        }
    }
}
