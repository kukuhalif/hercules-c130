using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;
using CheckingKnowledge;
namespace VirtualTraining.UI
{
    public class LoginPanel : UIPanel
    {
        [SerializeField] LoginQuiz loginQuiz;
        [SerializeField] HistoryManager historyManager;
        [SerializeField] QuizPopUp quizPopup;
        [SerializeField] InteractionButton startQuizButton;
        [SerializeField] InteractionButton historyResultButton;
        [SerializeField] InteractionButton closeLoginButton;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            startQuizButton.OnClickEvent += StartQuiz;
            historyResultButton.OnClickEvent += OpenHistory;
            closeLoginButton.OnClickEvent += CloseLoginPanel;

        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            startQuizButton.OnClickEvent -= StartQuiz;
            historyResultButton.OnClickEvent -= OpenHistory;
            closeLoginButton.OnClickEvent -= CloseLoginPanel;

        }

        void StartQuiz()
        {
            loginQuiz.StartQuiz();
        }

        void OpenHistory()
        {
            historyManager.OpenHistory();
        }

        void CloseLoginPanel()
        {
            quizPopup.ReloadQuiz();
        }

    }
}
