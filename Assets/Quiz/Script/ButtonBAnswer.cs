﻿using CheckingKnowledge;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CheckingKnowledge
{
    public class ButtonBAnswer : MonoBehaviour
    {
        public List<OptionalAnswer> DataAnswer;
        public bool IsTrue;

        private Dropdown _Dropdown;

        [SerializeField]
        private Color _TrueColor;
        [SerializeField]
        private Color _FalseColor;


        private Image _UIImage;
        private Color _DefaultColor;

        private void Awake()
        {
            _Dropdown = GetComponent<Dropdown>();
            _UIImage = GetComponent<Image>();
            _DefaultColor = _UIImage.color;
        }

        public void SetDropdownOption(Dropdown.OptionData optionData)
        {
            _Dropdown.options.Add(optionData);
        }

        public OptionalAnswer GetDropdownValue()
        {
            return DataAnswer[_Dropdown.value];
        }

        public void SetDropDownValue(int value)
        {
            _Dropdown.value = value;
        }

        public void SetCorrectionSign()
        {
            if (_UIImage == null)
                return;

            if (IsTrue)
                _UIImage.color = _TrueColor;
            else
                _UIImage.color = _FalseColor;
        }

        public void CheckValueData()
        {
            bool value = DataAnswer[_Dropdown.value].IsTrue ? true : false;
            IsTrue = value;
        }

        public bool GetAnswerResult()
        {
            return DataAnswer[_Dropdown.value].IsTrue;
        }

        public void ResetButtonData()
        {
            _Dropdown.value = 0;
            _Dropdown.options = new List<Dropdown.OptionData>();
            _UIImage.color = _DefaultColor;
            DataAnswer = new List<OptionalAnswer>();
        }
    }
}
