﻿using CheckingKnowledge;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CheckingKnowledge
{
    public class LoadTextData : MonoBehaviour
    {
        public static LoadTextData SelectedLoadTextData;

        public List<bool> AnswerStatus;
        public List<string> Reports;

        public Text[] DataText;

        public int quizAmount;
        public int rightAnswer;

        public string resultSnapshotData;

        private void Awake()
        {
            DataText = GetComponentsInChildren<Text>();
            GetComponent<Button>().onClick.AddListener(OnClick);
        }

        void OnClick()
        {
            SelectedLoadTextData = this;
        }
    }
}
