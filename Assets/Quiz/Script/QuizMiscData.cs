﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CheckingKnowledge
{
    public enum QuizButtonType
    {
        PREVIOUS,
        NEXT
    }

    [System.Serializable]
    public class PreviousNextButton
    {
        public QuizButtonType Type;
        public Button Button;
    }
}
