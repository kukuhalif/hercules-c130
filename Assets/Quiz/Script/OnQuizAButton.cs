﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace CheckingKnowledge
{
    public class OnQuizAButton : MonoBehaviour
    {
        [SerializeField]
        Color SelectedColor;
        [SerializeField]
        Color DeselectColor;

        [SerializeField]
        bool SwapSprite;

        Text ThisText;
        Image ThisImage;

        [SerializeField]
        bool AsToggle;
        public bool ToggleOn = false;
        [SerializeField]
        Image Pointer;
        [SerializeField]
        List<OnQuizAButton> OtherToggle = new List<OnQuizAButton>();

        Button ThisButton;
        Toggle ThisToggle;

        [SerializeField]
        Image[] Images;
        [SerializeField]
        float[] SelectedAlphaImages;
        [SerializeField]
        float[] OriginalAlphaImages;

        [SerializeField]
        GameObject Cahaya;

        [SerializeField]
        GameObject Blocker;
        RectTransform BlockerTransform;

        private void Awake()
        {
            ThisText = GetComponentInChildren<Text>();
            if (ThisText == null)
                ThisText = GetComponent<Text>();
            ThisImage = GetComponent<Image>();
            ThisButton = GetComponent<Button>();
            ThisToggle = GetComponent<Toggle>();
            if (Blocker != null)
                BlockerTransform = Blocker.GetComponent<RectTransform>();
        }

        private void OnEnable()
        {
            ToggleOn = false;
            OnDeselect();
        }

        private void OnDisable()
        {
            if (Blocker != null)
                Blocker.SetActive(false);
        }

        private void Start()
        {
            if (gameObject.activeSelf && AsToggle)
                StartCoroutine(SearchOtherToggleInParent());
        }

        IEnumerator SearchOtherToggleInParent()
        {
            yield return null;
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                if (transform.parent.GetChild(i).gameObject != gameObject && transform.parent.GetChild(i).gameObject.activeSelf)
                {
                    OnQuizAButton other = transform.parent.GetChild(i).gameObject.GetComponent<OnQuizAButton>();
                    if (other != null)
                        OtherToggle.Add(other);
                }
            }
        }

        IEnumerator SetBlockerToCenter()
        {
            yield return null;
            BlockerTransform.anchoredPosition = new Vector3();
        }

        public void OnSelect()
        {
            if (Blocker != null)
            {
                Blocker.SetActive(true);
                StartCoroutine(SetBlockerToCenter());
            }

            if (!SwapSprite)
                ThisText.color = SelectedColor;
            else
                ThisImage.color = SelectedColor;

            if (Pointer != null)
                Pointer.gameObject.SetActive(true);

            for (int i = 0; i < Images.Length; i++)
            {
                Color color = Images[i].color;
                color.a = SelectedAlphaImages[i];
                Images[i].color = color;
            }

            if (Cahaya != null)
                Cahaya.SetActive(true);
        }

        public void OnDeselect()
        {
            if (ToggleOn)
                return;

            if (Blocker != null)
                Blocker.SetActive(false);

            if (!SwapSprite)
                ThisText.color = DeselectColor;
            else
                ThisImage.color = DeselectColor;

            if (Pointer != null)
                Pointer.gameObject.SetActive(false);

            //StartCoroutine(DisableOtherPointer());

            for (int i = 0; i < Images.Length; i++)
            {
                Color color = Images[i].color;
                color.a = OriginalAlphaImages[i];
                Images[i].color = color;
            }

            if (Cahaya != null)
                Cahaya.SetActive(false);
        }

        public void OnClick()
        {
            if (AsToggle)
            {
                ToggleOn = true;
                OnSelect();
                for (int i = 0; i < OtherToggle.Count; i++)
                {
                    OtherToggle[i].ToggleOn = false;
                    OtherToggle[i].OnDeselect();
                }
            }
            else
                AudioPlayer.PlaySFX(SFX.ButtonClick);
        }
    }
}
