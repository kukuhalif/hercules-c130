﻿using CheckingKnowledge;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace CheckingKnowledge
{
    public enum PopUpType
    {
        LOGIN, QUIZ_A, QUIZ_B
    }
    public class QuizPopUp : MonoBehaviour
    {

        [SerializeField]
        private Text[] PopUpTexts;
        private Text PopUpText;

        [SerializeField]
        private CanvasGroup QuitPopsUI;

        [SerializeField]
        CanvasGroup ConfirmUI;

        // Use this for initialization
        void Awake()
        {
            EventManager.AddListener<PopUpQuizEvent>(PopUp);
        }

        void PopUp(PopUpQuizEvent e)
        {
            switch (e.Type)
            {
                case PopUpType.LOGIN:
                PopUpText = PopUpTexts[0];
                break;
                case PopUpType.QUIZ_A:
                PopUpText = PopUpTexts[1];
                break;
            }
            PopUpText.gameObject.SetActive(true);
            PopUpText.text = e.TextPops;

            if (e.Duration <= 0)
                return;

            StopCoroutine(PopUpDuration(0));
            StartCoroutine(PopUpDuration(e.Duration));
        }

        IEnumerator PopUpDuration(float duration)
        {
            yield return new WaitForSeconds(duration);
            PopUpText.text = "";
            PopUpText.gameObject.SetActive(false);

        }
        private void OnDestroy()
        {
            EventManager.RemoveListener<PopUpQuizEvent>(PopUp);
        }

        public void OnQuitButton()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            QuitPopsUI.gameObject.SetActive(true);
            QuitPopsUI.DOFade(1, 0.5f).OnComplete(DisableBlocker);
        }
        
        void DisableBlocker()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }

        public void OnCancelQuitButton()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            QuitPopsUI.DOFade(0, 0.5f).OnComplete(OnCancelQuitButtonAction);
        }

        void OnCancelQuitButtonAction()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
            QuitPopsUI.gameObject.SetActive(false);
        }

        public void OnFinishQuizAction()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            ConfirmUI.gameObject.SetActive(true);
            ConfirmUI.DOFade(0, 0f);
            ConfirmUI.DOFade(1, 0.5f).OnComplete(DisableBlocker);
        }

        public void CloseConfirmUI()
        {
            ConfirmUI.gameObject.SetActive(false);
        }

        public void ReloadQuiz()
        {
            StartCoroutine(ReloadRoutine());
        }

        IEnumerator ReloadRoutine()
        {
            AsyncOperation Async = SceneManager.LoadSceneAsync("Quiz", LoadSceneMode.Additive);
            Async.allowSceneActivation = false;
            if (!Async.isDone)
                yield return null;
            Async.allowSceneActivation = true;

            SceneManager.UnloadSceneAsync("Quiz");
        }
    }
}
