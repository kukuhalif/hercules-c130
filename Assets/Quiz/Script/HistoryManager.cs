﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
//using LCPrinter;
using VirtualTraining.Core;

namespace CheckingKnowledge
{
    public class HistoryManager : MonoBehaviour
    {

        [SerializeField]
        private CanvasGroup _HistoryCanvas;
        [SerializeField]
        private CanvasGroup _HistoryDetail;
        private Tween _Tween;

        [SerializeField]
        private Transform _HistoryQuizParent;

        [SerializeField]
        private GameObject _HistoryQuiz;

        void Awake()
        {
            //EventManager.AddListener<ShowDetailHistoryEvent>(ShowHistoryResult);
        }

        public void OpenHistory()
        {
            AudioPlayer.PlaySFX(SFX.ButtonClick);
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            _HistoryCanvas.gameObject.SetActive(true);
            LoadFromDatabase();
            if (_Tween != null)
                _Tween.Kill(true);
            _Tween = _HistoryCanvas.DOFade(1, 0.5f).OnComplete(DoneFadeIn);
        }
        public void DoneButton()
        {
            AudioPlayer.PlaySFX(SFX.ButtonClick);
            if (_Tween != null)
                _Tween.Kill(true);
            _Tween = _HistoryCanvas.DOFade(0, 0.5f).OnComplete(DoneFadeout).OnComplete(DoneFadeout);
        }
        void DoneFadeout()
        {
            _HistoryCanvas.gameObject.SetActive(false);
        }
        void DoneFadeIn()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }

        #region LoadDatabase
        void LoadFromDatabase()
        {
            EventManager.TriggerEvent(new LoadDataEvent(_HistoryQuizParent, _HistoryQuiz));
            LoadTextData.SelectedLoadTextData = null;
        }
        #endregion

        #region HistoryResult
        public void ShowHistoryResult(ShowDetailHistoryEvent e)
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            if (_Tween != null)
                _Tween.Kill(true);
            if (_HistoryDetail == null)
            {
                GameObject obj = GameObject.Find("HistoryResultView");
                _HistoryDetail = obj.GetComponent<CanvasGroup>();
            }
            _HistoryDetail.gameObject.SetActive(true);
            _Tween = _HistoryDetail.DOFade(1, 0.5f).OnComplete(DoneFadeout).OnComplete(DoneFadeintHistoryDetail);
        }
        void DoneFadeintHistoryDetail()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }

        public void CloseHistoryDetail()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            if (_Tween != null)
                _Tween.Kill(true);
            _Tween = _HistoryDetail.DOFade(0, 0.5f).OnComplete(DoneFadeout).OnComplete(DoneFadeouttHistoryDetail);
        }
        void DoneFadeouttHistoryDetail()
        {
            _HistoryDetail.gameObject.SetActive(false);
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }
        #endregion

        private void OnDestroy()
        {
            //EventManager.RemoveListener<ShowDetailHistoryEvent>(ShowHistoryResult);
        }

        public void OpenHistoryDetail()
        {
            if (LoadTextData.SelectedLoadTextData == null)
                return;

            if (LoadTextData.SelectedLoadTextData.Reports != null && LoadTextData.SelectedLoadTextData.AnswerStatus != null)
                EventManager.TriggerEvent(new ShowDetailHistoryEvent(LoadTextData.SelectedLoadTextData.Reports, LoadTextData.SelectedLoadTextData.AnswerStatus));
        }

        public void PrintReport()
        {
            if (LoadTextData.SelectedLoadTextData == null)
                return;

            string resultSnapshotData = LoadTextData.SelectedLoadTextData.resultSnapshotData;

            byte[] datas = System.Convert.FromBase64String(resultSnapshotData);

            Texture2D tex = new Texture2D(Screen.width, Screen.height);
            tex.LoadImage(datas);

            //Print.PrintTexture(tex.EncodeToPNG(), 1, "");

            PrinterManager.Print(tex);

            //Text[] dataTexts = LoadTextData.SelectedLoadTextData.DataText;
            //string reportData = "";

            //reportData =
            //    "username : " + dataTexts[0].text + "\n" +
            //    "materi : " + UppercaseWords(dataTexts[1].text) + "\n" +
            //    "student id : " + dataTexts[2].text + "\n" +
            //    "total score : " + dataTexts[5].text + "\n" +
            //    "right answer : " + LoadTextData.SelectedLoadTextData.rightAnswer + "\n" +
            //    "quiz amount : " + LoadTextData.SelectedLoadTextData.quizAmount + "\n" +
            //    "time : " + dataTexts[4].text + "\n" +
            //    "date : " + dataTexts[3].text;


            //QuizResult.PrintReport(reportData);
        }

        //public static string UppercaseWords(string value)
        //{
        //    value = value.ToLower();
        //    char[] array = value.ToCharArray();
        //    // Handle the first letter in the string.
        //    if (array.Length >= 1)
        //    {
        //        if (char.IsLower(array[0]))
        //        {
        //            array[0] = char.ToUpper(array[0]);
        //        }
        //    }
        //    // Scan through the letters, checking for spaces.
        //    // ... Uppercase the lowercase letters following spaces.
        //    for (int i = 1; i < array.Length; i++)
        //    {
        //        if (array[i - 1] == ' ')
        //        {
        //            if (char.IsLower(array[i]))
        //            {
        //                array[i] = char.ToUpper(array[i]);
        //            }
        //        }
        //    }
        //    return new string(array);
        //}
    }
}
