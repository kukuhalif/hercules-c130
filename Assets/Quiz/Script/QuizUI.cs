﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CheckingKnowledge;

namespace CheckingKnowledge
{
    [System.Serializable]
    public class QuizUI
    {

    }

    [System.Serializable]
    public class QuizAUI : QuizUI
    {
        public Text Question;
        public List<ButtonAAnswer> AnswerButton;
    }

    [System.Serializable]
    public class QuizBUI : QuizUI
    {
        public List<DataUIQuizB> Questions;

    }

    [System.Serializable]
    public class DataUIQuizB
    {
        public Text Question;
        public Text DefaultOption;
        public ButtonBAnswer AnswerButton;
        public GameObject ObjectUI;
    }
}