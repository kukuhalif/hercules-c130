﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheckingKnowledge
{
    public class OpenQuiz : MonoBehaviour
    {
        [SerializeField]
        string QuizLocation;
        public void Open()
        {
            Application.OpenURL(Application.streamingAssetsPath + QuizLocation);
        }
    }
}
