﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using VirtualTraining.Core;
using TMPro;
namespace CheckingKnowledge
{

    public class LoginQuiz : MonoBehaviour
    {
        private string _LoginName;
        private string _LoginId;

        public TMP_InputField Nama;
        public TMP_InputField StudentId;

        [SerializeField]
        public CanvasGroup _LoginParent;

        private Tween _Tween;

        private void Awake()
        {
            EventManager.AddListener<InitLoginEvent>(Init);
        }
        private void Start()
        {
            Nama.Select();
        }
        void Init(InitLoginEvent e)
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            Nama.text = "";
            StudentId.text = "";

            _LoginParent.gameObject.SetActive(true);
            if (_Tween != null)
                _Tween.Kill(true);
            _Tween = _LoginParent.DOFade(1, 0.5f).OnComplete(InitDone);
        }

        void InitDone()
        {
            EventManager.TriggerEvent(new BlockerButtonEvent(false));
        }

        public void StartQuiz()
        {
            if (Nama.text == "")
            {
                EventManager.TriggerEvent(new PopUpQuizEvent(3, "Please input your NAME"));
                //AudioPlayer.PlaySFX(SFX.Warning);
                return;
            }
            else if (StudentId.text == "")
            {
                EventManager.TriggerEvent(new PopUpQuizEvent(3, "Please input your STUDENT ID"));
                //AudioPlayer.PlaySFX(SFX.Warning);
                return;
            }

            AudioPlayer.PlaySFX(SFX.ButtonClick);
            _LoginName = Nama.text;
            _LoginId = StudentId.text;

            EventManager.TriggerEvent(new SetUsernameEvent(_LoginName, _LoginId));

            QuizManager qm = GetComponent<QuizManager>();

            if (qm.SelectedQuiz == null || qm.SelectedQuiz.Count <= 0)
            {
                EventManager.TriggerEvent(new PopUpQuizEvent(-1f, "Quiz data is EMPTY, please make sure data already exist"));
                return;
            }

            if (_Tween != null)
                _Tween.Kill(true);
            _Tween = _LoginParent.DOFade(0, 0.5f).OnComplete(SetLoginFalse);
            EventManager.TriggerEvent(new StartQuizEvent());

        }

        void SetLoginFalse()
        {
            _LoginParent.gameObject.SetActive(false);
        }
        private void OnDestroy()
        {
            EventManager.RemoveListener<InitLoginEvent>(Init);
        }

        #region ButtonKeyboard
        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Return))
            {
                StartQuiz();
            }
            if (Input.GetKeyUp(KeyCode.Tab))
            {
                if (Nama.isFocused)
                    StudentId.Select();
                else
                    Nama.Select();
            }
        }
        #endregion
    }
}
