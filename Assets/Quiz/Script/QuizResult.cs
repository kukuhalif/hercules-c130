﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
//using LCPrinter;
using VirtualTraining.Core;

namespace CheckingKnowledge
{
    public class QuizResult : MonoBehaviour
    {
        public GameObject UIObject;
        public Text QuizAmount;
        public Text RightAnswer;
        public Text TimeRemain;
        public Text TotalScore;
        public Text UserData;

        [SerializeField]
        private CanvasGroup _BGQuiz;

        Texture2D snapshot;

        public bool isShapshotDone;

        [SerializeField] GameObject[] enabledScreenshot;
        [SerializeField] GameObject[] disabledScreenshot;

        //string reportData;

        public void InitResult(int quizAmount, int rightAnswer, string time, string totalScore, string user, string materi, string studentId, string fullDate)
        {
            isShapshotDone = false;
            snapshot = null;

            _BGQuiz.DOFade(0, 0.5f);

            UIObject.GetComponent<CanvasGroup>().DOFade(1, 0.5f).OnComplete(Snapshot);
            UIObject.SetActive(true);

            QuizAmount.text = quizAmount.ToString();
            RightAnswer.text = rightAnswer.ToString();
            TimeRemain.text = time.ToString();
            TotalScore.text = totalScore.ToString();
            UserData.text = user + "\n" + studentId;

            //reportData =
            //    "username : " + user + "\n" +
            //    "materi : " + HistoryManager.UppercaseWords(materi) + "\n" +
            //    "student id : " + studentId + "\n" +
            //    "total score : " + totalScore + "\n" +
            //    "right answer : " + rightAnswer + "\n" +
            //    "quiz amount : " + quizAmount + "\n" +
            //    "time : " + time + "\n" +
            //    "date : " + fullDate;
        }

        void Snapshot()
        {
            StartCoroutine(SnapshotAction());
        }

        IEnumerator SnapshotAction()
        {
            //if (MenuManager.instance != null)
            //    MenuManager.instance.EnableOnTopUI(false, true);

            for (int i = 0; i < enabledScreenshot.Length; i++)
            {
                enabledScreenshot[i].SetActive(true);
            }

            for (int i = 0; i < disabledScreenshot.Length; i++)
            {
                disabledScreenshot[i].SetActive(false);
            }

            yield return null;
            yield return new WaitForEndOfFrame();

            snapshot = ScreenCapture.CaptureScreenshotAsTexture(4);
            isShapshotDone = true;

            yield return null;
            //if (MenuManager.instance != null)
            //    MenuManager.instance.EnableOnTopUI(true, true);

            for (int i = 0; i < enabledScreenshot.Length; i++)
            {
                enabledScreenshot[i].SetActive(false);
            }

            for (int i = 0; i < disabledScreenshot.Length; i++)
            {
                disabledScreenshot[i].SetActive(true);
            }
        }

        public string GetSnapshotData()
        {
            byte[] bytes = snapshot.EncodeToPNG();
            string enc = System.Convert.ToBase64String(bytes);
            return enc;
        }

        public void DoneQuiz()
        {
            //AudioPlayer.PlaySFX(SFX.OpenMenu);
            EventManager.TriggerEvent(new BlockerButtonEvent(true));
            _BGQuiz.DOFade(0, 0.5f).OnComplete(DoneFade);
        }

        void DoneFade()
        {
            // gausah pake ini

            //UIObject.SetActive(false);
            //EventManager.TriggerEvent(new BlockerButtonEvent(false));
            //EventManager.TriggerEvent(new InitLoginEvent());
            //EventManager.TriggerEvent(new InitQuizManagerEvent());

            // langsung mulai quiz scene dari awal
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Quiz");
            UnityEngine.SceneManagement.SceneManager.LoadScene("Quiz", UnityEngine.SceneManagement.LoadSceneMode.Additive);
        }

        public void PrintReport()
        {
            if (snapshot != null)
            {
                //Print.PrintTexture(snapshot.EncodeToPNG(), 1, "");

                PrinterManager.Print(snapshot);
            }
        }
    }
}