﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VirtualTraining.Core;

namespace CheckingKnowledge
{
    public class QuizBDisplay : QuizDisplay
    {
        public QuizBUI UIQuiz;
        public GameObject LockAnswer;

        public override void Awake()
        {
            CanvasG = ObjectUI.GetComponent<CanvasGroup>();
        }

        public override void Start()
        {
            EventManager.AddListener<QuizEvent>(ShowQuiz);
            EventManager.AddListener<CloseQuizUI>(CloseUIListener);
        }

        void CloseUIListener(CloseQuizUI e)
        {
            CloseQuiz();
        }

        //USED FOR LOGIC
        public override void ShowQuiz(QuizEvent e)
        {
            int currentId = 0;

            ObjectUI.SetActive(false);

            if (e is QuizBEvent)
            {
                EventManager.TriggerEvent(new BlockerButtonEvent(false)); //event biar button g bisa d spam
                ObjectUI.SetActive(true);

                QuizBEvent data = (QuizBEvent)e;
                _QuizTemp = data.QuizData;
                for (int i = 0; i < data.QuizData.Question.Count; i++)
                {
                    UIQuiz.Questions[i].ObjectUI.SetActive(true);
                    UIQuiz.Questions[i].Question.text = data.QuizData.Question[i].Question;
                    UIQuiz.Questions[i].AnswerButton.ResetButtonData();

                    for (int j = 0; j < data.QuizData.Question[i].OptionAnswer.Count; j++)
                    {
                        OptionalAnswer answer = data.QuizData.Question[i].OptionAnswer[j]; //disingkat biar g kepnjangan

                        Dropdown.OptionData dropdownOption = new Dropdown.OptionData();
                        dropdownOption.text = answer.Answer;

                        //add data answer ke data dropdownnya
                        UIQuiz.Questions[i].AnswerButton.DataAnswer.Add(answer);
                        //ada bug unity g mau baca list ke 0 di awal jadi d replae pakai default text dulu
                        if (j == 0)
                            UIQuiz.Questions[i].DefaultOption.text = dropdownOption.text;

                        UIQuiz.Questions[i].AnswerButton.SetDropdownOption(dropdownOption);

                        if (answer.Selected)
                            UIQuiz.Questions[i].AnswerButton.SetDropDownValue(j);
                        currentId++;
                    }
                    UIQuiz.Questions[i].AnswerButton.CheckValueData(); //ngasi tau kalo data yang di set d dropdown udah bener / belum
                }
            }
        }

        private bool CheckResult()
        {
            List<bool> value = new List<bool>();

            for (int i = 0; i < (_QuizTemp as QuizB).Question.Count; i++)
            {
                _QuizTemp.SendResultReport(UIQuiz.Questions[i].AnswerButton.GetDropdownValue());
                value.Add(UIQuiz.Questions[i].AnswerButton.GetAnswerResult());
            }

            if (value.TrueForAll(x => x == true))
                return true;
            return false;
        }

        public void OnValueChanged(ButtonBAnswer answerData)
        {
            answerData.CheckValueData();
        }

        //USED FOR BUTTON
        public void OnSubmit()
        {
            AudioPlayer.PlaySFX(SFX.ButtonClick);
            EventManager.TriggerEvent(new BlockerButtonEvent(true)); //event biar button g bisa di spam

            LockAnswer.SetActive(true);
            bool result = CheckResult() ? true : false;

            //_QuizTemp.SetResultAnswer(result);

            StartCoroutine(OnSubmitted(result));
        }
        IEnumerator OnSubmitted(bool result)
        {
            yield return new WaitForSeconds(0.1f);
            CloseQuiz();
            ObjectUI.SetActive(false);
            EventManager.TriggerEvent(new EndQuizEvent(_QuizTemp, result));
            LockAnswer.SetActive(false);
            for (int i = 0; i < UIQuiz.Questions.Count; i++)
                UIQuiz.Questions[i].ObjectUI.SetActive(false);
        }
        public override void CloseQuiz()
        {
            ObjectUI.SetActive(false);
        }

        public override void OnDestroy()
        {
            EventManager.RemoveListener<QuizEvent>(ShowQuiz);
            EventManager.RemoveListener<CloseQuizUI>(CloseUIListener);
        }
    }
}