﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VirtualTraining.Core;

namespace CheckingKnowledge
{
    public class QuizADisplay : QuizDisplay
    {
        public QuizAUI UIQuiz;
        public GameObject LockAnswer;
        private OptionalAnswer _AnswerTemp;

        public override void Awake()
        {
            CanvasG = ObjectUI.GetComponent<CanvasGroup>();
        }

        public override void Start()
        {
            EventManager.AddListener<QuizEvent>(ShowQuiz);
            EventManager.AddListener<CloseQuizUI>(CloseUIListener);
        }

        void CloseUIListener(CloseQuizUI e)
        {
            CloseQuiz();
        }

        //USED FOR LOGIC
        public override void ShowQuiz(QuizEvent e)
        {
            ObjectUI.SetActive(false);
            if (e is QuizAEvent)
            {
                EventManager.TriggerEvent(new BlockerButtonEvent(false)); //event biar button g bisa d spam
                ObjectUI.SetActive(true);
                QuizAEvent data = (QuizAEvent)e;
                _QuizTemp = data.QuizData;

                if (data == null)
                    return;

                //reset all button every loaded quiz A
                for (int i = 0; i < UIQuiz.AnswerButton.Count; i++)
                {
                    UIQuiz.AnswerButton[i].transform.parent.gameObject.SetActive(false);
                    UIQuiz.AnswerButton[i].gameObject.SetActive(false);
                }

                UIQuiz.Question.text = data.QuizData.Question;
                for (int i = 0; i < data.QuizData.AnswerOption.Count; i++)
                {
                    UIQuiz.AnswerButton[i].gameObject.SetActive(true);
                    UIQuiz.AnswerButton[i].transform.parent.gameObject.SetActive(true);
                    UIQuiz.AnswerButton[i].SetText(data.QuizData.AnswerOption[i].Answer);
                    UIQuiz.AnswerButton[i].SetDataAnswer(data.QuizData.AnswerOption[i]);
                    if (data.QuizData.AnswerOption[i].Selected)
                        UIQuiz.AnswerButton[i].GetComponentInChildren<OnQuizAButton>(true).OnClick();
                }
            }

        }

        private void OnEnable()
        {
        }

        #region Button Function
        public void SetChoice(ButtonAAnswer b)
        {
            LockAnswer.SetActive(true);
            _AnswerTemp = b.AnswerData;
        }

        public void SubmitAnswer()
        {
            if (_AnswerTemp == null)
            {
                //AudioPlayer.PlaySFX(SFX.Warning);
                EventManager.TriggerEvent(new PopUpQuizEvent(1, "Please select your choice !!", PopUpType.QUIZ_A));
                return;
            }

            AudioPlayer.PlaySFX(SFX.ButtonClick);
            EventManager.TriggerEvent(new BlockerButtonEvent(true)); //event biar button g bisa di spam

            bool isTrue = false;
            if (_AnswerTemp.CheckAnswer())
            {
                isTrue = true;
                _QuizTemp.SendResultReport(_AnswerTemp);
            }
            else
                isTrue = false;

            StartCoroutine(OnSelectChoice(_AnswerTemp));
        }
        #endregion
        IEnumerator OnSelectChoice(OptionalAnswer a)
        {
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < UIQuiz.AnswerButton.Count; i++)
                UIQuiz.AnswerButton[i].ResetButton();
            ObjectUI.SetActive(false);

            //send answered quiz to temp Quiz A data
            _QuizTemp.SendResultReport(_AnswerTemp);

            EventManager.TriggerEvent(new EndQuizEvent(_QuizTemp, a.CheckAnswer()));
            LockAnswer.SetActive(false);
            _AnswerTemp = null;
            CloseQuiz();
        }

        public override void CloseQuiz()
        {
            for (int i = 0; i < UIQuiz.AnswerButton.Count; i++)
                UIQuiz.AnswerButton[i].gameObject.SetActive(false);
            ObjectUI.SetActive(false);
        }
        public override void OnDestroy()
        {
            EventManager.RemoveListener<QuizEvent>(ShowQuiz);
            EventManager.RemoveListener<CloseQuizUI>(CloseUIListener);
        }
    }
}
