﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CheckingKnowledge;

namespace CheckingKnowledge
{
    public abstract class QuizDisplay : MonoBehaviour
    {
        public GameObject ObjectUI;
        protected CanvasGroup CanvasG;
        protected Quiz _QuizTemp;

        public abstract void Awake();

        public abstract void Start();
        
        public abstract void ShowQuiz(QuizEvent e);

        public abstract void CloseQuiz();

        public abstract void OnDestroy();
    }
}