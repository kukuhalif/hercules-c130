﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CheckingKnowledge
{

    public class QuizReportButton : MonoBehaviour
    {
        [SerializeField]
        Text No, Status, PopupText;
        
        string[] Reports;

        public void Setup(int no, bool status, string[] reports)
        {
            No.text = no.ToString();
            Status.text = status ? "Correct" : "Incorrect";
            Reports = reports;
        }

        public void OpenPopup()
        {
            string report = "";
            for (int i = 0; i < Reports.Length; i++)
            {
                report += ("\nNo " + No.text + ". " + Reports[i] + "\n");
            }
            PopupText.text = report;
        }
    }

}