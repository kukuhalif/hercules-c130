﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CheckingKnowledge
{
    public abstract class QuizHistory
    {

    }

    [System.Serializable]
    public class QuizAHistory : QuizHistory
    {

    }

    [System.Serializable]
    public class QuizBHistory : QuizHistory
    {

    }
}
