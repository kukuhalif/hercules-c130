using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.IO;
using VirtualTraining.Core;
using VirtualTraining.UI;

namespace CheckingKnowledge
{
    public class QuizManager : MonoBehaviour
    {
        [Header("FROM DATABASE")]
        private QuizDatabase _DBQuiz;
        private SaveLoadManager _DBHistory;

        [Header("MANAGER")]
        int QuizAmount;
        public List<Quiz> SelectedQuiz;

        private int _CurrentCount = 0, _LastQuestion = 0;

        [Header("MANAGER")]
        public Text CountQuizText;
        public Text TimeCount;

        [SerializeField]
        QuizReportManager ReportManager;

        [SerializeField]
        private Text _Date;

        [SerializeField]
        private Text _Username;

        [SerializeField]
        private Text _Tittle;

        private float _TimeDuration = 0;
        private bool _StopTimer = false;
        string _Minutes = "";
        string _Seconds = "";
        string _Time = "";
        string _FullDate = "";
        string _User = "", _Id = "";

        int _TrueAnswer = 0;

        private QuizResult _QuizResult;

        [Header("LOGIN")]
        [SerializeField]
        private GameObject _ButtonBlocker;

        [SerializeField]
        private CanvasGroup _DefaultQuizBG;

        [SerializeField]
        private PreviousNextButton[] Buttons;
        // Use this for initialization

        bool DatabaseInit;

        [SerializeField]
        GameObject ChooseLanguageMenu;
        [SerializeField]
        GameObject LoadingLoadDatabaseUI;

        [SerializeField]
        InteractionToggle RandomToggle;

        void Awake()
        {
            EventManager.AddListener<EndQuizEvent>(EndQuiz);
            EventManager.AddListener<SetUsernameEvent>(SetUsername);
            EventManager.AddListener<BlockerButtonEvent>(SetInvisibleBlock);
            EventManager.AddListener<InitQuizManagerEvent>(Init);
            EventManager.AddListener<StartQuizEvent>(StartQuiz);

            _DBQuiz = GetComponent<QuizDatabase>();
            _QuizResult = GetComponent<QuizResult>();
            _DBHistory = GetComponent<SaveLoadManager>();

            DatabaseInit = false;
        }

        private void Start()
        {
            Init(null); //initsialisai tanpa event di awal
            RemoveTempImageResult();
        }

        public void OnMateriDropdownChanged(Dropdown dropdown)
        {
            if (dropdown.value == 0)
            {
                RandomToggle.IsOn = true;
                //RandomToggle.interactable = false;
            }
            else
            {
                RandomToggle.IsOn = false;
                //RandomToggle.interactable = true;
                AudioPlayer.PlaySFX(SFX.ButtonClick);
            }
        }

        public void InitQuizData()
        {
            LoadingLoadDatabaseUI.SetActive(true);
            if (!DatabaseInit)
            {
                _DBQuiz.Init();
                _DBHistory.Init();
            }
            StartCoroutine(WaitLoadQuizData());
        }

        IEnumerator WaitLoadQuizData()
        {
            while (!_DBQuiz.LoadDatabaseFinished)
            {
                yield return null;
            }
            DatabaseInit = true;
            SelectedQuiz = _DBQuiz.GetQuizList(ref QuizAmount, RandomToggle.IsOn);

            LoadingLoadDatabaseUI.SetActive(false);
            ChooseLanguageMenu.SetActive(false);
        }

        void Init(InitQuizManagerEvent e)
        {
            _Tittle.text = VirtualTrainingSceneManager.ProjectName;

            ResetAll();

            string dayName = System.DateTime.Now.ToString("dddd");
            string day = System.DateTime.Now.Day.ToString();
            string month = System.DateTime.Now.Month.ToString();
            string year = System.DateTime.Now.Year.ToString();
            _FullDate = day + "-" + month + "-" + year;
            _Date.text = dayName + ", " + _FullDate;

            _DefaultQuizBG.DOFade(1, 0);

            //StartCoroutine(StartDelayed());

            if (SelectedQuiz == null || SelectedQuiz.Count <= 0)
                return;
        }

        private void Update()
        {
            if (!_StopTimer)
            {
                _TimeDuration += Time.deltaTime;
                float minutes = Mathf.Floor(_TimeDuration / 60);
                float seconds = Mathf.RoundToInt(_TimeDuration % 60);

                if (minutes < 10)
                    _Minutes = "0" + minutes.ToString();
                else
                    _Minutes = minutes.ToString();
                if (seconds < 10)
                    _Seconds = "0" + Mathf.RoundToInt(seconds).ToString();
                else
                    _Seconds = seconds.ToString();
                _Time = _Minutes + ":" + _Seconds;
                TimeCount.text = _Time;
            }
        }
        void StartQuiz(StartQuizEvent e)
        {
            _StopTimer = false;
            PrevNextButton(0);
        }

        public void ShowQuiz()
        {
            if (_LastQuestion <= _CurrentCount && _LastQuestion <= SelectedQuiz.Count - 1)
                _LastQuestion = _CurrentCount;
            if (_CurrentCount >= QuizAmount)
                return;

            if (SelectedQuiz.Count <= 0)
                return;

            SelectedQuiz[_CurrentCount].ShowQuiz();
            CountQuizText.text = "Question " + (_CurrentCount + 1).ToString() + " of " + QuizAmount;
        }

        void EndQuiz(EndQuizEvent e)
        {
            e.QuizData.SetResultAnswer(e.ResultAnswer);
            StartCoroutine(DoneQuiz());
        }

        IEnumerator DoneQuiz()
        {
            yield return new WaitForSeconds(0);
            _CurrentCount++;
            if (_CurrentCount > _LastQuestion)
                _LastQuestion = _CurrentCount;
            if (_CurrentCount < QuizAmount)
            {
                PrevNextButton(0); // SHOW QUIZ
            }
            else
            {
                EventManager.TriggerEvent(new BlockerButtonEvent(false));
                //_UICanvas.DOFade(0, 1);
                float totalResult = GetAllResultScore();

                _StopTimer = true;

                string dayName = System.DateTime.Now.ToString("dddd");
                string day = System.DateTime.Now.Day.ToString();
                string month = System.DateTime.Now.Month.ToString();
                string year = System.DateTime.Now.Year.ToString();
                string date = day + "-" + month + "-" + year;
                string fullDate = dayName + ", " + date;

                _QuizResult.gameObject.SetActive(true);
                _QuizResult.InitResult(QuizAmount, _TrueAnswer, _Time, totalResult.ToString("F2"), _User, _DBQuiz.SelectMateriDropdown.options[_DBQuiz.SelectMateriDropdown.value].text, _Id, fullDate);

                ReportManager.SetReportDatas(SelectedQuiz);

                while (!_QuizResult.isShapshotDone)
                    yield return null;

                yield return null;
                ResultQuizData r = new ResultQuizData(_User, _Id, _FullDate, _Time, totalResult.ToString("F2"), ReportManager.GetReports(), ReportManager.GetAnswerStatus(), _DBQuiz.SelectMateriDropdown.options[_DBQuiz.SelectMateriDropdown.value].text, _QuizResult.GetSnapshotData());

                ResetPlayedQuizState();
            }
            //Debug.Log("QUIZ DONE");
        }

        public void ForceDoneQuiz()
        {
            EventManager.TriggerEvent(new CloseQuizUI());
            for (; _CurrentCount < QuizAmount; _CurrentCount++)
            {
                if (SelectedQuiz.Count > _CurrentCount)
                {
                    SelectedQuiz[_CurrentCount].Skip = true;
                    SelectedQuiz[_CurrentCount].ResetState();
                }
            }
            StartCoroutine(DoneQuiz());
        }

        void ResetPlayedQuizState()
        {
            for (int i = 0; i < SelectedQuiz.Count; i++)
            {
                SelectedQuiz[i].ResetState();
                SelectedQuiz[i].Skip = false;
            }
        }

        private float GetAllResultScore()
        {
            float total = 0;
            _TrueAnswer = 0;



            for (int i = 0; i < SelectedQuiz.Count; i++)
            {
                if (!SelectedQuiz[i].Skip)
                {
                    bool isTrue = SelectedQuiz[i].GetLastResultQuiz();
                    if (isTrue)
                        _TrueAnswer++;
                }
            }

            total = Mathf.Round(_TrueAnswer) / Mathf.Round(QuizAmount) * 100;

            return total;
        }

        private void SetUsername(SetUsernameEvent e)
        {
            _User = e.User;
            _Id = e.Id;
            _Username.text = "Welcome " + e.User;
        }

        private void SetInvisibleBlock(BlockerButtonEvent e)
        {
            _ButtonBlocker.SetActive(e.IsBlock);
        }

        void ResetAll()
        {
            _StopTimer = true;
            _ButtonBlocker.SetActive(false);
            SelectedQuiz = new List<Quiz>();
            _TimeDuration = 0;
            _CurrentCount = 0;
            _LastQuestion = 0;

            for (int i = 0; i < Buttons.Length; i++)
                Buttons[i].Button.interactable = true;
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<EndQuizEvent>(EndQuiz);
            EventManager.RemoveListener<SetUsernameEvent>(SetUsername);
            EventManager.RemoveListener<BlockerButtonEvent>(SetInvisibleBlock);
            EventManager.RemoveListener<InitQuizManagerEvent>(Init);
            EventManager.RemoveListener<StartQuizEvent>(StartQuiz);
        }

        public void PrevNextButton(int count)
        {
            _CurrentCount += count;

            if (_CurrentCount <= 0)
            {
                _CurrentCount = 0;
                for (int i = 0; i < Buttons.Length; i++)
                {
                    if (Buttons[i].Type == QuizButtonType.PREVIOUS)
                        Buttons[i].Button.interactable = false;

                    if (_LastQuestion == _CurrentCount)
                    {
                        if (Buttons[i].Type == QuizButtonType.NEXT)
                            Buttons[i].Button.interactable = false;
                    }
                    else
                    {
                        if (Buttons[i].Type == QuizButtonType.NEXT)
                            Buttons[i].Button.interactable = true;
                    }
                }
            }
            else if (_CurrentCount >= SelectedQuiz.Count - 1)
            {
                for (int i = 0; i < Buttons.Length; i++)
                {
                    if (Buttons[i].Type == QuizButtonType.NEXT)
                        Buttons[i].Button.interactable = false;
                }
            }
            else
            {
                for (int i = 0; i < Buttons.Length; i++)
                {
                    if (_LastQuestion > _CurrentCount)
                        Buttons[i].Button.interactable = true;
                    else
                    {
                        if (Buttons[i].Type == QuizButtonType.NEXT)
                            Buttons[i].Button.interactable = false;
                        else
                            Buttons[i].Button.interactable = true;
                    }
                }
            }
            ShowQuiz();
        }

        public void PlaySFX(string p_name)
        {

        }

        public void CloseQuiz()
        {
            VirtualTrainingSceneManager.CloseQuiz();
        }

        public static void RemoveTempImageResult()
        {
            foreach (var file in Directory.GetFiles(Application.persistentDataPath))
            {
                FileInfo file_info = new FileInfo(file);
                if (file_info.Extension == ".png" && file_info.Name.Contains("report_temp_"))
                {
                    file_info.Delete();
                }
            }
        }
    }
}
