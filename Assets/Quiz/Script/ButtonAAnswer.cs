﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CheckingKnowledge;

namespace CheckingKnowledge
{

    public class ButtonAAnswer : MonoBehaviour
    {
        public OptionalAnswer AnswerData;

        [SerializeField]
        private Text _AnswerText;

        [SerializeField]
        private Color _TrueColor;
        [SerializeField]
        private Color _FalseColor;

        private Image _UIImage;
        private Color _DefaultColor;

        void Awake()
        {
            //AnswerText = GetComponentInChildren<Text> ();
            _UIImage = GetComponent<Image>();
            _DefaultColor = _UIImage.color;
        }
        // Use this for initialization
        public void SetDataAnswer(OptionalAnswer answer)
        {
            AnswerData = answer;
        }

        public void SetText(string text)
        {
            _AnswerText.text = text;
        }

        public void SetCorrectionSign()
        {
            if (AnswerData.CheckAnswer())
                _UIImage.color = _TrueColor;
            else
                _UIImage.color = _FalseColor;
        }

        public void ResetButton()
        {
            if (_UIImage == null)
                return;
            _UIImage.color = _DefaultColor;
        }
    }
}
