﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CheckingKnowledge
{
    public abstract class QuizEvent : GameEvent
    {

    }

    public class CloseQuizUI : GameEvent
    {

    }

    public class QuizAEvent : QuizEvent
    {
        public QuizA QuizData;

        public QuizAEvent(QuizA quizData)
        {
            QuizData = quizData;
        }
    }
    public class QuizBEvent : QuizEvent
    {
        //DUMMY QUIZ B
        public QuizB QuizData;

        public QuizBEvent(QuizB quizData)
        {
            QuizData = quizData;
        }
    }
    #region end quiz
    public class EndQuizEvent : GameEvent {
        public Quiz QuizData;
        public bool ResultAnswer;

        public EndQuizEvent(Quiz quizData, bool resultAnswer)
        {
            QuizData = quizData;
            ResultAnswer = resultAnswer;
        }
    }
    #endregion

    #region QuizManager
    public class InitQuizManagerEvent : GameEvent
    {

    }
    public class StartQuizEvent : GameEvent
    {

    }
    public class SetUsernameEvent : GameEvent
    {
        public string User;
        public string Id;

        public SetUsernameEvent(string user, string id)
        {
            User = user;
            Id = id;
        }
    }
    #endregion

    #region SaveLoad
    public class SaveDataEvent : GameEvent {

        public ResultQuizData Result;

        public SaveDataEvent(ResultQuizData result) {
            Result = result;
        }
    }

    public class LoadDataEvent : GameEvent
    {
        public Transform DataParent;
        public GameObject InstantiateObject;

        public LoadDataEvent(Transform parent, GameObject obj)
        {
            DataParent = parent;
            InstantiateObject = obj;
        }
    }
    #endregion

    #region Misc
    public class BlockerButtonEvent : GameEvent
    {
        public bool IsBlock;

        public BlockerButtonEvent(bool isBlock)
        {
            IsBlock = isBlock;
        }
    }
    #endregion

    #region QuizHistory
    public class ShowDetailHistoryEvent : GameEvent
    {
        public List<string> reports;
        public List<bool> status;

        public ShowDetailHistoryEvent(List<string> reports, List<bool> status)
        {
            this.reports = reports;
            this.status = status;
        }
    }
    #endregion

    #region LoginQuiz
    public class InitLoginEvent : GameEvent
    {
    }
    #endregion

    #region Pop Up
    public class PopUpQuizEvent : GameEvent
    {
        public float Duration;
        public string TextPops;
        public PopUpType Type;

        public PopUpQuizEvent(float duration, string text, PopUpType type = PopUpType.LOGIN)
        {
            Duration = duration;
            TextPops = text;
            Type = type;
        }
    }
    #endregion

}
