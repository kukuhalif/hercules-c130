﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace CheckingKnowledge
{
    public class QuizReportPopup : MonoBehaviour
    {
        [SerializeField]
        GameObject Popup;
        [SerializeField]
        Text TextPopup;

        private void Awake()
        {
            EventManager.AddListener<ShowDetailHistoryEvent>(ShowHistoryResult);
        }

        private void OnDestroy()
        {
            EventManager.RemoveListener<ShowDetailHistoryEvent>(ShowHistoryResult);
        }

        void ShowHistoryResult(ShowDetailHistoryEvent e)
        {
            string report = "";
            for (int i = 0; i < e.reports.Count; i++)
            {
                report += "No " + (i + 1) + (e.status[i] ? " is correct" : " is incorrect") + "\n" + e.reports[i] + "\n\n";
            }
            TextPopup.text = report;
            Popup.SetActive(true);
        }
    }
}
