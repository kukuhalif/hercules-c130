﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PrinterManager
{
    public static void Print(Texture2D texture)
    {
        string date = DateTime.Now.ToLongTimeString().Replace(@"/", @" ").Replace(@":", @" ");
        string path = Application.persistentDataPath + "/report_temp_" + date + ".png";
        SaveTextureToFile(texture, path);

        System.Diagnostics.Process process = new System.Diagnostics.Process();
        process.StartInfo.CreateNoWindow = false;
        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        process.StartInfo.UseShellExecute = true;
        process.StartInfo.FileName = path;
        process.StartInfo.Verb = "print";

        process.Start();
    }

    static void SaveTextureToFile(Texture2D texture, string filename)
    {
        //Bitmap bmp = new Bitmap(50, 50);
        //Graphics g = Graphics.FromImage(bmp);

        ////paint the bitmap here with g. …
        ////I just fill a recctangle here
        //g.FillRectangle(Brushes.Green, 0, 0, 50, 50);

        ////dispose and save the file
        //g.Dispose();
        //bmp.Save(@"filepath", System.Drawing.Imaging.ImageFormat.Png);
        //bmp.Dispose();

        File.WriteAllBytes(filename, texture.EncodeToPNG());
    }
}
