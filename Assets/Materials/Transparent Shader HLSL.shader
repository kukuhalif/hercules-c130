﻿Shader "Virtual Training/Transparent"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 0)
		[NoScaleOffset]_MainTex("Color Map", 2D) = "white" {}
		[NoScaleOffset]_BumpMap("Normal Map", 2D) = "white" {}
		_NormalMapStrength("Normal Map Strength", Range(0, 10)) = 1
		_Metallic("Metallic", Range(0, 1)) = 0
		[NoScaleOffset]_MetallicMap("Metallic Map", 2D) = "white" {}
		_Smoothness("Roughness", Range(0, 1)) = 0
		[NoScaleOffset]_RoughnessMap("Roughness Map", 2D) = "white" {}
		_Occlusion("Occlusion", Range(0, 1)) = 1
		[NoScaleOffset]_OcclusionMap("Occlusion Map", 2D) = "white" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0, 0, 0, 0)
		[NoScaleOffset]_EmissionMap("Emission Map", 2D) = "white" {}
		_UvOffset("Uv Offset", Vector) = (0, 0, 0, 0)
		_UvTiling("Uv Scale", Vector) = (1, 1, 0, 0)
		[ToggleUI]_IgnoreCutaway("Ignore Cutaway", Float) = 0
		[ToggleUI]_Highlight("Highlight", Float) = 0
		[HDR]_HighlightColor("Highlight Color", Color) = (0.708604, 0.9245283, 0.1264685, 0)
		[ToggleUI]_Blink("Blink", Float) = 0
		_BlinkSpeed("BlinkSpeed", Float) = 5
		[ToggleUI]_Selected("Selected", Float) = 0
		_SelectedColor("Selected Color", Color) = (1, 0.937262, 0, 0)
		[ToggleUI]_Fresnel("Fresnel", Float) = 0
		[HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
		[HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
		[HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
	}
		SubShader
	{
		Tags
		{
			"RenderPipeline" = "UniversalPipeline"
			"RenderType" = "Transparent"
			"UniversalMaterialType" = "Lit"
			"Queue" = "Transparent"
		}
		Pass
		{
			Name "Universal Forward"
			Tags
			{
				"LightMode" = "UniversalForward"
			}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma multi_compile_instancing
	#pragma multi_compile_fog
	#pragma multi_compile _ DOTS_INSTANCING_ON
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		#pragma multi_compile _ _SCREEN_SPACE_OCCLUSION
	#pragma multi_compile _ LIGHTMAP_ON
	#pragma multi_compile _ DIRLIGHTMAP_COMBINED
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
	#pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
	#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
	#pragma multi_compile _ _SHADOWS_SOFT
	#pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
	#pragma multi_compile _ SHADOWS_SHADOWMASK
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TANGENT_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_VIEWDIRECTION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_FORWARD
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 viewDirectionWS;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 lightmapUV;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 sh;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 fogFactorAndVertexLight;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 shadowCoord;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TangentSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceViewDirection;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TimeParameters;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp3 : TEXCOORD3;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp4 : TEXCOORD4;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 interp5 : TEXCOORD5;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp6 : TEXCOORD6;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp7 : TEXCOORD7;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp8 : TEXCOORD8;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.tangentWS;
		output.interp3.xyzw = input.texCoord0;
		output.interp4.xyz = input.viewDirectionWS;
		#if defined(LIGHTMAP_ON)
		output.interp5.xy = input.lightmapUV;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.interp6.xyz = input.sh;
		#endif
		output.interp7.xyzw = input.fogFactorAndVertexLight;
		output.interp8.xyzw = input.shadowCoord;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.tangentWS = input.interp2.xyzw;
		output.texCoord0 = input.interp3.xyzw;
		output.viewDirectionWS = input.interp4.xyz;
		#if defined(LIGHTMAP_ON)
		output.lightmapUV = input.interp5.xy;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.sh = input.interp6.xyz;
		#endif
		output.fogFactorAndVertexLight = input.interp7.xyzw;
		output.shadowCoord = input.interp8.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
{
	Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
}

void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Sine_float(float In, out float Out)
{
	Out = sin(In);
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
{
	Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

void Unity_SquareRoot_float4(float4 In, out float4 Out)
{
	Out = sqrt(In);
}

void Unity_OneMinus_float4(float4 In, out float4 Out)
{
	Out = 1 - In;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float3 NormalTS;
	float3 Emission;
	float Metallic;
	float Smoothness;
	float Occlusion;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_bf62dc0526abdc83ba56517d43550c58_Out_0 = UnityBuildTexture2DStructNoScale(_BumpMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0 = SAMPLE_TEXTURE2D(_Property_bf62dc0526abdc83ba56517d43550c58_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0);
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_R_4 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.r;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_G_5 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.g;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_B_6 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.b;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_A_7 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0 = _NormalMapStrength;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2;
	Unity_NormalStrength_float((_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.xyz), _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3;
	Unity_Branch_float3(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	Unity_Branch_float3(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3, _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0 = _Fresnel;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0 = _BlinkSpeed;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2;
	Unity_Multiply_float(_Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0, IN.TimeParameters.x, _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1;
	Unity_Sine_float(_Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2, _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1;
	Unity_Absolute_float(_Sine_996a9839dbce8489b50e69d6d6035b32_Out_1, _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_8c9aee5c6a077186b88690a0324c08e3_Out_0 = UnityBuildTexture2DStructNoScale(_EmissionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8c9aee5c6a077186b88690a0324c08e3_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_R_4 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.r;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_G_5 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.g;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_B_6 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.b;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_A_7 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0 = _EmissionColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2;
	Unity_Multiply_float(_SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_370d0127a140638c9e67a21675208dbf_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_7a3a17f852073683a046fc8205563a96_Out_3;
	Unity_Branch_float4(_Property_370d0127a140638c9e67a21675208dbf_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Branch_7a3a17f852073683a046fc8205563a96_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2, _Branch_7a3a17f852073683a046fc8205563a96_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2;
	Unity_Multiply_float((_Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1.xxxx), _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_9386a4382df9c280b20071a34523f709_Out_3;
	Unity_Branch_float4(_Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3, _Branch_9386a4382df9c280b20071a34523f709_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3;
	Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 5, _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2;
	Unity_Multiply_float(_Branch_9386a4382df9c280b20071a34523f709_Out_3, (_FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3.xxxx), _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3;
	Unity_Branch_float4(_Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0, _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2, _Branch_9386a4382df9c280b20071a34523f709_Out_3, _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_9605a9496564da87924b6b35634c2d2c_Out_0 = UnityBuildTexture2DStructNoScale(_MetallicMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0 = SAMPLE_TEXTURE2D(_Property_9605a9496564da87924b6b35634c2d2c_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_R_4 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.r;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_G_5 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.g;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_B_6 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.b;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_A_7 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_e75ba5a3d5426c808435f2e633d56155_Out_0 = _Metallic;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2;
	Unity_Multiply_float(_SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0, (_Property_e75ba5a3d5426c808435f2e633d56155_Out_0.xxxx), _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3, _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_1076645d419aac8c893705d9f9f245f0_Out_0 = UnityBuildTexture2DStructNoScale(_RoughnessMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_1076645d419aac8c893705d9f9f245f0_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_R_4 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.r;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_G_5 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.g;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_B_6 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.b;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_A_7 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_99c6c012f6d3228e8e52261ac16b586a_Out_0 = _Smoothness;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2;
	Unity_Multiply_float(_SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0, (_Property_99c6c012f6d3228e8e52261ac16b586a_Out_0.xxxx), _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1;
	Unity_SquareRoot_float4(_Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2, _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1;
	Unity_OneMinus_float4(_SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3, _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0 = UnityBuildTexture2DStructNoScale(_OcclusionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0 = SAMPLE_TEXTURE2D(_Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_R_4 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.r;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_G_5 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.g;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_B_6 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.b;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_A_7 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0 = _Occlusion;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2;
	Unity_Multiply_float(_SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0, (_Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0.xxxx), _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_629cf04941951c8c878f9285842448f4_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3, _Branch_629cf04941951c8c878f9285842448f4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.NormalTS = _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	surface.Emission = (_Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3.xyz);
	surface.Metallic = (_Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3).x;
	surface.Smoothness = (_Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3).x;
	surface.Occlusion = (_Branch_629cf04941951c8c878f9285842448f4_Out_3).x;
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 unnormalizedNormalWS = input.normalWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	const float renormFactor = 1.0 / length(unnormalizedNormalWS);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceNormal = renormFactor * input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceViewDirection = input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpacePosition = input.positionWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.uv0 = input.texCoord0;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
	#endif

	#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
	#else
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif

	#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

		return output;
	}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "GBuffer"
	Tags
	{
		"LightMode" = "UniversalGBuffer"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma multi_compile_instancing
	#pragma multi_compile_fog
	#pragma multi_compile _ DOTS_INSTANCING_ON
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		#pragma multi_compile _ LIGHTMAP_ON
	#pragma multi_compile _ DIRLIGHTMAP_COMBINED
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
	#pragma multi_compile _ _SHADOWS_SOFT
	#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
	#pragma multi_compile _ _GBUFFER_NORMALS_OCT
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TANGENT_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_VIEWDIRECTION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_GBUFFER
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 viewDirectionWS;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 lightmapUV;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 sh;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 fogFactorAndVertexLight;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 shadowCoord;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TangentSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceViewDirection;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TimeParameters;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp3 : TEXCOORD3;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp4 : TEXCOORD4;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 interp5 : TEXCOORD5;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp6 : TEXCOORD6;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp7 : TEXCOORD7;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp8 : TEXCOORD8;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.tangentWS;
		output.interp3.xyzw = input.texCoord0;
		output.interp4.xyz = input.viewDirectionWS;
		#if defined(LIGHTMAP_ON)
		output.interp5.xy = input.lightmapUV;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.interp6.xyz = input.sh;
		#endif
		output.interp7.xyzw = input.fogFactorAndVertexLight;
		output.interp8.xyzw = input.shadowCoord;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.tangentWS = input.interp2.xyzw;
		output.texCoord0 = input.interp3.xyzw;
		output.viewDirectionWS = input.interp4.xyz;
		#if defined(LIGHTMAP_ON)
		output.lightmapUV = input.interp5.xy;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.sh = input.interp6.xyz;
		#endif
		output.fogFactorAndVertexLight = input.interp7.xyzw;
		output.shadowCoord = input.interp8.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
{
	Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
}

void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Sine_float(float In, out float Out)
{
	Out = sin(In);
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
{
	Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

void Unity_SquareRoot_float4(float4 In, out float4 Out)
{
	Out = sqrt(In);
}

void Unity_OneMinus_float4(float4 In, out float4 Out)
{
	Out = 1 - In;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float3 NormalTS;
	float3 Emission;
	float Metallic;
	float Smoothness;
	float Occlusion;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_bf62dc0526abdc83ba56517d43550c58_Out_0 = UnityBuildTexture2DStructNoScale(_BumpMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0 = SAMPLE_TEXTURE2D(_Property_bf62dc0526abdc83ba56517d43550c58_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0);
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_R_4 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.r;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_G_5 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.g;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_B_6 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.b;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_A_7 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0 = _NormalMapStrength;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2;
	Unity_NormalStrength_float((_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.xyz), _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3;
	Unity_Branch_float3(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	Unity_Branch_float3(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3, _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0 = _Fresnel;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0 = _BlinkSpeed;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2;
	Unity_Multiply_float(_Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0, IN.TimeParameters.x, _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1;
	Unity_Sine_float(_Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2, _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1;
	Unity_Absolute_float(_Sine_996a9839dbce8489b50e69d6d6035b32_Out_1, _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_8c9aee5c6a077186b88690a0324c08e3_Out_0 = UnityBuildTexture2DStructNoScale(_EmissionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8c9aee5c6a077186b88690a0324c08e3_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_R_4 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.r;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_G_5 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.g;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_B_6 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.b;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_A_7 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0 = _EmissionColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2;
	Unity_Multiply_float(_SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_370d0127a140638c9e67a21675208dbf_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_7a3a17f852073683a046fc8205563a96_Out_3;
	Unity_Branch_float4(_Property_370d0127a140638c9e67a21675208dbf_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Branch_7a3a17f852073683a046fc8205563a96_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2, _Branch_7a3a17f852073683a046fc8205563a96_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2;
	Unity_Multiply_float((_Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1.xxxx), _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_9386a4382df9c280b20071a34523f709_Out_3;
	Unity_Branch_float4(_Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3, _Branch_9386a4382df9c280b20071a34523f709_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3;
	Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 5, _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2;
	Unity_Multiply_float(_Branch_9386a4382df9c280b20071a34523f709_Out_3, (_FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3.xxxx), _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3;
	Unity_Branch_float4(_Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0, _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2, _Branch_9386a4382df9c280b20071a34523f709_Out_3, _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_9605a9496564da87924b6b35634c2d2c_Out_0 = UnityBuildTexture2DStructNoScale(_MetallicMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0 = SAMPLE_TEXTURE2D(_Property_9605a9496564da87924b6b35634c2d2c_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_R_4 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.r;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_G_5 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.g;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_B_6 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.b;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_A_7 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_e75ba5a3d5426c808435f2e633d56155_Out_0 = _Metallic;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2;
	Unity_Multiply_float(_SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0, (_Property_e75ba5a3d5426c808435f2e633d56155_Out_0.xxxx), _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3, _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_1076645d419aac8c893705d9f9f245f0_Out_0 = UnityBuildTexture2DStructNoScale(_RoughnessMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_1076645d419aac8c893705d9f9f245f0_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_R_4 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.r;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_G_5 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.g;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_B_6 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.b;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_A_7 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_99c6c012f6d3228e8e52261ac16b586a_Out_0 = _Smoothness;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2;
	Unity_Multiply_float(_SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0, (_Property_99c6c012f6d3228e8e52261ac16b586a_Out_0.xxxx), _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1;
	Unity_SquareRoot_float4(_Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2, _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1;
	Unity_OneMinus_float4(_SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3, _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0 = UnityBuildTexture2DStructNoScale(_OcclusionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0 = SAMPLE_TEXTURE2D(_Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_R_4 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.r;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_G_5 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.g;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_B_6 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.b;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_A_7 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0 = _Occlusion;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2;
	Unity_Multiply_float(_SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0, (_Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0.xxxx), _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_629cf04941951c8c878f9285842448f4_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3, _Branch_629cf04941951c8c878f9285842448f4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.NormalTS = _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	surface.Emission = (_Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3.xyz);
	surface.Metallic = (_Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3).x;
	surface.Smoothness = (_Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3).x;
	surface.Occlusion = (_Branch_629cf04941951c8c878f9285842448f4_Out_3).x;
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 unnormalizedNormalWS = input.normalWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	const float renormFactor = 1.0 / length(unnormalizedNormalWS);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceNormal = renormFactor * input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceViewDirection = input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpacePosition = input.positionWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.uv0 = input.texCoord0;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
	#endif

	#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
	#else
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif

	#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

		return output;
	}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityGBuffer.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRGBufferPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "ShadowCaster"
	Tags
	{
		"LightMode" = "ShadowCaster"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On
	ColorMask 0

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma multi_compile_instancing
	#pragma multi_compile _ DOTS_INSTANCING_ON
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_SHADOWCASTER
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "DepthOnly"
	Tags
	{
		"LightMode" = "DepthOnly"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On
	ColorMask 0

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma multi_compile_instancing
	#pragma multi_compile _ DOTS_INSTANCING_ON
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_DEPTHONLY
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "DepthNormals"
	Tags
	{
		"LightMode" = "DepthNormals"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma multi_compile_instancing
	#pragma multi_compile _ DOTS_INSTANCING_ON
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TANGENT_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_DEPTHNORMALSONLY
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TangentSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp3 : TEXCOORD3;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.tangentWS;
		output.interp3.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.tangentWS = input.interp2.xyzw;
		output.texCoord0 = input.interp3.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
{
	Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
}

void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 NormalTS;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_bf62dc0526abdc83ba56517d43550c58_Out_0 = UnityBuildTexture2DStructNoScale(_BumpMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0 = SAMPLE_TEXTURE2D(_Property_bf62dc0526abdc83ba56517d43550c58_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0);
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_R_4 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.r;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_G_5 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.g;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_B_6 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.b;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_A_7 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0 = _NormalMapStrength;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2;
	Unity_NormalStrength_float((_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.xyz), _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3;
	Unity_Branch_float3(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	Unity_Branch_float3(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3, _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.NormalTS = _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);
#endif



#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthNormalsOnlyPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "Meta"
	Tags
	{
		"LightMode" = "Meta"
	}

		// Render State
		Cull Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD2
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_VIEWDIRECTION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_META
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv2 : TEXCOORD2;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 viewDirectionWS;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceViewDirection;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TimeParameters;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp3 : TEXCOORD3;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.texCoord0;
		output.interp3.xyz = input.viewDirectionWS;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.texCoord0 = input.interp2.xyzw;
		output.viewDirectionWS = input.interp3.xyz;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Sine_float(float In, out float Out)
{
	Out = sin(In);
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
{
	Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float3 Emission;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0 = _Fresnel;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0 = _BlinkSpeed;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2;
	Unity_Multiply_float(_Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0, IN.TimeParameters.x, _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1;
	Unity_Sine_float(_Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2, _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1;
	Unity_Absolute_float(_Sine_996a9839dbce8489b50e69d6d6035b32_Out_1, _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_8c9aee5c6a077186b88690a0324c08e3_Out_0 = UnityBuildTexture2DStructNoScale(_EmissionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8c9aee5c6a077186b88690a0324c08e3_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_R_4 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.r;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_G_5 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.g;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_B_6 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.b;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_A_7 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0 = _EmissionColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2;
	Unity_Multiply_float(_SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_370d0127a140638c9e67a21675208dbf_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_7a3a17f852073683a046fc8205563a96_Out_3;
	Unity_Branch_float4(_Property_370d0127a140638c9e67a21675208dbf_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Branch_7a3a17f852073683a046fc8205563a96_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2, _Branch_7a3a17f852073683a046fc8205563a96_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2;
	Unity_Multiply_float((_Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1.xxxx), _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_9386a4382df9c280b20071a34523f709_Out_3;
	Unity_Branch_float4(_Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3, _Branch_9386a4382df9c280b20071a34523f709_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3;
	Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 5, _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2;
	Unity_Multiply_float(_Branch_9386a4382df9c280b20071a34523f709_Out_3, (_FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3.xxxx), _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3;
	Unity_Branch_float4(_Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0, _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2, _Branch_9386a4382df9c280b20071a34523f709_Out_3, _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.Emission = (_Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3.xyz);
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 unnormalizedNormalWS = input.normalWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	const float renormFactor = 1.0 / length(unnormalizedNormalWS);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceNormal = renormFactor * input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceViewDirection = input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpacePosition = input.positionWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.uv0 = input.texCoord0;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
	#endif

	#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
	#else
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif

	#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

		return output;
	}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

	ENDHLSL
}
Pass
{
		// Name: <None>
		Tags
		{
			"LightMode" = "Universal2D"
		}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 4.5
	#pragma exclude_renderers gles gles3 glcore
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_2D
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

	ENDHLSL
}
	}
		SubShader
	{
		Tags
		{
			"RenderPipeline" = "UniversalPipeline"
			"RenderType" = "Transparent"
			"UniversalMaterialType" = "Lit"
			"Queue" = "Transparent"
		}
		Pass
		{
			Name "Universal Forward"
			Tags
			{
				"LightMode" = "UniversalForward"
			}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma multi_compile_instancing
	#pragma multi_compile_fog
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		#pragma multi_compile _ _SCREEN_SPACE_OCCLUSION
	#pragma multi_compile _ LIGHTMAP_ON
	#pragma multi_compile _ DIRLIGHTMAP_COMBINED
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
	#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
	#pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
	#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
	#pragma multi_compile _ _SHADOWS_SOFT
	#pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
	#pragma multi_compile _ SHADOWS_SHADOWMASK
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TANGENT_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_VIEWDIRECTION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_FORWARD
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 viewDirectionWS;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 lightmapUV;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 sh;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 fogFactorAndVertexLight;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 shadowCoord;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TangentSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceViewDirection;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TimeParameters;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp3 : TEXCOORD3;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp4 : TEXCOORD4;
		#endif
		#if defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float2 interp5 : TEXCOORD5;
		#endif
		#endif
		#if !defined(LIGHTMAP_ON)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp6 : TEXCOORD6;
		#endif
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp7 : TEXCOORD7;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp8 : TEXCOORD8;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.tangentWS;
		output.interp3.xyzw = input.texCoord0;
		output.interp4.xyz = input.viewDirectionWS;
		#if defined(LIGHTMAP_ON)
		output.interp5.xy = input.lightmapUV;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.interp6.xyz = input.sh;
		#endif
		output.interp7.xyzw = input.fogFactorAndVertexLight;
		output.interp8.xyzw = input.shadowCoord;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.tangentWS = input.interp2.xyzw;
		output.texCoord0 = input.interp3.xyzw;
		output.viewDirectionWS = input.interp4.xyz;
		#if defined(LIGHTMAP_ON)
		output.lightmapUV = input.interp5.xy;
		#endif
		#if !defined(LIGHTMAP_ON)
		output.sh = input.interp6.xyz;
		#endif
		output.fogFactorAndVertexLight = input.interp7.xyzw;
		output.shadowCoord = input.interp8.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
{
	Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
}

void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Sine_float(float In, out float Out)
{
	Out = sin(In);
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
{
	Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

void Unity_SquareRoot_float4(float4 In, out float4 Out)
{
	Out = sqrt(In);
}

void Unity_OneMinus_float4(float4 In, out float4 Out)
{
	Out = 1 - In;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float3 NormalTS;
	float3 Emission;
	float Metallic;
	float Smoothness;
	float Occlusion;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_bf62dc0526abdc83ba56517d43550c58_Out_0 = UnityBuildTexture2DStructNoScale(_BumpMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0 = SAMPLE_TEXTURE2D(_Property_bf62dc0526abdc83ba56517d43550c58_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0);
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_R_4 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.r;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_G_5 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.g;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_B_6 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.b;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_A_7 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0 = _NormalMapStrength;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2;
	Unity_NormalStrength_float((_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.xyz), _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3;
	Unity_Branch_float3(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	Unity_Branch_float3(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3, _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0 = _Fresnel;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0 = _BlinkSpeed;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2;
	Unity_Multiply_float(_Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0, IN.TimeParameters.x, _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1;
	Unity_Sine_float(_Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2, _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1;
	Unity_Absolute_float(_Sine_996a9839dbce8489b50e69d6d6035b32_Out_1, _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_8c9aee5c6a077186b88690a0324c08e3_Out_0 = UnityBuildTexture2DStructNoScale(_EmissionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8c9aee5c6a077186b88690a0324c08e3_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_R_4 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.r;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_G_5 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.g;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_B_6 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.b;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_A_7 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0 = _EmissionColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2;
	Unity_Multiply_float(_SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_370d0127a140638c9e67a21675208dbf_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_7a3a17f852073683a046fc8205563a96_Out_3;
	Unity_Branch_float4(_Property_370d0127a140638c9e67a21675208dbf_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Branch_7a3a17f852073683a046fc8205563a96_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2, _Branch_7a3a17f852073683a046fc8205563a96_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2;
	Unity_Multiply_float((_Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1.xxxx), _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_9386a4382df9c280b20071a34523f709_Out_3;
	Unity_Branch_float4(_Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3, _Branch_9386a4382df9c280b20071a34523f709_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3;
	Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 5, _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2;
	Unity_Multiply_float(_Branch_9386a4382df9c280b20071a34523f709_Out_3, (_FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3.xxxx), _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3;
	Unity_Branch_float4(_Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0, _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2, _Branch_9386a4382df9c280b20071a34523f709_Out_3, _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_9605a9496564da87924b6b35634c2d2c_Out_0 = UnityBuildTexture2DStructNoScale(_MetallicMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0 = SAMPLE_TEXTURE2D(_Property_9605a9496564da87924b6b35634c2d2c_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_R_4 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.r;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_G_5 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.g;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_B_6 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.b;
	float _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_A_7 = _SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_e75ba5a3d5426c808435f2e633d56155_Out_0 = _Metallic;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2;
	Unity_Multiply_float(_SampleTexture2D_357d60208ff62f81a3345f13ea8462aa_RGBA_0, (_Property_e75ba5a3d5426c808435f2e633d56155_Out_0.xxxx), _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ad0eae21466e088aaa5ac1f81ea001bc_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_35c9775cd19a9c878db7cf87307c2078_Out_3, _Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_1076645d419aac8c893705d9f9f245f0_Out_0 = UnityBuildTexture2DStructNoScale(_RoughnessMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_1076645d419aac8c893705d9f9f245f0_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_R_4 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.r;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_G_5 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.g;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_B_6 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.b;
	float _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_A_7 = _SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_99c6c012f6d3228e8e52261ac16b586a_Out_0 = _Smoothness;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2;
	Unity_Multiply_float(_SampleTexture2D_3315d0bea92edd81b7636fccf8bc16bb_RGBA_0, (_Property_99c6c012f6d3228e8e52261ac16b586a_Out_0.xxxx), _Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1;
	Unity_SquareRoot_float4(_Multiply_00974dbef6e57c86841e102dd39a2ca6_Out_2, _SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1;
	Unity_OneMinus_float4(_SquareRoot_12eb355887a5cb848e68709e59614c74_Out_1, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _OneMinus_54a3f633020f3b838997370f85e30aeb_Out_1, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3cebd291189dec879bb3eb2f4e841233_Out_3, _Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0 = UnityBuildTexture2DStructNoScale(_OcclusionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0 = SAMPLE_TEXTURE2D(_Property_559f04c841ea1c89bf925aaa3c1df1dc_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_R_4 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.r;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_G_5 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.g;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_B_6 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.b;
	float _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_A_7 = _SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0 = _Occlusion;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2;
	Unity_Multiply_float(_SampleTexture2D_70969c4250453388bd8fafa49b1ed18d_RGBA_0, (_Property_f1da3ae8d16f5088abca4cfbb371d845_Out_0.xxxx), _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_0e7d2a5453c77d85951cc7e93dfb3dd7_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_629cf04941951c8c878f9285842448f4_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float4(0, 0, 0, 0), _Branch_3f7a4cd090ce2b8b801462a14dcd9165_Out_3, _Branch_629cf04941951c8c878f9285842448f4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.NormalTS = _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	surface.Emission = (_Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3.xyz);
	surface.Metallic = (_Branch_1f1eb1f663bed388b1140a4a954aa0d6_Out_3).x;
	surface.Smoothness = (_Branch_28a1329b92242b83b0cd5a08b4b8c373_Out_3).x;
	surface.Occlusion = (_Branch_629cf04941951c8c878f9285842448f4_Out_3).x;
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 unnormalizedNormalWS = input.normalWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	const float renormFactor = 1.0 / length(unnormalizedNormalWS);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceNormal = renormFactor * input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceViewDirection = input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpacePosition = input.positionWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.uv0 = input.texCoord0;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
	#endif

	#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
	#else
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif

	#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

		return output;
	}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "ShadowCaster"
	Tags
	{
		"LightMode" = "ShadowCaster"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On
	ColorMask 0

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma multi_compile_instancing
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_SHADOWCASTER
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "DepthOnly"
	Tags
	{
		"LightMode" = "DepthOnly"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On
	ColorMask 0

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma multi_compile_instancing
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_DEPTHONLY
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "DepthNormals"
	Tags
	{
		"LightMode" = "DepthNormals"
	}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite On

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma multi_compile_instancing
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TANGENT_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_DEPTHNORMALSONLY
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TangentSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp3 : TEXCOORD3;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.tangentWS;
		output.interp3.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.tangentWS = input.interp2.xyzw;
		output.texCoord0 = input.interp3.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
{
	Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
}

void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 NormalTS;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_bf62dc0526abdc83ba56517d43550c58_Out_0 = UnityBuildTexture2DStructNoScale(_BumpMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0 = SAMPLE_TEXTURE2D(_Property_bf62dc0526abdc83ba56517d43550c58_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0);
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_R_4 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.r;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_G_5 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.g;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_B_6 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.b;
	float _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_A_7 = _SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0 = _NormalMapStrength;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2;
	Unity_NormalStrength_float((_SampleTexture2D_4aa2de52573a4ff0bb69485b851a6a72_RGBA_0.xyz), _Property_da2f227988c63f86a70b9c880d5b0d75_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3;
	Unity_Branch_float3(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _NormalStrength_c9646228e50e49638822cb09830320ff_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	Unity_Branch_float3(_Or_1aab9267a405378e98608221cbe687fd_Out_2, float3(0, 0, 0), _Branch_125c3c5cea66c48dbc54f4e692e52e69_Out_3, _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.NormalTS = _Branch_a78340df5e7f6789aa25e507f58d48ba_Out_3;
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);
#endif



#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthNormalsOnlyPass.hlsl"

	ENDHLSL
}
Pass
{
	Name "Meta"
	Tags
	{
		"LightMode" = "Meta"
	}

		// Render State
		Cull Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD2
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_NORMAL_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_VIEWDIRECTION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_META
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv2 : TEXCOORD2;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 viewDirectionWS;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpaceViewDirection;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 TimeParameters;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp1 : TEXCOORD1;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp2 : TEXCOORD2;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp3 : TEXCOORD3;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyz = input.normalWS;
		output.interp2.xyzw = input.texCoord0;
		output.interp3.xyz = input.viewDirectionWS;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.normalWS = input.interp1.xyz;
		output.texCoord0 = input.interp2.xyzw;
		output.viewDirectionWS = input.interp3.xyz;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Sine_float(float In, out float Out)
{
	Out = sin(In);
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
{
	Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float3 Emission;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0 = _Fresnel;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0 = _BlinkSpeed;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2;
	Unity_Multiply_float(_Property_bdb8ae1d3a5c248685ddce8dbeedcd70_Out_0, IN.TimeParameters.x, _Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1;
	Unity_Sine_float(_Multiply_367ca68756fa3284abdfc7709ec2d1e4_Out_2, _Sine_996a9839dbce8489b50e69d6d6035b32_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1;
	Unity_Absolute_float(_Sine_996a9839dbce8489b50e69d6d6035b32_Out_1, _Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_8c9aee5c6a077186b88690a0324c08e3_Out_0 = UnityBuildTexture2DStructNoScale(_EmissionMap);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8c9aee5c6a077186b88690a0324c08e3_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_R_4 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.r;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_G_5 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.g;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_B_6 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.b;
	float _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_A_7 = _SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0 = _EmissionColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2;
	Unity_Multiply_float(_SampleTexture2D_37d4365d20b58088b0ab062227d418bf_RGBA_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_370d0127a140638c9e67a21675208dbf_Out_0 = _Blink;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_7a3a17f852073683a046fc8205563a96_Out_3;
	Unity_Branch_float4(_Property_370d0127a140638c9e67a21675208dbf_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Property_f2eea97f3eec0882b12b3f7902d27633_Out_0, _Branch_7a3a17f852073683a046fc8205563a96_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_ffabd2949d0e3983a7e62747e292ab5d_Out_2, _Branch_7a3a17f852073683a046fc8205563a96_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2;
	Unity_Multiply_float((_Absolute_a3ecfae702482d84a705ce9903d440c8_Out_1.xxxx), _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc42b6038bd1f8a8c1242eb496ce101_Out_3, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_9386a4382df9c280b20071a34523f709_Out_3;
	Unity_Branch_float4(_Property_ea2e99d5d5b1ad8d8c72af81b77351e5_Out_0, _Multiply_5e2751ec6331ad8cb6371dd1043c0dac_Out_2, _Branch_97fbdbfd630cec8d826bf23edb93b6d5_Out_3, _Branch_9386a4382df9c280b20071a34523f709_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3;
	Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 5, _FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2;
	Unity_Multiply_float(_Branch_9386a4382df9c280b20071a34523f709_Out_3, (_FresnelEffect_74fb1bd3ae03c987bde9e617a71ea30c_Out_3.xxxx), _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3;
	Unity_Branch_float4(_Property_5b66ff0e45d5bd849ea1a30668a310df_Out_0, _Multiply_7b28ad3f934afe8b941360b19d284f54_Out_2, _Branch_9386a4382df9c280b20071a34523f709_Out_3, _Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.Emission = (_Branch_4e2dd6fb8ee732888701d42af3c59f83_Out_3.xyz);
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 unnormalizedNormalWS = input.normalWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	const float renormFactor = 1.0 / length(unnormalizedNormalWS);
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceNormal = renormFactor * input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
	#endif



	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpaceViewDirection = input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.WorldSpacePosition = input.positionWS;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.uv0 = input.texCoord0;
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	output.TimeParameters = _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
	#endif

	#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
	#else
	#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
	#endif

	#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

		return output;
	}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

	ENDHLSL
}
Pass
{
		// Name: <None>
		Tags
		{
			"LightMode" = "Universal2D"
		}

		// Render State
		Cull Back
	Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
	ZTest LEqual
	ZWrite Off

		// Debug
		// <None>

		// --------------------------------------------------
		// Pass

		HLSLPROGRAM

		// Pragmas
		#pragma target 2.0
	#pragma only_renderers gles gles3 glcore d3d11
	#pragma multi_compile_instancing
	#pragma vertex vert
	#pragma fragment frag

		// DotsInstancingOptions: <None>
		// HybridV1InjectedBuiltinProperties: <None>

		// Keywords
		// PassKeywords: <None>
		#pragma multi_compile CLIP_PLANE CLIP_SPHERE CLIP_CORNER CLIP_BOX CLIP_NONE

	#if defined(CLIP_PLANE)
		#define KEYWORD_PERMUTATION_0
	#elif defined(CLIP_SPHERE)
		#define KEYWORD_PERMUTATION_1
	#elif defined(CLIP_CORNER)
		#define KEYWORD_PERMUTATION_2
	#elif defined(CLIP_BOX)
		#define KEYWORD_PERMUTATION_3
	#else
		#define KEYWORD_PERMUTATION_4
	#endif


		// Defines
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _SURFACE_TYPE_TRANSPARENT 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _AlphaClip 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMALMAP 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define _NORMAL_DROPOFF_TS 1
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_NORMAL
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TANGENT
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define ATTRIBUTES_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_POSITION_WS
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_TEXCOORD0
	#endif

	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#define VARYINGS_NEED_CULLFACE
	#endif

		#define FEATURES_GRAPH_VERTEX
		/* WARNING: $splice Could not find named fragment 'PassInstancing' */
		#define SHADERPASS SHADERPASS_2D
		/* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

		// Includes
		#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

		// --------------------------------------------------
		// Structs and Packing

		struct Attributes
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionOS : POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 normalOS : NORMAL;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 tangentOS : TANGENT;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0 : TEXCOORD0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : INSTANCEID_SEMANTIC;
		#endif
		#endif
	};
	struct Varyings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 positionWS;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 texCoord0;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};
	struct SurfaceDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 WorldSpacePosition;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 uv0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float FaceSign;
		#endif
	};
	struct VertexDescriptionInputs
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceNormal;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpaceTangent;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 ObjectSpacePosition;
		#endif
	};
	struct PackedVaryings
	{
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 positionCS : SV_POSITION;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float3 interp0 : TEXCOORD0;
		#endif
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		float4 interp1 : TEXCOORD1;
		#endif
		#if UNITY_ANY_INSTANCING_ENABLED
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint instanceID : CUSTOM_INSTANCE_ID;
		#endif
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
		#endif
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
		#endif
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
		FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
		#endif
		#endif
	};

		#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	PackedVaryings PackVaryings(Varyings input)
	{
		PackedVaryings output;
		output.positionCS = input.positionCS;
		output.interp0.xyz = input.positionWS;
		output.interp1.xyzw = input.texCoord0;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	Varyings UnpackVaryings(PackedVaryings input)
	{
		Varyings output;
		output.positionCS = input.positionCS;
		output.positionWS = input.interp0.xyz;
		output.texCoord0 = input.interp1.xyzw;
		#if UNITY_ANY_INSTANCING_ENABLED
		output.instanceID = input.instanceID;
		#endif
		#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
		output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
		#endif
		#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
		output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
		#endif
		#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
		output.cullFace = input.cullFace;
		#endif
		return output;
	}
	#endif

	// --------------------------------------------------
	// Graph

	// Graph Properties
	CBUFFER_START(UnityPerMaterial)
float4 _Color;
float4 _MainTex_TexelSize;
float4 _BumpMap_TexelSize;
float _NormalMapStrength;
float _Metallic;
float4 _MetallicMap_TexelSize;
float _Smoothness;
float4 _RoughnessMap_TexelSize;
float _Occlusion;
float4 _OcclusionMap_TexelSize;
float4 _EmissionColor;
float4 _EmissionMap_TexelSize;
float2 _UvOffset;
float2 _UvTiling;
half _IgnoreCutaway;
half _Highlight;
half4 _HighlightColor;
half _Blink;
half _BlinkSpeed;
half _Selected;
half4 _SelectedColor;
half _Fresnel;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);
TEXTURE2D(_BumpMap);
SAMPLER(sampler_BumpMap);
TEXTURE2D(_MetallicMap);
SAMPLER(sampler_MetallicMap);
TEXTURE2D(_RoughnessMap);
SAMPLER(sampler_RoughnessMap);
TEXTURE2D(_OcclusionMap);
SAMPLER(sampler_OcclusionMap);
TEXTURE2D(_EmissionMap);
SAMPLER(sampler_EmissionMap);
float3 _SectionPlane;
float3 _SectionPoint;
float _Radius;
half _Inverse;
float3 _SectionScale;
float4x4 _WorldToBoxMatrix;

// Graph Functions

void Unity_Or_half(half A, half B, out half Out)
{
	Out = A || B;
}

void Unity_Branch_half4(half Predicate, half4 True, half4 False, out half4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
	Out = UV * Tiling + Offset;
}

void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
{
	Out = A * B;
}

void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
{
	Out = Predicate ? True : False;
}

void Unity_Multiply_float(float A, float B, out float Out)
{
	Out = A * B;
}

void Unity_Branch_float(float Predicate, float True, float False, out float Out)
{
	Out = Predicate ? True : False;
}

void Unity_Not_float(float In, out float Out)
{
	Out = !In;
}

void Unity_And_float(float A, float B, out float Out)
{
	Out = A && B;
}

void getWpos_float(float3 pos, out float3 Out) {
	Out = GetAbsolutePositionWS(pos);
}

void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
{
	Out = A - B;
}

void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
{
	Out = dot(A, B);
}

void Unity_Comparison_Greater_float(float A, float B, out float Out)
{
	Out = A > B ? 1 : 0;
}

void Unity_Or_float(float A, float B, out float Out)
{
	Out = A || B;
}

struct Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(float3 Vector3_1AEFA16C, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float3 _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1);
	float3 _Property_915ade5a8886a686a82e14198868650c_Out_0 = Vector3_D61E2AED;
	float3 _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2;
	Unity_Subtract_float3(_getWposCustomFunction_2193d0a68f8ba68d8f61756747aa2447_Out_1, _Property_915ade5a8886a686a82e14198868650c_Out_0, _Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2);
	float3 _Property_2bfff938118ff58284f324543594887e_Out_0 = Vector3_1AEFA16C;
	float _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2;
	Unity_DotProduct_float3(_Subtract_e27639cc8ec14f86a90260ef621a68ed_Out_2, _Property_2bfff938118ff58284f324543594887e_Out_0, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2);
	float _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _DotProduct_597a252c1dfee0858d3cf5960338d618_Out_2, _Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2);
	float _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2;
	Unity_Comparison_Greater_float(_Multiply_b8ed235df01cca82a65a6ab73d6618dd_Out_2, 0, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2);
	float _Or_3129c5dec674b68180aec44072cf0d04_Out_2;
	Unity_Or_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Comparison_d69de0e29eb2088693cb58dfd19f2abb_Out_2, _Or_3129c5dec674b68180aec44072cf0d04_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_3129c5dec674b68180aec44072cf0d04_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Distance_float3(float3 A, float3 B, out float Out)
{
	Out = distance(A, B);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
	Out = A - B;
}

struct Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(float Vector1_7E9A1F65, float3 Vector3_D61E2AED, float Boolean_3DAEDA48, float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da IN, out float OutVector1_1)
{
	float _IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0 = max(0, IN.FaceSign);
	float _Not_56600d7043308a88a4558b5d01064754_Out_1;
	Unity_Not_float(_IsFrontFace_95ea98181c1c648eb414e52735f92139_Out_0, _Not_56600d7043308a88a4558b5d01064754_Out_1);
	float _Property_a14f04943a69a989a02870be6fab9408_Out_0 = Boolean_E5602702;
	float _And_12affe12daf4cb8490b0a67158b4a15d_Out_2;
	Unity_And_float(_Not_56600d7043308a88a4558b5d01064754_Out_1, _Property_a14f04943a69a989a02870be6fab9408_Out_0, _And_12affe12daf4cb8490b0a67158b4a15d_Out_2);
	float _Property_149a2b19420d7386bd0d7d0e14167604_Out_0 = Boolean_3DAEDA48;
	float _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3;
	Unity_Branch_float(_Property_149a2b19420d7386bd0d7d0e14167604_Out_0, -1, 1, _Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3);
	float _Property_fee6372b3f60e18f8484060430e93b19_Out_0 = Vector1_7E9A1F65;
	float3 _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1);
	float3 _Property_b546558159f411878182cfde24c55155_Out_0 = Vector3_D61E2AED;
	float _Distance_f7392644fe85fb888293a743b8878a66_Out_2;
	Unity_Distance_float3(_getWposCustomFunction_2f0b0fc2ca570d8f9def56a4bc42b7a9_Out_1, _Property_b546558159f411878182cfde24c55155_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2);
	float _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2;
	Unity_Subtract_float(_Property_fee6372b3f60e18f8484060430e93b19_Out_0, _Distance_f7392644fe85fb888293a743b8878a66_Out_2, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2);
	float _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2;
	Unity_Multiply_float(_Branch_b86fd2b382047b809fe0252bc8b86ae1_Out_3, _Subtract_84721a0d2b27328891b73e532497c3a0_Out_2, _Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2);
	float _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2;
	Unity_Comparison_Greater_float(_Multiply_ec54c9c66c06b382806ad0dd0ececa82_Out_2, 0, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2);
	float _Or_1033b8317756448795368bb7962924ae_Out_2;
	Unity_Or_float(_And_12affe12daf4cb8490b0a67158b4a15d_Out_2, _Comparison_06b3990de57bb088aada519ee89b1c20_Out_2, _Or_1033b8317756448795368bb7962924ae_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_Or_1033b8317756448795368bb7962924ae_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

void Unity_Multiply_float(float4x4 A, float4 B, out float4 Out)
{
	Out = mul(A, B);
}

struct Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipcorner_634dcb851748abe44854d7d9a472dc55(float Vector1_AFEACB7D, float Boolean_C989AFF8, float Boolean_384D9642, float4x4 Matrix4_EC01D29A, Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 IN, out float OutVector1_1)
{
	float _IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0 = max(0, IN.FaceSign);
	float _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1;
	Unity_Not_float(_IsFrontFace_e05d10c060c44d8782173b56d1306d6c_Out_0, _Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1);
	float _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0 = Boolean_384D9642;
	float _And_5db030bfce4eb48788d919377fc37e6d_Out_2;
	Unity_And_float(_Not_19b9a72ed1a4c8819d92ef536057ff4a_Out_1, _Property_2d9a407e23d70d8181414f7282af3f0d_Out_0, _And_5db030bfce4eb48788d919377fc37e6d_Out_2);
	float4x4 _Property_274584a4ad08e78e8794ae9099241498_Out_0 = Matrix4_EC01D29A;
	float3 _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1);
	float _Split_7939415ee341cc89b2c74fd17d45b20d_R_1 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[0];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_G_2 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[1];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_B_3 = _getWposCustomFunction_b0aabc79d634828e8d7c3adeb1f63f70_Out_1[2];
	float _Split_7939415ee341cc89b2c74fd17d45b20d_A_4 = 0;
	float4 _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0 = float4(_Split_7939415ee341cc89b2c74fd17d45b20d_R_1, _Split_7939415ee341cc89b2c74fd17d45b20d_G_2, _Split_7939415ee341cc89b2c74fd17d45b20d_B_3, 1);
	float4 _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2;
	Unity_Multiply_float(_Property_274584a4ad08e78e8794ae9099241498_Out_0, _Vector4_3025200ac2e3648ab07f680b0f9f4545_Out_0, _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2);
	float _Split_c12963a7eaba668cb61be5cb58577251_R_1 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[0];
	float _Split_c12963a7eaba668cb61be5cb58577251_G_2 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[1];
	float _Split_c12963a7eaba668cb61be5cb58577251_B_3 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[2];
	float _Split_c12963a7eaba668cb61be5cb58577251_A_4 = _Multiply_6e900e2ece44ea85a85259021de9dbd2_Out_2[3];
	float _Comparison_b144588815c9168eb1b436163f46330d_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_R_1, 0, _Comparison_b144588815c9168eb1b436163f46330d_Out_2);
	float _Comparison_1546e3410f433782a447ed652e3131b5_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_G_2, 0, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2);
	float _And_b4b26d853e66298381a84584ce3f5bd9_Out_2;
	Unity_And_float(_Comparison_b144588815c9168eb1b436163f46330d_Out_2, _Comparison_1546e3410f433782a447ed652e3131b5_Out_2, _And_b4b26d853e66298381a84584ce3f5bd9_Out_2);
	float _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2;
	Unity_Comparison_Greater_float(_Split_c12963a7eaba668cb61be5cb58577251_B_3, 0, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2);
	float _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2;
	Unity_And_float(_And_b4b26d853e66298381a84584ce3f5bd9_Out_2, _Comparison_d26910ebdb0dd6899999fcafcb924126_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2);
	float _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2;
	Unity_Or_float(_And_5db030bfce4eb48788d919377fc37e6d_Out_2, _And_1d1dbdb2e4bba68ea6630834c60f09cb_Out_2, _Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2);
	float _Property_933ed0743554ca84a8792fcd2bda9432_Out_0 = Boolean_C989AFF8;
	float _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3;
	Unity_Branch_float(_Property_933ed0743554ca84a8792fcd2bda9432_Out_0, 0, 1, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3);
	float _Property_55a506463957128c90f8dabc1e6675d0_Out_0 = Vector1_AFEACB7D;
	float _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
	Unity_Branch_float(_Or_1ecab7fd5b676c89bd0bb41ff9b12bf4_Out_2, _Branch_43fd0ee4069a18888d662ecfe62bbeda_Out_3, _Property_55a506463957128c90f8dabc1e6675d0_Out_0, _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3);
	OutVector1_1 = _Branch_7c868d7ed0b47c80868985a9d6c7cc26_Out_3;
}

void Unity_Absolute_float(float In, out float Out)
{
	Out = abs(In);
}

void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
{
	Out = A * B;
}

struct Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a
{
	float3 WorldSpacePosition;
	float FaceSign;
};

void SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(float3 Vector3_D867F751, float Vector1_7E168AF7, float Boolean_59E4DA04, float Boolean_E9F24FDB, float4x4 Matrix4_CECE6779, Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a IN, out float OutVector1_1)
{
	float4x4 _Property_2c348e3bec73a881abc8518c2656df66_Out_0 = Matrix4_CECE6779;
	float3 _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1;
	getWpos_float(IN.WorldSpacePosition, _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1);
	float _Split_36b395eda1043c80858a63a9ce7d4953_R_1 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[0];
	float _Split_36b395eda1043c80858a63a9ce7d4953_G_2 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[1];
	float _Split_36b395eda1043c80858a63a9ce7d4953_B_3 = _getWposCustomFunction_13017e25df2e3088a17ba2cad27dbd3c_Out_1[2];
	float _Split_36b395eda1043c80858a63a9ce7d4953_A_4 = 0;
	float4 _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0 = float4(_Split_36b395eda1043c80858a63a9ce7d4953_R_1, _Split_36b395eda1043c80858a63a9ce7d4953_G_2, _Split_36b395eda1043c80858a63a9ce7d4953_B_3, 1);
	float4 _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2;
	Unity_Multiply_float(_Property_2c348e3bec73a881abc8518c2656df66_Out_0, _Vector4_cdb1c4ee2487608881aac5ae3ecc0b0d_Out_0, _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2);
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[0];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[1];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[2];
	float _Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_A_4 = _Multiply_1d04f5aafb2cef87aa95cc0623d040a2_Out_2[3];
	float _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_R_1, _Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1);
	float3 _Property_c68b1356af1e92848fda82eee9e6b467_Out_0 = Vector3_D867F751;
	float3 _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2;
	Unity_Multiply_float(_Property_c68b1356af1e92848fda82eee9e6b467_Out_0, float3(0.5, 0.5, 0.5), _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2);
	float _Split_72ed567f33a1c389a2bb8519b7c99576_R_1 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[0];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_G_2 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[1];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_B_3 = _Multiply_0c6bb3541004b58294f4708acfe93ca4_Out_2[2];
	float _Split_72ed567f33a1c389a2bb8519b7c99576_A_4 = 0;
	float _Subtract_0386215837f7098080a9ff1566317136_Out_2;
	Unity_Subtract_float(_Absolute_c02c8a896ee28c888b1145bab30e5aa1_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_R_1, _Subtract_0386215837f7098080a9ff1566317136_Out_2);
	float _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2;
	Unity_Comparison_Greater_float(_Subtract_0386215837f7098080a9ff1566317136_Out_2, 0, _Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2);
	float _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_G_2, _Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1);
	float _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2;
	Unity_Subtract_float(_Absolute_41b012a93ead958d96229b2e9ef2a0fd_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_G_2, _Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2);
	float _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2;
	Unity_Comparison_Greater_float(_Subtract_32ceb6ee0658558fbfc1f4d2f2b1dbee_Out_2, 0, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2);
	float _Or_f3fbb081552e3684beb52f10403b3e00_Out_2;
	Unity_Or_float(_Comparison_2ec40a8fab0bd08c91c47498cfa75f4c_Out_2, _Comparison_2d8250691d35c985b11668acc98e9ea0_Out_2, _Or_f3fbb081552e3684beb52f10403b3e00_Out_2);
	float _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1;
	Unity_Absolute_float(_Split_e6f1e757cd8e4b8f9de9d517a8c4cfcd_B_3, _Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1);
	float _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2;
	Unity_Subtract_float(_Absolute_a6270234d7684d8aaaa9f0622b196887_Out_1, _Split_72ed567f33a1c389a2bb8519b7c99576_B_3, _Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2);
	float _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2;
	Unity_Comparison_Greater_float(_Subtract_66f3b6645950fa82a91e14eaecbaa87a_Out_2, 0, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2);
	float _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2;
	Unity_Or_float(_Or_f3fbb081552e3684beb52f10403b3e00_Out_2, _Comparison_286fbd0cbb5f9c8bb90a0e23dedadf71_Out_2, _Or_e58a2da9c31ee587af03709dc2605c2a_Out_2);
	float _IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0 = max(0, IN.FaceSign);
	float _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1;
	Unity_Not_float(_IsFrontFace_bc5fb14b9eb0f881957b0168cdb1a708_Out_0, _Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1);
	float _Property_e7ee5a15222aab809ecb036db708059d_Out_0 = Boolean_E9F24FDB;
	float _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2;
	Unity_And_float(_Not_f982369c850dae8bbdf38c80ee3cdcbf_Out_1, _Property_e7ee5a15222aab809ecb036db708059d_Out_0, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2);
	float _Or_8d0202171fefa388a847c0c17fdc046e_Out_2;
	Unity_Or_float(_Or_e58a2da9c31ee587af03709dc2605c2a_Out_2, _And_7f32c27e35804b839b8030fabcbfbc5e_Out_2, _Or_8d0202171fefa388a847c0c17fdc046e_Out_2);
	float _Property_0f86700521b9fc82a656db806a89abb4_Out_0 = Boolean_59E4DA04;
	float _Branch_98e9601632835481a58e3ed9a9295be8_Out_3;
	Unity_Branch_float(_Property_0f86700521b9fc82a656db806a89abb4_Out_0, 0, 1, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3);
	float _Property_209b25c756ad8a899e596647f3f1fdca_Out_0 = Vector1_7E168AF7;
	float _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
	Unity_Branch_float(_Or_8d0202171fefa388a847c0c17fdc046e_Out_2, _Branch_98e9601632835481a58e3ed9a9295be8_Out_3, _Property_209b25c756ad8a899e596647f3f1fdca_Out_0, _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3);
	OutVector1_1 = _Branch_8e6dc9390a0f6080841d3ab92766a343_Out_3;
}

struct Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f
{
	float FaceSign;
};

void SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(float Vector1_E143C85A, float Boolean_E5602702, float Boolean_E999C2D4, Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f IN, out float OutVector1_1)
{
	float _IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0 = max(0, IN.FaceSign);
	float _Not_119162585b02928da89fffa0c1cffeeb_Out_1;
	Unity_Not_float(_IsFrontFace_fc82d393337e3c8590bef12c8550d6db_Out_0, _Not_119162585b02928da89fffa0c1cffeeb_Out_1);
	float _Property_875d439714afd787922006d051224cdc_Out_0 = Boolean_E5602702;
	float _And_f849f8c40149888db78f33eacbaddd1b_Out_2;
	Unity_And_float(_Not_119162585b02928da89fffa0c1cffeeb_Out_1, _Property_875d439714afd787922006d051224cdc_Out_0, _And_f849f8c40149888db78f33eacbaddd1b_Out_2);
	float _Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0 = Boolean_E999C2D4;
	float _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3;
	Unity_Branch_float(_Property_e91d429f8a8cfe8d8260e50b503462e6_Out_0, 0, 1, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3);
	float _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0 = Vector1_E143C85A;
	float _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
	Unity_Branch_float(_And_f849f8c40149888db78f33eacbaddd1b_Out_2, _Branch_cbd9a44c7903978d8fd9adb3bb9b33fa_Out_3, _Property_83c6e9ead0e7e781bd378966062d8a37_Out_0, _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3);
	OutVector1_1 = _Branch_c4793f2bd4fd048cac3bebef097cdcc6_Out_3;
}

// Graph Vertex
struct VertexDescription
{
	float3 Position;
	float3 Normal;
	float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
	VertexDescription description = (VertexDescription)0;
	description.Position = IN.ObjectSpacePosition;
	description.Normal = IN.ObjectSpaceNormal;
	description.Tangent = IN.ObjectSpaceTangent;
	return description;
}

// Graph Pixel
struct SurfaceDescription
{
	float3 BaseColor;
	float Alpha;
	float AlphaClipThreshold;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
	SurfaceDescription surface = (SurfaceDescription)0;
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0 = _Highlight;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_3c66247b83d38384a2df70af278f0e46_Out_0 = _Selected;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Or_1aab9267a405378e98608221cbe687fd_Out_2;
	Unity_Or_half(_Property_92e9d1f9d77b258396b0eb3d56ea0e67_Out_0, _Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Or_1aab9267a405378e98608221cbe687fd_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0 = _SelectedColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Property_3e84cdddce0899829d5038ab9eda7531_Out_0 = _HighlightColor;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half4 _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3;
	Unity_Branch_half4(_Property_3c66247b83d38384a2df70af278f0e46_Out_0, _Property_6e2e4074f7346a83b2aacbc4e0c70717_Out_0, _Property_3e84cdddce0899829d5038ab9eda7531_Out_0, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0 = max(0, IN.FaceSign);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	UnityTexture2D _Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _UV_16cb4ebb61c93b8ab43910876c492148_Out_0 = IN.uv0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0 = _UvTiling;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0 = _UvOffset;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float2 _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3;
	Unity_TilingAndOffset_float((_UV_16cb4ebb61c93b8ab43910876c492148_Out_0.xy), _Property_ed9e68ad8569c38fb92c431491b7e3c3_Out_0, _Property_ae635edc3d986b88a19f4505578e3dd4_Out_0, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6bb75132415c0f8b8ffc9cec9b19250d_Out_0.tex, UnityBuildSamplerStateStruct(SamplerState_Linear_Repeat).samplerstate, _TilingAndOffset_b4b05b47621af08cb588fc64fb6a8985_Out_3);
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_R_4 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.r;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_G_5 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.g;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_B_6 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.b;
	float _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7 = _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0.a;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Property_47870e7373b702829a9b0e27c611550a_Out_0 = _Color;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2;
	Unity_Multiply_float(_SampleTexture2D_5daca3cfc21e028cb52224a310c07851_RGBA_0, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3;
	Unity_Branch_float4(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_95fbf11c1fb0c784a0b894dd0ff95820_Out_2, _Property_47870e7373b702829a9b0e27c611550a_Out_0, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4 _Branch_4805df5565606f8ea8ca133da3b78744_Out_3;
	Unity_Branch_float4(_Or_1aab9267a405378e98608221cbe687fd_Out_2, _Branch_164014770ee6ec80b569b088b12f0b7e_Out_3, _Branch_4cc09d5701b5848a8563e332d8aea8e4_Out_3, _Branch_4805df5565606f8ea8ca133da3b78744_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0 = _IgnoreCutaway;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_R_1 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[0];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_G_2 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[1];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_B_3 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[2];
	float _Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4 = _Property_47870e7373b702829a9b0e27c611550a_Out_0[3];
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Multiply_269bf244a478448cbd5290d0118990ba_Out_2;
	Unity_Multiply_float(_Split_8a5bfc7f53a5148faa6ec33b99f74896_A_4, _SampleTexture2D_5daca3cfc21e028cb52224a310c07851_A_7, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	Unity_Branch_float(_IsFrontFace_4137e86fa1a14d80aa218b9e0c977d0e_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0 = _SectionPlane;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_0200f1aac01bf38fabc15ff1ea4e955f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4 _clipplane_2d71670d3acc178f9d0c2f662d82fe3a;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.WorldSpacePosition = IN.WorldSpacePosition;
	_clipplane_2d71670d3acc178f9d0c2f662d82fe3a.FaceSign = IN.FaceSign;
	float _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	SG_clipplane_bc2d8ac1cc8ffa944bf20d3589a281b4(_Property_12fc297728e4e88d8eed46b1afb2e2cc_Out_0, _Property_43d6d983c7d78e83abdeeb9b09de345b_Out_0, _Property_8ff3ede4e319cc828efdc64ddf6064ae_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_0200f1aac01bf38fabc15ff1ea4e955f, 1, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a, _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0 = _Radius;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_937336d85ab76b8bb05333e8229c3971_Out_0 = _SectionPoint;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	half _Property_195258a99b82828981557766170a7321_Out_0 = _Inverse;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_47d24ebf819dc186aa7cbae51eb670ca = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipsphere_2646aa13428f35f4a9731cb9fd4650da _clipsphere_80c202d9293deb8790313cd539bbbc05;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.WorldSpacePosition = IN.WorldSpacePosition;
	_clipsphere_80c202d9293deb8790313cd539bbbc05.FaceSign = IN.FaceSign;
	float _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	SG_clipsphere_2646aa13428f35f4a9731cb9fd4650da(_Property_c2dfac0d0389f881b9d24f9ce7a98ccb_Out_0, _Property_937336d85ab76b8bb05333e8229c3971_Out_0, _Property_195258a99b82828981557766170a7321_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, Boolean_47d24ebf819dc186aa7cbae51eb670ca, 1, _clipsphere_80c202d9293deb8790313cd539bbbc05, _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_97446a83d2ed4d86baff8c3a56a9fff3 = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_f101c000f1d34f829e85db14596abb42_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipcorner_634dcb851748abe44854d7d9a472dc55 _clipcorner_b40d27802cde0787b2856a80b982859c;
	_clipcorner_b40d27802cde0787b2856a80b982859c.WorldSpacePosition = IN.WorldSpacePosition;
	_clipcorner_b40d27802cde0787b2856a80b982859c.FaceSign = IN.FaceSign;
	float _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	SG_clipcorner_634dcb851748abe44854d7d9a472dc55(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_97446a83d2ed4d86baff8c3a56a9fff3, _Property_f101c000f1d34f829e85db14596abb42_Out_0, _clipcorner_b40d27802cde0787b2856a80b982859c, _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float3 _Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0 = _SectionScale;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float Boolean_8607cdfb7a1071888f8259d7e8acfb8f = 0;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float4x4 _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0 = _WorldToBoxMatrix;
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a _clipbox_392441b97339e48693e7d26a7abd9086;
	_clipbox_392441b97339e48693e7d26a7abd9086.WorldSpacePosition = IN.WorldSpacePosition;
	_clipbox_392441b97339e48693e7d26a7abd9086.FaceSign = IN.FaceSign;
	float _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	SG_clipbox_fe8fe3b0b3457bd4ab112b3010b3497a(_Property_18a206d1dfa63485ab98e15221c7c4aa_Out_0, _Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 1, Boolean_8607cdfb7a1071888f8259d7e8acfb8f, _Property_01392c8c50c90e88acf259a5fdcaf6a0_Out_0, _clipbox_392441b97339e48693e7d26a7abd9086, _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	Bindings_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb;
	_clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb.FaceSign = IN.FaceSign;
	float _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	SG_clipnone_bb83fccb80c28a747a2cf620cc7d4a8f(_Multiply_269bf244a478448cbd5290d0118990ba_Out_2, 0, 1, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb, _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1);
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	#if defined(CLIP_PLANE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipplane_2d71670d3acc178f9d0c2f662d82fe3a_OutVector1_1;
	#elif defined(CLIP_SPHERE)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipsphere_80c202d9293deb8790313cd539bbbc05_OutVector1_1;
	#elif defined(CLIP_CORNER)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipcorner_b40d27802cde0787b2856a80b982859c_OutVector1_1;
	#elif defined(CLIP_BOX)
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipbox_392441b97339e48693e7d26a7abd9086_OutVector1_1;
	#else
	float _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0 = _clipnone_c1b7e93c9f4e3d8a8dd5fa146dbee0eb_OutVector1_1;
	#endif
	#endif
	#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
	float _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	Unity_Branch_float(_Property_dc5aaa2ec32e6d8ebfdb37e0d6c8457d_Out_0, _Branch_14ec57c795f4118db44511e3a399a53c_Out_3, _CLIP_42732a1e953a1f8eb961cc5aeffe2e55_Out_0, _Branch_478316bc1f876a8fbbd7045fff297888_Out_3);
	#endif
	surface.BaseColor = (_Branch_4805df5565606f8ea8ca133da3b78744_Out_3.xyz);
	surface.Alpha = _Branch_478316bc1f876a8fbbd7045fff297888_Out_3;
	surface.AlphaClipThreshold = _Branch_14ec57c795f4118db44511e3a399a53c_Out_3;
	return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
	VertexDescriptionInputs output;
	ZERO_INITIALIZE(VertexDescriptionInputs, output);

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceNormal = input.normalOS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpaceTangent = input.tangentOS.xyz;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.ObjectSpacePosition = input.positionOS;
#endif


	return output;
}
	SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
	SurfaceDescriptionInputs output;
	ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.WorldSpacePosition = input.positionWS;
#endif

#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
output.uv0 = input.texCoord0;
#endif

#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#if defined(KEYWORD_PERMUTATION_0) || defined(KEYWORD_PERMUTATION_1) || defined(KEYWORD_PERMUTATION_2) || defined(KEYWORD_PERMUTATION_3) || defined(KEYWORD_PERMUTATION_4)
BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif

#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

	return output;
}

	// --------------------------------------------------
	// Main

	#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

	ENDHLSL
}
	}
		CustomEditor "ShaderGraph.PBRMasterGUI"
		FallBack "Hidden/Shader Graph/FallbackError"
}